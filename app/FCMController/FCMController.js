/**
 *
 * FCM Messagenger controller
 **/
import {Component} from 'react'
import firebase from 'react-native-firebase'
import {Actions} from "react-native-router-flux";
import {AsyncStorage} from "react-native";

export default class FCMController extends Component{

    constructor(props){
        super(props)
    }

    componentDidMount() {

        firebase.messaging().hasPermission()
            .then(enabled => {
                if (enabled) {
                    firebase.messaging().getToken().then( async token => {
                        console.log("LOG: ", token);
                        await AsyncStorage.setItem('device_token',token);
                    })
                    // user has permissions
                } else {
                    firebase.messaging().requestPermission()
                        .then(() => {
                            firebase.messaging().getToken().then( async token => {
                                await AsyncStorage.setItem('device_token',token);
                                //console.log("LOG: ", token);
                            });
                            //alert("User Now Has Permission")
                        })
                        .catch(error => {
                            //alert("Error", error)
                            // User has rejected permissions
                        });
                }
            });

        this.notificationListener = firebase.notifications().onNotification((notification) => {

            // Process your notification as required
            const {
                body,
                data,
                notificationId,
                sound,
                subtitle,
                title
            } = notification;
            console.log("LOG: ", notification);


        });

        this.notificationListener = firebase.notifications().onNotificationOpened((notification)=>{
            this.switchToScreenBasedOnAlertType(notification.notification._data);
        })

    }

    /**
     * @function to switch among screens using  alertType
     */
    switchToScreenBasedOnAlertType  =(notification) =>{

        console.log("notification_item",notification);

        switch (notification.notification_for) {
            case 'venue_event':{
                Actions.VENUEDETAILS({venueId:notification.sender_id});
                return
            }
            case'artist_notification':{
                Actions.Artist({artistId:notification.sender_id});
                return
            }
            default: {
                //alert(.description);
                return
            }
        }
    }




    render(){
        return null
    }

}
