/**
 * Search Detail Module
 *
 **/

import { connect } from "react-redux";
import { serviceApi } from "app/redux/actions";

function mapStateToProps(state) {
  const { user: userData } = state.auth;
  const { nextSearchResults } = state.serviceApi;
  const { isLoading } = state.shared;
  return { userData, isLoading, nextSearchResults };
}

function mapDispatchToProps(dispatch) {
  return {
    searchNextFormData: (formData, searchTerm) =>
      dispatch(serviceApi.searchNextFormData(formData, searchTerm))
  };
}

export default function withConnect(WrrapedComponent) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(WrrapedComponent);
}
