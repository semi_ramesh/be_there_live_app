import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Platform,
  ScrollView,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
  ImageBackground,
  SectionList,
  PixelRatio,
  ActivityIndicator
} from "react-native";

const KEYS_TO_FILTERS = ["user.name", "subject"];
import NavigationBar from "app/Components/Reusables/CustomNavBar";
import { Images } from "../../constant/images";
import { fontFamilies, SearchBTLoader } from "../../constant/appConstant";
const rowHeight = 50;
import withConnect from "./withConnectSearchDetail";
import sectionListGetItemLayout from "react-native-section-list-get-item-layout";
import { customOrange, customPurple } from "../Authentication/styles";
import { ListItem } from "react-native-elements";
const { width, height } = Dimensions.get("window");
import Player from "app/Components/HDAudioPlayer/index";
import { Actions } from "react-native-router-flux";
import Video from "react-native-video";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import VideoPlayer from "app/Components/HDAudioPlayer/videoPlayer";
import RefreshableList from "app/Components/Reusables/RefreshableList";
import LinearGradient from "react-native-linear-gradient";
class SearchDetailComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTerm: "a",
      searchResults: [],
      isPlaying: false,
      currentVideo: "",
      currentAudio: "",
      activeShowMoreArray1: [],
      activeTitle: "artist",
      nextPage: 2,
      totalResults: 0,
      searchIndex: 0
    };
    this.getItemLayout = sectionListGetItemLayout({
      // The height of the row with rowData at the given sectionIndex and rowIndex
      getItemHeight: (rowData, sectionIndex, rowIndex) =>
        sectionIndex === 0 ? 100 : 50,

      // These three properties are optional
      getSeparatorHeight: () => 0 / PixelRatio.get(), // The height of your separators
      getSectionHeaderHeight: () => 20, // The height of your section headers
      getSectionFooterHeight: () => 10 // The height of your section footers
    });
    this.searchNextFormData = this.props.searchNextFormData.bind(this);
  }

  static defaultProps = {
    searchResults: [],
    nextSearchResults: []
  };

  componentDidUpdate(nextProps, nextState) {
    console.log("Components DidUpdate", nextProps, nextState);
  }

  // shouldComponentUpdate(){
  //
  //     return false
  // }

  componentDidMount() {}

  componentWillMount() {
    console.log("Come back");
    const { data, title, totalRecords } = this.props;
    this.setState({
      activeShowMoreArray1: data,
      activeTitle: title,
      totalResults: totalRecords,
      nextPage: 2
    });

    this.getNextResults();
  }

  componentWillUnmount() {
    this.setState({
      activeShowMoreArray1: []
    });
    // return null
  }

  renderEmptyData = () => {
    return (
      <View style={{ flex: 1 }}>
        <Text>No match found </Text>
      </View>
    );
  };

  renderPlaceholder = () => {
    return (
      <View style={{ flex: 1 }}>
        <Text>No match found </Text>
      </View>
    );
  };
  renderScrollComponent = () => {
    return <ScrollView />;
  };
  componentWillReceiveProps(nextProps) {
    if (
      this.props.nextSearchResults !== nextProps.nextSearchResults &&
      nextProps.nextSearchResults
    ) {
      if (this.state.activeShowMoreArray1.length === this.state.totalResults) {
        return;
      }
      let newData = this.state.activeShowMoreArray1;

      let data = nextProps.nextSearchResults.search_results[0].data;

      newData.push(...data);

      this.setState({
        activeShowMoreArray1: newData,
        nextPage: nextProps.nextSearchResults.next_page
      });
    }
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term });
  }

  onEndVideo = () => {
    this.setState({
      videoPlayer: false,
      paused: true
    });
  };

  playVideo = async item => {
    this.setState({
      currentVideo: item,
      videoPlayer: true
    });
    this.player.presentFullscreenPlayer();
  };

  renderVideo = () => {
    return (
      <Video
        ref={ref => {
          this.player = ref;
        }}
        //          fullscreen={this.state.fullScreen}
        source={{ uri: `${this.state.currentVideo}` }}
        style={{ flex: 1 }}
        onFullscreenPlayerDidDismiss={() =>
          this.setState({ fullScreen: false, videoPlayer: false })
        }
        onEnd={this.onEndVideo}
        paused={this.state.paused}
      />
    );
  };

  renderItemDummyDataForFlatList = ({ item, index }) => {
    let sectionCount = this.state.activeShowMoreArray1.length;
    let upColorAlfha = (1 / sectionCount) * index;
    let downColorAlpha = (1 / sectionCount) * (index + 1);
    let upColor = "rgba(0 ,0,0," + upColorAlfha + ")";
    let downColor = "rgba(0 ,0,0," + downColorAlpha + ")";

    let itemName = item.name;
    let subTitle = item.designation === null ? "" : "";
    let imageUrl =
      item.user_profile_image === null || item.user_profile_image === undefined
        ? Images.logoImage
        : { uri: item.user_profile_image.image_path };
    switch (this.state.activeTitle) {
      case "event": {
        itemName = item.artist_name;
        subTitle = `${item.event_date}, ${item.event_time}`;
        imageUrl = item.image_url ? { uri: item.image_url } : Images.logoImage;
        break;
      }
      case "track": {
        itemName = item.track_title;
        subTitle = item.track_time;
        imageUrl = Images.playBtn;
        break;
      }
      case "album": {
        itemName = item.album_title;
        subTitle = `album_${index}`;
        imageUrl = item.album_cover ? { uri: item.album_cover } : Images.user1;
        break;
      }
      case "tour": {
        itemName = item.line_up;
        subTitle = item.venue_name;
        imageUrl = Images.logoImage;
        break;
      }
      case "video": {
        itemName = item.video;
        subTitle = `videos_${index}`;
        imageUrl = item.thumbnail ? { uri: item.thumbnail } : Images.playBtn;
        break;
      }
    }

    let sectionStyle =
      index === 0
        ? styles.topIndex
        : index === sectionCount - 1
        ? styles.lastIndex
        : styles.middleIndex;
    if (index === 0 && sectionCount === 1) {
      sectionStyle = [styles.topIndex, styles.lastIndex];
    }

    return (
      <View
        style={[
          sectionStyle,
          {
            marginHorizontal: 10,
            borderRightWidth: 1,
            borderLeftWidth: 1,
            borderColor: "rgba(76,76,76,1)"
          }
        ]}
      >
        <LinearGradient colors={[upColor, downColor]}>
          <ListItem
            onPress={() => this.listItemClcked(item)}
            titleStyle={{
              color: "white",
              textAlign: "left",
              fontFamily: fontFamilies.ITCAvantGardeStdBook,
              fontSize: 12
            }}
            // roundAvatar
            rightIcon={{
              name: "angle-right",
              type: "font-awesome",
              style: { fontSize: 25 }
            }}
            containerStyle={{
              marginHorizontal: 10,
              borderTopWidth: 0,
              justifyContent: "center",
              borderBottomWidth: index === sectionCount - 1 ? 0 : 0.5,
              borderBottomColor: "rgba(151,151,151,0.3)"
            }}
            title={itemName}
            subtitle={
              <Text
                style={{
                  marginLeft: 10,
                  fontWeight: "bold",
                  textAlign: "left",
                  color: "white",
                  fontFamily: fontFamilies.ITCAvantGardeStdBold,
                  fontSize: 8
                }}
              >
                {subTitle}
              </Text>
            }
            //  subtitle={subTitle}
            // avatar={imageUrl}
          />

          {!subTitle && (
            <ListItem
              onPress={() => this.listItemClcked(item)}
              titleStyle={{
                color: "white",
                textAlign: "left",
                fontFamily: fontFamilies.ITCAvantGardeStdBook,
                fontSize: 12
              }}
              // roundAvatar
              rightIcon={{
                name: "angle-right",
                type: "font-awesome",
                style: { fontSize: 25 }
              }}
              containerStyle={{
                marginHorizontal: 10,
                borderTopWidth: 0,
                justifyContent: "center",
                borderBottomWidth: index === sectionCount - 1 ? 0 : 0.5,
                borderBottomColor: "rgba(151,151,151,0.3)"
              }}
              title={itemName}
              //subtitle={<Text style={{marginLeft:10,fontWeight:'bold',textAlign:'left',color:'white', fontFamily:fontFamilies.ITCAvantGardeStdBold, fontSize:10}}>{subTitle}</Text>}
              //  subtitle={subTitle}
              // avatar={imageUrl}
            />
          )}
        </LinearGradient>
      </View>
    );
  };

  listItemClcked = item => {
    if (this.state.activeTitle === "track") {
      this.isDownClicked(item);
    }
    if (this.state.activeTitle === "venue") {
      this.venueClicked(item);
    }
    if (this.state.activeTitle === "artist") {
      // alert('we are working on it');
      //return
      this.artistClicked(item);
    }
    if (this.state.activeTitle === "video") {
      //alert('We are working on it.')
      this.playVideo(item);
    }
    if (
      this.state.activeTitle === "album" ||
      this.state.activeTitle === "tour"
    ) {
      this.albumClicked(item);
    }
    if (this.state.activeTitle === "trending_user") {
      if (item.user_type_id === 3) {
        this.artistClicked(item);
      } else {
        this.venueClicked(item);
      }
    }
    if (this.state.activeTitle === "event") {
      this.venueClicked(item);
    }
  };

  isDownClicked = item => {
    this.setState({
      isPlaying: !this.state.isPlaying,
      currentAudio: item
    });
  };

  artistClicked = async item => {
    //return
    //artistId
    await Actions.Artist({ artistId: item.id, popScene: "SearchDetail" });
  };

  venueClicked = async item => {
    await Actions.VENUEDETAILS({ venueId: item.id, popScene: "SearchDetail" });
  };

  albumClicked = async item => {
    await Actions.Artist({ artistId: item.user_id, popScene: "SearchDetail" });
  };

  renderSectionheader = ({ section: { title, data } }) => {
    return (
      <View
        style={{
          backgroundColor: "transparent",
          justifyContent: "flex-start",
          marginTop: 10,
          paddingVertical: 5,
          marginHorizontal: 10
        }}
      >
        <Text style={{ color: customPurple }}>{title.toUpperCase()}</Text>
      </View>
    );
  };

  leftIconComponent = () => {
    return (
      <MaterialIcons
        name={this.state.isPlaying ? "volume-mute" : "volume-up"}
        size={45}
        color="white"
      />
    );
  };

  rightIconComponent = () => {
    return (
      <MaterialIcons
        name={this.state.isPlaying ? "pause" : "play-arrow"}
        size={45}
        color="white"
      />
    );
  };

  getNextResults = () => {
    // return new Promise((resolve, reject)=>{
    //
    // })
    console.log("isthis calling");

    if (
      this.state.activeShowMoreArray1.length < this.state.totalResults &&
      this.state.activeShowMoreArray1.length !== 0 &&
      this.state.totalResults !== 0
    ) {
      let formData = new FormData();
      formData.append("search_type", this.state.activeTitle);
      formData.append("next_page", this.state.nextPage);
      if (!this.onEndReachedCalledDuringMomentum) {
        this.searchNextFormData(formData, this.state.searchTerm);
        this.onEndReachedCalledDuringMomentum = true;
      }
    }
  };

  renderFooter = loading => {
    console.log("Footer Rendered");
    return <View>{this.props.isLoading && <BTLoader />}</View>;
  };

  renderFooter1 = () => {
    if (!this.props.isLoading) {
      return null;
    }

    return <SearchBTLoader />;
  };

  render() {
    const video = this.renderVideo();
    let noFound = this.state.searchResults.length === 0;

    if (this.state.videoPlayer) {
      return (
        <VideoPlayer
          tracks={[this.state.currentVideo]}
          isDownPressed={this.onEndVideo}
        />
      );
    }
    if (this.state.isPlaying) {
      return (
        <Player
          tracks={[this.state.currentAudio]}
          isDownPressed={this.isDownClicked}
        />
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <NavigationBar title="SEARCH" search back />
        <ImageBackground style={styles.container} source={Images.searchBg}>
          <ImageBackground source={Images.searchBgOverlay} style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
              <FlatList
                keyExtractor={(item, index) => {
                  return item + index;
                }}
                onMomentumScrollBegin={() => {
                  this.onEndReachedCalledDuringMomentum = false;
                }}
                data={this.state.activeShowMoreArray1}
                renderItem={this.renderItemDummyDataForFlatList}
                onEndReached={this.getNextResults}
                onEndReachedThreshold={0.5}
                ListFooterComponent={this.renderFooter1}
              />
            </View>
          </ImageBackground>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    justifyContent: "flex-start"
  },
  emailItem: {
    borderBottomWidth: 0.5,
    color: "white",
    borderColor: "rgba(0,0,0,0.3)",
    padding: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  emailSubject: {
    color: "white",
    textAlign: "center"
  },
  searchInput: {
    color: "white",
    marginLeft: 8,
    fontSize: 15,
    //backgroundColor:'red',
    width: width - 70,
    height: 40
  },
  noItemFound: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  topIndex: {
    borderTopWidth: 1
  },

  lastIndex: {
    borderBottomWidth: 0.5
  },

  middleIndex: {},

  searchHolder: {
    justifyContent: "center",
    flexDirection: "row",
    color: "white",
    padding: 4,
    marginHorizontal: 10,
    borderColor: "rgba(39,39,39,1)",
    borderWidth: 1
  },
  modal3: {
    height: Dimensions.get("window").height - 100,
    width: Dimensions.get("window").width - 20,
    backgroundColor: "black"
  },
  modal: {
    // justifyContent: 'center',
    // alignItems: 'center'
  },
  text: {
    color: "black",
    fontSize: 22
  }
});

export default withConnect(SearchDetailComponent);
