/**
 * Search Module
 *
 **/

import { connect } from "react-redux";
import { serviceApi } from "app/redux/actions";

function mapStateToProps(state) {
  const { user: userData } = state.auth;
  const { searchResults } = state.serviceApi;
  const { isLoading } = state.shared;
  return { userData, searchResults, isLoading };
}

function mapDispatchToProps(dispatch) {
  return {
    seacrhDataforTerm: searchTerm =>
      dispatch(serviceApi.seacrhDataforTerm(searchTerm))
  };
}

export default function withConnect(WrrapedComponent) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(WrrapedComponent);
}
