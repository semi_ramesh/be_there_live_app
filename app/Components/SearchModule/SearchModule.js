import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  SectionList,
  PixelRatio
} from "react-native";
import SearchInput from "react-native-search-filter";
import NavigationBar from "app/Components/Reusables/CustomNavBar";
import { Images } from "../../constant/images";
import { fontFamilies } from "../../constant/appConstant";
import withConnect from "./withConnectSearchModule";
import sectionListGetItemLayout from "react-native-section-list-get-item-layout";
import { customPurple } from "../Authentication/styles";
import { ListItem } from "react-native-elements";
const { width } = Dimensions.get("window");
import Player from "app/Components/HDAudioPlayer/index";
import { BTLoader } from "../../constant/appConstant";
import { Actions } from "react-native-router-flux";
import Video from "react-native-video";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import VideoPlayer from "app/Components/HDAudioPlayer/videoPlayer";
import Modal from "react-native-modalbox";
import LinearGradient from "react-native-linear-gradient";

class SearchComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: "a",
      searchResults: [],
      isPlaying: false,
      currentVideo: "",
      currentAudio: "",
      activeTitle: "artist",
      nextPage: 1,
      albumArt: undefined
    };
    this.getItemLayout = sectionListGetItemLayout({
      // The height of the row with rowData at the given sectionIndex and rowIndex
      getItemHeight: (rowData, sectionIndex, rowIndex) =>
        sectionIndex === 0 ? 100 : 50,

      // These three properties are optional
      getSeparatorHeight: () => 0 / PixelRatio.get(), // The height of your separators
      getSectionHeaderHeight: () => 20, // The height of your section headers
      getSectionFooterHeight: () => 10 // The height of your section footers
    });
    this.seacrhDataforTerm = this.props.seacrhDataforTerm.bind(this);
  }

  static defaultProps = {
    searchResults: [],
    nextSearchResults: []
  };

  componentDidMount() {
    // this.searchResultsWithData().then(data=>{
    //
    // })
  }

  renderEmptyData = () => {
    return (
      <View style={{ flex: 1 }}>
        <Text>No match found </Text>
      </View>
    );
  };

  renderPlaceholder = () => {
    return (
      <View style={{ flex: 1 }}>
        <Text>No match found </Text>
      </View>
    );
  };
  renderScrollComponent = () => {
    return <ScrollView />;
  };

  searchResultsWithData = async () => {
    await this.seacrhDataforTerm("a");
  };

  componentWillReceiveProps(nextProps) {
    console.log("search Result", nextProps);

    if (
      this.props.searchResults.search_results !==
        nextProps.searchResults.search_results &&
      nextProps.searchResults.search_results
    ) {
      console.log("search Result is diff.");
      this.setState({ searchResults: nextProps.searchResults.search_results });
    }
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term });
  }

  renderItem = ({ item, index, section }) => {
    return (
      <TouchableOpacity
        onPress={() => alert(item)}
        key={index}
        style={styles.emailItem}
      >
        <View>
          <Text style={[styles.emailSubject, { fontWeight: "500" }]}>
            {item}
          </Text>
          <Text style={styles.emailSubject}>{item}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  onEndVideo = () => {
    this.setState({
      videoPlayer: false,
      paused: true
    });
  };

  playVideo = async item => {
    this.setState({
      currentVideo: item,
      videoPlayer: true
    });
    this.player.presentFullscreenPlayer();
  };

  renderVideo = () => {
    return (
      <Video
        ref={ref => {
          this.player = ref;
        }}
        //          fullscreen={this.state.fullScreen}
        source={{ uri: `${this.state.currentVideo}` }}
        style={{ flex: 1 }}
        onFullscreenPlayerDidDismiss={() =>
          this.setState({ fullScreen: false, videoPlayer: false })
        }
        onEnd={this.onEndVideo}
        paused={this.state.paused}
      />
    );
  };

  renderLastView = section => {
    return (
      <TouchableOpacity
        style={styles.numRecords}
        onPress={() =>
          this.toSearchDetail(
            section.data,
            section.title,
            section.num_of_records
          )
        }
      >
        <Text style={styles.seeMore}>{"Show more "}</Text>
      </TouchableOpacity>
    );
  };

  renderItemDummyData = ({ item, index, section }) => {
    let sectionCount = section.data.length;
    let upColorAlfha = (1 / sectionCount) * index;
    let downColorAlpha = (1 / sectionCount) * (index + 1);
    let upColor = "rgba(0 ,0,0," + upColorAlfha + ")";
    let downColor = "rgba(0 ,0,0," + downColorAlpha + ")";

    let itemName = item.name;
    let subTitle = item.designation;
    let imageUrl =
      item.user_profile_image === null || item.user_profile_image === undefined
        ? Images.logoImage
        : { uri: item.user_profile_image.image_path };
    switch (section.title) {
      case "event": {
        itemName = item.artist_name;
        subTitle = `${item.event_date}, ${item.event_time}`;
        imageUrl = item.image_url ? { uri: item.image_url } : Images.logoImage;
        break;
      }
      case "track": {
        itemName = item.track_title;
        subTitle = item.track_time;
        imageUrl = Images.playBtn;
        break;
      }
      case "album": {
        itemName = item.album_title;
        subTitle = `album_${index}`;
        imageUrl = item.album_cover ? { uri: item.album_cover } : Images.user1;
        break;
      }
      case "tour": {
        itemName = item.line_up;
        subTitle = item.venue_name;
        imageUrl = Images.logoImage;
        break;
      }
      case "video": {
        itemName = item.video;
        subTitle = `videos_${index}`;
        imageUrl = item.thumbnail ? { uri: item.thumbnail } : Images.playBtn;
        break;
      }
    }

    let sectionStyle =
      index == 0
        ? styles.topIndex
        : index === sectionCount - 1
        ? styles.lastIndex
        : styles.middleIndex;
    if (index === 0 && sectionCount === 1) {
      sectionStyle = [styles.topIndex, styles.lastIndex];
    }

    return (
      <View style={[sectionStyle, styles.sectionViewStyle]}>
        <LinearGradient colors={[upColor, downColor]}>
          <ListItem
            onPress={() => this.listItemClcked(item, section.title)}
            titleStyle={styles.listTitle}
            rightIcon={{
              name: "angle-right",
              type: "font-awesome",
              style: { fontSize: 25 }
            }}
            containerStyle={[
              styles.listContainerView,
              { borderBottomWidth: index === sectionCount - 1 ? 0 : 0.5 }
            ]}
            title={itemName}
            subtitle={<Text style={styles.subTitleContainer}>{subTitle}</Text>}
          />
          {!subTitle && (
            <ListItem
              onPress={() => this.listItemClcked(item, section.title)}
              titleStyle={styles.listTitle}
              rightIcon={{
                name: "angle-right",
                type: "font-awesome",
                style: { fontSize: 25 }
              }}
              containerStyle={[
                styles.listContainerView,
                { borderBottomWidth: index === sectionCount - 1 ? 0 : 0.5 }
              ]}
              title={itemName}
            />
          )}
          {index === sectionCount - 1 &&
            section.data.length !== section.num_of_records &&
            this.renderLastView(section)}
        </LinearGradient>
      </View>
    );
  };

  toSearchDetail = (data, aTitle, totalRecords) => {
    const moreSerchData = {
      data: data,
      title: aTitle,
      totalRecords: totalRecords
    };
    Actions.SearchDetail(moreSerchData);
  };

  listItemClcked = (item, title) => {
    if (title === "track") {
      this.playAudio(item);
    }
    if (title === "venue") {
      this.venueClicked(item);
    }
    if (title === "artist") {
      // alert('we are working on it');
      //return
      this.artistClicked(item);
    }
    if (title === "video") {
      //alert('We are working on it.')
      this.playVideo(item);
    }
    if (title === "album" || title === "tour") {
      this.albumClicked(item);
      //alert('we are working on it');
      return;
    }
    if (title === "trending_user") {
      if (item.user_type_id === 3) {
        //alert('we are working on it');
        this.artistClicked(item);

        return;
      } else {
        this.venueClicked(item);
      }
    }
    if (title === "event") {
      this.venueClicked(item);
    }
  };

  isDownClicked = item => {
    this.setState({
      isPlaying: false,
      currentAudio: item
    });
  };

  playAudio = item => {
    console.log("Item:", item);
    this.setState({
      isPlaying: true,
      currentAudio: item,
      albumArt: item.user_album.album_cover
    });
  };

  artistClicked = async item => {
    await Actions.Artist({ artistId: item.id, popScene: "Search" });
  };

  venueClicked = async item => {
    await Actions.VENUEDETAILS({ venueId: item.id, popScene: "Search" });
  };

  albumClicked = async item => {
    await Actions.Artist({ artistId: item.user_id, popScene: "Search" });
  };

  renderSectionheader = ({ section: { title, data } }) => {
    // if(data.length===0){
    //     return null
    // }
    //console.log("section",section);
    return (
      <View
        style={{
          backgroundColor: "transparent",
          justifyContent: "flex-start",
          marginTop: 10,
          paddingVertical: 5,
          marginHorizontal: 10
        }}
      >
        <Text
          style={{
            color: customPurple,
            fontFamily: fontFamilies.ITCAvantGardeGothicBookRegular,
            fontSize: 10
          }}
        >
          {title.toUpperCase()}
        </Text>
      </View>
    );
  };

  renderSectionFooter = ({ section: { title, data } }) => {
    if (data.length === 0) {
      return (
        <View
          style={{
            height: 60,
            marginHorizontal: 10,
            borderWidth: 1,
            borderColor: "rgba(74,74,74,1)"
          }}
        >
          <LinearGradient
            colors={["rgba(0,0,0,0)", "rgba(0,0,0,1)"]}
            style={{ flex: 1, justifyContent: "center", paddingLeft: 20 }}
          >
            <Text
              style={{
                color: "white",
                fontFamily: fontFamilies.ITCAvantGardeGothicBookRegular,
                fontSize: 14
              }}
            >
              {"No Results Found"}
            </Text>
          </LinearGradient>
        </View>
      );
    }
  };

  leftIconComponent = () => {
    return (
      <MaterialIcons
        name={this.state.isPlaying ? "volume-mute" : "volume-up"}
        size={45}
        color="white"
      />
    );
  };

  rightIconComponent = () => {
    return (
      <MaterialIcons
        name={this.state.isPlaying ? "pause" : "play-arrow"}
        size={45}
        color="white"
      />
    );
  };

  render() {
    const video = this.renderVideo();
    let noFound = this.state.searchResults.length === 0;
    // if(this.state.videoPlayer){
    //     return(<VideoPlayer tracks ={[this.state.currentVideo]} isDownPressed = {this.onEndVideo}/>)
    //   }
    // if(this.state.isPlaying){
    //     return(<Player tracks ={[this.state.currentAudio]} isDownPressed = {this.isDownClicked}/>)
    // }
    return (
      <View style={{ flex: 1 }}>
        <NavigationBar title="SEARCH" search back />
        <ImageBackground style={styles.container} source={Images.searchBg}>
          <ImageBackground source={Images.searchBgOverlay} style={{ flex: 1 }}>
            <ImageBackground
              source={Images.gradientBg}
              style={styles.searchHolder}
            >
              <Image
                source={Images.searchIcon}
                style={{ height: 15, width: 15, top: 7.5 }}
              />
              <SearchInput
                returnKeyType={"search"}
                onSubmitEditing={() =>
                  this.seacrhDataforTerm(this.state.searchTerm)
                }
                placeholderTextColor={"white"}
                onChangeText={term => {
                  this.searchUpdated(term);
                }}
                style={styles.searchInput}
                placeholder="SEARCH"
              />
            </ImageBackground>
            {noFound && !this.props.isLoading && (
              <View style={styles.noItemFound}>
                <Text style={styles.emailSubject}>
                  {
                    "No Match Found! \nPlease search something using your keywords."
                  }
                </Text>
              </View>
            )}
            {!noFound && (
              <ScrollView style={{ marginVertical: 10 }}>
                <SectionList
                  initialNumToRender={100}
                  renderItem={this.renderItemDummyData}
                  renderSectionHeader={this.renderSectionheader}
                  renderSectionFooter={this.renderSectionFooter}
                  sections={this.state.searchResults}
                  keyExtractor={(item, index) => item + index}
                  getItemLayout={this.getItemLayout}
                />
              </ScrollView>
            )}
            {this.props.isLoading && <BTLoader />}
          </ImageBackground>
          <Modal
            style={{ flex: 1, backgroundColor: "black" }}
            position={"top"}
            ref={"modal"}
            isOpen={this.state.isPlaying}
            onClosed={() => this.setState({ isPlaying: false })}
          >
            {this.state.isPlaying && (
              <ScrollView>
                <Player
                  tracks={[this.state.currentAudio]}
                  isDownPressed={this.isDownClicked}
                  albumArt={this.state.albumArt}
                />
              </ScrollView>
            )}
          </Modal>
          <Modal
            style={{ flex: 1, backgroundColor: "black" }}
            position={"top"}
            ref={"modal"}
            isOpen={this.state.videoPlayer}
            onClosed={() => this.setState({ videoPlayer: false })}
          >
            {this.state.videoPlayer && (
              <ScrollView>
                <VideoPlayer
                  tracks={[this.state.currentVideo]}
                  isDownPressed={this.onEndVideo}
                />
              </ScrollView>
            )}
          </Modal>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    justifyContent: "flex-start"
  },
  emailItem: {
    borderBottomWidth: 0.5,
    color: "white",
    borderColor: "rgba(151,151,151,0.3)",
    padding: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  emailSubject: {
    color: "white",
    textAlign: "center"
  },
  searchInput: {
    color: "white",
    marginLeft: 8,
    fontSize: 12,
    width: width - 70,
    height: 40,
    fontFamily: fontFamilies.ITCAvantGardeGothicBookRegular,
    top: -5
  },

  noItemFound: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  topIndex: {
    borderTopWidth: 1
  },

  lastIndex: {
    borderBottomWidth: 0.5
  },

  middleIndex: {},

  searchHolder: {
    overflow: "hidden",
    justifyContent: "center",
    flexDirection: "row",
    marginHorizontal: 10,
    borderColor: "rgba(76,76,76,1)",
    borderWidth: 0.5,
    height: 32
  },

  sectionViewStyle: {
    marginHorizontal: 10,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderColor: "rgba(76,76,76,1)",
    justifyContent: "center"
  },
  listTitle: {
    color: "white",
    textAlign: "left",
    fontFamily: fontFamilies.ITCAvantGardeStdBook,
    fontSize: 12
  },
  listContainerView: {
    marginHorizontal: 10,
    borderTopWidth: 0,
    justifyContent: "center",

    borderBottomColor: "rgba(151,151,151,0.3)"
  },
  subTitleContainer: {
    marginLeft: 10,
    fontWeight: "bold",
    textAlign: "left",
    color: "white",
    fontFamily: fontFamilies.ITCAvantGardeStdBold,
    fontSize: 8
  },
  numRecords: {
    padding: 5,
    height: 30,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 10,
    marginVertical: 10
  },
  seeMore: {
    textAlign: "center",
    color: "purple",
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
    fontSize: 12
  }
});

export default withConnect(SearchComponent);
