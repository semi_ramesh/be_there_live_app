/**
 * @ProvidesModule app.containers.Spotlight.SpotLights
 */
import React, {Component} from "react";
import {
  View,
  Text,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Image,
  FlatList,
  ScrollView,
  StyleSheet,
} from "react-native";
import styles from "./styles";
import NavigationBar from "app/Components/Reusables/CustomNavBar";
import Swiper from "react-native-swiper";
import {Images} from "app/constant/images";
import {customOrange, customPurple} from "app/Components/Authentication/styles";
const {
  width:deviceWidth,
  height:deviceHeight,
} = Dimensions.get("window");
import FitImage from "react-native-fit-image";
import Modal from "react-native-modalbox";
import Player  from "app/Components/HDAudioPlayer/index";
import DataRow from "./DataRow";
import EventItem from "./EventItem";
import  withConnect from "./withConnectSpotLight";
import {BTLoader, fontFamilies} from "app/constant/appConstant";
import NoDataComponent from "../Reusables/noData";
import {Actions} from "react-native-router-flux";

class SpotLight extends React.PureComponent {

    static defaultProps = {
      spotLight:{},
      likedSongs:[],
    };
    constructor(props) {
      super(props);
      this.state = {
        trendingSwiperData:[],
        events:[],
        crouselData:[],
        isPlaying:false,
        songs:[],
        isLiking:false,
        isFollowClicked:false,
        isFollowing:false,
        currentItem :0,
        isFollow:false,
      };
      this.followUnFollow = this.props.followUnFollow.bind(this);
      this.getSpotLightData = this.props.getSpotLightData.bind(this);
      this.changeTheLikeStatus = this.props.changeTheLikeStatus.bind(this);

    }

    componentDidMount() {

      this.getSpotLightData();

    }

    componentWillUnmount() {
      this.setState({
        isPlaying:false,
      });
    }

    onExit() {
      this.setState({
        isPlaying:false,
      });
    }

    componentWillReceiveProps(nextProps) {
      if (this.props.spotLight !== nextProps.spotLight && nextProps.spotLight) {
        this.setState({
          trendingSwiperData:nextProps.spotLight.data.promoted_artists,
          crouselData:nextProps.spotLight.data.trending_songs,
          isFollowClicked:false,

        });
      }

      if (this.props.likedSongs !== nextProps.likedSongs && nextProps.likedSongs) {
        this.setState({
          crouselData:nextProps.likedSongs,
          isLiking:false,
          isFollowClicked:false,
        });
      }

      if (nextProps.spotLightFollow && this.props.spotLightFollow !== nextProps.spotLightFollow) {

        this.setState({
          trendingSwiperData:nextProps.spotLightFollow.promoted_artists,
        });
      }
    }


    playSong = (song) => {
      this.setState({
        isPlaying:!this.state.isPlaying,
        songs:[song],
      });
    };


    followUnfollow= (item) => {
      this.setState({
        isFollowClicked:true,
        isFollow:!this.state.isFollow,
      });
      let formData = new FormData();
      let followStatus = item.is_checked_by_user === 1 ? 0 : 1;
      formData.append("following_id", item.user_id);
      formData.append("follower_status", followStatus);
      formData.append("response_type", "spotlight");
      let dataKey =  "spotLightFollow";
      this.followUnFollow({formData, dataKey});
    };

    onLikeDisLike = (item) => {
      this.setState({
        isLiking:true,
      });
      let likeStatus = item.is_liked_by_user === 0 ? 1 : 0;
      let formData = new FormData();
      formData.append("id", Number(item.id));
      formData.append("type", "song");
      formData.append("like_status", likeStatus);
      this.changeTheLikeStatus(formData);

    };

    renderRow = (data) => {

      return <DataRow data={data.item}
        playSong={this.playSong}
        onLike={this.onLikeDisLike}/>;
    };

    renderEvent = (data) => {
      return <EventItem data={data}
        onFollow={this.followUnfollow}
        isActive={data.index === this.currentItem}/>;
    };

    downClicked = () => {
      this.setState({
        isPlaying:false,
      });
    };

    onTapFullBtn = () => {
      Actions.SpotLightPurchase();
    };


    swiperChangeIndex = (index) => {
      this.setState({
        currentItem:index,
      });
      this.flatSwipper.scrollToIndex(index);
    };


    render() {

      let isNotFeatured = this.props.userData && this.props.userData.user_type_id === 3 && this.props.userData.is_featured === 0;
      if (this.props.isLoading && !this.state.isLiking && !this.state.isFollowClicked) {return (<View style={styles.topView}><BTLoader/></View>);}

      if (this.state.trendingSwiperData.length === 0 && this.state.crouselData.length === 0) {
        return (<NoDataComponent title={"SPOTLIGHT"} onReloadData={this.getSpotLightData}/>);
      }
      return (<View style={styles.topView}>
        <NavigationBar title={"SPOTLIGHT"}/>
        <ScrollView>
          <View style={styles.container}>
            {this.state.trendingSwiperData.length > 0 && <View style={GStyle.swipper}>
              <Swiper style={GStyle.wrapper}
                ref={ref => this.swiper = ref}
                activeDotColor={customOrange}
                dotColor={"gray"}
                autoplay
                onChangedIndex={this.swiperChangeIndex}
                onMomentumScrollEnd={this.onScrollBeginDrag}
                paginationStyle={GStyle.pagingnation}
                loop>
                { this.state.trendingSwiperData.map( (item, index ) => {
                  let imagePath = item.image_path ? {uri:item.image_path} : Images.placeholder;
                  let ImageForFollow = item.is_checked_by_user === 1 ? Images.unfollowUser : Images.follow;
                  return <View key={`MyKey_${index}`} style={GStyle.slide}>
                    <View style={GStyle.slideTextContainer}>
                      <Text style={GStyle.slideText} numberOfLines={1}>{item.artist_name}</Text>
                    </View>
                    <FitImage style={[GStyle.slide1]} source={imagePath} key={index} resizeMode={"cover"}/>
                    <TouchableOpacity  style={GStyle.genreTitle1} onPress={() => this.followUnfollow(item)}>
                      <Image source={ImageForFollow} style={GStyle.followImage}/>
                    </TouchableOpacity>
                  </View>;
                })}
              </Swiper>
            </View>}
            <View style={styles.listing}>
              <FlatList
                ref={ref => this.flatSwipper = ref}
                data={this.state.trendingSwiperData}
                horizontal={true}
                space={10}
                renderItem={this.renderEvent}/>
            </View>
            {isNotFeatured && <TouchableOpacity  style={GStyle.button} onPress={this.onTapFullBtn}>
              <ImageBackground source={Images.pushAlertBg} style={GStyle.buttonBgImage}>
                <Text style={GStyle.getStartedText}>{"GET FEATURED"}</Text>
              </ImageBackground>
            </TouchableOpacity>}
            <View style={styles.listing}>
              <FlatList data={this.state.crouselData} renderItem={this.renderRow}/>
            </View>
          </View>
        </ScrollView>
        <Modal style={GStyle.modal}
          position={"top"}
          ref={"modal"}
          isOpen={this.state.isPlaying}
          onClosed={() => this.setState({isPlaying: false})}>
          {this.state.isPlaying &&
            <ScrollView>
              <Player isDownPressed={this.downClicked}
                tracks={this.state.songs} albumArt={this.state.songs[0].album_details.album_cover_path}/>
            </ScrollView>}</Modal>
      </View>);
    }


}

export default withConnect(SpotLight);
const GStyle = StyleSheet.create({
  container:{
    backgroundColor:"#000000",
    flex:1,
  },
  swipper:{
    flex:1,
    backgroundColor:"rgba(18,0,20,1.0)",
    height:deviceHeight / 3,
    width:deviceWidth,
  },
  slide1:{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  slide: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "transparent",
    alignItems: "stretch",
  },
  slideText: {
    color: "white",
    padding:5,
    fontSize:12,
    fontWeight:"600",
    fontFamily:fontFamilies.ITCAvantGardeStdBook,
  },
  slideTextContainer: {
    position:"absolute",
    bottom:0,
    zIndex:9999,
    backgroundColor:"#000",
    opacity:0.7,
    width: deviceWidth,
    height: 30,
    justifyContent:"center",
  },
  genreContainer:{

  },
  genreTitle:{
    position:"absolute",
    width:deviceWidth / 3.5,
    alignItems:"center",
    justifyContent:"center",
    top:(deviceWidth / 3.5) / 2,
  },
  genreTitle1:{
    backgroundColor:"transparent",
    flex:0,
    alignItems:"flex-end",
    position:"absolute",
    right:0,
    top:0,


  },
  followImage:{
    height:deviceWidth * 0.20,
    width:deviceWidth * 0.20,
  },
  button:{
    marginVertical:25,
    height:45,
    alignItems:"center",
    justifyContent:"center",
    flex:1,
    marginHorizontal:8,
        
  },
  buttonBgImage:{
    height:45,
    width:deviceWidth - 20,
    justifyContent:"center",
    alignItems:"center",
    borderWidth:1,
    borderColor:"rgba(144,31,0,1.0)",
  },
  getStartedText:{
    fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular,
    color:"white",
    fontWeight:"500",
    fontSize:16,
  },
  modal:{
    flex:1,
    backgroundColor:"black",
  },
  pagingnation:{
    backgroundColor:"transparent",
    height:20,
    bottom:0,
    justifyContent:"flex-end",
    paddingRight:20,
  },
});