/**
 *
 */
import React, {Component} from "react";

import {
  View,
  ImageBackground,
  Text,
  AsyncStorage,
} from "react-native";
import {Images} from "../../constant/images";
import NavigationBar from "app/Components/Reusables/CustomNavBar";
import styles, {SCREEN_WIDTH} from "./styles";
import BTLButton from "../Reusables/containers/BTLButton";
import Icon from "react-native-vector-icons/FontAwesome";
import {Actions} from "react-native-router-flux";
export  default class SpotLightPaymentSuccessFul extends Component {

  componentWillMount() {

  }

  constructor(props) {
    super(props);
    this.onAuthorize = this.props.onAuthorize.bind(this);

  }

    goToSpotLight = async () => {
      console.log("mydata", this.props.spotLightData);
      if (this.props.spotLightData) {
        await AsyncStorage.setItem("userData", JSON.stringify(this.props.spotLightData), async () => {
          await this.onAuthorize(this.props.spotLightData);
          Actions.SpotLight({userData:this.props.spotLightData});
        });
      }

    };




    render() {

      return (<ImageBackground style={styles.container} source={Images.searchBg}>
        <View style={{backgroundColor:"rgba(0,0,0,0.75)", flex:1}}>
          <NavigationBar title="SPOTLIGHT" />
          <View style={styles.confirmation}>
            <Text style={styles.normalText}>CONFIRMATION</Text>
          </View>
          <View style={ styles.congrats}>
            <Icon name="cog" size={20} color={"white"} style={{color:"white", fontWeight:"200"}}/>
            <Text style={styles.congratsText}>CONGRATULATIONS!</Text>
            <BTLButton  onPress={this.goToSpotLight} btnStyle={{height:50, flex:0, width:SCREEN_WIDTH - 20}} title="GO TO SPOTLIGHT"/>
          </View>
        </View>
      </ImageBackground>);
    }

}

