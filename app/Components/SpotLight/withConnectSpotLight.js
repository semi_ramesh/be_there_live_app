/**
 * @ProvidesModule app.containers.SpotLightsWithconnect
 *
 */
import React from 'react'
import {connect} from 'react-redux'
import {auth,serviceApi} from 'app/redux/actions'

function mapStateToProps(state){

    const {user:userData} = state.auth;
    const {isLoading} = state.shared;
    const {
        spotLight,
        likedSongs,
        featuredPlan,
        features,
        spotLightFollow
    } = state.serviceApi;

    return{
        userData,
        spotLight,
        isLoading,
        likedSongs,
        featuredPlan,
        features,
        spotLightFollow
    }
}

function mapDispatchToProps(dispatch){

     return {
          followUnFollow:({formData,dataKey})=>dispatch(serviceApi.followUnfollow({formData,dataKey})),
          onAuthorize:(formData)=>dispatch(auth.authorize(formData)),
          uploadProfilePicture:(formData) =>dispatch(serviceApi.uploadProfilePicture(formData)),
          getSpotLightData :()=>dispatch(serviceApi.getSpotLightData()),
          changeTheLikeStatus:(formData)=>dispatch(serviceApi.changeTheLikeStatus(formData)),
          getPlanForFeatured:()=>dispatch(serviceApi.getPlanForFeatured()),
          payAndGetfeaturedAmongUsers:(formData)=>dispatch(serviceApi.payAndGetfeaturedAmongUsers(formData))
      }
}

export default function withConnect(WrrapedComponent){

   return connect(mapStateToProps,mapDispatchToProps)(WrrapedComponent)
}