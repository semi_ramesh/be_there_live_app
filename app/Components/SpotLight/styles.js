/**
 * @ProvideModule BTL.HDAudioPlayer.Component.style
 */
import React from 'react'
import {
    StyleSheet,
    Dimensions,
    Platform,
    PixelRatio
} from 'react-native'
import {
    customOrange
} from "app/Components/Authentication/styles";
import {fontFamilies} from "app/constant/appConstant";
const {
    height,
    width
} = Dimensions.get('window');
export default StyleSheet.create({
    topView: {
        backgroundColor: '#000000',
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center'

    },
    container: {
        flex: 10,
        backgroundColor: 'black'
    },
    profileInfoContainer: {
        width: width,
        backgroundColor: 'black',
        flexDirection: 'row'
    },
    gridView: {
        flex: 0,
        backgroundColor: 'black',
        width: width,
    },
    orangeTextStyle: {
        marginBottom: 10,
        left: 10,
        fontSize: normalize(24),
        color: customOrange,
        fontWeight: '300'
    },
    imgStyle: {
        marginLeft: 10,
        resizeMode: 'contain',
        height: 175,
        width: 175,
    },
    nameDetail: {
        marginTop: 50,
        marginLeft: -8,
    },
    textDetail: {
        height: 230,
        flexDirection: 'row',
        justifyContent: 'space-around',
        flex: 1,

    },

    boldAndBig: {
        marginTop: 12,
        fontSize: normalize(22),
        color: 'white',
        fontWeight: '400'
    },
    normal: {
        marginTop: 2,
        fontSize: normalize(16),
        color: 'white',
        fontWeight: '100',
        fontStyle: 'italic'
    },
    normal1: {
        marginTop: 2,
        fontSize: normalize(13),
        color: 'white',
        fontWeight: '200',

    },
    settingIcon: {
        marginTop: 50,
    },
    socialInfoContainer: {

    },
    socialInfo: {
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center'

    },
    socialIcon: {
        marginTop: 4,
        height: 12,
        width: 12,
        resizeMode: 'contain',
        marginRight: 5,

    },
    socialText: {
        fontSize: normalize(16),
        color: 'white'
    },
    editBackG: {
        marginTop: 10,
        height: 40,
        width: 150,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black'
    },
    editText: {
        textAlign: 'center',
        color: 'white',
        fontSize: normalize(16),
    },
    editBtnStyle: {
        height: 40,
        width: 150,

    },
    countPlate: {
        flex: 0,
        marginTop: 10,
        height: 70,
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: 'black'
    },
    plates: {
        height: 70,
        width: ((width / 5) - 5),
        alignItems: 'center'

    },
    itemStyle: {
        height: width / 2,
        width: width / 2,
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
    },
    minus: {
        height: width / 5,
        width: width / 5,
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        backgroundColor: 'transparent'

    },
    swipper: {
        flex: 0,
        backgroundColor: 'gray',
        height: height / 2.3,
        width: width,
        justifyContent:'center',
        alignItems:'center'
    },
    crousel: {
        flex: 2,
        backgroundColor: 'green',
    },
    listing: {
        flex: 2,
        backgroundColor: 'black',
    },

    wrapper: {},
    slide1: {
        //resizeMode:'contain',
        //flex:1,
        padding:0,
        resizeMode:'contain'
    },
    text: {
        color: '#fff',
        fontSize: normalize(30),
        fontWeight: 'bold',
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
    },
    confirmation:{
        flex:1,
        alignItems:'center'
    },
    congrats:{
        flex:3,
        alignItems:'center',

    },
    congratsText:{
        fontSize:21,
        marginVertical:30,
        color: '#fff',
        fontWeight: '400',
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
    },
    normalText:{
        fontSize:21,
        marginVertical:5,
        color: '#fff',
        fontWeight: '400',
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
    }
})



 export const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320;

export function normalize(size) {
  const newSize = size * scale ;
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}