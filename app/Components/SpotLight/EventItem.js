/* eslint-disable react-native/no-unused-vars */
/**
 * @ProvideModules app.Components.SpotLight.EventItem
 */

import React, {Component} from "react";
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ImageBackground,
  PixelRatio,
  Platform,
} from "react-native";
import {Images} from "app/constant/images";
import {customPurple, customOrange} from "app/Components/Authentication/styles";
import {web} from "react-native-communications";
import moment from "moment";
import {fontFamilies} from "app/constant/appConstant";

export default class EventItem extends Component {


    static defaultProps ={
      onFollow :() => {},
    };

    constructor(props) {
      super(props);
      this.state = {
        image:props.data.item.is_checked_by_user === 1 ? Images.unfollowUser : Images.follow,
      };
    }

    ticketPortalTapped=() => {
      web(`${this.props.data.item.ticket_link}`);
    };

    onFollow = (data) => {
      this.setState({
        image: this.state.image === Images.follow ? Images.unfollowUser : Images.follow,
      });
      this.props.onFollow(data);
    };

    render() {

      const {
        title,
        artist,
        timeDuration,
        is_checked,
      } = this.props.data.item;
      const {amount,
        artist_name,
        id,
        image_path,
        is_checked_by_user,
        plan_id,
        promote_from,
        promote_till,
        event_date,
        ticket_link,
        event_time,
        user_id,
        venue_name} = this.props.data.item;
      const {isActive} = this.props;
      const backgroundColor = this.props.data.index === 0 ? "black" : "rgba(48,12,79,1)";
      let image  = is_checked_by_user === 1 ? Images.unfollowUser : Images.follow;
      return (
        <View style={styles.container}>
          <View style={[styles.innerContainer, {backgroundColor:backgroundColor}]}>
            <Text style={styles.timeText}>{(`${moment(promote_from).format("dddd, MMMM DD ,YYYY")}`).toUpperCase()}</Text>
            <Text style={[styles.titleText, {fontWeight:"900"}]}>{venue_name}</Text>
            <Text style={styles.artistText}>{artist_name}</Text>
            <TouchableOpacity style={styles.ticketPortalBtn} onPress={this.ticketPortalTapped}>
              <ImageBackground source={Images.gradientBg} style={styles.gradientBgStyle}>
                <Text style={styles.ticketText}>{"Ticket Portal".toUpperCase()}</Text>
              </ImageBackground>
            </TouchableOpacity>
            <TouchableOpacity  style={styles.followBg} onPress={() => this.onFollow(this.props.data.item)}>
              <Image source={image} style={styles.followImage}/>
            </TouchableOpacity>
          </View>
        </View>);
    }
}

/** STYLES **/
const styles = StyleSheet.create({

  container:{
    flex:1,
    width:210,
  },
  innerContainer:{
    flex:1,
    width:200,
    backgroundColor:"rgba(48,12,79,1)",
    marginVertical:5,
    marginHorizontal:5,

  },
  timeText:{
    marginTop:5,
    color:"white",
    marginLeft:5,
    fontSize:10,
    fontWeight:"500",
    fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular,
  },

  titleText:{
    marginTop:10,
    marginLeft:10,
    color:"white",
    fontWeight:"200",
    fontSize:15,
    fontFamily:fontFamilies.ITCAvantGardeGothicDemi2,
  },
  artistText:{
    marginTop:2,
    color:"white",
    marginLeft:10,
    fontSize:12,
    fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular,
  },

  followBg:{
    backgroundColor:"transparent",
    flex:0,
    alignItems:"flex-end",
    position:"absolute",
    right:0,
    top:0,
  },

  followImage:{
    height:40,
    width:40,
  },
  ticketPortalBtn:{
    width:200,
    height:30,
    marginTop:10,
    marginHorizontal:0,
    justifyContent:"center",
    alignItems:"center",
  },
  gradientBgStyle:{
    height:30,
    width:200,
    justifyContent:"center",
    alignItems:"center",
    alignSelf:"center",
  },
  ticketText:{
    fontSize:12,
    color:customOrange,
    fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular,
  },

});


const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get("window");

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320;

export function normalize(size) {
  const newSize = size * scale;
  if (Platform.OS === "ios") {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  }
}