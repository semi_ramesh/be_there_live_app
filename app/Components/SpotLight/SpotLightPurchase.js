/**
 * @ProvideModules app.Components.SpotLights.SpotLightPurchase
 */

import React, { Component } from "react";
import {
  View,
  Button,
  TouchableOpacity,
  Text,
  ImageBackground,
  AsyncStorage,
  Image,
  ScrollView,
  StyleSheet
} from "react-native";
import styles from "./styles";
import withConnect from "./withConnectSpotLight";
import {
  customOrange,
  customPurple
} from "app/Components/Authentication/styles";
import stripe from "tipsi-stripe";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import NavigationBar from "app/Components/Reusables/CustomNavBar";
import { Images } from "app/constant/images";
import {
  BTLoader,
  deviceWidth,
  fontFamilies,
  getExtensionFromMimeType,
  showInfoMessage,
  stripApiKey,
  BTL_REDIRECT_URL
} from "app/constant/appConstant";

import BTLTextInput from "app/Components/Reusables/containers/BTLTextInput";

import BTLButton from "app/Components/Reusables/containers/BTLButton";
import moment from "moment";
import BTLImagePickerController from "app/utils/imagePicker";
import _ from "lodash";
import DateTimePicker from "react-native-modal-datetime-picker";
import CheckBox from "app/thirdPartyComponents/react-native-checkbox/checkbox.js";

const theme = {
  primaryBackgroundColor: "white",
  secondaryBackgroundColor: "rgba(18,0,20,1.0)",
  primaryForegroundColor: "white",
  secondaryForegroundColor: customOrange,
  accentColor: customOrange,
  errorColor: "red"
};

class SpotLightPurchase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      checked: true,
      isDateTimePickerVisible: false,
      date: moment().format("DD/MM/YY"),
      artist: "",
      venue: "",
      ticketPortal: "",
      stripeResponse: {},
      profileImage: Images.user1,
      activePlan: undefined,
      activePlanArray: [],
      profieImage: Images.user1,
      formatedDate: moment(),
      profileImageUrl: ""
    };
    this.uploadProfilePicture = this.props.uploadProfilePicture.bind(this);
    this.getPlanForFeatured = this.props.getPlanForFeatured.bind(this);
    this.payAndGetfeaturedAmongUsers = this.props.payAndGetfeaturedAmongUsers.bind(
      this
    );
  }

  componentDidMount() {
    this.getPlanForFeatured();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.featuredPlan) {
      this.setState({
        activePlan: nextProps.featuredPlan.data[0],
        activePlanArray: nextProps.featuredPlan.data,
        checked: this.state.activePlan
          ? this.state.activePlan.is_active === 1
          : false
      });
    }
    if (this.props.features !== nextProps.features && nextProps.features) {
    }
  }

  takePickAndUploadImage = async () => {
    try {
      await BTLImagePickerController.pickImage()
        .then(response => {
          this.setState({
            profileImage: { uri: response.uri },
            profileImageUrl: { url: response.url },
            followOrunFollow: true
          });
          this.setState({
            isLoading: true
          });
        })
        .then(error => {});
    } catch (e) {}
  };

  validateForm = () => {
    const { date, venue, artist, ticketPortal, profileImage } = this.state;

    if (_.isEmpty(date)) {
      showInfoMessage("Please select date first.");
      return false;
    }
    if (_.isEmpty(venue)) {
      showInfoMessage("Please enter venue name where you perform.");
      return false;
    }
    if (_.isEmpty(ticketPortal) || !ticketPortal.includes("http")) {
      alert("Ticket Portal link must start with http:// or https://");
      return false;
    }
    if (profileImage === Images.user1) {
      alert(
        "Please add your banner image of show to get featured in spotlight."
      );
      return false;
    }
    return true;
  };

  handleCreacteSourcePress = async () => {
    if (!this.validateForm()) {
      return;
    }

    if (!this.state.checked) {
      alert("Please select plan first.");
      return;
    }

    this.setState({
      loading: true
    });
    stripe.setOptions({ publishableKey: stripApiKey });
    const options = {
      smsAutofillDisabled: true,
      requiredBillingAddressFields: "full", // or 'full'
      theme
    };
    let __this = this;
    await stripe
      .paymentRequestWithCardForm(options)
      .then(async response => {
        this.setState({
          stripeResponse: response
        });
        await __this.paymentApiCall(response);
      })
      .catch(error => {
        this.setState({
          loading: false
        });
      });
  };

  paymentApiCall = async response => {
    let responseData = response;
    let {
      date,
      artist,
      venue,
      formatedDate,
      ticketPortal,
      profileImage
    } = this.state;
    try {
      let formData = new FormData();
      let ymdDate = moment(formatedDate).format("YYYY-MM-DD");
      formData.append("stripeToken", responseData.tokenId);
      formData.append("plan_id", this.state.activePlan.id);
      formData.append("user_id", this.props.userData.id);
      formData.append("billing_address", JSON.stringify(responseData.card));
      formData.append("artist_name", artist);
      formData.append("venue_name", venue);
      formData.append("ticket_link", ticketPortal);
      formData.append("promote_from_date", ymdDate);
      if (profileImage !== Images.user1) {
        let timeStamp = Math.floor(Date.now() / 1000);
        let infoDict = getExtensionFromMimeType(this.state.profileImageUrl.url);
        formData.append("image", {
          uri: this.state.profileImage.uri,
          name: `${timeStamp}${infoDict.extension}`,
          data: response.data,
          filename: `${timeStamp}${infoDict.extension}`,
          type: `${infoDict.mime}`
        });
      }
      await this.payAndGetfeaturedAmongUsers(formData);
      this.setState({
        loading: false
      });
    } catch (e) {
      this.setState({
        loading: false
      });
    }
  };

  selectPlan = (item, index) => {
    let array = this.state.activePlanArray;
    array.map((data, index) => {
      if (data === item) {
        array[index].is_active = item.is_active === 1 ? 0 : 1;
      }
    });
    this.setState({
      activePlanArray: array,
      checked: !this.state.checked
    });
  };

  _showDateTimePicker = text => {
    this.setState({
      isDateTimePickerVisible: true
    });
  };
  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    let date1 = moment(date).format("DD/MM/YYYY");
    this.setState({
      date: date1,
      formatedDate: date,
      isDateTimePickerVisible: false
    });
    this._hideDateTimePicker();
  };

  renderPlanHolder = () => {
    if (!this.state.activePlan) {
      return null;
    }
    return (
      <ImageBackground
        style={spotStyle.planHolder}
        source={Images.spotLightPurchase}
      >
        {this.state.activePlanArray &&
          this.state.activePlanArray.map((item, index) => {
            return (
              <View style={spotStyle.checkBoxContainer}>
                <CheckBox
                  containerStyle={spotStyle.checkBox}
                  label={`$${item.amount}`}
                  subLabel={item.hours + " hours"}
                  checkedImage={Images.check}
                  uncheckedImage={Images.unCheck}
                  checked={this.state.checked}
                  onChange={() => this.selectPlan(item, index)}
                  labelStyle={{
                    color: "white",
                    fontWeight: "400",
                    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
                  }}
                />
              </View>
            );
          })}
        <View style={spotStyle.underLine} />
        <View style={spotStyle.checkBoxContainer}>
          <Text style={spotStyle.detailText}>Purchase Total</Text>
          <Text style={spotStyle.dollerSign}>
            ${this.state.activePlan.amount}
          </Text>
        </View>
      </ImageBackground>
    );
  };

  uploadImage = () => {
    this.takePickAndUploadImage();
  };

  onChangeTicketPortInput = text => {
    // if (text.includes("https")) {
    //   text.replace("https", "");
    // }
    // if (text.includes("http")) {
    //   text.replace("http", "");
    // }

    let position = 0;
    let b = "https";

    let output = [text.slice(0, position), b, text.slice(position)].join("");

    this.setState({ ticketPortal: `${text}` });
  };
  onSubmitting = text => {
    console.log("textonsubmitt", text);
  };

  onfocus = text => {
    console.log("focus", text);
  };

  render() {
    const PlanHolderView = this.renderPlanHolder();
    return (
      <View style={styles.topView}>
        <NavigationBar
          title={"SPOTLIGHT"}
          onSearch={this.onSearch}
          back={true}
        />
        <KeyboardAwareScrollView>
          <View style={styles.container}>
            <View style={{ paddingHorizontal: 8 }}>
              <Text style={spotStyle.textStyle}>UPLOAD IMAGE:</Text>
              <ImageBackground
                source={this.state.profileImage}
                style={spotStyle.userImage}
              />
              <TouchableOpacity
                style={spotStyle.update}
                onPress={this.uploadImage}
              >
                <Image
                  style={{ height: 30, width: 30 }}
                  source={Images.penImage}
                />
              </TouchableOpacity>
            </View>
            <View style={{ paddingVertical: 10 }}>
              <BTLTextInput
                placeholder={moment().format("DD/MM/YY")}
                label={"DATE:"}
                value={this.state.date}
                isDate
                date={this.state.date}
                onTap={this._showDateTimePicker}
                onFocus={this._showDateTimePicker}
              />
              <BTLTextInput
                placeholder={"NAME"}
                value={this.state.artist}
                onChangeText={text => {
                  this.setState({ artist: text });
                }}
                label={"ARTIST:"}
              />
              <BTLTextInput
                placeholder={"NAME"}
                label={"VENUE:"}
                value={this.state.venue}
                onChangeText={text => {
                  this.setState({ venue: text });
                }}
              />
              <BTLTextInput
                placeholder={"Website http://"}
                value={this.state.ticketPortal}
                onFocus={this.onfocus}
                onSubmit={this.onSubmitting}
                onChangeText={this.onChangeTicketPortInput}
                label={"TICKET PORTAL LINK:"}
              />
            </View>
            {PlanHolderView}
            <BTLButton
              title={"Pay Now"}
              onPress={this.handleCreacteSourcePress}
            />
            <DateTimePicker
              minimumDate={new Date()}
              mode={this.state.pickerMode}
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
            />
          </View>
        </KeyboardAwareScrollView>
        {this.props.isLoading && <BTLoader />}
      </View>
    );
  }
}

export default withConnect(SpotLightPurchase);

const spotStyle = StyleSheet.create({
  userImage: {
    width: deviceWidth - 20,
    height: deviceWidth / 1.5,
    resizeMode: "contain",
    alignSelf: "center",
    marginTop: 4
  },
  textStyle: {
    fontSize: 13,
    color: customPurple,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },

  content: {
    padding: 5
  },
  update: {
    position: "absolute",
    bottom: -10,
    right: 10
  },

  planHolder: {
    height: 100,
    padding: 2,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    borderColor: "rgba(255,255,255,0.5)",
    borderWidth: 1,
    margin: 8
  },

  checkBox: {
    backgroundColor: "transparent",
    borderWidth: 0
  },
  checkBoxContainer: {
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  checkBoxTitle: {
    color: "white",
    fontSize: 16,
    height: 21,
    fontWeight: "900",
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },

  detailText: {
    color: "white",
    fontSize: 13,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },
  underLine: {
    height: 80,
    width: 1,
    marginHorizontal: 8,
    backgroundColor: "rgba(255,255,255,0.5)"
  },
  dollerSign: {
    color: "white",
    fontSize: 20,
    fontWeight: "900",
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  }
});
