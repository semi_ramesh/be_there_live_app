/**
 *@ProvidesModule app.containers.Spotlight.DataRow
 *
 * **/

import React,{Component} from 'react';
import {
    View,
    Image,
    Platform,
    Text,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from 'react-native';
import {Images} from "app/constant/images";
import {customPurple} from "app/Components/Authentication/styles";
import FitImage from 'react-native-fit-image';
import {Actions} from 'react-native-router-flux';
const {width,height} = Dimensions.get('window');
import RNShineButton from 'react-native-shine-button';
import {fontFamilies} from "app/constant/appConstant";

export default class DataRow extends Component {

    static defaultProps ={
        onLike :()=>{}
    };

    constructor(props){
        super(props);
        this.state ={
            image:props.data.is_liked_by_user ===1?Images.heart_redImage:Images.heart_white,
            count : Number(props.data.likes.likes)
        };
    }

    like = (data)=>{
        this.setState({
            image: this.state.image === Images.heart_redImage?Images.heart_white:Images.heart_redImage,
            count :this.state.image === Images.heart_redImage &&this.state.count>1?this.state.count-1:this.state.count+1
        });
        this.props.onLike(data);
    };

    goToArtist = async (id)=>{
        await Actions.Artist({artistId:id,popScene:'SpotLight'});
    };


    render(){

         const {data,playSong,onLike} = this.props;
         let trackTime = Number(data.track_time);
         if(trackTime<60){
            trackTime = `00:${trackTime}`;
         }
         else{
            trackTime  = `${trackTime/60}:${trackTime%60}`;
         }
         if(trackTime === 'NaN:NaN'){
            trackTime = '00:00';
         }
         let LikeImage = data.is_liked_by_user ===1?Images.heart_redImage:Images.heart_white;
         let source = data.album_details?{uri:data.album_details.album_cover_path}:Images.placeholder;
         return(<View style = {styles.container}>
                 <TouchableOpacity  style = {styles.imageContainer} onPress = {()=>this.goToArtist(data.user_id)}>
                     <FitImage source = {source} style = {styles.dataRowStyle}/>
                 </TouchableOpacity>
                 <ImageBackground style = {styles.content} source = {Images.spotlightCellBg}>
                      <Text   style = {[styles.textStyle,styles.trendingExtension]}>{'TRENDING'}</Text>
                      <Text style = {[styles.boldTextStyle,{marginVertical:6}]}>{data.album_details?data.album_details.album_title:'Unknown'}</Text>
                      <View style = {styles.underLine}/>
                 <View style = {styles.underTheLine}>
                 <TouchableOpacity style ={styles.songBtn} onPress = {()=>playSong(data)}>
                     <Image source = {Images.playBtn} style = {styles.playBtnContainer}/>
                     <Text  numberOfLines = {1} style = {styles.trackTitle}>{data.track_title.toUpperCase()}</Text>
                     <Text style = {styles.trackTimeText}>{trackTime}</Text>
                 </TouchableOpacity>
                 <View style = {styles.likeBtnContainer}>
                    {Platform.OS === 'ios'&&<RNShineButton shape={'lik'} color={"transparent"}
                                                            disabled={this.props.isLoading}
                                                            fillColor={"transparent"}
                                                            size={20}
                                                            onChange={()=>this.like(data)}>
                        {<Image source = {this.state.image} style = {{height:20,width:20}}/>}
                    </RNShineButton>}
                    {Platform.OS ==='android'&&<RNShineButton shape={"heart-circle"}
                                                              disabled={this.props.isLoading}
                                                              size={20}
                                                              onChange={()=>this.like(data)}>
                        {<Image source = {this.state.image} style = {styles.likeImage}/>}
                        </RNShineButton>}
                    {/*<Text style = {{color:'rgba(255,255,255, 0.5)',marginHorizontal:2,fontSize:12}}>{this.state.count}</Text>*/}
                 </View>
                 </View>
        </ImageBackground>
    </View>);
    }
 }

/** STYLES **/
const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        flex:1,
        height:120,
        padding:8,
        marginVertical:15
    },
    imageContainer:{
        flex:2,
        justifyContent:'center',
        alignItems:'flex-start',
    },
    fontStyles:{
        color:'rgba(255,255,255, 0.5)'
    },
    dataRowStyle:{
          width:(2*width)/5.5,
          height:110,
          //resizeMode:'stretch'
    },

    content:{
        flex:3,
        padding:5,
        paddingLeft:10,
        height:107,
        backgroundColor:'rgba(36,1,39,1.0)'
    },

    textStyle:{
        color:customPurple
    },
    boldTextStyle:{
        marginTop:18,
        color:'white',
        fontSize:18,
        fontFamily:fontFamilies.ITCAvantGardeGothicDemi2
    },

    followBtn:{
        position:'absolute',
        backgroundColor:'transparent',
        flex:0,
        alignItems:'flex-end',
        width:60,
        alignSelf:"flex-end"
    },
    followImage:{
        top:0,
        height:50,
        width:50,
        resizeMode:'contain'
    },
    underLine:{
        marginVertical:2,
        height:1.1,
        flex:0.010,
        backgroundColor:'rgba(48,40,50,1.0)',
    },

    songBtn:{
        flex:2,
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        marginVertical:5
    },
    underTheLine:{
        flex:1,
        flexDirection:'row',
    },
    trackTitle:{
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular,
        marginRight:5,
        color:'rgba(255,255,255, 0.5)',
        width:80,fontSize:12
    },
    playBtnContainer:{
        marginRight:5,
        height:16,
        width:16
    },
    trackTimeText:{
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular,
        color:'rgba(255,255,255, 0.5)',
        fontSize:12,
        width:60
    },
    likeBtnContainer:{
        flexDirection:'row-reverse',
        flex:1,
        alignItems:'center'
    },
    likeImage:{
        height:20,
        width:20
    },
    trendingExtension:{
        letterSpacing :5,
        color:'#B721FF',
        fontFamily:fontFamilies.AvantGardeExtraLight
    }

});