/***
 * @ProvideModules app.Components.SubscriptionInfo
 */

import React, { Component } from "react";
import {
  Dimensions,
  ImageBackground,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  AsyncStorage
} from "react-native"
import { Button } from "react-native-elements";
import { Images } from "app/constant/images";
import CommonStylesheet from "app/utils/CommonStylesheet";
import {
  subscriptionTitle,
  SubscriptionUser,
  LoginUser
} from "./SubscriptionsData";
import { Actions } from "react-native-router-flux";
import stripe from "tipsi-stripe";
import {
  BTLoader,
  fontFamilies,
  showInfoMessage,
  stripApiKey,
  BTL_REDIRECT_URL
} from "app/constant/appConstant";
import ServiceApi from "app/utils/ServiceApi/index";
import NavBar from "app/Components/Reusables/CustomNavBar";
import CheckBox from "app/thirdPartyComponents/react-native-checkbox/checkbox.js";
const { height, width } = Dimensions.get("window");
const customOrange = "#e53100";
const customPurple = "#da2df9";
const theme = {
  primaryBackgroundColor: "white",
  secondaryBackgroundColor: "rgba(18,0,20,1.0)",
  primaryForegroundColor: "white",
  secondaryForegroundColor: customOrange,
  accentColor: customOrange,
  errorColor: "red"
};

export default class SubscriptionInfo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      title: "SUBSCRIPTION",
      subscriptionTitle: subscriptionTitle,
      subscriptionInfo: SubscriptionUser[props.subs_type],
      loading: false,
      source: null,
      checked: false,
      activePlan: "",
      price: 0,
      subscribeArray: [],
      subscriptionText: [],
      stripeResponse: ""
    };
  }
  static defaultProps = {
    subscriptionDetail: []
  };

  componentDidMount() {
    this.setState({
      subscribeArray: this.props.subscriptionDetail,
      activePlan: this.props.subscriptionDetail[0]
    });
  }

  handleCreacteSourcePress = async () => {
    this.setState({
      loading: true
    });

    stripe.setOptions({ publishableKey: stripApiKey });
    const options = {
      smsAutofillDisabled: true,
      requiredBillingAddressFields: "full", // or 'full'
      theme
    };
    let __this = this;
    await stripe
      .paymentRequestWithCardForm(options)
      .then(async response => {
        this.setState({
          stripeResponse: response
        });
        await __this.paymentApiCall(response);
      })
      .catch(error => {
        this.setState({
          loading: false
        });
      });
  };

  paymentApiCall = async response => {
    let responseData = response;
    let userinfo = this.props.userData;
    try {
      let formData = new FormData();
      formData.append("stripeToken", responseData.tokenId);
      formData.append("plan_id", this.state.activePlan.id);
      formData.append("user_id", this.props.userData.id);
      formData.append("user_id", this.props.userData.id);
      formData.append("billing_address", JSON.stringify(responseData.card));
      const response = await ServiceApi.makePaymentBasedOnFormData(formData);
      this.goToLogin();
    } catch (e) {
      this.setState({
        loading: false
      });
      showInfoMessage("Something went wrong.");
    }
  };

  selectThePrice = (itemData, index) => {
    let subscribeArrayM = this.state.subscribeArray;
    this.state.subscribeArray.map(item => {
      if (item === itemData) {
        if (subscribeArrayM[index].checked === 0) {
          subscribeArrayM[index].checked = 1;
        }
        if (item.checked === 1) {
          subscribeArrayM[index].checked = 0;
        }

        if (item.checked === 1) {
          this.setState({
            subscriptionText: subscribeArrayM[index].subscription_description
          });
        }
        this.setState({
          subscribeArray: subscribeArrayM
        });
      }
    });
  };

  goToLogin = () => {
    alert(
      "You have subscribed successfully, We have sent an email for verification on your registered email ID.Please verify your email and login."
    );
    Actions.LOGIN();
  };

  title = item => {
    return (
      <Text style={SubsStyle.dollor}>
        {`$${item.dollor}\n`}
        <Text style={SubsStyle.type}>{item.plan}</Text>
      </Text>
    );
  };

  render() {
    const { subscriptionInfo, subscriptionText } = this.state;
    return (
      <ImageBackground source={Images.bgImage} style={SubsStyle.topContainer}>
        <NavBar title={"SUBSCRIBE"} search back />
        <View style={SubsStyle.container}>
          <Text style={[SubsStyle.title, CommonStylesheet.whiteColor]}>
            {this.state.title}
          </Text>
          <Text numberOfLines={20} style={[SubsStyle.subTitle]}>
            {subscriptionInfo}
          </Text>
          <View style={SubsStyle.infoContainer}>
            <View style={SubsStyle.priceBox}>
              {this.state.subscribeArray.map((item, index) => {
                return (
                  <CheckBox
                    label={"$" + item.amount}
                    subLabel={item.duration_text}
                    containerStyle={SubsStyle.checkbox}
                    checked={item.checked === 1}
                    checkedImage={Images.check}
                    uncheckedImage={Images.unCheck}
                    onChange={this.selectThePrice(item, index)}
                  />
                );
              })}
            </View>
            <View style={SubsStyle.services}>
              {this.state.subscribeArray.map((item, index) => {
                return (
                  <View>
                    <Text style={SubsStyle.serviceTitle}>
                      {"Service Details:".toUpperCase()}
                    </Text>
                    <View style={SubsStyle.serviceInfo}>
                      <Text
                        numberOfLines={20}
                        style={[SubsStyle.points, { width: width / 2 }]}
                      >
                        {" "}
                        {"\u2022"} {item.subscription_description}
                      </Text>
                    </View>
                  </View>
                );
              })}
            </View>
          </View>
          <View style={{ width: width, padding: 5 }}>
            <Button
              title={"SUBSCRIBE"}
              onPress={this.handleCreacteSourcePress}
              buttonStyle={{ backgroundColor: "#E53100" }}
            />
            {this.props.userType === 2 && (
              <Button
                title={"Skip"}
                onPress={this.goToLogin}
                buttonStyle={{ backgroundColor: "transparent", marginTop: 10 }}
              />
            )}
          </View>
          {this.state.loading && <BTLoader />}
        </View>
      </ImageBackground>
    );
  }
}

const SubsStyle = StyleSheet.create({
  topContainer: {
    top: 0,
    bottom: 0,
    height: height - 0,
    width: width,
    flex: 1
  },
  container: {
    flex: 3,
    alignItems: "center",
    justifyContent: "center"
  },
  priceBox: {
    borderWidth: 1,
    //borderColor:'',
    borderRightColor: "white",
    borderBottomColor: "#141319",
    borderLeftColor: "#141319",
    borderTopColor: "#141319"
    // borderColor:'transparent',
  },
  title: {
    fontSize: 20,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },
  subTitle: {
    fontSize: 15,
    color: customPurple,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },
  infoContainer: {
    // backgroundColor:"green",
    flexDirection: "row",
    width: width,
    margin: 10,
    paddingHorizontal: 20
  },
  dollor: {
    marginLeft: 5,
    color: "white",
    fontSize: 20,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },

  type: {
    marginLeft: 5,
    fontSize: 10,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
    color: "white"
  },
  checkbox: {
    backgroundColor: "transparent",
    borderWidth: 0
  },
  services: {
    marginLeft: 15
  },
  serviceInfo: {
    marginTop: 10
    //paddingHorizontal:4,
  },
  serviceTitle: {
    color: "white",
    fontFamily: fontFamilies.ITCAvantGardeGothicDemi2
  },
  points: {
    color: "white",
    width: width - 20 / 2,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  }
});
