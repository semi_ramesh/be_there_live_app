export const subscriptionTitle = [
 'Individual member details',
 'Artist member details',
 'Venue member details'
];

export const SubscriptionUser = ['Individual','Artist','Venue'];


export const LoginUser = {
    userType:2,
    changeType(type) {
        this.userType = type;
    }
};