import React  from "react";
import {
  Dimensions,
  ImageBackground,
  StyleSheet,
  View,
  TouchableOpacity,
  Text
} from "react-native";
import { Images } from "../../constant/images";
import CommonStylesheet from "../../utils/CommonStylesheet";
import Icon from "react-native-vector-icons/FontAwesome";
import { Actions } from "react-native-router-flux";
import { subscriptionTitle, SubscriptionUser } from "./SubscriptionsData";
const { height, width } = Dimensions.get("window");
import ServiceApi from "app/utils/ServiceApi/index";
import {
  BTLoader,
  fontFamilies,
  showInfoMessage
} from "app/constant/appConstant";
import NavBar from "app/Components/Reusables/CustomNavBar";
export default class Subscription extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      title: "SUBSCRIPTION",
      subscriptionType: [2, 3, 4],
      subscriptionTitle: subscriptionTitle,
      subscriptionInfo: subscriptionTitle[1],
      SubscriptionUser: SubscriptionUser,
      isLoading: false,
      source: null
    };
  }

  getSubscription = async type => {
    this.selectRole(type);
    return;
    if (type === 0) {
      Actions.SUBSCRIPTIONINFO({ subs_type: type });
      // this.selectRole(type);
    } else {
      showInfoMessage(
        "For this type user , we are working on it, we will implement soon."
      );
    }
  };

  getSubscriptionInfo = type => {
    this.setState({
      subscriptionInfo: this.state.subscriptionTitle[type]
    });
  };

  selectRole = async type => {
    console.log("type_id", type);
    this.setState({ isLoading: true });
    let Data = this.props.NewUserData;
    try {
      let formData = new FormData();
      let typeData = this.state.subscriptionType[type];
      formData.append("user_id", Data.id);
      formData.append("user_type_id", this.state.subscriptionType[type]);
      const response = await ServiceApi.subscribeUser(formData);
      console.log("yserType", typeData);
      this.setState({ isLoading: false });
      Actions.SUBSCRIPTIONINFO({
        userType: typeData,
        subscriptionDetail: response.subscription_details,
        userData: Data
      });
    } catch (error) {
      this.setState({ isLoading: false });
      showInfoMessage("Some thing went wrong ,Please try again.", error);
    }
  };

  render() {
    return (
      <ImageBackground source={Images.bgImage} style={SubsStyle.topContainer}>
        <NavBar title={"SUBSCRIPTION"} search back />
        <View style={SubsStyle.container}>
          {this.state.SubscriptionUser.map((title, index) => {
            let imageBg = index === 0 ? Images.OrangeImage : Images.gradientBg;
            return (
              <SubsButton
                gradientBg={imageBg}
                title={title}
                type={index}
                onTap={this.getSubscription}
                onInfoTap={this.getSubscriptionInfo}
              />
            );
          })}
          <View style={SubsStyle.bottomText}>
            <Text style={[SubsStyle.textTitle, CommonStylesheet.whiteColor]}>
              {this.state.subscriptionInfo.toUpperCase()}
            </Text>
            <View style={{ flex: 1, textAlign: "justify" }}>
              <Text
                style={[SubsStyle.textDesc, CommonStylesheet.whiteColor]}
                adjustsFontSizeToFit={true}
              >
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries
              </Text>
            </View>
          </View>
        </View>
        {this.state.isLoading && <BTLoader />}
      </ImageBackground>
    );
  }
}

const SubsButton = props => {
  return (
    <ImageBackground
      source={props.gradientBg}
      style={SubsStyle.buttonBackground}
    >
      <TouchableOpacity
        style={[
          SubsStyle.subsButton,
          { justifyContent: "space-around", alignItems: "center" }
        ]}
        onPress={() => props.onTap(props.type)}
      >
        <Text
          style={[
            CommonStylesheet.whiteColor,
            {
              width: 170,
              fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
            }
          ]}
        >
          {props.title.toUpperCase()}
        </Text>
        <TouchableOpacity
          onPress={() => props.onInfoTap(props.type)}
          style={{ backgroundColor: "transparent" }}
        >
          <Icon
            style={{ justifyContent: "center", right: 0 }}
            name={"info-circle"}
            size={21}
            color={"white"}
          />
        </TouchableOpacity>
      </TouchableOpacity>
    </ImageBackground>
  );
};

const SubsStyle = StyleSheet.create({
  topContainer: {
    flex: 1
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    fontSize: 20
  },
  buttonBackground: {
    width: width - 30,
    height: null,
    margin: 7
  },
  subsButton: {
    width: width - 20,
    margin: 10,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  bottomText: {
    flex: 0.4,
    //marginTop:15,
    alignItems: "center",
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular

    // backgroundColor:"red"
  },
  textTitle: {
    fontSize: 13,
    fontFamily: fontFamilies.ITCAvantGardeGothicDemi2
  },
  textDesc: {
    margin: 14,
    textAlign: "justify",
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  }
});

const styles1 = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  header: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instruction: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  source: {
    width: "100%",
    height: 120
  }
});
