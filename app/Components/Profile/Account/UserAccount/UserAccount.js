import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  StyleSheet,
  TextInput,
  AsyncStorage,
  Alert,
  Platform
} from "react-native"
import styles from "../../EditProfile/styles";
import stripe from "tipsi-stripe";
const stripApiKey = "pk_test_UHNl4cC6tHkAjbIciiMSGLM2";
import { Actions } from "react-native-router-flux";
import CommonStylesheet from "../../../../utils/CommonStylesheet";
import CheckBox from "app/thirdPartyComponents/react-native-checkbox/checkbox.js";

import { Images } from "../../../../constant/images";
import { FormInput, Button } from "react-native-elements";
import { customPurple, customOrange } from "../../../Authentication/styles";
import {
  BTLoader,
  fontFamilies,
  showInfoMessage
} from "app/constant/appConstant";
const { width } = Dimensions.get("window");
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import withConnect from "./withConnectUserAccout";
import { AutoGrowingTextInput } from "react-native-autogrow-textinput";
import _ from "lodash";
import ServiceApi from "app/utils/ServiceApi/index";

const theme = {
  primaryBackgroundColor: "white",
  secondaryBackgroundColor: "rgba(18,0,20,1.0)",
  primaryForegroundColor: "white",
  secondaryForegroundColor: customOrange,
  accentColor: customOrange,
  errorColor: "red"
};

class UserAccount extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      checked: true,

      userDetail: {
        userID: "",
        payemnt: "",
        billingAddress: "",
        country: "",
        subscription: ""
      },
      userId: "",
      paymentType: "",
      billingAddress: "",
      country: "",
      loading: false,
      stripeResponse: {},

      subscriptionType: 0,
      activePlan: {}
    };
    this.getAccountDetailIndividuals = this.props.getAccountDetailIndividuals.bind(
      this
    );
    this.updateAccountDetail = this.props.updateAccountDetail.bind(this);
    this.onSignOut = this.props.onSignOut.bind(this);
  }

  /**
   * Edit or Update Individual account
   */
  editIndividualUserAccount = async () => {
    if (!this.validateFormOfIndividulaUser()) {
      return;
    }

    const {
      userId,
      paymentType,
      billingAddress,
      country,
      subscriptionType
    } = this.state;

    let formData = new FormData();
    formData.append("user_id", userId);
    formData.append("payment_type", paymentType);
    formData.append("billing_address", billingAddress);
    formData.append("country", country);
    formData.append("subscription_type", subscriptionType);

    await this.updateAccountDetail(formData);
  };

  /***
   * Validate Form
   */
  validateFormOfIndividulaUser = () => {
    const {
      userId,
      paymentType,
      billingAddress,
      country,
      subscriptionType
    } = this.state;

    if (_.isEmpty(userId)) {
    }
    if (_.isEmpty(paymentType)) {
    }
    if (_.isEmpty(billingAddress)) {
      showInfoMessage("Billing address is blank.");
      return false;
    }
    if (_.isEmpty(country)) {
      showInfoMessage("Please enter your country name.");
      return false;
    }
    return true;
  };

  handleCreacteSourcePress = async () => {
    this.setState({
      loading: true
    });

    stripe.setOptions({ publishableKey: stripApiKey });

    const options = {
      smsAutofillDisabled: true,
      requiredBillingAddressFields: "full", // or 'full'
      theme
    };
    let __this = this;
    await stripe
      .paymentRequestWithCardForm(options)
      .then(async response => {
        this.setState({
          stripeResponse: response
        });
        await __this.paymentApiCall(response);
      })
      .catch(error => {
        this.setState({
          loading: false
        });
      });
  };

  paymentApiCall = async response => {
    let responseData = response;
    let userinfo = this.props.userData;
    let {
      date,
      artist,
      venue,
      formatedDate,
      ticketPortal,
      profileImage
    } = this.state;
    try {
      let formData = new FormData();
      formData.append("stripeToken", responseData.tokenId);
      formData.append("plan_id", this.state.activePlan.id);
      formData.append("user_id", this.props.userData.id);
      formData.append("billing_address", JSON.stringify(responseData.card));
      const response = await ServiceApi.makePaymentBasedOnFormData(formData);
      if (response.status === true) {
        this.getAccountDetailForInd().then(response => {});
      }
    } catch (e) {
      this.setState({
        loading: false
      });
    }
  };

  editUserId = text => {
    this.setState({
      userId: text
    });
  };

  paymentType = text => {
    this.setState({
      paymentType: text
    });
  };

  billingAddresss = text => {
    this.setState({
      billingAddress: text
    });
  };

  countryName = text => {
    this.setState({
      country: text
    });
  };

  selectFreeSubscriptionType = () => {
    this.setState({
      subscriptionType: 0
    });
  };

  selectPaidSubscriptionType = () => {
    this.setState({
      subscriptionType: 1
    });
  };

  componentDidMount(props) {
    if (this.props.userData) {
      this.setState({
        userId: this.props.userData.email
      });
      this.getAccountDetailForInd().then(response => {});
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.accountDetail !== nextProps.accountDetail &&
      nextProps.accountDetail
    ) {
      if (nextProps.accountDetail.data) {
        const { userDetail, subscription_type } = nextProps.accountDetail.data;

        this.setState({
          userId: userDetail.email,
          paymentType: nextProps.accountDetail.data.payment_type
            ? nextProps.accountDetail.data.payment_type.toUpperCase() +
              " \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 " +
              userDetail.user_cards.card_last4_digit
            : "",
          country: userDetail.country,
          subscriptionType: userDetail.is_subscribed === "1" ? 1 : 0
        });

        if (userDetail.billing_address) {
          console.log("billing address:", userDetail.billing_address);
          const {
            address_line_1,
            address_line_2,
            city,
            country
          } = userDetail.billing_address;

          const add_1 = address_line_1 === null ? "" : address_line_1;
          const add_2 = address_line_2 === null ? "" : address_line_2;
          const t_city = city === null ? "" : city;
          const temp1 = add_1 === "" ? add_2 : ", " + add_2;
          const temp2 = add_2 === "" ? t_city : ", " + t_city;

          this.setState({
            billingAddress: address_line_1 + temp1 + temp2,
            country: country
          });
        }
      }
    }

    if (nextProps.myProfile.data.subscription_plan_indi) {
      this.setState({
        activePlan: nextProps.myProfile.data.subscription_plan_indi
      });
    }
  }

  getAccountDetailForInd = async () => {
    await this.getAccountDetailIndividuals(this.props.userData);
  };

  logout = () => {
    Alert.alert(
      "Are you sure to logout ?",
      "",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Yes", onPress: this.confirmLogOut }
      ],
      { cancelable: false }
    );
  };

  confirmLogOut = () => {
    AsyncStorage.removeItem("userData", result => {
      this.onSignOut();
      Actions.LOGIN();
    });
  };

  title = item => {
    return (
      <Text style={userAccountStyle.dollor}>
        {`${item.dollor}\n`}
        <Text style={userAccountStyle.type}>{item.plan}</Text>
      </Text>
    );
  };

  render() {
    // if(this.props.isLoading){
    //      // return <EditProfilePlaceHolder/>
    //     return (<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
    //         <BTLoader/></View>)

    //   }

    const { userDetail, records } = this.state;
    const { amount, duration_text } = this.state.activePlan;
    return (
      <KeyboardAwareScrollView
        style={userAccountStyle.KeyboardAwareScrollViewStyle}
      >
        <View style={userAccountStyle.KeyboardAwareScrollViewInner}>
          <View style={styles.formInput}>
            <Text style={CommonStylesheet.formTitle}>EMAIL ID:</Text>
            <View style={[styles.gradientImage]}>
              <TextInput
                style={styles.textInputCustomStyle}
                placeholderTextColor="white"
                underlineColorAndroid="rgba(0,0,0,0)"
                value={this.state.userId}
                editable={false}
                onChangeText={this.editUserId}
              />
            </View>
          </View>
          <View style={styles.formInput}>
            <Text style={CommonStylesheet.formTitle}>PAYMENT TYPE:</Text>
            <View style={[styles.gradientImage, { paddingHorizontal: 10 }]}>
              <AutoGrowingTextInput
                placeholderTextColor="white"
                underlineColorAndroid="rgba(0,0,0,0)"
                value={this.state.paymentType}
                onChangeText={this.paymentType}
                editable={false}
                style={userAccountStyle.AutoGrowingTextInputStyle}
              />
            </View>
          </View>
          <View style={styles.formInput}>
            <Text style={CommonStylesheet.formTitle}>BILLING ADDRESS:</Text>
            <View
              style={[
                styles.gradientImage,
                { height: 100, paddingHorizontal: 10 }
              ]}
            >
              <AutoGrowingTextInput
                placeholderTextColor="white"
                multiline={true}
                //editable = {(Platform.OS==='android')?true:false}
                editable={false}
                numberOfLines={10}
                underlineColorAndroid="rgba(0,0,0,0)"
                onChangeText={this.billingAddresss}
                value={this.state.billingAddress}
                style={{
                  backgroundColor: "transparent",
                  color: "white",
                  height: 100
                }}
              />
            </View>
          </View>
          <View style={styles.formInput}>
            <Text style={CommonStylesheet.formTitle}>COUNTRY / REGION:</Text>
            <View style={[styles.gradientImage]}>
              <TextInput
                style={styles.textInputCustomStyle}
                placeholderTextColor="white"
                //editable = {(Platform.OS==='android')?true:false}
                editable={false}
                underlineColorAndroid="rgba(0,0,0,0)"
                value={this.state.country}
                onChangeText={this.countryName}
              />
            </View>
          </View>
          <View style={styles.formInput}>
            <Text style={userAccountStyle.memberShipName}>
              MEMBERSHIP TYPE:
            </Text>
            <ImageBackground
              source={Images.spotLightPurchase}
              style={[styles.gradientImage, userAccountStyle.imageBackGroundAd]}
            >
              <View style={userAccountStyle.imageBackGroundInner}>
                <View style={userAccountStyle.membershipInnerSub}>
                  <Text style={[userAccountStyle.name, { fontSize: 16 }]}>
                    {"Individual"}
                  </Text>
                </View>
                <View style={userAccountStyle.membershipInnerSub1}>
                  <PlanCheckboxes
                    label={"$0"}
                    subLabel={"FREE"}
                    checked={this.state.subscriptionType === 0}
                  />
                  <PlanCheckboxes
                    label={"$" + this.state.activePlan.amount}
                    checked={this.state.subscriptionType === 1}
                    subLabel={`${
                      this.state.activePlan.duration_text
                    }`.toUpperCase()}
                    onChange={this.handleCreacteSourcePress}
                  />
                </View>
              </View>
            </ImageBackground>
          </View>
          <View style={styles.formInput}>
            <TouchableOpacity
              style={[styles.jkStyle, userAccountStyle.buttonStyle]}
              onPress={this.editIndividualUserAccount}
            >
              <Text style={userAccountStyle.name}>SAVE</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.formInput}>
            <TouchableOpacity
              style={[styles.jkStyle, userAccountStyle.buttonStyle]}
              onPress={this.logout}
            >
              <Text style={userAccountStyle.name}>LOGOUT</Text>
            </TouchableOpacity>
          </View>
          {this.props.isLoading && <BTLoader />}
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

function PlanCheckboxes(props) {
  const style = StyleSheet.create({
    notificationCheckbox: {
      backgroundColor: "transparent",
      borderColor: "transparent",
      margin: 0,
      padding: 0,
      alignItems: "center",
      // width:width*0.23
      flex: 1
    }
  });

  return (
    <CheckBox
      label={props.label}
      subLabel={props.subLabel}
      checkboxStyle={{ height: 20, width: 20 }}
      size={20}
      checkedImage={Images.check}
      uncheckedImage={Images.unCheck}
      checked={props.checked}
      onChange={props.onChange}
    />
  );
}

const userAccountStyle = StyleSheet.create({
  KeyboardAwareScrollViewStyle: {
    backgroundColor: "black",
    paddingBottom: 10
  },

  KeyboardAwareScrollViewInner: {
    marginBottom: 20,
    flex: 1
  },

  name: {
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
    color: "white"
  },
  memberShipName: {
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
    color: customOrange,
    fontSize: 12
  },
  dollor: {
    marginLeft: 5,
    color: "white",
    fontSize: 20,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },

  type: {
    marginLeft: 5,
    fontSize: 10,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
    color: "white"
  },
  buttonStyle: {
    width: width - 20,
    left: 0,
    bottom: 10,
    top: 0
  },

  imageBackGroundAd: {
    paddingTop: 0,
    height: 80,
    backgroundColor: "transparent"
  },

  imageBackGroundInner: {
    flexDirection: "row",
    justifyContent: "space-between"
  },

  membershipInnerSub: {
    flex: 1,
    borderRightWidth: 1,
    borderRightColor: "rgba(80,80,80,1)",
    backgroundColor: "transparent",
    marginVertical: 5,
    justifyContent: "center"
  },

  membershipInnerSub1: {
    flex: 3,
    flexDirection: "row",
    justifyContent: "space-around",
    backgroundColor: "transparent",
    left: 5
  },

  AutoGrowingTextInputStyle: {
    height: 80,
    backgroundColor: "transparent",
    color: "white"
  }
});

export default withConnect(UserAccount);
