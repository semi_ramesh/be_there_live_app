
/***
 * @provideModule  HDAudioPlayer.containers.profile.account.withConnectUserAccount
 */
import React from 'react'
import {auth,serviceApi,shared} from 'app/redux/actions/index'
import {connect} from 'react-redux'

function mapStateToProps(state) {
    const {user:userData} = state.auth;
    const {isLoading} = state.shared;
    const{ accountDetail,myProfile}  = state.serviceApi;
    return{
        userData,
        accountDetail,
        isLoading,
        myProfile
    }
}

function mapDispatchToProps(dispatch) {

    return{
        getAccountDetailIndividuals:(formData)=>dispatch(serviceApi.getAccountDetail(formData)),
        updateAccountDetail:(formData)=>dispatch(serviceApi.updateAccountDetail(formData)),
        onSignOut:(userData)=>dispatch(auth.deAuthorize(userData)),
    }

}


export default  function  withConnect(wrrapperComponent) {

    return connect(mapStateToProps,mapDispatchToProps)(wrrapperComponent);
}
