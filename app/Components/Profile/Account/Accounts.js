import React ,{Component} from 'react'
import UserAccount from "./UserAccount/UserAccount";
import ArtistAccount from "./ArtistAccount/ArtistAccount";
import VenueAccount from "./VenueAccount/VenueAccount";
import {LoginUser} from "../../Subscription/SubscriptionsData";


export default class Accounts extends React.PureComponent{

    constructor(props) {
        super(props);
        this.state = {}
    }
    render(){
        switch(LoginUser.userType) {
            case 2:
                return <UserAccount/>
            case 3:
                return <ArtistAccount/>
            case 4:
                return <VenueAccount/>
            default:
                return <UserAccount/>;
        }
    }


}