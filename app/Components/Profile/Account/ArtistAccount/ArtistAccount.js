/*
* @ProvidesModuleArtistAccount
* Artist Account

* */

import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  TextInput,
  AsyncStorage,
  Alert,
  Platform
} from "react-native";
// just for check
import styles from "../../EditProfile/styles";
import { Actions } from "react-native-router-flux";
import { Images } from "../../../../constant/images";
import CommonStylesheet from "../../../../utils/CommonStylesheet";
import CheckBox from "app/thirdPartyComponents/react-native-checkbox/checkbox.js";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import withConnect from "./withConnectArtistAccount";
import _ from "lodash";
import {
  BTLoader,
  showInfoMessage,
  fontFamilies
} from "../../../../constant/appConstant";
const { width } = Dimensions.get("window");

class ArtistAccount extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      checked: true,
      activePlan: {
        amount: "7.99",
        duration_text: "MONTHLY"
      },
      userID: "",
      payemnt: "",
      billingAddress: "",
      country: "",
      subscription: "",
      mobile: "",
      email: "",
      instagram: "",
      facebook: "",
      twitter: ""
    };
    this.getAccountDetail = this.props.getAccountDetailOfArtist.bind(this);
    this.updateAccountDetail = this.props.updateAccountDetailOfArtist.bind(
      this
    );
    this.onSignOut = this.props.onSignOut.bind(this);
  }

  onChangeTwitter = text => {
    this.setState({
      twitter: text
    });
  };

  onChangeTextPhone = text => {
    this.setState({
      mobile: text
    });
  };

  onChangeCountryName = text => {
    this.setState({
      country: text
    });
  };

  onChangeInstaGram = text => {
    this.setState({
      instagram: text
    });
  };

  validateForm = () => {
    const { mobile, instagram, twitter } = this.state;

    if (_.isEmpty(mobile)) {
      showInfoMessage("Please enter mobile number.");
      return false;
    }
    if (_.isEmpty(instagram)) {
      showInfoMessage("Please enter your instagram account.");
      return false;
    }
    if (_.isEmpty(twitter)) {
      showInfoMessage("Please enter your twitter account.");
      return false;
    }
    return true;
  };

  componentDidMount() {
    this.getAccountDetail();
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.artistAccountDetail &&
      this.props.artistAccountDetail !== nextProps.artistAccountDetail
    ) {
      if (nextProps.artistAccountDetail.data.monthly_plan_for_user) {
        this.setState({
          activePlan: nextProps.artistAccountDetail.data.monthly_plan_for_user
        });
      }

      const paymentType = nextProps.artistAccountDetail.data.payment_type
        ? nextProps.artistAccountDetail.data.payment_type
        : "CARD";
      if (nextProps.artistAccountDetail.data.userDetail) {
        const {
          user_cards,
          billing_address,
          email,
          twitter,
          instagram,
          username,
          phone_no
        } = nextProps.artistAccountDetail.data.userDetail;
        console.log("billing address", billing_address);
        this.setState({
          userID: username,
          payemnt:
            paymentType.toUpperCase() +
            " \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 " +
            user_cards.card_last4_digit,
          billingAddress: billing_address ? billing_address : "",
          country: billing_address ? billing_address.country : "",
          subscription: true,
          mobile: phone_no,
          email: email,
          instagram: instagram,
          twitter: twitter
        });
      }
    }
  }

  updateAccount = () => {
    if (!this.validateForm()) {
      return;
    }
    try {
      const { mobile, instagram, country, twitter } = this.state;

      let formData = new FormData();
      formData.append("phone_no", mobile);
      formData.append("instagram", instagram);
      formData.append("country", country);
      formData.append("twitter", twitter);
      this.updateAccountDetail(formData);
    } catch (e) {}
  };

  logout = () => {
    Alert.alert(
      "Are you sure to logout ?",
      "",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Yes", onPress: this.confirmLogOut }
      ],
      { cancelable: false }
    );
  };

  confirmLogOut = () => {
    AsyncStorage.removeItem("userData", result => {
      this.onSignOut();
      Actions.LOGIN();
    });
  };

  title = item => {
    return (
      <Text style={artistAccountStyle.dollor}>
        {`${item.dollor}\n`}
        <Text style={artistAccountStyle.type}>{item.plan}</Text>
      </Text>
    );
  };

  render() {
    const { userDetail, records, billingAddress } = this.state;
    let address = "";
    if (billingAddress) {
      const {
        address_line_1,
        address_line_2,
        city,
        state,
        country,
        zipcode
      } = billingAddress;
      const add_1 = address_line_1 === null ? "" : address_line_1;
      const add_2 = address_line_2 === null ? "" : address_line_2;
      const t_city = city === null ? "" : city;
      const t_state = state === null ? "" : state;
      const temp1 = add_1 === "" ? add_2 : ", " + add_2;
      const temp2 = add_2 === "" ? t_city : ", " + t_city;
      const temp3 = t_city === "" ? t_state : ", " + t_state;
      address = add_1 + temp1 + temp2 + temp3;
    }

    return (
      <View style={artistAccountStyle.superView}>
        <KeyboardAwareScrollView>
          <View style={[styles.wrapper, { paddingBottom: 20 }]}>
            <View style={styles.innerWrapper}>
              <View style={styles.profileForm}>
                <View style={styles.formInput}>
                  <Text style={CommonStylesheet.formTitle}>USER NAME:</Text>
                  <View
                    style={[
                      styles.gradientImage,
                      artistAccountStyle.gradientImageBg
                    ]}
                  >
                    <TextInput
                      style={styles.textInputCustomStyle}
                      placeholderTextColor="white"
                      underlineColorAndroid="rgba(0,0,0,0)"
                      value={this.state.email}
                      editable={false}
                    />
                  </View>
                </View>
                <View style={styles.formInput}>
                  <Text style={CommonStylesheet.formTitle}>TEXT:</Text>
                  <View
                    style={[
                      styles.gradientImage,
                      artistAccountStyle.gradientImageBg
                    ]}
                  >
                    <TextInput
                      style={styles.textInputCustomStyle}
                      placeholderTextColor="white"
                      underlineColorAndroid="rgba(0,0,0,0)"
                      onChangeText={this.onChangeTextPhone}
                      value={this.state.mobile}
                    />
                  </View>
                </View>
                <View style={styles.formInput}>
                  <Text style={CommonStylesheet.formTitle}>EMAIL:</Text>
                  <View
                    style={[
                      styles.gradientImage,
                      artistAccountStyle.gradientImageBg
                    ]}
                  >
                    <TextInput
                      style={styles.textInputCustomStyle}
                      placeholderTextColor="white"
                      underlineColorAndroid="rgba(0,0,0,0)"
                      editable={false}
                      value={this.state.email}
                    />
                  </View>
                </View>
                <View style={styles.formInput}>
                  <Text style={CommonStylesheet.formTitle}>INSTAGRAM:</Text>
                  <View
                    style={[
                      styles.gradientImage,
                      artistAccountStyle.gradientImageBg
                    ]}
                  >
                    <TextInput
                      style={styles.textInputCustomStyle}
                      placeholderTextColor="white"
                      onChangeText={this.onChangeInstaGram}
                      underlineColorAndroid="rgba(0,0,0,0)"
                      value={this.state.instagram}
                    />
                  </View>
                </View>
                <View style={styles.formInput}>
                  <Text style={CommonStylesheet.formTitle}>TWITER:</Text>
                  <View
                    style={[
                      styles.gradientImage,
                      artistAccountStyle.gradientImageBg
                    ]}
                  >
                    <TextInput
                      style={styles.textInputCustomStyle}
                      placeholderTextColor="white"
                      underlineColorAndroid="rgba(0,0,0,0)"
                      onChangeText={this.onChangeTwitter}
                      value={this.state.twitter}
                    />
                  </View>
                </View>
                <View style={styles.formInput}>
                  <Text style={CommonStylesheet.formTitle}>
                    MEMBERSHIP TYPE:
                  </Text>
                  <ImageBackground
                    source={Images.spotLightPurchase}
                    style={[
                      styles.gradientImage,
                      artistAccountStyle.memberImageBg
                    ]}
                  >
                    <View style={artistAccountStyle.memberBgInner}>
                      <View style={artistAccountStyle.memberSubInner1}>
                        <Text style={artistAccountStyle.memberArtistText}>
                          {"Artist"}
                        </Text>
                      </View>
                      <View style={artistAccountStyle.memberSubInner2}>
                        <PlanCheckboxes
                          label={"$" + this.state.activePlan.amount}
                          checked={true}
                          subLabel={this.state.activePlan.duration_text.toUpperCase()}
                          editable={false}
                        />
                      </View>
                    </View>
                  </ImageBackground>
                </View>
                <View style={styles.formInput}>
                  <Text style={CommonStylesheet.formTitle}>PAYMENT TYPE:</Text>
                  <View
                    style={[
                      styles.gradientImage,
                      artistAccountStyle.gradientImageBg
                    ]}
                  >
                    <TextInput
                      style={styles.textInputCustomStyle}
                      placeholderTextColor="white"
                      editable={false}
                      underlineColorAndroid="rgba(0,0,0,0)"
                      value={this.state.payemnt}
                    />
                  </View>
                </View>
                <View style={styles.formInput}>
                  <Text style={CommonStylesheet.formTitle}>
                    BILLING ADDRESS:
                  </Text>
                  <View
                    style={[
                      styles.gradientImage,
                      artistAccountStyle.gradientImageBg,
                      { height: 100 }
                    ]}
                  >
                    <TextInput
                      style={styles.textInputCustomStyle}
                      placeholderTextColor="white"
                      multiline={true}
                      // editable = {(Platform.OS==='android')?true:false}
                      editable={false}
                      numberOfLines={10}
                      underlineColorAndroid="rgba(0,0,0,0)"
                      value={address}
                    />
                  </View>
                </View>
                <View style={styles.formInput}>
                  <Text style={CommonStylesheet.formTitle}>
                    COUNTRY / REGION:
                  </Text>
                  <View
                    source={Images.gradientBg}
                    style={[
                      styles.gradientImage,
                      artistAccountStyle.gradientImageBg
                    ]}
                  >
                    <TextInput
                      style={styles.textInputCustomStyle}
                      placeholderTextColor="white"
                      //editable = {(Platform.OS==='android')?true:false}
                      editable={false}
                      onChangeText={this.onChangeCountryName}
                      underlineColorAndroid="rgba(0,0,0,0)"
                      value={this.state.country}
                    />
                  </View>
                </View>
              </View>
            </View>

            <TouchableOpacity
              onPress={this.updateAccount}
              style={[styles.jkStyle, artistAccountStyle.saveButtonView]}
            >
              <Text style={artistAccountStyle.buttonText}>SAVE</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={this.logout}
              style={[styles.jkStyle, artistAccountStyle.logoutButtonView]}
            >
              <Text style={artistAccountStyle.buttonText}>LOGOUT</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
        {this.props.isLoading && <BTLoader />}
      </View>
    );
  }
}

function PlanCheckboxes(props) {
  const style = StyleSheet.create({
    notificationCheckbox: {
      backgroundColor: "transparent",
      borderColor: "transparent",
      margin: 0,
      padding: 0,
      alignItems: "flex-start"
    }
  });

  return (
    <CheckBox
      label={props.label}
      subLabel={props.subLabel}
      checkboxStyle={{ height: 20, width: 20 }}
      size={20}
      checkedImage={Images.check}
      uncheckedImage={Images.unCheck}
      checked={props.checked}
    />
  );
}

const artistAccountStyle = StyleSheet.create({
  superView: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "black"
  },

  gradientBg: {
    width: width - 20
  },

  memberImageBg: {
    paddingTop: 0,
    height: 60,
    backgroundColor: "transparent"
  },

  memberBgInner: {
    flexDirection: "row",
    justifyContent: "space-between"
  },

  memberSubInner1: {
    flex: 1,
    borderRightWidth: 1,
    borderRightColor: "rgba(80,80,80,1)",
    backgroundColor: "transparent",
    marginVertical: 5,
    justifyContent: "center"
  },

  memberSubInner2: {
    flex: 3,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    left: 5
  },

  memberArtistText: {
    color: "white",
    fontSize: 16
  },

  saveButtonView: {
    width: width - 20,
    bottom: 10,
    marginHorizontal: 10,
    top: 0
  },

  buttonText: {
    color: "white",
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },

  logoutButtonView: {
    width: width - 20,
    position: "relative",
    bottom: 10,
    top: 10,
    marginHorizontal: 10
  },

  dollor: {
    marginLeft: 5,
    color: "white",
    fontSize: 20,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },

  type: {
    marginLeft: 5,
    fontSize: 10,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
    color: "white"
  }
});

export default withConnect(ArtistAccount);
