import React from "react";
import { auth, serviceApi, shared } from "app/redux/actions/index";
import { connect } from "react-redux";

function mapStateToProps(state) {
  const { user: userData } = state.auth;
  const { isLoading } = state.shared;
  const { artistAccountDetail, userProfile, myProfile } = state.serviceApi;
  return {
    userData,
    artistAccountDetail,
    isLoading,
    userProfile,
    myProfile
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAccountDetailOfArtist: () => dispatch(serviceApi.getAccountOfArtist()),
    updateAccountDetailOfArtist: formData =>
      dispatch(serviceApi.updateAccountOfArtist(formData)),
    onSignOut: userData => dispatch(auth.deAuthorize(userData))
  };
}

export default function withConnect(wrrapperComponent) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(wrrapperComponent);
}
