import React from 'react'
import {auth,serviceApi,shared} from 'app/redux/actions/index'
import {connect} from 'react-redux'

function mapStateToProps(state) {
    const {user:userData} = state.auth;
    const {isLoading} = state.shared;
    const{ accountDetail,accountDetail2,myProfile}  = state.serviceApi;
    return{
        userData,
        accountDetail,
        isLoading,
        accountDetail2,
        myProfile
    }
}

function mapDispatchToProps(dispatch) {

    return{

        getAccountDetail:(formData)=>dispatch(serviceApi.getAccountDetail(formData)),
        postAccountDetail:(formData)=>dispatch(serviceApi.updateAccountDetail(formData)),
        onSignOut:(userData)=>dispatch(auth.deAuthorize(userData))

    }

}


export default  function  withConnect(wrrapperComponent) {

    return connect(mapStateToProps,mapDispatchToProps)(wrrapperComponent);
}
