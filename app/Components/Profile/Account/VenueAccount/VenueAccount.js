import React ,{Component} from 'react'
import {
    View,
    Text,
    ImageBackground,
    TouchableOpacity,
    Dimensions,
    TextInput,
    AsyncStorage,
    Alert
} from 'react-native';

import styles from '../../EditProfile/styles';
import {Actions} from 'react-native-router-flux';
import {Images} from "../../../../constant/images";
import CheckBox from "app/thirdPartyComponents/react-native-checkbox/checkbox.js"
import {customOrange} from "../../../Authentication/styles";
import {KeyboardAwareScrollView} from  'react-native-keyboard-aware-scrollview';
import withConnect from './withConnectVenueAccount'
import {BTLoader, showInfoMessage,fontFamilies} from "../../../../constant/appConstant";
import CommonStylesheet from "../../../../utils/CommonStylesheet";
import _ from 'lodash'

const {width} = Dimensions.get('window');

class VenueAccount extends React.PureComponent{

    constructor(props) {

        super(props);
        this.state = {
            checked: true,
            paymentType: '',
            userDetail: {
                userID: 'john@gmail.com',
                billing_address: {
                    address_line_1: "Deep",
                    address_line_2: "Hgjfg",
                    city: "Ffsgjkhfg",
                    country: "AF",
                    created_at: "2018-10-05 12:46:38",
                    full_name: "Jay",
                    id: 13,
                    state: "Raj",
                    status: "1",
                    updated_at: "2018-10-05 12:46:38",
                    user_id: 56,
                    zipcode: "5346"
                },
                payemnt: 'VISA **** **** **** 5268',
                billingAddress: 'Kim Daily 4221 Lincon Avenue Loas-Angeles, 90018-1764 (323) 555-6291',
                country: 'United State',
                subscription: 'free',
                phone_no: '',
                email: '',
                instagram: 'songstress',
                facebook: 'https://www.facebook.com/songstress.hill',
                twitter: 'songstress_hill',
                username: '',
                user_cards: { card_id: 'Visa....' },
                user_payment: [{
                    billing_address_id: '34', subscription: {
                        amount: "8.99",
                        created_at: "2018-09-21 00:53:21",
                        duration: 30,
                        duration_text: "per month",
                        id: 5,
                        status: "0",
                        subscription_description: "Monthy subscription for venue",
                        subscription_name: "venue_monthly",
                        token: "plan_De0lNpzK17Ozeb",
                        updated_at: "2018-09-21 00:53:21",
                        user_type_id: "4"
                    }
                }]

            }
        };
        this.getAccountDetail = this.props.getAccountDetail.bind(this);
        this.postAccountDetail = this.props.postAccountDetail.bind(this);
        this.onSignOut = this.props.onSignOut.bind(this);
    }

    componentDidMount() {
        this.getAccountDetail()
    }

    validateFormContainingAccountDetail = () => {
        const {
            phone_no,
            billing_address
        } = this.state.userDetail;
        const { country } = billing_address;

        if (_.isEmpty(phone_no)) {
            showInfoMessage('Please enter your contact number.');
            return false
        }
        if (_.isEmpty(country)) {
            showInfoMessage('Please enter your country name.');
            return false;

        }
        return true
    }


    editAccountDetail = () => {
        if (!this.validateFormContainingAccountDetail()) {
            return;
        }
        const { phone_no, billing_address } = this.state.userDetail;
        const { country } = billing_address;
        let formData = new FormData();
        formData.append('country', country);
        this.postAccountDetail(formData);

    };

    logout = () => {
        Alert.alert(
            'Are you sure to logout ?',
            '',
            [
                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Yes', onPress: this.confirmLogOut },
            ],
            { cancelable: false }
        )
    };

    confirmLogOut = () => {
        AsyncStorage.removeItem("userData", () => {
            Actions.LOGIN();
            this.onSignOut();
        });
    };


    componentWillReceiveProps(nextProps) {
        if (this.props.accountDetail !== nextProps.accountDetail && nextProps.accountDetail.payment_details) {

            const payment = (nextProps.accountDetail.payment_type) ? nextProps.accountDetail.payment_type : ''

            if (nextProps.accountDetail.payment_details.billing_address) {
                this.setState({
                    userDetail: nextProps.accountDetail.payment_details,
                    paymentType: payment.toUpperCase()
                })
            }
        }
    }

    editPhoneNumber = (text) => {
        this.setState({
            userDetail: { ...this.state.userDetail, phone_no: text }
        })
    };

    editCountry = (text) => {
        this.setState({
            userDetail: { ...this.state.userDetail, billing_address: { ...this.state.userDetail.billing_address, country: text } }
        })
    }

    title = (item) => {
        return <Text style={venueAccountStyle.dollor}>{`${item.dollor}\n`}<Text style={venueAccountStyle.type}>{item.plan}</Text></Text>
    }
    render() {

        const { userDetail } = this.state;
        const {
            address_line_1,
            address_line_2,
            city,
            country,
            state
        } = userDetail.billing_address;

        const paymentType = this.state.paymentType + ' \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 ' + userDetail.user_cards.card_last4_digit;
        const paymentAmount = (userDetail.user_payment[0].amount) ? userDetail.user_payment[0].amount : '8.99';
        const paymentText = (userDetail.user_payment[0].subscription.duration_text) ? userDetail.user_payment[0].subscription.duration_text : 'MONTHLY';

        const add_1 = (address_line_1 === null) ? '' : address_line_1;
        const add_2 = (address_line_2 === null) ? '' : address_line_2;
        const t_city = (city === null) ? '' : city;
        const t_state = (state === null) ? '' : state;

        const temp1 = (add_1 === '') ? add_2 : ', ' + add_2;
        const temp2 = (add_2 === '') ? t_city : ', ' + t_city;
        const temp3 = (t_city === '') ? t_state : ', ' + t_state;

        let billingAddress = add_1 + temp1 + temp2 + temp3;

        return (
            <View style={venueAccountStyle.wrapper}>
                <KeyboardAwareScrollView>
                    <View style={venueAccountStyle.superView}>
                        <View style={styles.profileForm}>
                            <View style={styles.formInput}>
                                <Text style={CommonStylesheet.formTitle}>USER ID</Text>
                                <View style={venueAccountStyle.gradientImage}>
                                    <TextInput placeholderTextColor="white"
                                        style={styles.textInputCustomStyle}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        editable={false}
                                        value={this.state.userDetail.email} />
                                </View>
                            </View>
                            <View style={styles.formInput}>
                                <Text style={CommonStylesheet.formTitle}>TEXT</Text>
                                <View onPress={this.editPhoneNumber}
                                    style={venueAccountStyle.gradientImage}>
                                    <TextInput placeholderTextColor="white"
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        style={styles.textInputCustomStyle}
                                        value={this.state.userDetail.phone_no} />
                                </View>
                            </View>
                            <View style={styles.formInput}>
                                <Text style={CommonStylesheet.formTitle}>EMAIL</Text>
                                <View style={venueAccountStyle.gradientImage}>
                                    <TextInput placeholderTextColor="white"
                                        style={styles.textInputCustomStyle}
                                        editable={false}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        value={this.state.userDetail.email} />
                                </View>
                            </View>
                            <View style={styles.formInput}>
                                <Text style={[CommonStylesheet.formTitle, venueAccountStyle.membershipText]}>MEMBERSHIP TYPE:</Text>
                                <ImageBackground source={Images.spotLightPurchase}
                                    style={[styles.gradientImage, venueAccountStyle.imageBgStyle]}>
                                    <View style={venueAccountStyle.memberView}>
                                        <View style={venueAccountStyle.memberInnerView}>
                                            <Text style={venueAccountStyle.venueText}>{'Venue'}</Text>
                                        </View>
                                        <View style={venueAccountStyle.membershipAmountView}>
                                            <PlanCheckboxes label={'$' + paymentAmount}
                                                checked={true}
                                                subLabel={paymentText.toUpperCase()}
                                                editable={false} />
                                        </View>
                                    </View>
                                </ImageBackground>
                            </View>
                            <View style={styles.formInput}>
                                <Text style={CommonStylesheet.formTitle}>PAYMENT TYPE</Text>
                                <View style={venueAccountStyle.gradientImage}>
                                    <TextInput placeholderTextColor="white"
                                        style={styles.textInputCustomStyle}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        editable={false}
                                        value={paymentType} />
                                </View>
                            </View>
                            <View style={styles.formInput}>
                                <Text style={CommonStylesheet.formTitle}>BILLING ADDRESS</Text>
                                <View style={[venueAccountStyle.gradientImage, { height: 100 }]}>
                                    <TextInput placeholderTextColor="white"
                                        multiline={true}
                                        //editable = {(Platform.OS==='android')?true:false}
                                        editable={false}
                                        numberOfLines={10}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        value={`${billingAddress}`}
                                        style={[styles.textInputCustomStyle, { height: 100 }]} />
                                </View>
                            </View>
                            <View style={styles.formInput}>
                                <Text style={CommonStylesheet.formTitle}>COUNTRY / REGION</Text>
                                <View style={venueAccountStyle.gradientImage}>
                                    <TextInput placeholderTextColor="white"
                                        //editable = {(Platform.OS==='android')?true:false}
                                        editable={false}
                                        onTextChange={this.editCountry}
                                        style={styles.textInputCustomStyle}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        value={country} />
                                </View>
                            </View>
                        </View>
                        <TouchableOpacity onPress={this.editAccountDetail}
                            style={[styles.jkStyle, venueAccountStyle.saveButton]}>
                            <Text style={venueAccountStyle.buttonText}>SAVE</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.logout}
                            style={[styles.jkStyle, venueAccountStyle.logOutButton]}>
                            <Text style={venueAccountStyle.buttonText}>LOGOUT</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>
                {this.props.isLoading && <BTLoader />}
            </View>
        )
    }
}


function PlanCheckboxes(props) {

    return (<CheckBox label={props.label}
        subLabel={props.subLabel}
        checkboxStyle={{ height: 20, width: 20 }}
        size={20}
        checkedImage={Images.check}
        uncheckedImage={Images.unCheck}
        checked={props.checked}
    />)

}

const venueAccountStyle  ={
   wrapper:{
       backgroundColor: '#000000',
       flex:1,
       alignItems:'center',
       justifyContent:'center',
       padHorizontal:10,
   },

   membershipText:{ 
       color: customOrange 
    },

    imageBgStyle:{ 
        paddingTop: 0, 
        height: 60, 
        backgroundColor: 'transparent' 
    },

    memberView:{ 
        flexDirection: 'row', 
        justifyContent: 'space-between' 
    },

    memberInnerView:{ 
        flex: 1, 
        borderRightWidth: 1, 
        borderRightColor: 'rgba(80,80,80,1)', 
        backgroundColor: 'transparent', 
        marginVertical: 5, 
        justifyContent: 'center' 
    },
    venueText:{ 
        color: 'white', 
        fontSize: 16 
    },

    membershipAmountView:{ 
        flex: 3, 
        justifyContent: 'center', 
        alignItems: 'center', 
        backgroundColor: 'transparent', 
        left: 5 
    },

    saveButton:{ 
        width: width - 20, 
        bottom: 10, 
        marginHorizontal: 10, 
        marginVertical: 15 
    },

    buttonText:{ 
        color: "white", 
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular 
    },

    logOutButton:{ 
        width: width - 20, 
        position: 'relative', 
        bottom: 10, 
        marginHorizontal: 10 
    },

   gradientImage:{
        top:3,
        height:40,
        width:width-20,
        marginBottom:10,
        justifyContent:'center',
        paddingHorizontal:10,
        backgroundColor:'rgba(23,23,23,1)', 
        borderWidth : 1,
        borderColor:'rgba(39,39,39,1)',
   },

   superView:{
       flex:1,
       paddingBottom: 20, 
   },

    dollor: {
        marginLeft: 5,
        color: 'white',
        fontSize: 20,
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
    },

    type :{
        marginLeft:5,
        fontSize:10,
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular,
        color:'white',
    }

};

export default withConnect(VenueAccount);