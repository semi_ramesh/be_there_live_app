/**
 * @ProvideModules STYLES.PROFILE
 *
 */

import React,{Component} from 'react'
import {StyleSheet,Dimensions,Platform} from 'react-native'
import {customOrange} from "../Authentication/styles";
import {fontFamilies} from "../../constant/appConstant";
const {height,width} = Dimensions.get('window');
const window = Dimensions.get('window');
const AVATAR_SIZE = 120;
const ROW_HEIGHT = 60;
const PARALLAX_HEADER_HEIGHT = 350;
const STICKY_HEADER_HEIGHT = 70;
export const isIphone5s = height === 568
export default StyleSheet.create({

     topView:{
        backgroundColor:'#000000',
        flex:1,
        alignItems:'center',
        justifyContent:'center'
     },

    userProfieImageContainer:{
        flex:1,
        alignItems:'flex-start'
    },

    cog :{
       backgroundColor:'transparent',
        width:40,
        alignItems:'flex-end',
        right:10
    },

    pencil:{
         position:'absolute',
        bottom:0,
        right:5
    },
    container:{
        flex:10,
        backgroundColor:'black'
    },

    profileInfoContainer:{
        width:width-20,
        backgroundColor:'transparent',
        flexDirection:'row',
    },

    gridView:{
        ...Platform.select({android:{flex:2.2},ios:{flex:2}}),
        //backgroundColor:'red',
        //width:width,
        width:width,
        //alignItems:'stretch',
        justifyContent:'flex-start',
       // height:height
    },


    placeHolderView:{
         backgroundColor:'rgba(70,6,80,1)',
         justifyContent:'center',
         paddingHorizontal:0,
         paddingVertical:0,
         top:0
    },
    orangeTextStyle:{
        marginBottom:10,
        left:10,
        fontSize:24,
        color:customOrange,
        fontWeight:'300'
    },
    imgStyle:{
        marginLeft:0,
        height:width*0.5*0.6,
        width:width*0.5,
    },
    nameDetail:{
        //marginTop:50,
        marginLeft:-8,
    },
    textDetail:{
        marginLeft:0,
        flexDirection:'row',
        justifyContent:'space-between',
        flex:0.68,


    },

     nameAlignView:{
        marginLeft:-5,
        flexDirection:'row',
        justifyContent:'space-around',
        flex:0,
         //backgroundColor:'green'

    },

    boldAndBig:{
        fontSize:18,
        width:width*0.3,
        //height:30,
       // marginBottom:5,
        color:'white',
        fontWeight:'400',
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
    },
    boldAndBig2:{
        fontSize:20,
        color:'white',
        fontWeight:'400',
    },
    normal:{
        //marginTop:2,
        fontSize:10,
        color:'white',
        fontWeight:'100',
        fontStyle:'italic'
    },
    normal1:{
        //marginTop:2,
        fontSize:9,
        color:'white',
        fontWeight:'200',
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
    },

    normal2:{
        marginTop:2,
        fontSize:10,
        color:customOrange,
        alignSelf:'center',
        fontWeight:'200',
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
    },

    settingIcon:{

    },
    socialInfoContainer:{

    },

    socialInfo:{
        flex:0,
        flexDirection:'row',
        alignItems:'center'

    },

    socialIcon:{
        marginTop:4,
        height:12,
        width:12,
        resizeMode:'contain',
        marginRight:5,
    },

    socialText:{
        fontSize:10,
        color:'white',
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
    },

    editBackG:{
        marginTop:10,
        height:40,
        width:150,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'black'
    },

    editText:{
        textAlign:'center',
        color:'white',
        fontSize:16,
    },

    editBtnStyle:{
        height:40,
        width:150,
        justifyContent:'center'

    },

    countPlate:{
        flex:0,
        marginTop:10,
        height:50,
        justifyContent:'space-between',
        flexDirection:'row',
        backgroundColor:'black',
        borderTopWidth:0.4,
        borderBottomWidth:0.4,
        alignItems:'center',
        borderColor:'rgba(48,40,50,1.0)'
    },

    plates:{
        height:42,
        width :((width/4)-10),
        alignItems:'center',
    },
    touchPlates:{
        height:42,
        width :((width/4)-10),
        alignItems:'center',
        justifyContent:'center',
        marginHorizontal:3,
    },

    editButtonBg:{
        
        height:30,
        width :(width*0.4),
        alignItems:'center',
        justifyContent:'center',

        marginHorizontal:5,

    },
        editButton:{
        height:30,
        width :(width*0.4),
        alignItems:'center',
        justifyContent:'center',
        marginHorizontal:0,
        top:5
    },
    itemStyle:{
        alignItems:'flex-start',
        justifyContent:'flex-end',
        


         },
    followedStyle : {
        bottom:-1,
        position:'absolute',
        alignItems:'flex-start',
        left:0,


    },

    followPlus:{
        top:-1,
        position:'absolute',
        alignItems:'flex-end',
        alignSelf:'flex-end'
    },
    minus:{
        height:width/5,
        width:width/5,
        alignItems:'flex-start',
        justifyContent:'flex-end',
        backgroundColor:'transparent'

    },
    editStyle:{
        position:'absolute',
        right:0,
        bottom:0,
        borderColor:'white',
        borderWidth:2,
        backgroundColor:'transparent'
    },
    container1: {
        flex: 1,
        backgroundColor: 'black'
    },
    background: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: window.width,
        height: PARALLAX_HEADER_HEIGHT
    },
    stickySection: {
        height: STICKY_HEADER_HEIGHT,
        width: 300,
        justifyContent: 'flex-end'
    },
    stickySectionText: {
        color: 'white',
        fontSize: 20,
        margin: 10
    },
    fixedSection: {
        position: 'absolute',
        bottom: 10,
        right: 10
    },
    fixedSectionText: {
        color: '#999',
        fontSize: 20
    },
    parallaxHeader: {
        alignItems: 'center',
        //flex: 0.875,
        ...Platform.select({ios: isIphone5s?{flex:2}:{flex:1.3},android:{flex: 2}}),
       // height:0.7*height,
        flexDirection: 'column',
        //paddingTop: 5,
        backgroundColor:'black'
    },
    avatar: {
        marginBottom: 10,
        borderRadius: AVATAR_SIZE / 2
    },
    sectionSpeakerText: {
        color: 'white',
        fontSize: 24,
        paddingVertical: 5
    },
    sectionTitleText: {
        color: 'white',
        fontSize: 18,
        paddingVertical: 5
    },
    row: {
        overflow: 'hidden',
        paddingHorizontal: 10,
        height: ROW_HEIGHT,
        backgroundColor: 'white',
        borderColor: '#ccc',
        borderBottomWidth: 1,
        justifyContent: 'center'
    },
    rowText: {
        fontSize: 20
    },
    textInputCustomStyle:{
        color:'white',
        height:40,
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
    }

})