import React ,{Component} from 'react'
import UserNotifications from "./UserNotifications/UserNotifications";
import ArtistNotifications from "./ArtistNotifications/ArtistNotifications";
import VenueNotifications from "./VenueNotifications/VenueNotifications";
import {LoginUser} from "../../Subscription/SubscriptionsData";


export default class Notifications extends React.PureComponent{

    constructor(props) {
        super(props);
        this.state = {}
    }
    render(){
        switch(LoginUser.userType) {
            case 2:
                return <UserNotifications/>
            case 3:
                return <ArtistNotifications/>
            case 4:
                return <VenueNotifications/>
            default:
                return <UserNotifications/>;
        }
    }
}