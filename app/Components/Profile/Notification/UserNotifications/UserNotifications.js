import React ,{Component} from 'react'
import {
    View, Text, ImageBackground, TouchableOpacity, Dimensions,
    StyleSheet,TextInput,Platform
} from 'react-native'
import styles from '../../EditProfile/styles'
import Icon from 'react-native-vector-icons/FontAwesome'
import {customOrange, customPurple} from "../../../Authentication/styles";
import CommonStylesheet from "../../../../utils/CommonStylesheet";
import { PermissionsAndroid } from 'react-native';

const {width} = Dimensions.get('window');
import MapView,{Marker} from 'react-native-maps'
import withConnect from './withConnectUserNotifications'
import {KeyboardAwareScrollView} from  'react-native-keyboard-aware-scrollview'
import { BTLoader, showInfoMessage, fontFamilies,GOOGLE_API_KEY} from "../../../../constant/appConstant";
import { Switch } from 'react-native-switch';
import Geocoder from 'react-native-geocoding';

import _ from 'lodash'

class UserNotifications extends Component{

    constructor(props) {

        super(props);
        this.state = {
            checked: false,
            emailChecked: false,
            latitude: 0,
            longitude: 0,
            mobile: '',
            email: '',
            city: '',
            islocate: false,
            zipcode: '',
            selectedLat: 0,
            selectedLong: 0,
            locationGtanted: false,
            region: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,

            }
        };
        this.getNotificationDetail = this.props.getNotificationDetail.bind(this);
        this.updateNotificationDetail = this.props.updateNotificationDetail.bind(this);
    }


    /**
     * Component life cycle
   **/
    componentDidMount() {

        if (Platform.OS === "android") {
            this.requestLocationPermission();
        }
        this.locateUser();
        this.getNotificationDetail(this.props.userData);
    }

    componentDidUnMount() {
        if (this.watchID) {
            navigator.geolocation.clearWatch(this.watchID);
        }
    }

    requestLocationPermission = async () => {

        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'BTL App Location Permission',
                    'message': 'BTL App needs access to your location '

                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.setState({

                    locationGtanted: true

                });
                this.locateUser();

            } else {
            }
        } catch (err) {
        }
    }

    locateUser = () => {

        if (Platform.OS === "android") {
            if (!this.state.locationGtanted) {
                return;
            }
        }

        this.setState({
            islocate: true
        });

        this.watchID = navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: Number(position.coords.latitude),
                    longitude: Number(position.coords.longitude),
                    error: null,
                    region: {
                        latitude: Number(position.coords.latitude),
                        longitude: Number(position.coords.longitude),
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }
                });
                console.log('Current location called', position);
                this.getLocationName()
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );

    };


    /** Get USER Notification Detail**/
    getIndividualUserNotificationDetail = async ()=>{
        await this.getNotificationDetail(this.props.userData);
    };

    /** Update User Notification Detail **/
    updateIndividualUserNotificationDetail = async () => {

        if (!this.validateNotificationForm()) {
            return;
        }
        const {
            selectedLat,
            selectedLong,
            email,
            mobile,
            city,
            zipcode,
            checked,
            emailChecked
        } = this.state;
        const mobileCheck = (checked) ? 1 : 0;
        const emailCheck = (emailChecked) ? 1 : 0;
        let formData = new FormData();

        formData.append("city", city);
        formData.append("zipcode", zipcode);
        formData.append("latitude", selectedLat);
        formData.append("longitude", selectedLong);
        formData.append("phone_no", mobile);
        formData.append("email", email);
        formData.append("isEmail", emailCheck);
        formData.append("isMobile", mobileCheck);
        await this.updateNotificationDetail(formData);
    };

    /** Validate Individual Form **/
    validateNotificationForm = () => {
        const {
            latitude,
            longitude,
            email,
            mobile,
            city,
            zipcode
        } = this.state;

        if (_.isEmpty(city)) {
            showInfoMessage('Please enter name of your city .')
            return false;
        }

        if (_.isEmpty(mobile)) {
            showInfoMessage('Please enter name of your contact number to get notification.')
            return false;
        };
        if (_.isEmpty(email)) {
            showInfoMessage("Please enter your email to get email notification.")
            return false;
        }
        return true
    };

    componentWillReceiveProps(nextProps) {

        if (nextProps.notification) {
            const { email,
                isEmail,
                isMobile,
                phone_no,
                zipcode,
                latitude,
                longitude,
                city } = nextProps.notification;
            this.setState({
                checked: (isMobile === 1) ? true : false,
                emailChecked: (isEmail === 1) ? true : false,
                selectedLat: latitude !== null ? Number(latitude) : this.state.selectedLat,
                selectedLong: longitude !== null ? Number(longitude) : this.state.selectedLong,
                region: {
                    latitude: latitude !== null ? Number(latitude) : this.state.selectedLat,
                    longitude: longitude !== null ? Number(longitude) : this.state.selectedLong,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                },
                mobile: phone_no,
                email: email,
                city: (city === null) ? this.state.city : city,
                zipcode: (zipcode === null) ? this.state.zipcode : zipcode,
            });

        }

    }


    editYourCity = (text) => {
        this.setState({
            city: text
        })
    };

    editYourZipCode = (text) => {
        this.setState({
            zipcode: text
        })
    }


  
    onMapPress = (e) => {

        this.setState({
            selectedLat: e.nativeEvent.coordinate.latitude,
            selectedLong: e.nativeEvent.coordinate.longitude,
            islocate: false,
            region: {
                latitude: e.nativeEvent.coordinate.latitude,
                longitude: e.nativeEvent.coordinate.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            }


        })
        this.getLocationName();

    };

    isEmailChecked = () => {
        this.setState({
            emailChecked: this.state.emailChecked === false ? true : false,
        })
    };

    isMobileChecked = () => {
        this.setState({
            checked: this.state.checked === false ? true : false
        })
    };


    /**
     *  Getting current Place name using Google Geo location
     * */
    getLocationName = async () => {

        if (this.state.latitude === 0 || this.state.longitude === 0) {
            this.setState({
                latitude: 26.9124,
                longitude: 75.7873,
            })
        }
        // const lat = (this.state.selectedLat===0)?this.state.latitude:this.state.selectedLat;
        // const long = (this.state.selectedLong===0)?this.state.longitude:this.state.selectedLong;

        const lat = (this.state.islocate) ? this.state.latitude : this.state.selectedLat;
        const long = (this.state.islocate) ? this.state.longitude : this.state.selectedLong;

        Geocoder.init(GOOGLE_API_KEY);
        Geocoder.from(lat, long)
            .then(json => {
                let city = ''
                let zip = ''
                json.results[0].address_components.map((item) => {

                    let types = item.types;
                    if (types.indexOf('locality') != '-1') {
                        city = item.long_name
                    }
                    else if (types.indexOf('postal_code') != '-1') {
                        zip = item.long_name
                    }

                });

                this.setState({
                    city: city,
                    zipcode: zip
                })
            })
            .catch(error => console.log("Geocoding error", error));

    }

    onRegionChange=(region)=> {
       this.setState({ region:region });
    }

    render(){

        if (this.props.isLoading) {
            return (<View style={wrapperStyle.loaderView}>
                <BTLoader /></View>)

        }

        const containerSwitch = this.state.checked === true ? wrapperStyle.switchChecked : wrapperStyle.switchUnChecked
        const containerSwitch2 = this.state.emailChecked === true ? wrapperStyle.switchChecked : wrapperStyle.switchUnChecked
        return (
            <View style={wrapperStyle.superView}>
                <KeyboardAwareScrollView style={wrapperStyle.KeyboardAwareScrollViewStyle}>
                    <View style={[styles.container, wrapperStyle.KeyboardAwareScrollViewInner]}>
                        <View style={styles.profileForm}>
                            <View style={styles.formInput}>
                                <Text style={CommonStylesheet.formTitle}>TEXT:</Text>
                                <ImageBackground style={[styles.gradientImage, wrapperStyle.textImageBg]}>
                                    <TextInput style={styles.textInputCustomStyle} placeholderTextColor="white" underlineColorAndroid='rgba(0,0,0,0)' value={this.state.mobile} editable={false} />
                                    <Switch
                                        containerStyle={containerSwitch}
                                        onValueChange={this.isMobileChecked}
                                        value={this.state.checked}
                                        backgroundActive={'black'}
                                        backgroundInactive={'black'}
                                        circleActiveColor={customPurple}
                                        circleInActiveColor={'white'} />
                                </ImageBackground>
                            </View>
                            <View style={styles.formInput}>
                                <Text style={CommonStylesheet.formTitle}>EMAIL:</Text>

                                <ImageBackground style={[styles.gradientImage, wrapperStyle.textImageBg]}>
                                    <TextInput editable={false} style={[styles.textInputCustomStyle, { width: width * 0.6 }]} placeholderTextColor="white" underlineColorAndroid='rgba(0,0,0,0)' value={this.state.email} />
                                    <Switch
                                        onValueChange={this.isEmailChecked}
                                        value={this.state.emailChecked}
                                        backgroundActive={'black'}
                                        backgroundInactive={'black'}
                                        containerStyle={containerSwitch2}
                                        circleActiveColor={customPurple}
                                        circleInActiveColor={'white'} />
                                </ImageBackground>
                            </View>
                        </View>
                        <View style={wrapperStyle.locationTextView}>
                            <Text style={CommonStylesheet.formTitle}>MY LOCATION:</Text>
                        </View>
                        <View style={[styles.container, { marginBottom: 15 }]}>
                            <View style={wrapperStyle.map}>
                                <MapView style={StyleSheet.absoluteFillObject}
                                    showsUserLocation={true}
                                    showsMyLocationButton={true}
                                    onPress={this.onMapPress}
                                    initialRegion={this.state.region}
                                    region={this.state.region}
                                    onRegionChange={this.onRegionChange}>
                                    <Marker
                                        coordinate={{
                                            latitude: this.state.selectedLat,
                                            longitude: this.state.selectedLong
                                        }}
                                        title={this.state.city}
                                        description={'My show to night'} />
                                </MapView>

                                <View style={wrapperStyle.locationIconView}>
                                    <TouchableOpacity title={'Location'}
                                        style={wrapperStyle.locationStyle}
                                        onPress={this.locateUser}>
                                        <Icon size={50} name={'user'} color={customOrange} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.profileForm}>
                                <View style={styles.formInput}>
                                    <Text style={CommonStylesheet.formTitle}>CITY:</Text>
                                    <ImageBackground
                                        style={styles.gradientImage}>
                                        <TextInput style={styles.textInputCustomStyle} placeholderTextColor="white"
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            value={this.state.city}
                                            onChangeText={this.editYourCity} />
                                    </ImageBackground>
                                </View>
                                <View style={styles.formInput}>
                                    <Text style={CommonStylesheet.formTitle}>ZIP:</Text>
                                    <ImageBackground style={styles.gradientImage}>
                                        <TextInput style={styles.textInputCustomStyle} placeholderTextColor="white"
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            value={this.state.zipcode}
                                            onChangeText={this.editYourZipCode}
                                        />
                                    </ImageBackground>
                                </View>
                            </View>
                            <TouchableOpacity
                                style={[styles.jkStyle, wrapperStyle.saveButton]}
                                onPress={this.updateIndividualUserNotificationDetail}>
                                <Text style={wrapperStyle.name}>SAVE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </KeyboardAwareScrollView>
                {this.props.isLoading && <BTLoader />}
            </View>)
    }
}


const wrapperStyle = {

    superView: {
        flex: 1,
        justifyContent: 'center'
    },

    loaderView:{ 
        flex: 1, 
        backgroundColor: 'transparent', 
        justifyContent: 'center' 
    },

    KeyboardAwareScrollViewStyle: {
        backgroundColor: 'black'
    },

    KeyboardAwareScrollViewInner: {
        marginTop: 0,
        marginBottom: 20
    },

    textImageBg: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    locationTextView: {
        backgroundColor: 'transparent',
        alignSelf: 'flex-start',
        paddingHorizontal: 10,
        marginTop: 15
    },

    locationIconView: {
        position: 'absolute',
        marginBottom: 0,
        marginRight: 10
    },

    map: {

        width: width - 30,
        marginBottom: 10,
        height: 200,
        marginHorizontal: 10
    },

    locationStyle: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        width: 50
    },

    name: {
        color: 'white',
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
    },

    switchChecked: {
        borderColor: customPurple,
        borderWidth: 1
    },

    switchUnChecked: {
        borderColor: 'white',
        borderWidth: 1
    },

    saveButton: {
        width: width - 20,
        left: 0,
        bottom: 0,
        top: 10
    }

};

export  default  withConnect(UserNotifications)