import React from 'react'
import {connect} from 'react-redux'
import {auth,serviceApi} from 'app/redux/actions/index'

function mapStateToProps(state){
    const {user:userData} = state.auth;
    const {
        myProfile,
        genereList,
        profileImage,
        notification
     } = state.serviceApi;
    const {isLoading} = state.shared;
    return{
        userData,
        myProfile,
        genereList,
        isLoading,
        profileImage,
        notification
    }
}

function mapDispatchToProps(dispatch){
    return {
        getProfile:(formData) => dispatch(serviceApi.getProfile(formData)),
        getNotificationDetail:(formData)=>dispatch(serviceApi.getNotificationSetting(formData)),
        updateNotificationDetail:(formData)=>dispatch(serviceApi.updateNotification(formData))
    }
}

export default function withConnect(WrrapedComponent){

    return connect(mapStateToProps,mapDispatchToProps)(WrrapedComponent)
}