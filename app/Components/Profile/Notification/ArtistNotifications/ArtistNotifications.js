import React ,{Component} from 'react'
import {
    View,
    Text,
    Image,
    ImageBackground,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
    TextInput,
    Platform
} from 'react-native'
import styles from '../../EditProfile/styles'
import Icon from 'react-native-vector-icons/FontAwesome'
import Geocoder from 'react-native-geocoding';
import { PermissionsAndroid } from 'react-native';
import {Images} from "../../../../constant/images";
import CheckBox from "app/thirdPartyComponents/react-native-checkbox/checkbox.js"
import {customOrange, iconColor} from "../../../Authentication/styles";
import CommonStylesheet from "../../../../utils/CommonStylesheet";
const {width} = Dimensions.get('window');
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import MapView,{Marker} from 'react-native-maps'
import Modal from 'react-native-modalbox'
import withConnect from './withConnectArtistNotification'
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput'
import { web } from 'react-native-communications';
import {newStyle} from "../../EditProfile/EditArtistProfile/ArtistProfile";
import {BTLoader, showInfoMessage,fontFamilies,GOOGLE_API_KEY} from "../../../../constant/appConstant";
import moment from 'moment'
import _ from 'lodash';
import DateTimePicker from 'react-native-modal-datetime-picker';
class ArtistNotifications extends Component{

    constructor(props) {
        super(props);
        this.state = {
            checked: true,
            alertMessage: '',
            mobile: '',
            email: '',
            city: '',
            zipCode: '',
            alert: '',
            address: '',
            tours: [],
            locationGtanted: false,
            tourDate: moment().format('YYYY/MM/DD'),
            tourTime: moment().format('hh:mm:ss'),
            line_up: 'Information',
            ticket_link: 'https://',
            venueName: '',
            latitude: 0,
            longitude: 0,
            region: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            includeLocation: false,
            selectedLat: 0,
            selectedLong: 0,
            isDateTimePickerVisible: false
        }

        this.pushNotificationToFollowers = this.props.pushNotificationToFollowers.bind(this);
        this.addNewToursForArtist = this.props.addNewTours.bind(this);
    }

    /**
         * Component life cycle
       **/
    componentDidMount() {
        if (Platform.OS === "android") {
            this.requestLocationPermission();

        }
        this.locateUser();
    }

    componentDidUnMount() {

        navigator.geolocation.clearWatch(this.watchID);
    }

    requestLocationPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'BTL App Location Permission',
                    'message': 'BTL App needs access to your location '

                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.setState({

                    locationGtanted: true

                });
                this.locateUser();

            }
        } catch (err) {
        }
    }

    locateUser = () => {

        if (Platform.OS === "android") {
            if (!this.state.locationGtanted) {
                return;
            }
        }
        this.setState({
            selectedLat: 0,
            selectedLong: 0,
        });
        this.watchID = navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: Number(position.coords.latitude),
                    longitude: Number(position.coords.longitude),
                    error: null,
                    region: {
                        latitude: Number(position.coords.latitude),
                        longitude: Number(position.coords.longitude),
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }
                });
                this.getLocationName()
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 },
        );
    };

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            userDetail: { [name]: value }
        });
    };

    messageTextChange = (text) => {
        this.setState({
            alertMessage: text
        })
    };

    handleTourDateChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            tourDate: { [name]: value }
        });
    };

    addMoreDate = () => {
        this.setState({
            venueName: '',
            ticket_link:'https://'
        })
        this.refs.modal1.open();
    }


    _showDateTimePicker = (text) => {
        this.setState({
            isDateTimePickerVisible: true,
            pickerMode: text
        })
    };

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        let date1 = moment(date).format("YYYY/MM/DD");
        this.setState({
            tourDate: date1
        });

        this._hideDateTimePicker();

    };

    componentWillReceiveProps(nextProps) {

        if (nextProps.userProfile && this.props.userProfile !== nextProps.userProfile) {
            if (nextProps.userProfile.data.user_details) {
                if (nextProps.userProfile.data.user_details.tour) {
                    this.setState({
                        tours: nextProps.userProfile.data.user_details.tour
                    })
                }
            }
        }
        if (nextProps.toursData && this.props.toursData !== nextProps.toursData) {
            this.setState({
                tours: nextProps.toursData.data
            })
        }

    }

    openTicketLink = (url) => {
        web(url)
       // web('https://github.com/facebook')
    };

    includeLocationOrNot = () => {
        this.setState({
            includeLocation: !this.state.includeLocation
        })
    };

    validateTourDateForm = () => {
        const {
            line_up,
            ticket_link,
            venueName,
            tourDate } = this.state;
        if (_.isEmpty(line_up)) {
            showInfoMessage('Please enter some lineup.')
            return false
        }
        if (_.isEmpty(ticket_link) || ticket_link ==='https://' || !this.ValidURL(ticket_link)) {
            showInfoMessage('Please enter youer ticket link');
            return false

        }
        if (_.isEmpty(venueName)) {
            showInfoMessage('Please enter your venue name')
            return false

        }
        if (_.isEmpty(tourDate)) {

            showInfoMessage('Please enter your tour date');
            return false

        }
        return true;
    }


     ValidURL = (str)=> {
         console.log(" Validate function called", str);
         var regex =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        if(!regex .test(str)) {
          return false;
        } else {
          return true;
        }
      }
      
    addNewTourDates = async () => {

        if (!this.validateTourDateForm()) {
            return
        }
        try {
            let formData = new FormData();
            formData.append('tour_date', this.state.tourDate);
            formData.append('tour_time', this.state.tourTime);
            formData.append('venue_name', this.state.venueName);
            formData.append('ticket_link', this.state.ticket_link);
            await this.addNewToursForArtist(formData);
            await this.closeModel()
        }
        catch (e) {
        }
    };

    closeModel = () =>{
        this.refs.modal1.close()
    };

    /**
    *  Getting current Place name using Google Geo location
    * */
    getLocationName = async () => {
        if (this.state.latitude === 0 || this.state.longitude === 0) {
            this.setState({
                latitude: 26.9124,
                longitude: 75.7873
            })
        }

        const lat = (this.state.selectedLat === 0) ? this.state.latitude : this.state.selectedLat;
        const long = (this.state.selectedLong === 0) ? this.state.longitude : this.state.selectedLong;

        Geocoder.init(GOOGLE_API_KEY);
        Geocoder.from(lat, long)
            .then(json => {
                let street = '';
                let route = '';
                let colony = '';
                let city = ''
                let zip = '';
                let address = '';

                json.results[0].address_components.map((item) => {
                    let types = item.types;
                    if (types.indexOf('street_number') != '-1') {
                        street = item.long_name
                    }
                    else if (types.indexOf('route') != '-1') {
                        route = item.long_name
                    }
                    else if (types.indexOf('sublocality') != '-1') {
                        colony = item.long_name
                    }
                    else if (types.indexOf('locality') != '-1') {
                        city = item.long_name
                    }
                    else if (types.indexOf('postal_code') != '-1') {
                        zip = item.long_name
                    }
                });

                route = (street) ? ',' + route : route;
                colony = (street || route) ? ',' + colony : colony;
                address = street + route + colony

                this.setState({
                    address: address,
                    city: city,
                    zipCode: zip
                })

            })
            .catch(error => console.log("GeoLocation error", error));
    }



    //Here we will add newDates

    onMapPress = (e) => {
        this.setState({
            selectedLat: e.nativeEvent.coordinate.latitude,
            selectedLong: e.nativeEvent.coordinate.longitude,

        })
        this.getLocationName();
    };

    sendPushToFollowerForYourTour = () => {
        if (_.isEmpty(this.state.alertMessage)) {
            showInfoMessage('Please enter your message.')
            return
        }
        let formData = new FormData();
        const aLet = (this.state.selectedLat === 0 || this.state.selectedLat === null) ? this.state.latitude : this.state.selectedLat
        const aLong = (this.state.selectedLong === 0 || this.state.selectedLong === null) ? this.state.latitude : this.state.selectedLong

        formData.append('message', this.state.alertMessage, this.state.selectedLong);
        if (this.state.includeLocation) {
            formData.append('longitude', aLet);
            formData.append('latitude', aLong);
            formData.append('city', this.state.city);
            formData.append('address', this.state.address);
            formData.append('zipcode', this.state.zipCode);
        }
        this.pushNotificationToFollowers(formData)
    };

    setYourAddress = (text) => {
        this.setState({
            address: text
        })
    };

    setYourCity = (text) => {
        this.setState({
            city: text
        })
    };

    setYourZip =(text)=>{
         this.setState({
             zipCode:text
         })
    }

    setTourDate = (text) => {
        this.setState({
            tourDate: text
        })
    }

    setVenueName = (text) => {
        this.setState({
            venueName: text
        })
    };


    setTicketLink = (text) => {
        this.setState({
            ticket_link: text
        })

    };

    setLineUPText = (text) => {
        this.setState({
            line_up: text
        })
    }

    addMoreAlbum = () => {
        this.refs.modal1.open();
    };

    onRegionChange = (region) => {
        this.setState({ region: region });
    }

    render() {

        const { alertMessage } = this.state;

        return (
            <View style={notificationStyle.wrapper}>
                <KeyboardAwareScrollView style={notificationStyle.KeyboardAwareScrollView}>
                    <View style={notificationStyle.innerWrapper}>
                        <Text style={CommonStylesheet.formTitle}>{"ALERT"}</Text>
                        <View style={notificationStyle.alertContainer}>
                            <View style={notificationStyle.alertContainerBox}>
                                <View style={notificationStyle.alertBox}>
                                    <AutoGrowingTextInput
                                        style={[styles.textInputCustomStyle, { height: 90 }]}
                                        value={alertMessage}
                                        placeholderTextColor={'lightgray'}
                                        placeholder={'Alert message will goes here'}
                                        onChangeText={this.messageTextChange}
                                        multiline={true}
                                    />
                                </View>
                            </View>
                            <View style={notificationStyle.alertButtonContainer}>
                                <CheckBox label={'INCLUDE LOCATION'}
                                    labelStyle={notificationStyle.checkBoxText}
                                    size={20}
                                    checkedImage={Images.check}
                                    uncheckedImage={Images.unCheck}
                                    checked={this.state.includeLocation}
                                    onChange={this.includeLocationOrNot}
                                    containerStyle={notificationStyle.notificationCheckbox} />
                            </View>

                        </View>
                        {this.state.includeLocation &&
                            <View style={notificationStyle.locationContainer}>
                                <Text style={[CommonStylesheet.titles, { marginTop: 5 }]}>{"ALERT LOCATIONS  "}
                                    <Icon name={'map-marker'} color={iconColor} size={21} />
                                </Text>
                                <View style={wrapperStyle.map}>
                                    <MapView style={StyleSheet.absoluteFillObject}
                                        showsUserLocation={true}
                                        showsMyLocationButton={true}
                                        onPress={this.onMapPress}
                                        initialRegion={this.state.region}
                                        region={this.state.region}
                                        onRegionChange={this.onRegionChange}>
                                        <Marker
                                            coordinate={{
                                                latitude: this.state.selectedLat,
                                                longitude: this.state.selectedLong
                                            }}
                                            title={this.state.city}
                                            description={'My show to night'}
                                        />
                                    </MapView>
                                    <View style={notificationStyle.locationIconView}>
                                        <TouchableOpacity title={'Location'}
                                            style={wrapperStyle.locationStyle}
                                            onPress={this.locateUser}>
                                            <Icon size={50} name={'user'} color={customOrange} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={notificationStyle.LocationInfoContainer}>
                                    <View style={notificationStyle.lcoationInfo}>
                                        <Text style={CommonStylesheet.formTitle}>ADDRESS:</Text>
                                        <ImageBackground
                                            style={notificationStyle.textInputStyle1}>
                                            <TextInput style={styles.textInputCustomStyle}
                                                placeholderTextColor="white"
                                                onChangeText={this.setYourAddress}
                                                underlineColorAndroid='rgba(0,0,0,0)'
                                                value={this.state.address} />
                                        </ImageBackground>
                                    </View>
                                    <View style={notificationStyle.lcoationInfo}>
                                        <Text style={CommonStylesheet.formTitle}>CITY:</Text>
                                        <ImageBackground style={notificationStyle.textInputStyle1}>
                                            <TextInput style={styles.textInputCustomStyle}
                                                placeholderTextColor="white"
                                                onChangeText={this.setYourCity}
                                                underlineColorAndroid='rgba(0,0,0,0)'
                                                value={this.state.city} />
                                        </ImageBackground>
                                    </View>
                                    <View style={notificationStyle.lcoationInfo}>
                                        <Text style={CommonStylesheet.formTitle}>ZIPCODE:</Text>
                                        <ImageBackground
                                            style={notificationStyle.textInputStyle1}>
                                            <TextInput style={styles.textInputCustomStyle}
                                                placeholderTextColor="white"
                                                onChangeText={this.setYourZip}
                                                underlineColorAndroid='rgba(0,0,0,0)'
                                                value={this.state.zipCode} />
                                        </ImageBackground>
                                    </View>
                                </View>
                            </View>}
                        <TouchableOpacity style={notificationStyle.pushAlert}
                            onPress={this.sendPushToFollowerForYourTour}>
                            <ImageBackground source={Images.pushAlertBg}
                                style={notificationStyle.pushAlertBgView}>
                                <Text style={notificationStyle.pushAlertText}>{'PUSH ALERT'}</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                        <View style={{ marginTop: 15 }}>
                            <Text style={CommonStylesheet.formTitle}>{"TOUR DATES"}</Text>
                            {this.state.tours.map((item) => {
                                return <View key={item.id} style={notificationStyle.locationContainer}>
                                    <View style={notificationStyle.LocationInfoContainer} >
                                        <View style={notificationStyle.lcoationInfo}>
                                            <Text style={CommonStylesheet.formTitle}>ARTIST / BAND:</Text>
                                            <ImageBackground style={notificationStyle.textInputStyle1}>
                                                <TextInput
                                                    editable={false}
                                                    borderColor='rgba(39,39,39,1)'
                                                    style={styles.textInputCustomStyle}
                                                    placeholderTextColor="white"
                                                    underlineColorAndroid='rgba(0,0,0,0)'
                                                    value={item.venue_name} />
                                            </ImageBackground>
                                        </View>

                                        <View style={notificationStyle.dateTimeView}>
                                            <View style={notificationStyle.lcoationInfo}>
                                                <Text style={CommonStylesheet.formTitle}>DATE:</Text>
                                                <ImageBackground style={[notificationStyle.textInputStyle, { width: (width - 60) / 2 }]}>
                                                    <TextInput
                                                        editable={false}
                                                        style={styles.textInputCustomStyle}
                                                        placeholderTextColor="white"
                                                        underlineColorAndroid='rgba(0,0,0,0)'
                                                        value={item.tour_date} />
                                                </ImageBackground>
                                            </View>

                                            <View style={notificationStyle.lcoationInfo}>
                                                <Text style={CommonStylesheet.formTitle}>TIME:</Text>
                                                <ImageBackground style={[notificationStyle.textInputStyle, { width: (width - 60) / 2 }]}>
                                                    <TextInput
                                                        editable={false}
                                                        style={styles.textInputCustomStyle}
                                                        placeholderTextColor="white"
                                                        underlineColorAndroid='rgba(0,0,0,0)'
                                                        value={item.tour_time} />
                                                </ImageBackground>
                                            </View>
                                        </View>

                                        <View style={notificationStyle.lcoationInfo}>
                                            <Text style={CommonStylesheet.formTitle}>TICKET PORTAL LINK:</Text>
                                            <TouchableOpacity
                                                onPress={() => this.openTicketLink(item.ticket_link)}
                                                style={[notificationStyle.textInputStyle1 , { height: 40, justifyContent: 'center' }]}>
                                                <Text style={notificationStyle.ticketLinkText}>{item.ticket_link}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            })}
                            </View>

                        <TouchableOpacity style={notificationStyle.addNewButton} onPress={this.addMoreDate}>
                            <Text style={notificationStyle.addMoreDateText}>{'ADD MORE DATE'}</Text>
                            <View style={notificationStyle.addMoreIconView}>
                                <Image source={Images.follow} style={notificationStyle.addMoreIcon} />
                            </View>
                        </TouchableOpacity>

                    </View>
                </KeyboardAwareScrollView>
                {this.props.isLoading && <BTLoader />}

                <Modal
                    style={[mstyles.modal]}
                    ref={"modal1"}
                    swipeToClose={false}
                    position="center"
                    onClosingState={this.onClosingState}>
                    {this.props.isLoading && <BTLoader />}
                    <View style={notificationStyle.modelSuperView}>
                        <KeyboardAwareScrollView style={{ flex: 1 }}>
                            <View style={[newStyle.albumImage, { paddingBottom: 20 }]}>
                                <View style={[newStyle.formInput, { marginBottom: 5 }]}>
                                    <Text style={CommonStylesheet.formTitle}>ARTIST / BAND</Text>
                                    <ImageBackground
                                        style={notificationStyle.textInputStyle}>
                                        <TextInput style={styles.textInputCustomStyle}
                                            placeholderTextColor="white"
                                            onChangeText={this.setVenueName}
                                            underlineColorAndroid='transparent'
                                            value={this.state.venueName} />
                                    </ImageBackground>
                                </View>

                                <View style={[newStyle.formInput, { marginBottom: 5 }]}>
                                    <Text style={CommonStylesheet.formTitle}>EVENT DATE</Text>
                                    <ImageBackground
                                        style={notificationStyle.textInputStyle}>
                                        <TouchableOpacity style={notificationStyle.modelDateTimeView} onPress={() => this._showDateTimePicker('date')}>
                                            <Text style={[styles.textInputCustomStyle, { top: 6 }]}>{this.state.tourDate}</Text>
                                        </TouchableOpacity>
                                    </ImageBackground>
                                </View>

                                <View style={[newStyle.formInput, { marginBottom: 5 }]}>
                                    <Text style={CommonStylesheet.formTitle}>EVENT TIME</Text>
                                    <ImageBackground
                                        style={notificationStyle.textInputStyle}>
                                        <TouchableOpacity style={notificationStyle.modelDateTimeView} onPress={() => this._showDateTimePicker('time')}>
                                            <Text style={[styles.textInputCustomStyle, { top: 6 }]}>{this.state.tourTime}</Text>
                                        </TouchableOpacity>
                                    </ImageBackground>
                                </View>


                                <View style={[newStyle.formInput, { marginBottom: 5 }]}>
                                    <Text style={CommonStylesheet.formTitle}>TICKET-LINK</Text>
                                    <ImageBackground
                                        style={notificationStyle.textInputStyle}>
                                        <TextInput style={styles.textInputCustomStyle}
                                            placeholderTextColor="white"
                                            onChangeText={this.setTicketLink}
                                            underlineColorAndroid='transparent'
                                            value={this.state.ticket_link} />
                                    </ImageBackground>
                                </View>


                                <TouchableOpacity style={[newStyle.wrapperBtnComponent, { marginTop: 10, width: width - 20 }]} onPress={this.addNewTourDates}>
                                    <Text style={notificationStyle.buttonText}>{'SAVE'}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={this.closeModel}
                                    style={[newStyle.wrapperBtnComponent, { marginTop: 5, width: width - 20 }]}>
                                    <Text style={notificationStyle.buttonText}>{'CANCEL'}</Text>
                                </TouchableOpacity>
                            </View>

                        </KeyboardAwareScrollView>
                    </View>
                </Modal>

                <DateTimePicker
                    minimumDate={new Date()}
                    mode={this.state.pickerMode}
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                />
            </View>
        )
    }
}



function NotificationCheckboxes(props) {

    const style = StyleSheet.create({
        notificationCheckbox: {
            backgroundColor: '#181818',
            borderColor: '#181818',
            margin: 0,
            padding: 0,
            alignItems: 'flex-start'
        },

        labelStyle: {
            color: 'white',
            fontWeight: 'normal',
            fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
        }


    });

    return (
        <CheckBox label={props.title}
            labelStyle={style.labelStyle}
            size={20}
            checkedColor={customOrange}
            checkedImage={Images.check}
            uncheckedImage={Images.unCheck}
            onChange={props.onChange}
            containerStyle={style.notificationCheckbox} />
    )

}
const wrapperStyle = {
    map:{
        marginTop:20,
        padding:5,
        width: width-50,
        marginBottom:10,
        height:200,
    },

    locationStyle :{
        height:50,
        justifyContent:'center',
        alignItems:'center',
        width:50}
};

const notificationStyle = StyleSheet.create({

    KeyboardAwareScrollView:{ 
        paddingBottom: 20 
    },

    checkBoxText:{ 
        color: 'white', 
        fontWeight: 'normal', 
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular, 
        fontSize: 14 
    },

    dateTimeView:{ 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        width: width - 50 
    },

    pushAlertBgView:{ 
        width: width - 20, 
        height: 40, 
        justifyContent: 'center' 
    },

    pushAlertText:{ 
        color: 'white', 
        top: (Platform.OS === 'ios') ? 5 : 0, 
        textAlign: 'center', 
        fontFamily: fontFamilies.ITCAvantGardeStdBook 
    },

    addMoreDateText:{ 
        color: customOrange, 
        top: (Platform.OS === 'ios') ? 5 : 0, 
        textAlign: 'center', 
        fontFamily: fontFamilies.ITCAvantGardeStdBook 
    },

    addMoreIconView:{ 
        position: 'absolute', 
        top: 0, 
        right: 0 
    },

    addMoreIcon:{ 
        height: 40, 
        width: 40 
    },

    locationIconView:{ 
        position: 'absolute', 
        marginBottom: 0, 
        marginRight: 10 
    },

    ticketLinkText:{ 
        color: 'white', 
        textAlign: 'left' 
    },

    modelSuperView:{
        paddingVertical: 10,
        flex: 1,
        borderWidth: 3, 
        //borderColor: "#240900",
        paddingHorizontal: 5,
        borderRadius: 10
    },

    modelDateTimeView:{ 
        width: width - 50, 
        height: 40 
    },

    buttonText:{ 
        color: 'white', 
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular 
    },

    wrapper:{
        flex:1,
        backgroundColor:'black',
        paddingHorizontal:10,
        justifyContent:'center'
    },

    innerWrapper:{
        marginTop: 15 
    },

    alertContainer: {
        flex:1,
        marginTop:5,
    },
    alertContainerBox:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },

    alertBox:{
        flex:3,
        backgroundColor:'#181818',
        height:width/3,
        justifyContent:'flex-start',
        paddingHorizontal:8
    },

    alertButtonContainer:{
        flexDirection:'row',
        backgroundColor:'#181818'
    },

    pushAlert:{
        marginVertical: 10,
        borderWidth:1,
        borderColor:"#9a2100",
        alignItems:'center',
        width:width-20,
        height:40,
        alignSelf:'center',
        justifyContent:'center'
    },

    addNewButton:{
        backgroundColor:'transparent',
        width:width-20,
        alignItems:'center',
        justifyContent:'center',
        marginBottom: 20, 
        backgroundColor: 'rgba(23,23,23,1)', 
        height: 40, 
        marginTop: 5
    },

    locationContainer:{
        flex:1,
        marginVertical:12,
        borderColor:"#240900",
        borderWidth:3,
        borderRadius:10,
        paddingHorizontal:10,
    },

    LocationInfoContainer:{
        marginTop:10,
        marginBottom:10
    },

    lcoationInfo:{
        marginTop:15,
    },

    notificationCheckbox: {
        backgroundColor:'#181818',
        borderColor:'#181818',
        marginLeft:10,
        padding:0,
        alignItems:'flex-start'
    },


    textInputStyle:{
      marginTop:2,
      borderWidth : 1,
      borderColor:'rgba(39,39,39,1)',
      width:width-20,
      paddingHorizontal:10,
      backgroundColor:'rgba(23,23,23,1)',
    },


    textInputStyle1:{
        marginTop:2,
        borderWidth : 1,
        borderColor:'rgba(39,39,39,1)',
        width:width-50,
        paddingHorizontal:10,
        backgroundColor:'rgba(23,23,23,1)',
      },

});

const mstyles = StyleSheet.create({

    modal: {
        flex:1,
        bottom:10,
        paddingBottom:10,
        backgroundColor:'black'
    },
});

export default withConnect(ArtistNotifications)