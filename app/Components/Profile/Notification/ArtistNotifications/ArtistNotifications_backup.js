import React ,{Component} from 'react'
import {
    View, Text, Image, ImageBackground, TouchableOpacity, Dimensions, ScrollView, Button,
    StyleSheet
} from 'react-native'
import styles from '../../EditProfile/styles'
import Icon from 'react-native-vector-icons/FontAwesome'

import {Images} from "../../../../constant/images";
import {FormInput, CheckBox} from "react-native-elements";
import {customPurple} from "../../../Authentication/styles";
const {width} = Dimensions.get('window');
export default class ArtistNotifications extends React.PureComponent{

    constructor(props) {

        super(props);
        this.state = {
            checked: true,
            userDetail: {
                mobile: '(323)5556291',
                email: 'goombah11@gmail.com',
                city: 'Los Angeles',
                zipcode: '90018'
            }
        }
    }

    profile = () => {

    }

    render(){
        const {userDetail,records} = this.state;


        return(
            <View style = {styles.wrapper}>
                <ScrollView>
                    <View style={[styles.innerWrapper, {margin:5}]}>
                        <View style={notificationStyle.container}>

                            <View style={notificationStyle.sideImage}>
                                <Image source={Images.user1} resizeMode={'cover'} style={{height: width/3, width:width/3}}   />
                            </View>
                            <View style={[notificationStyle.profileForm]}>
                                <View style={[styles.formInput, {backgroundColor:'#1A1A1C'}]}>

                                    <ImageBackground source = {Images.gradientBg} style = {[styles.gradientImage,{ height:85}]}>
                                        <FormInput placeholderTextColor="white" multiline = {true}  numberOfLines = {10}
                                                   underlineColorAndroid='rgba(0,0,0,0)' value={this.state.userDetail.billingAddress}
                                                   style={{ height:300, backgroundColor:'red'}}
                                        />
                                    </ImageBackground>
                                </View>
                                <View style={[styles.formInput, {backgroundColor:'#1A1A1C'}]}>
                                    <NotificationCheckboxes title={'Include Location'.toUpperCase()} checked={this.state.checked}  />
                                </View>
                                <View style={{alignItems:'flex-end', borderWidth:1, borderColor:'blue', marginTop:90}}>
                                    <Button
                                        title="PUSH ALERT"
                                        color={'#D62E01'}
                                        buttonStyle={{
                                            width: 250,
                                            height: 40,
                                            borderWidth: 0,
                                            marginRight:80,
                                            paddingRight:80,
                                            backgroundColor:'#1F0800',
                                        }}
                                    />
                                </View>
                            </View>
                        </View>
                        <Text style={{color:'white', marginTop:10}}>MY LOCATION <Icon name={'map-marker'}/></Text>
                        <View style={notificationStyle.container}>
                            <View style={notificationStyle.sideImage}>
                                <Image source={Images.location_maps} resizeMode={'cover'} style={{height: width/3, width:width/3}}   />
                            </View>
                            <View style={[notificationStyle.profileForm]}>
                                <View style={styles.formInput}>
                                    <Text style={{ color: customPurple}}>{'Address'}</Text>
                                    <ImageBackground source = {Images.gradientBg} style = {styles.gradientImage}>
                                        <FormInput placeholderTextColor="white"  underlineColorAndroid='rgba(0,0,0,0)' value={this.state.userDetail.billingAddress} />
                                    </ImageBackground>
                                </View>
                                <View style={styles.formInput}>
                                    <Text style={{ color: customPurple}}>{'City'}</Text>
                                    <ImageBackground source = {Images.gradientBg} style = {styles.gradientImage}>
                                        <FormInput placeholderTextColor="white"  underlineColorAndroid='rgba(0,0,0,0)' value={this.state.userDetail.billingAddress} />
                                    </ImageBackground>
                                </View>
                                <View style={styles.formInput}>
                                    <Text style={{ color: customPurple}}>{'Zip'}</Text>
                                    <ImageBackground source = {Images.gradientBg} style = {styles.gradientImage}>
                                        <FormInput placeholderTextColor="white"  underlineColorAndroid='rgba(0,0,0,0)' value={this.state.userDetail.billingAddress} />
                                    </ImageBackground>
                                </View>
                                <View style={{alignItems:'center'}}>
                                    <Button
                                        title="PUSH ALERT"
                                        color={'#D62E01'}
                                        buttonStyle={{
                                            width: 150,
                                            height: 40,
                                            borderWidth: 0,
                                            marginRight:30,
                                            backgroundColor:'#1F0800',
                                        }}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={notificationStyle.container}>
                            <View style={notificationStyle.sideImage}>
                                <Text style={{height: width/3, width:width/3, color:'white'}} > {'TOUR DATE'} </Text>
                            </View>
                            <View style={[notificationStyle.profileForm]}>
                                <View style={styles.formInput}>
                                    <Text style={{ color: customPurple}}>{'DATE'}</Text>
                                    <ImageBackground source = {Images.gradientBg} style = {styles.gradientImage}>
                                        <FormInput placeholderTextColor="white"  underlineColorAndroid='rgba(0,0,0,0)' value={this.state.userDetail.billingAddress} />
                                    </ImageBackground>
                                </View>
                                <View style={styles.formInput}>
                                    <Text style={{ color: customPurple}}>{'LINE - UP'}</Text>
                                    <ImageBackground source = {Images.gradientBg} style = {styles.gradientImage}>
                                        <FormInput placeholderTextColor="white"  underlineColorAndroid='rgba(0,0,0,0)' value={this.state.userDetail.billingAddress} />
                                    </ImageBackground>
                                </View>
                                <View style={styles.formInput}>
                                    <Text style={{ color: customPurple}}>{'VENUE'}</Text>
                                    <ImageBackground source = {Images.gradientBg} style = {styles.gradientImage}>
                                        <FormInput placeholderTextColor="white"  underlineColorAndroid='rgba(0,0,0,0)' value={this.state.userDetail.billingAddress} />
                                    </ImageBackground>
                                </View>
                                <View style={styles.formInput}>
                                    <Text style={{ color: customPurple}}>{'TICKET PORTAL'}</Text>
                                    <ImageBackground source = {Images.gradientBg} style = {styles.gradientImage}>
                                        <FormInput placeholderTextColor="white"  underlineColorAndroid='rgba(0,0,0,0)' value={this.state.userDetail.billingAddress} />
                                    </ImageBackground>
                                </View>
                                <View style={{alignItems:'center'}}>
                                    <Button
                                        title="ADD MORE"
                                        color={'#D62E01'}
                                        buttonStyle={{
                                            width: 150,
                                            height: 40,
                                            borderWidth: 0,
                                            marginRight:30,
                                            backgroundColor:'#1F0800',
                                        }}
                                    />
                                </View>
                            </View>
                        </View>

                    </View>
                </ScrollView>
            </View>
        )
    }
}



function NotificationCheckboxes(props){

    const style = StyleSheet.create({
        notificationCheckbox: {
            backgroundColor:'#1A1A1C',
            borderColor:'#1A1A1C',
            margin:0,
            padding:0,
            alignItems:'flex-start',
        }
    });

    return(
        <CheckBox title={props.title} textStyle={{ color: customPurple}}
                  size={20}
                  checkedColor={'white'}
                  uncheckedColor={'white'}
                  checked={props.checked}
                  containerStyle={style.notificationCheckbox} />
    )
}

const notificationStyle = StyleSheet.create({
    sideImage: {
        height:(width/3),
        width:(width/3),
        /*justifyContent: 'center',
        alignItems: 'center',*/
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        marginTop:10
    },
    profileForm :{
    }

});