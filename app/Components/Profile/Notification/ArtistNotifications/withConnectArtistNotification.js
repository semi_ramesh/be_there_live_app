import {View} from 'react-native';
import React from 'react'
import {connect} from 'react-redux'
import {auth,serviceApi} from 'app/redux/actions'


function mapStateToProps(state) {

    const {isLoading}  = state.shared;
    const {userProfile,toursData,push} = state.serviceApi;
    const {userData} = state.auth;

    return{isLoading, userProfile, toursData,push}
}


function  mapDispatchToProps(dispatch) {
    return{
        pushNotificationToFollowers :(formData)=>dispatch(serviceApi.pushNotificationToFollowers(formData)),
        addNewTours:(formData)=>dispatch(serviceApi.addNewTours(formData))
    }

}

export default function withConnect(WrrapedComponent){

    return connect(mapStateToProps,mapDispatchToProps)(WrrapedComponent)
}