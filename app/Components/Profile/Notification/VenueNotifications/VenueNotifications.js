import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    ImageBackground,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
    TextInput
} from 'react-native'
import styles from '../../EditProfile/styles'
import { Images } from "../../../../constant/images";
import { customOrange} from "../../../Authentication/styles";
import { styleConstant } from "../../../../utils/CommonStylesheet";
import CommonStylesheet from "../../../../utils/CommonStylesheet";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import withConnect from './withConnectVenueNotifications';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment'
import { BTLoader, getExtensionFromMimeType, showInfoMessage, fontFamilies } from "../../../../constant/appConstant";
import BTLImagePickerController from "../../../../utils/imagePicker";
import Modal from 'react-native-modalbox'

const { width } = Dimensions.get('window');



import _ from 'lodash'
class VenueNotifications extends Component {

    constructor(props) {
        super(props);
        this.state = {
            events: {
                artist: '',
                date: moment().format("YYYY-MM-DD"),
                time: moment().format('hh:mm:ss'),
                ticket_portal: 'https://'
            },
            currentImage: Images.placeholder,
            selectedImageBlob: '',
            toNightEvents: [],
            upComingEvents: [],
            pickerMode: 'date',
            isDateTimePickerVisible: false,
            swipeToClose: true,
        };

        this.sendPushAlert = this.props.sendPushAlert.bind(this);
        this.deleteEvent = this.props.deleteEvent.bind(this);
        this.addNewEvent = this.props.addNewEvent.bind(this);
        this.getNotificationDetail = this.props.getNotificationDetail.bind(this);
        this.updateNotificationDetail = this.props.updateNotificationDetail.bind(this);
    }


    _showDateTimePicker = (text) => {
        this.setState({
            isDateTimePickerVisible: true,
            pickerMode: text
        })
    };

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {

        if (this.state.pickerMode === 'time') {

            let time = moment(date).format('hh:mm:ss')
            this.setState({
                events: { ...this.state.events, time: time }
            })
        }
        else {
            let date1 = moment(date).format("YYYY-MM-DD")
            this.setState({
                events: { ...this.state.events, date: date1 }
            })
        }
        this._hideDateTimePicker();

    };


    componentDidMount() {
        this.getNotificationDetail()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.notification) {
            if (nextProps.notification.data) {
                const {
                    upcoming_events,
                    to_night_event } = nextProps.notification.data;
                this.setState({
                    toNightEvents: to_night_event,
                    upComingEvents: upcoming_events
                })
            }

        }
        if (nextProps.venueEventList) {
            if (nextProps.venueEventList.data) {
                const {
                    upcoming_events,
                    to_night_event } = nextProps.venueEventList.data;
                this.setState({
                    toNightEvents: to_night_event,
                    upComingEvents: upcoming_events
                })
            }
        }

    }

    editYourName = (text) => {
        this.setState({
            events: { ...this.state.events, artist: text }
        })
    };

    editTicketLink = (text) => {
        this.setState({
            events: { ...this.state.events, ticket_portal: text }
        })
    };




    uploadImageOfEvent = () => {
        if (!this.validateAllFeilds()) {
            return
        }
        /**
         * artist_name:  (required)
         event_date:  (required)
         event_time:  (required)
         ticket_link:  (required)
         event_image:  (required
         */
        try {
            const {

                artist,
                date,
                ticket_portal,
                time
            } = this.state.events;
            const { selectedImageBlob } = this.state;
            let formData = new FormData();
            formData.append('artist_name', artist);
            formData.append('event_date', date);
            formData.append('event_time', time);
            formData.append('ticket_link', ticket_portal);
            formData.append('event_image', selectedImageBlob);
            this.addNewEvent(formData)
            this.closeModel()
        }
        catch (e) {
            showInfoMessage('Something went wrong , Please try again.')
        }
    };

    validateAllFeilds = () => {
        const {
            artist,
            date,
            ticket_portal,
            time,
        } = this.state.events;
        const { selectedImageBlob } = this.state;

        if (_.isEmpty(selectedImageBlob)) {
            showInfoMessage('Please upload event cover.')
            return false
        }

        if (_.isEmpty(artist)) {
            showInfoMessage('Please enter artist name.')
            return false
        }
        if (_.isEmpty(date)) {
            showInfoMessage('Please enter date.')
            return false
        }

        if (_.isEmpty(ticket_portal) || ticket_portal ==='https://' || !this.ValidURL(ticket_portal)) {
            showInfoMessage('Please enter your ticket portal.');
            return false

        }
        if (_.isEmpty(time)) {
            showInfoMessage('Please enter your event time.');
            return false

        }
        return true
    };

    ValidURL = (str)=> {
        var regex =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
       if(!regex .test(str)) {
         return false;
       } else {
         return true;
       }
     }

    captureOrTakeImage = async () => {
        try {
            await BTLImagePickerController.pickImage().then(response => {

                this.setState({
                    currentImage: { uri: response.uri }
                });

                let timeStamp = Math.floor(Date.now() / 1000);
                let infoDict = getExtensionFromMimeType(response.uri)
                let blob = {
                    uri: response.uri,
                    name: `${timeStamp}.${infoDict.extension}`,
                    filename: `${timeStamp}.${infoDict.extension}`,
                    type: `${infoDict.mime}`
                }

                this.setState({ selectedImageBlob: blob })

            }).then(error => {
            });
        }
        catch (e) {
        }
    };


    closeModel = () => {
        this.refs.modal1.close()
        this.setState({
            currentImage: Images.placeholder,
            selectedImageBlob: {},
            events: {
                artist: '',
                date: moment().format("YYYY-MM-DD"),
                time: moment().format('hh:mm:ss'),
                ticket_portal: 'https://'
            }
        });
    };

    addMoreEvent = () => {
        this.refs.modal1.open();
    };

    renderItem = (item) => {
        const {
            artist_name,
            event_date,
            event_time,
            image_path,
            ticket_link,
        } = item;

        return (<View style={notificationStyle.locationContainer}>
            <View style={notificationStyle.mapView}>
                <Image source={{ uri: image_path }}
                    style={notificationStyle.imageStyle}
                    resizeMode={'cover'} />
            </View>
            <View style={notificationStyle.LocationInfoContainer}>
                <View style={notificationStyle.lcoationInfo}>
                    <Text style={CommonStylesheet.formTitle}>ARTIST/BAND:</Text>
                    <View
                        style={notificationStyle.gradientBgStyle}>
                        <TextInput placeholderTextColor="white"
                            editable={false}
                            style={styles.textInputCustomStyle}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            value={artist_name} />
                    </View>
                </View>
                <View style={notificationStyle.dateUpperView}>
                    <View style={notificationStyle.lcoationInfo}>
                        <Text style={CommonStylesheet.formTitle}>DATE:</Text>
                        <View
                            style={[notificationStyle.gradientBgStyle, notificationStyle.dateBg]}>
                            <TextInput placeholderTextColor="white"
                                editable={false}
                                style={styles.textInputCustomStyle}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                value={event_date} />
                        </View>
                    </View>
                    <View style={notificationStyle.lcoationInfo}>
                        <Text style={CommonStylesheet.formTitle}>TIME:</Text>
                        <View
                            style={[notificationStyle.gradientBgStyle, notificationStyle.dateBg]}>
                            <TextInput placeholderTextColor="white"
                                editable={false}
                                style={styles.textInputCustomStyle}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                value={event_time} />
                        </View>
                    </View>
                </View>
                <View style={notificationStyle.lcoationInfo}>
                    <Text style={CommonStylesheet.formTitle}>TICKET PORTAL LINK:</Text>
                    <View
                        style={notificationStyle.gradientBgStyle}>
                        <TextInput
                            editable={false}
                            style={styles.textInputCustomStyle}
                            placeholderTextColor="white"
                            underlineColorAndroid='rgba(0,0,0,0)'
                            value={ticket_link} />
                    </View>
                </View>
            </View>

            <TouchableOpacity style={[notificationStyle.pushAlert, { marginVertical: 10 }]} onPress={() => this.sendPushAlert(item.id)}>
                <ImageBackground source={Images.pushAlertBg}
                    style={notificationStyle.pushAlertBg}>
                    <Text style={notificationStyle.pushAlertText}>{'PUSH ALERT'}</Text>
                </ImageBackground>
            </TouchableOpacity>
            {/*<TouchableOpacity style={[notificationStyle.pushAlert, {marginVertical :0, backgroundColor:'red'}]} onPress = {()=>this.deleteEvent(item.id)}>*/}
            {/*<Text style={{color:'white'}}>{'DELETE'}</Text>*/}
            {/*</TouchableOpacity>*/}
        </View>)
    }



    /***
     * Render function
     ***/
    render() {

        return (<View style={[notificationStyle.wrapper]}>
            <KeyboardAwareScrollView>
                <View style={[notificationStyle.innerWrapper]}>
                    <View style={notificationStyle.locationContainer}>
                        <Text style={[CommonStylesheet.formTitle, { color: customOrange }]}>{`TONIGHT EVENTS(${this.state.toNightEvents.length})`}</Text>
                        {this.state.toNightEvents.map(item => {
                            return this.renderItem(item)
                        })}
                    </View>
                    <View style={notificationStyle.locationContainer}>
                        <Text style={[CommonStylesheet.formTitle, { color: customOrange }]}>{`UP COMING EVENTS(${this.state.upComingEvents.length})`}</Text>
                        {this.state.upComingEvents.map(item => {
                            return this.renderItem(item)
                        })}
                    </View>

                    <TouchableOpacity style={[notificationStyle.pushAlert, notificationStyle.addMoreButtonView]}
                        onPress={this.addMoreEvent}>
                        <Text style={[VenueNotifications.addMoreText,{color:customOrange}]}>{'ADD MORE EVENTS'}</Text>
                        <View style={notificationStyle.addMoreIcon}>
                            <Image source={Images.follow} style={notificationStyle.addMOreIconImage} />
                        </View>
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>

            {this.props.isLoading && <BTLoader />}

            <DateTimePicker
                minimumDate={new Date()}
                mode={this.state.pickerMode}
                isVisible={this.state.isDateTimePickerVisible}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDateTimePicker}
            />

            <Modal
                style={[notificationStyle.modal]}
                ref={"modal1"}
                swipeToClose={false}
                position="top"
                onClosingState={this.onClosingState}>
                {this.props.isLoading && <BTLoader />}
                <View style={[notificationStyle.wrapper]}>
                    <KeyboardAwareScrollView>
                        <View style={notificationStyle.innerWrapper}>
                            <View style={[notificationStyle.locationContainer, notificationStyle.modelInnerView]}>
                                <View style={notificationStyle.modelTitleView}>
                                    <Text style={[CommonStylesheet.titles, { fontSize: 20 }]}>{"Add New Event"}</Text>
                                </View>
                                <View style={notificationStyle.mapView}>
                                    <TouchableOpacity onPress={this.captureOrTakeImage}>
                                        <Image source={this.state.currentImage} style={notificationStyle.modelImage} resizeMode={'cover'} />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={this.captureOrTakeImage} style={[notificationStyle.pushAlert, notificationStyle.uploadEventCoverView]}>
                                        <Text style={notificationStyle.uploadEventCoverText}>{'Upload event cover'}</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={[notificationStyle.LocationInfoContainer]}>
                                    <View style={notificationStyle.lcoationInfo}>
                                        <Text style={CommonStylesheet.formTitle}>ARTIST / BAND:</Text>
                                        <View
                                            style={notificationStyle.gradientBgStyle}>
                                            <TextInput placeholderTextColor="white"
                                                style={styles.textInputCustomStyle}
                                                onChangeText={this.editYourName}
                                                underlineColorAndroid='rgba(0,0,0,0)'
                                                value={this.state.events.artist} />
                                        </View>
                                    </View>
                                    <View style={notificationStyle.lcoationInfo}>
                                        <Text style={CommonStylesheet.formTitle}>DATE:</Text>
                                        <View
                                            style={notificationStyle.gradientBgStyle}>
                                            <TouchableOpacity onPress={() => this._showDateTimePicker('date')}
                                                style={notificationStyle.dateTimePickerView}>
                                                <Text style={styles.textInputCustomStyle}>{this.state.events.date}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={notificationStyle.lcoationInfo}>
                                        <Text style={CommonStylesheet.formTitle}>TIME:</Text>

                                        <View
                                            style={notificationStyle.gradientBgStyle}>
                                            <TouchableOpacity onPress={() => this._showDateTimePicker('time')}
                                                style={notificationStyle.dateTimePickerView}>
                                                <Text placeholderTextColor="white"
                                                    style={styles.textInputCustomStyle}>{this.state.events.time}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={notificationStyle.lcoationInfo}>
                                        <Text style={CommonStylesheet.formTitle}>TICKET PORTAL LINK:</Text>
                                        <View
                                            style={notificationStyle.gradientBgStyle}>
                                            <TextInput
                                                style={styles.textInputCustomStyle}
                                                placeholderTextColor="white"
                                                onChangeText={this.editTicketLink}
                                                underlineColorAndroid='rgba(0,0,0,0)'
                                                value={this.state.events.ticket_portal} />
                                        </View>
                                    </View>
                                </View>
                                <TouchableOpacity style={[notificationStyle.pushAlert, notificationStyle.saveButtonView]} onPress={this.uploadImageOfEvent}>
                                    <Text style={notificationStyle.buttonText}>{'SAVE'}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={this.closeModel}
                                    style={[notificationStyle.pushAlert, { backgroundColor: '#240900' }]}>
                                    <Text style={notificationStyle.buttonText}>{'CLOSE'}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </View>
            </Modal>
        </View>
        )
    }
}


const notificationStyle = StyleSheet.create({

    imageStyle: {
        width: null,
        height: width / 3
    },

    dateUpperView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: width - 20
    },

    dateBg: {
        width: (width - 30) / 2
    },

    pushAlertBg: {
        width: width - 20,
        height: 40,
        paddingHorizontal: 5,
        justifyContent: 'center'
    },

    pushAlertText: {
        color: 'white',
        textAlign: 'center',
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
    },

    addMoreButtonView: {
        marginVertical: 5,
        backgroundColor: 'rgba(23,23,23,1)',
        height: 40
    },

    addMoreText: {
        color: customOrange,
        textAlign: 'center',
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
    },

    addMoreIcon: {
        position: 'absolute',
        top: 0, right: 0
    },

    addMOreIconImage: {
        height: 40,
        width: 40
    },

    modelTitleView: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },

    modelInnerView: {
        borderBottomColor: 'transparent',
        justifyContent: 'center'
    },

    modelImage: {
        width: null,
        height: width / 2.2
    },

    uploadEventCoverView: {
        marginVertical: 10,
        backgroundColor: '#240900'
    },

    uploadEventCoverText: {
        color: '#DA2F00',
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
    },

    dateTimePickerView: {
        flex: 1,
        justifyContent: 'center'
    },

    saveButtonView: {
        marginVertical: 15,
        backgroundColor: '#240900'
    },

    buttonText: {
        color: '#DA2F00',
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
    },

    wrapper: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center'

        //alignSelf: 'stretch'
    },
    innerWrapper: {
        //backgroundColor:'green'
        width: width - 20,
        marginHorizontal: 10,
        paddingVertical: 20,
    },

    pushAlert: {
        backgroundColor: 'transparent',
        width: width - 20,
        padding: 10,
        alignItems: 'center',
    },

    locationContainer: {
        flex: 1,
        marginTop: 10,
    },

    mapView: {
        marginTop: 10,
    },

    LocationInfoContainer: {
        marginTop: 10
    },

    lcoationInfo: {
        marginTop: 15,
    },

    gradientBgStyle: {
        marginTop: 2,
        borderWidth: 1,
        borderColor: 'rgba(39,39,39,1)',
        width: width - 20,
        paddingHorizontal: 15,
        marginLeft: 0,
        backgroundColor: 'rgba(23,23,23,1)'
    },

    modal: {
        flex: 1,
        bottom: 0,
        paddingBottom: 10,
        backgroundColor: 'black'
    },

});

export default withConnect(VenueNotifications);