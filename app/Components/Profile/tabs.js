import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Animated,
  ImageBackground,
  Text
} from "react-native";
import { TabView, TabBar, SceneMap } from "react-native-tab-view";
import Notifications from "./Notification/Notifications";
import Accounts from "./Account/Accounts";
import NavigationBar from "../Reusables/CustomNavBar";
import { Images } from "../../constant/images";
import ProfileTab from "./EditProfile/Profile";
import { fontFamilies } from "../../constant/appConstant";

export default class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: "profile", title: "PROFILE" },
        { key: "notification", title: "NOTIFICATION" },
        { key: "account", title: "ACCOUNT" }
      ]
    };
  }

  _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          return (
            <ImageBackground
              key={`index_${i}`}
              source={
                this.state.index === i
                  ? Images.tabBarImage
                  : Images.tabGredientBg
              }
              style={[styles.tabBackground]}
            >
              <TouchableOpacity
                style={[styles.tabButtons]}
                onPress={() => this.setState({ index: i })}
              >
                <Text
                  style={{
                    color: "white",
                    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
                    fontSize: 12
                  }}
                >
                  {route.title}
                </Text>
              </TouchableOpacity>
            </ImageBackground>
          );
        })}
      </View>
    );
  };

  render() {
    return (
      <View style={{ width: Dimensions.get("window").width, flex: 1 }}>
        <NavigationBar
          title={"SETTINGS"}
          onSearch={this.onSearch}
          back={true}
          sceneKey={"Profile"}
        />
        <TabView
          navigationState={this.state}
          renderScene={SceneMap({
            profile: ProfileTab,
            notification: Notifications,
            account: Accounts
          })}
          swipeEnabled={false}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{ width: Dimensions.get("window").width, height: 500 }}
          indicatorStyle={{ backgroudColor: "black" }}
          renderTabBar={this._renderTabBar}
          style={{ flex: 10 }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  tabBar: {
    height: 50,
    flexDirection: "row",
    justifyContent: "space-between",
    width: Dimensions.get("window").width,
    backgroundColor: "#000000",

    borderBottomColor: "rgba(48,40,50,1)",
    borderBottomWidth: 1,
    borderTopColor: "rgba(48,40,50,1)",
    borderTopWidth: 1,
    padding: 3
  },
  tabItem: {
    flex: 1,
    alignItems: "center",
    padding: 26
  },
  tabBackground: {
    alignItems: "center",
    width: "33%"
  },
  tabButtons: {
    //width: '33%',
    height: 48,
    // backgroundColor: 'green',
    alignItems: "center",
    justifyContent: "center"
    //paddingTop:7,
    //color:'white'
  },
  tabBorder: {
    borderColor: "#483549",
    borderWidth: 1
  }
});
