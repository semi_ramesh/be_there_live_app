/**
 * @ProvideModules  Global_Profile
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  Platform,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
  ScrollView
} from "react-native";

import styles from "./styles"
import NavigationBar from "app/Components/Reusables/CustomNavBar";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome";
import {
  BTLoader,
  getExtensionFromMimeType,
  PlaceHolderComponent,
  fontFamilies
} from "../../constant/appConstant";

import { Images } from "../../constant/images";
import Artist from "../Artist/Artist";
import GridView from "react-native-super-grid";
import { LoginUser } from "../Subscription/SubscriptionsData";
import StripePaymentGatway from 'app/Components/Reusables/containers/StripePaymentGatway'
const { width } = Dimensions.get("window");
import withConnect from "./withConnectComponent";
import BTLImagePickerController from "app/utils/imagePicker";
import FitImage from "react-native-fit-image";
import NoDataComponent from "app/Components/Reusables/noData";

import _ from "lodash";


class Profile extends React.PureComponent {
  static defaultProps = {
    userData: null,
    myProfile: {}
  };

  constructor(props) {
    super(props);

    this.state = {
      myProfileData: undefined,
      profileImage: Images.placeholder,
      isReady: false,
      userDetail: {
        userName: "",
        designation: "",
        instaAccount: "",
        twitterAccount: "goombah_11_gal"
      },
      userType: 2,
      //image for all selected Section
      artistImage: Images.tabBarImage,
      venueImage: Images.tabGredientBg,
      followersImage: Images.tabGredientBg,
      followingImage: Images.tabGredientBg,
      records: {
        artist: 26,
        bands: 23,
        venue: 35,
        followers: 34,
        following: 20
      },
      dummyData: {},
      connections: [],
      isRefreshing: true,
      followOrunFollow: false,
      currentTab: "A",
      stopLoading: false
    };
    this.getProfile = this.props.getProfile.bind(this);
    this.followUnFollow = this.props.followUnFollow.bind(this);
    this.uploadProfilePicture = this.props.uploadProfilePicture.bind(this);
  }

  /**
   * This method will use to update status or follow or unfollow users
   * @param item
   * @returns {Promise<void>}
   */
  updateListSelectedNow = item => {
    this.setState({
      followOrunFollow: true
    });
    let follwersStatus = 0;
    if (!item.follow_details) {
      follwersStatus = 1;
    }

    if (item.follow_details) {
      follwersStatus = item.follow_details.follow_status == 1 ? 0 : 1;
    }
    let formData = new FormData();
    formData.append("following_id", item.id);
    formData.append("follower_status", follwersStatus);
    formData.append("user_type_id", this.props.userData.user_type_id);
    let dataKey = "genereList";
    this.followUnFollow({ formData, dataKey });
  };

  /**
   * Move To Profile
   */
  moveToSelectedUsersProfile = item => {
    this.setState({
      stopLoading: true
    });
    //artistId
    switch (item.user_type_id) {
      case 4:
        Actions.VENUEDETAILS({ venueId: item.id, popScene: "Profile" });
        return;

      case 3:
        Actions.Artist({ artistId: item.id, popScene: "Profile" });
        return;

      default:
        return;
    }
  };

  componentDidMount() {
    this.setState({
      isRefreshing: true
    });
    this.getIndividiualProfile().then(is => {});
    if (this.props.userData) {
      LoginUser.changeType(this.props.userData.user_type_id);
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      stopLoading: false
    });

    if (this.props.myProfile !== nextProps.myProfile && nextProps.myProfile) {
      this.setState({
        myProfileData: nextProps.myProfile.data,
        isReady: true,
        connections:
          this.state.myProfileData !== undefined
            ? this.state.myProfileData.users_artist
            : this.state.connections,
        artistImage: Images.tabBarImage,
        venueImage: Images.tabGredientBg,
        followersImage: Images.tabGredientBg,
        followingImage: Images.tabGredientBg,
        isRefreshing: false,
        followOrunFollow: false,
        profileImage:
          nextProps.myProfile.data &&
          nextProps.myProfile.data.userDetail &&
          nextProps.myProfile.data.userDetail.user_profile_image
            ? {
                uri:
                  nextProps.myProfile.data.userDetail.user_profile_image
                    .image_path
              }
            : this.state.profileImage
      });
      console.log("myProfileData", this.state.myProfileData);

      if (this.state.myProfileData !== undefined) {
        this.setTheConnectionAfterUpdatedProps();
      }

      if (
        nextProps.profileImage &&
        this.props.profileImage !== nextProps.profileImage
      ) {
        this.setState({
          followOrunFollow: false,
          profileImage: nextProps.profileImage.data.user_profile_image
            ? { uri: nextProps.profileImage.data.user_profile_image.image_path }
            : Images.placeholder
        });
      }
    }
    if (
      this.props.genereList !== nextProps.genereList &&
      nextProps.genereList
    ) {
      this.getUserProfileAfterFollowUnfollowAction();
    }
  }

  getUserProfileAfterFollowUnfollowAction = async () => {
    await this.getProfile();
  };

  setTheConnectionAfterUpdatedProps = () => {
    if (this.state.currentTab === "A") {
      this.onSelectArtist();
    }
    if (this.state.currentTab === "B") {
      this.onSelectVenue();
    }
    if (this.state.currentTab === "C") {
      this.onSelectFollowings();
    }
    if (this.state.currentTab === "D") {
      this.onSelectFollowers();
    }
  };

  takePickAndUploadImage = async () => {
    try {
      await BTLImagePickerController.pickImage()
        .then(response => {
          this.setState({
            profileImage: { uri: response.uri },
            followOrunFollow: true
          });
          this.setState({
            isLoading: true
          });
          let timeStamp = Math.floor(Date.now() / 1000);
          let infoDict = getExtensionFromMimeType(response.url);
          let formData = new FormData();
          formData.append("picture", {
            uri: response.uri,
            name: `${timeStamp}${infoDict.extension}`,
            data: response.data,
            filename: `${timeStamp}${infoDict.extension}`,
            type: `${infoDict.mime}`
          });
          this.uploadProfilePicture(formData);
        })
        .then(error => {
          // alert(error)
        });
    } catch (e) {}
  };

  getIndividiualProfile = async () => {
    await this.getProfile();
    this.setState({
      isReady: true
    });
    //return callback;
  };

  /**
   * Navigate Between UserRole interactions of current user.
   */
  onSelectArtist = () => {
    if (
      this.state.myProfileData.users_artist &&
      _.isArray(this.state.myProfileData.users_artist)
    )
      {this.setState({
             currentTab:'A',
             connections:this.state.myProfileData.users_artist,
             artistImage:Images.tabBarImage,
             venueImage:Images.tabGredientBg,
             followersImage:Images.tabGredientBg,
             followingImage:Images.tabGredientBg,
           });}
  };

  onSelectVenue = () => {
    if (
      this.state.myProfileData.users_venus &&
      _.isArray(this.state.myProfileData.users_venus)
    )
      {this.setState({
            currentTab:'B',
            connections:this.state.myProfileData.users_venus,
            artistImage:Images.tabGredientBg,
            venueImage:Images.tabBarImage,
            followersImage:Images.tabGredientBg,
            followingImage:Images.tabGredientBg,
        });}
  };

  onSelectFollowers = () => {
    if (
      this.state.myProfileData.users_follower &&
      _.isArray(this.state.myProfileData.users_follower)
    )
      {this.setState({
            currentTab:'D',
            connections: this.state.myProfileData.users_follower,
            artistImage:Images.tabGredientBg,
            venueImage:Images.tabGredientBg,
            followersImage:Images.tabBarImage,
            followingImage:Images.tabGredientBg,
        });}
  };

  onSelectFollowings = () => {
    if (
      this.state.myProfileData.users_following &&
      _.isArray(this.state.myProfileData.users_following)
    )
      {this.setState({
            currentTab:'C',
            connections:this.state.myProfileData.users_following,
            artistImage:Images.tabGredientBg,
            venueImage:Images.tabGredientBg,
            followersImage:Images.tabGredientBg,
            followingImage:Images.tabBarImage,
        });}
  };

  addConnections = () => {
    if (this.state.currentTab === "A") {
      Actions.GENRELIST();
    } else if (this.state.currentTab === "B") {
      Actions.Venue({ duration: 0 });
    } else {
      Actions.Search({ duration: 0 });
    }
  };

  onEditProfile = () => {
    if (Actions.currentScene !== "EDIT_PROFILE")
      this.setState({
        stopLoading: true
      });
    Actions.EDITPROFILE({ duration: 0 });
  };

  renderItem = item => {
    // if(item.id === "last"){
    //     return(<TouchableOpacity style = {{backgroundColor:'black', alignItems:'center'}} onPress = {this.addConnections} >
    //         <FitImage indicatorColor={'white'} style = {{height:width/3.5,width:width/3.5}} source = {Images.addNew}/>
    //         <Text style = {{color:'white'}}> </Text>
    //     </TouchableOpacity>)
    // }

    const userType =
      item.user_type_id === 2
        ? "Basic User"
        : item.user_type_id === 3
        ? "Artist"
        : "Venue";
    let image =
      item.follow_details && item.follow_details.follow_status === 1
        ? Images.unfollow
        : Images.follow;
    const bouncStyle =
      item.follow_details && item.follow_details.follow_status === 1
        ? styles.followedStyle
        : styles.followPlus;
    let uri = item.user_profile_image
      ? { uri: item.user_profile_image.image_path }
      : Images.placeholder;

    return (
      <TouchableOpacity
        onPress={() => {
          this.moveToSelectedUsersProfile(item);
        }}
        style={{
          width: width / 3.5,
          backgroundColor: "black",
          alignItems: "center"
        }}
      >
        <ImageBackground
          style={{ height: width / 3.5, width: width / 3.5 }}
          source={uri}
        >
          <TouchableOpacity
            style={[bouncStyle, { right: 0 }]}
            onPress={() => this.updateListSelectedNow(item)}
          >
            <Image style={{ height: 45, width: 45 }} source={image} />
          </TouchableOpacity>
        </ImageBackground>
        <Text
          numberOfLines={1}
          style={{
            marginTop: 5,
            fontSize: 10,
            color: "white",
            fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
          }}
        >
          {item.name.toLocaleUpperCase()}
        </Text>
      </TouchableOpacity>
    );
  };

  renderPlaceHolder = () => {
    return (
      <View style={styles.placeHolderView}>
        <NavigationBar title={"PROFILE"} />
        <PlaceHolderComponent />
      </View>
    );
  };

  onLayoutGrid = () => {
    this.setTheConnectionAfterUpdatedProps();
  };

  render() {
    const { records, myProfileData } = this.state;
    const { myProfile } = this.props;
    const noDataFound =
      this.state.connections && this.state.connections.length === 0;
    if (
      this.props.isLoading &&
      !this.state.followOrunFollow &&
      !this.state.isLoading
    ) {
      return (
        <View style={styles.topView}>
          <BTLoader />
        </View>
      );
    }
    if (myProfileData === undefined) {
      return <View style={styles.topView} />;
    }

    const {
      userDetail,
      users_artist_count: artistCount,
      users_follower_count: followersCount,
      users_following_count: followingCount,
      users_venus_count: venusCount
    } = myProfileData;
    if (!userDetail) {
      return (
        <NoDataComponent
          text={"Something went wrong please try again."}
          reloadData={this.getIndividiualProfile}
          isLoading={this.props.isLoading}
          title={"PROFILE"}
        />
      );
    }
    return (
      <View style={styles.topView}>
        <View style={styles.parallaxHeader}>
          <NavigationBar title={"PROFILE"} />
          <View style={styles.profileInfoContainer}>
            <TouchableOpacity
              style={styles.userProfieImageContainer}
              onPress={this.takePickAndUploadImage}
            >
              <ImageBackground
                source={this.state.profileImage}
                style={styles.imgStyle}
              >
                <TouchableOpacity
                  style={styles.pencil}
                  onPress={this.takePickAndUploadImage}
                >
                  <Icon name={"pencil"} size={21} color={"white"} />
                </TouchableOpacity>
              </ImageBackground>
            </TouchableOpacity>
            <View style={styles.textDetail}>
              <View style={styles.nameDetail}>
                <View style={styles.nameAlignView}>
                  <View>
                    <Text style={styles.boldAndBig}>{userDetail.name}</Text>
                    {userDetail.designation && (
                      <Text
                        style={[
                          styles.normal,
                          {
                            fontFamily:
                              fontFamilies.ItcAvantGardeGothicBookRegular
                          }
                        ]}
                      >
                        {userDetail.designation}
                      </Text>
                    )}
                  </View>
                  <TouchableOpacity
                    onPress={this.onEditProfile}
                    style={styles.cog}
                  >
                    <Icon
                      name="cog"
                      size={18}
                      color={"white"}
                      style={[styles.settingIcon, { top: 0 }]}
                    />
                  </TouchableOpacity>
                </View>

                <View style={styles.socialInfoContainer}>
                  {userDetail.instagram && (
                    <View style={[styles.socialInfo, { marginTop: 5 }]}>
                      <Image source={Images.insta} style={styles.socialIcon} />
                      <Text style={styles.socialText}>
                        {userDetail.instagram}
                      </Text>
                    </View>
                  )}
                  {userDetail.twitter && (
                    <View style={styles.socialInfo}>
                      <Image
                        source={Images.twitter}
                        style={styles.socialIcon}
                      />
                      <Text style={styles.socialText}>
                        {userDetail.twitter}
                      </Text>
                    </View>
                  )}
                  <TouchableOpacity
                    style={styles.editButton}
                    onPress={this.onEditProfile}
                  >
                    <ImageBackground
                      style={styles.editButtonBg}
                      source={Images.editProfileBg}
                    >
                      <Text style={styles.normal2}>{"EDIT PROFILE"}</Text>
                    </ImageBackground>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.countPlate}>
            <TouchableOpacity
              style={styles.touchPlates}
              onPress={this.onSelectArtist}
            >
              <ImageBackground
                style={styles.plates}
                source={this.state.artistImage}
              >
                <Text style={styles.boldAndBig2}>{artistCount}</Text>
                <Text style={styles.normal1}>{"ARTIST"}</Text>
              </ImageBackground>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.touchPlates}
              onPress={this.onSelectVenue}
            >
              <ImageBackground
                style={styles.plates}
                source={this.state.venueImage}
              >
                <Text style={styles.boldAndBig2}>{venusCount}</Text>
                <Text style={styles.normal1}>{"VENUE"}</Text>
              </ImageBackground>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.touchPlates}
              onPress={this.onSelectFollowings}
            >
              <ImageBackground
                style={styles.plates}
                source={this.state.followingImage}
              >
                <Text style={styles.boldAndBig2}>{followingCount}</Text>
                <Text style={styles.normal1}>{"FOLLOWING"}</Text>
              </ImageBackground>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.touchPlates}
              onPress={this.onSelectFollowers}
            >
              <ImageBackground
                style={styles.plates}
                source={this.state.followersImage}
              >
                <Text style={styles.boldAndBig2}>{followersCount}</Text>
                <Text style={styles.normal1}>{"FOLLOWERS"}</Text>
              </ImageBackground>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.gridView}>
          <GridView
            itemDimension={width / 3.5}
            items={this.state.connections}
            renderItem={this.renderItem}
            spacing={10}
            extraData={this.state}
            onLayout={this.onLayoutGrid}
          />
        </View>

        {this.props.isLoading && <BTLoader />}
      </View>
    );
  }
}

export default withConnect(Profile);