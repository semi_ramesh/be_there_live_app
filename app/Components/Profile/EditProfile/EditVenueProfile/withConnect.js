/** Make Connection redux thinngs **/

import React from 'react';

import {connect} from 'react-redux';
import {auth,serviceApi,shared} from 'app/redux/actions';

function  mapStateToProps(state) {

    const {user:userData} = state.auth;
    const {isLoading} = state.shared;
    const {
    userProfile,
    ProfileData,
    profileImage,
    crouselImages} = state.serviceApi


    return{
        userData,
        isLoading,
        userProfile,
        ProfileData,
        profileImage,
        crouselImages

    }

}


function  mapDispatchToProps(dispatch) {

    return{
         removeCrouselImage:(id)=>dispatch(serviceApi.removeCrouselImage(id)),
         uploadProfilePicture:(formData)=>dispatch(serviceApi.uploadProfilePicture(formData)),
         getProfileForEdit :(formData)=>dispatch(serviceApi.getProfileForEdit(formData)),
         updateVenueProfileProfile :(formData) =>dispatch(serviceApi.updateProfile(formData)),
         uploadCrouselImages:(formData)=>dispatch(serviceApi.uploadCrouselImages(formData)),
 }

}

export default function withConnect(wrapperComponent) {
    return connect(mapStateToProps,mapDispatchToProps)(wrapperComponent)
}