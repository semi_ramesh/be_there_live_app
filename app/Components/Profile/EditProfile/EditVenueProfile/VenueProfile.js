import React ,{Component} from 'react'
import{
    View,
    Text,
    Image,
    ImageBackground,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
    TextInput,Alert,
    Platform
} from 'react-native';

import { PermissionsAndroid } from 'react-native';

import styles from '../styles'
import {Images} from "../../../../constant/images";
import GridView from 'react-native-super-grid';
import {Button} from "react-native-elements";
import {customOrange, customPurple} from "../../../Authentication/styles";
import Icon from 'react-native-vector-icons/FontAwesome';
const {width,height} = Dimensions.get('window');
import CommonStylesheet from "../../../../utils/CommonStylesheet";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import MapView,{Marker} from 'react-native-maps';
import withConnect from './withConnect';
import  _ from 'lodash'
import {BTLoader, getExtensionFromMimeType, showInfoMessage,fontFamilies,GOOGLE_API_KEY} from "../../../../constant/appConstant";
import BTLImagePickerController from "../../../../utils/imagePicker";
import Geocoder from 'react-native-geocoding';


class VenueProfile extends Component{
    constructor(props) {
        super(props);
        this.state = {
            isFirst: true,
            profileImage: Images.placeholder,
            locationGtanted: false,
            userDetail: {
                name: 'Kim',
                last_name: 'Daily',
                user_name: 'PawGirl22',
                instagram: 'goombah11',
                twitter: 'goombah_11_gal',
                mobile: '(323)5556291',
                email: 'goombah11@gmail.com',
                city: 'Los Angeles',
                zipcode: '90018',
                address: 'address.',
                phone_no: '(213)-318-1800',
                about_me: 'Bio',
                latitude: 0,
                longitude: 0
            },
            islocate: false,
            t_address: '',
            t_city: '',
            t_zip: '',
            t_lat: 0,
            t_long: 0,
            venuesImages: [{ id: 'last', image: Images.addNew }],
            test: false,
            latitude: 0,
            longitude: 0,
            region: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
        };

        this.getProfileForEdit = this.props.getProfileForEdit.bind(this);
        this.uploadProfilePicture = this.props.uploadProfilePicture.bind(this);
        this.updateVenueProfileProfile = this.props.updateVenueProfileProfile.bind(this);
        this.uploadCrouselImages = this.props.uploadCrouselImages.bind(this);
        this.removeCrouselImage = this.props.removeCrouselImage.bind(this);

    }

    requestLocationPermission = async () => {

        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'BTL App Location Permission',
                    'message': 'BTL App needs access to your location '
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.setState({
                    locationGtanted: true
                });
                this.locateUser();
            }

        } catch (err) {
        }
    }

    locateUser = () => {

        if (Platform.OS === "android") {
            if (!this.state.locationGtanted) {
                return;
            }
        }
        this.setState({
            islocate: true
        })
        this.watchID = navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    t_lat: Number(position.coords.latitude),
                    t_long: Number(position.coords.longitude),
                    region: {
                        latitude: Number(position.coords.latitude),
                        longitude: Number(position.coords.longitude),
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    },
                    error: null,
                });
                this.getLocationName()
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 },
        );
    };

    componentDidUnMount() {
        navigator.geolocation.clearWatch(this.watchID);
    }


    captureOrTakeandUploadImage = async () => {
        try {
            await BTLImagePickerController.pickImage().then(response => {

                this.setState({
                    profileImage: response,

                });

                let timeStamp = Math.floor(Date.now() / 1000);
                let infoDict = getExtensionFromMimeType(response.url)
                let formData = new FormData();

                formData.append('picture', {
                    uri: response.uri,
                    name: `${timeStamp}.${infoDict.extension}`,
                    filename: `${timeStamp}.${infoDict.extension}`,
                    type: `${infoDict.mime}`
                });

                this.uploadProfilePicture(formData);

            }).then(error => {
            });
        }
        catch (e) {
        }
    };

    /**
     * Remove image from crousel
     */

     conformRemoveImage = (item)=>{

        Alert.alert(
              'Are you sure to remove crousel image ?',
              '',
              [
                {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Yes', onPress: ()=> this.removeImage(item)},
              ],
              { cancelable: false }
        )
    }

    removeImage = (item) => {
        this.removeCrouselImage(item.id);
    };


    /**
     * upload CrouselImages
     * @returns {*}
     */
    uploadCrouselImagesToServer = async () => {
        try {
            await BTLImagePickerController.pickImage().then(response => {

                let timeStamp = Math.floor(Date.now() / 1000);
                let infoDict = getExtensionFromMimeType(response.url);
                let formData = new FormData();

                formData.append('image', {
                    uri: response.uri,
                    name: `${timeStamp}.${infoDict.extension}`,
                    filename: `${timeStamp}.${infoDict.extension}`,
                    type: `${infoDict.mime}`
                });
                this.uploadCrouselImages(formData)

            })
            .then(error => {
            });

        }
        catch (e) {
        }
    };

    componentWillReceiveProps(nextProps) {

        if (nextProps.userProfile) {
            const { data } = nextProps.userProfile;
            if (data) {
                const { userDetail } = data;
                if (userDetail) {

                    this.setState({
                        isFirst: false,
                        userDetail: userDetail,
                        latitude: (userDetail.latitude !== null) ? userDetail.latitude : this.state.latitude,
                        longitude: (userDetail.longitude !== null) ? userDetail.longitude : this.state.longitude,
                        t_address: (userDetail.address !== null) ? userDetail.address : this.state.t_address,
                        t_city: (userDetail.city !== null) ? userDetail.city : this.state.t_city,
                        t_zip: (userDetail.zipcode !== null) ? userDetail.zipcode : this.state.t_zip,
                        profileImage: userDetail.user_profile_image ? { uri: userDetail.user_profile_image.image_path } : Images.placeholder,
                    });

                    const { user_carousel_images } = userDetail;
                    if (user_carousel_images) {
                        user_carousel_images.forEach(item => {
                            if (item.id === "last") {
                                user_carousel_images.pop();
                            }
                        });
                        user_carousel_images.push({ id: 'last', image: Images.addNew });
                        this.setState({
                            venuesImages: user_carousel_images
                        });
                    }
                }
            }
            if (nextProps.profileImage) {
                this.setState({
                    profileImage: nextProps.profileImage ? { uri: nextProps.profileImage.data.user_profile_image.image_path } : this.state.profileImage
                })
            }
        }

        if (nextProps.ProfileData) {
            const { venue_details } = nextProps.ProfileData.data;
            this.setState({
                userDetail: venue_details,
                t_lat: (venue_details.latitude !== null) ? venue_details.latitude : this.state.t_lat,
                t_long: (venue_details.longitude !== null) ? venue_details.longitude : this.state.t_long,
                t_address: (venue_details.address !== null) ? venue_details.address : this.state.t_address,
                t_city: (venue_details.city !== null) ? venue_details.city : this.state.t_city,
                t_zip: (venue_details.zipcode !== null) ? venue_details.zipcode : this.state.t_zip,
            })
        }

        if (nextProps.crouselImages) {
            const { user_carousel_images } = nextProps.crouselImages.data;
            user_carousel_images.forEach(item => {
                if (item.id === "last") {
                    user_carousel_images.pop();
                }
            });
            user_carousel_images.push({ id: 'last', image: Images.placeholder });
            this.setState({
                venuesImages: user_carousel_images,
            });
        }
    };

   componentDidMount(){

    if(Platform.OS==="android")
      {
              this.requestLocationPermission();

      }
          this.getProfileForEdit();
          this.locateUser();
    };


    getprofileEditForVenue =  async() =>{
        await this.getProfileForEdit();
    }

   onChangeVenueName = (text) =>{
        this.setState({
             userDetail:{...this.state.userDetail,
              name:text
             }

        })
   };

    onChangeVenueBio = (text) =>{
        const {userDetail } = this.state;
        this.setState({
           userDetail:{...this.state.userDetail,about_me:text}

        })
    };

    onChangeAddress = (text)=>{
        this.setState({
            userDetail:{...this.state.userDetail,
            address:text},
            t_address:text
        })
    };

    onChangeCityText = (text) =>{
        this.setState({
            userDetail:{...this.state.userDetail,
            city:text},
            t_city:text
        })
    };

    onChangeZipCode = (text) =>{
        this.setState({
           userDetail:{ ...this.state.userDetail,
            zipcode:text},
            t_zip:text,
        })
    };

    onChangeMobile = (text)=>{
        this.setState({
            userDetail:{
            ...this.state.userDetail,
            phone_no:text
            }
        })
    }

    onMapPress = (e) => {

        this.setState({
            latitude: e.nativeEvent.coordinate.latitude,
            longitude: e.nativeEvent.coordinate.longitude,
            islocate: false,
        })
        this.getLocationName();
    };


    updateYourGenrealDetail = () => {
        const {
            phone_no,
            name,
            about_me,
            address,
            city,
            zipcode
        } = this.state.userDetail;

        const { 
            t_address,
            t_city,
            t_zip,
            latitude,
            longitude
        } = this.state;

        const aLat = (latitude === 0 || latitude === null) ? this.state.t_lat : latitude
        const aLong = (longitude === 0 || longitude === null) ? this.state.t_long : longitude

        if (!this.validateForm()) {
            return
        }

        try {
            let formData = new FormData();
            formData.append('name', name);
            formData.append('phone_no', phone_no);
            formData.append('about_me', about_me);
            formData.append('latitude', aLat);
            formData.append('longitude', aLong);
            formData.append('address', t_address);
            formData.append('city', t_city);
            formData.append('zipcode', t_zip);

            this.updateVenueProfileProfile(formData);
        }
        catch (e) {
            showInfoMessage('Something went wrong , please try again.')
        }
    };

    validateForm = () =>{
        const {
            name,
            about_me,
         } = this.state.userDetail;

          const{t_address,
              t_city,
              t_zip} = this.state;


        if(_.isEmpty(name)){
            showInfoMessage('Please enter venue name.');
            return false
        }

        if(_.isEmpty(about_me)){
            showInfoMessage('Please enter some thing about your venue.')
            return false
        }

        if(_.isEmpty(t_address)){
            showInfoMessage('Please enter address of your venue.');
            return false
        }

        if(_.isEmpty(t_city)){
            showInfoMessage('Please enter your venue city.');
            return false
        }

        if(_.isEmpty(t_zip)){
            showInfoMessage('Please enter zip code.');
            return false
        }
        return true

    }


    /**
   *  Getting current Place name using Google Geo location
   * */
    getLocationName = async () => {

        if (this.state.t_lat === 0 || this.state.t_long === 0) {
            this.setState({
                t_lat: 74.009,
                t_long: 26.90
            })
        }

        const lat = (this.state.islocate) ? this.state.t_lat : this.state.latitude;
        const long = (this.state.islocate) ? this.state.t_long : this.state.longitude;

        Geocoder.init(GOOGLE_API_KEY);
        Geocoder.from(lat, long)
            .then(json => {

                let street = '';
                let route = '';
                let colony = '';
                let city = ''
                let zip = '';
                let address = '';

                json.results[0].address_components.map((item) => {

                    let types = item.types;
                    if (types.indexOf('street_number') != '-1') {
                        street = item.long_name
                    }
                    else if (types.indexOf('route') != '-1') {
                        route = item.long_name
                    }
                    else if (types.indexOf('sublocality') != '-1') {
                        colony = item.long_name
                    }
                    else if (types.indexOf('locality') != '-1') {
                        city = item.long_name
                    }

                    else if (types.indexOf('postal_code') != '-1') {
                        zip = item.long_name
                    }
                });

                route = (street) ? ',' + route : route;
                colony = (street || route) ? ',' + colony : colony;
                address = street + route + colony

                this.setState({

                    t_address: address,
                    t_city: city,
                    t_zip: zip
                })
            })
            .catch(error => console.log("GeoLocation error", error));
    }





    renderItem = (item) => {

        const itemSize = (width - 50) / 4;
        if (item.id === 'last') {
            return (<TouchableOpacity onPress={this.uploadCrouselImagesToServer}>
                <ImageBackground source={Images.addNew}
                    style={{
                        height: itemSize,
                        width: itemSize
                    }}
                />
            </TouchableOpacity>)
        }
        return (<ImageBackground source={{ uri: item.image_path }} style={{ height: itemSize, width: itemSize }} key={item.id}>
            <TouchableOpacity style={[styles.itemStyle]} onPress={() => this.conformRemoveImage(item)}>
                <Image style={{ height: itemSize * 0.4, width: itemSize * 0.4 }} source={Images.unfollow1} />
            </TouchableOpacity>
        </ImageBackground>
        )
    };


    render(){

         if(this.props.isLoading && this.state.isFirst){
          return (<View style={venueStyle.loderView}>
              <BTLoader/></View>)

        }
        
        return (
            <View style={venueStyle.superView}>
                <KeyboardAwareScrollView style={venueStyle.KeyboardAwareScrollView}>
                    <Text style={[CommonStylesheet.formTitle, venueStyle.uploadImageText]}>UPLOAD IMAGE:</Text>
                    <View style={venueStyle.parallaxHeader}>
                        <Image source={this.state.profileImage} style={venueStyle.imgStyle} />
                        <TouchableOpacity style={venueStyle.profileEditImage} onPress={this.captureOrTakeandUploadImage}>
                            <Image source={Images.profileEditButton} style={venueStyle.editImage} />
                        </TouchableOpacity>
                    </View>

                    <Text style={[CommonStylesheet.formTitle, venueStyle.crouselsText]}>CROUSELS:</Text>
                    <View style={{ width: width }}>
                        <GridView
                            itemDimension={(width - 50) / 4}
                            items={this.state.venuesImages}
                            renderItem={this.renderItem}
                            spacing={10} />
                    </View>
                    <View style={venueStyle.formInput}>
                        <Text style={[CommonStylesheet.formTitle, { marginBottom: 5 }]}>VENUE NAME:</Text>
                        <View style={venueStyle.gradientBgStyle}>
                            <TextInput placeholderTextColor="white"
                                style={styles.textInputCustomStyle}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                onChangeText={this.onChangeVenueName}
                                value={this.state.userDetail.name} />
                        </View>
                    </View>
                    <View style={venueStyle.formInput}>
                        <Text style={[CommonStylesheet.formTitle, { marginBottom: 5 }]}>VENUE BIO:</Text>
                        <View style={venueStyle.inputHolder}>
                            <TextInput placeholderTextColor="white"
                                multiline={true}
                                style={styles.textInputCustomStyle}
                                numberOfLines={90}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                onChangeText={this.onChangeVenueBio}
                                value={this.state.userDetail.about_me} />
                        </View>
                    </View>

                    <View style={venueStyle.formInput}>
                        <Text style={[CommonStylesheet.formTitle, { marginBottom: 5 }]}>{"LOCATION:"}</Text>
                        <View style={wrapperStyle.map}>
                            <MapView style={StyleSheet.absoluteFillObject}
                                showsUserLocation={true}
                                onPress={this.onMapPress}
                                initialRegion={this.state.region}
                                region={this.state.region}>
                                {this.state.latitude !== null && <Marker
                                    coordinate={{
                                        latitude: this.state.latitude,
                                        longitude: this.state.longitude
                                    }}
                                    title={this.state.t_city}
                                    description={'My venue location'}
                                />}
                            </MapView>
                            <View style={venueStyle.location}>
                                <TouchableOpacity title={'Location'}
                                    style={wrapperStyle.locationStyle}
                                    onPress={this.locateUser}>
                                    <Icon size={50} name={'user'} color={customOrange} />
                                </TouchableOpacity>
                            </View>
                        </View></View>


                    <View style={venueStyle.formInput}>
                        <Text style={[CommonStylesheet.formTitle, { marginBottom: 5 }]}>ADDRESS:</Text>
                        <View style={venueStyle.gradientBgStyle}>
                            <TextInput placeholderTextColor="white"
                                style={styles.textInputCustomStyle}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                onChangeText={this.onChangeAddress}
                                value={this.state.t_address} />
                        </View>
                    </View>
                    <View style={venueStyle.formInput}>
                        <Text style={[CommonStylesheet.formTitle, { marginBottom: 5 }]}>CITY:</Text>
                        <View style={venueStyle.gradientBgStyle}>
                            <TextInput placeholderTextColor="white"
                                onChangeText={this.onChangeCityText}
                                style={styles.textInputCustomStyle}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                value={this.state.t_city} />
                        </View>
                    </View>
                    <View style={venueStyle.formInput}>
                        <Text style={[CommonStylesheet.formTitle, { marginBottom: 5 }]}>ZIPCODE:</Text>
                        <View
                            style={venueStyle.gradientBgStyle}>
                            <TextInput style={styles.textInputCustomStyle}
                                onChangeText={this.onChangeZipCode}
                                keyBoardType={'numeric'}
                                placeholderTextColor="white"
                                underlineColorAndroid='rgba(0,0,0,0)'
                                value={this.state.t_zip} />
                        </View>
                    </View>
                    <View style={venueStyle.formInput}>
                        <Text style={[CommonStylesheet.formTitle, { marginBottom: 5 }]}>PHONE:</Text>
                        <View
                            style={venueStyle.gradientBgStyle}>
                            <TextInput placeholderTextColor="white"
                                style={styles.textInputCustomStyle}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                keyBoardType={'numeric'}
                                onChangeText={this.onChangeMobile}
                                value={this.state.userDetail.phone_no} />
                        </View>
                    </View>
                    <View style={[venueStyle.formInput, { alignItems: 'center' }]}>
                        <Button
                            title={'Save'}
                            onPress={this.updateYourGenrealDetail}
                            buttonStyle={venueStyle.saveBtnStyle} />
                    </View>
                </KeyboardAwareScrollView>
                {this.props.isLoading && <BTLoader />}
            </View>
        )
    }
}

const wrapperStyle = {
    
    map: {
        marginTop: 10,
        padding: 5,
        width: width - 20,
        marginBottom: 10,
        height: 200,
    },

    locationStyle: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        width: 50
    }
};

const venueStyle = StyleSheet.create({

    loderView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center'
    },

    superView: {
        flex: 1,
        justifyContent: 'center'
    },

    KeyboardAwareScrollView: {
        backgroundColor: 'black',
        marginBottom: 20
    },

    uploadImageText: {
        marginTop: 15,
        marginLeft: 10
    },

    profileEditImage: {
        position: 'absolute',
        bottom: 5,
        right: 15
    },

    editImage: {
        height: 40,
        width: 40
    },

    crouselsText: {
        marginTop: 15,
        marginLeft: 10,
        marginBottom: 5
    },

    formInput: {
        marginTop: 15,
        width: width - 20,
        marginHorizontal: 10
    },

    parallaxHeader: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'column',
        paddingTop: 10,
        justifyContent: 'center'
    },

    imgStyle: {

        height: height * 0.4,
        marginHorizontal: 10,
        width: width - 20,
        resizeMode: 'cover',
    },

    saveBtnStyle: {
        backgroundColor: "#301436",
        width: width - 20,
        height: 45,
        borderColor: "transparent",
    },

    gradientBgStyle: {
        borderWidth: 1,
        borderColor: 'rgba(39,39,39,1)',
        width: width - 20,
        paddingHorizontal: 10,
        backgroundColor: 'rgba(23,23,23,1)'
    },

    inputHolder: {
        height: 100,
        borderWidth: 1,
        borderColor: 'rgba(39,39,39,1)',
        width: width - 20,
        paddingHorizontal: 10,
        backgroundColor: 'rgba(23,23,23,1)'
    },

    location: {
        position: 'absolute',
        marginBottom: 0,
        marginRight: 10
    }
});

export default withConnect(VenueProfile);