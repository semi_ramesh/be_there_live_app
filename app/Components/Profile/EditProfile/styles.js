import {StyleSheet, Dimensions} from 'react-native';
import {customOrange, customPurple} from "../../Authentication/styles";
import {fontFamilies} from "../../../constant/appConstant";
const {height, width} =  Dimensions.get('window');

export default StyleSheet.create({
    wrapper: {
        backgroundColor: '#000000',
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },

    innerWrapper: {
        padding:10
    },

    profileTabs: {
        height: 41,
        flexDirection: 'row',
        flex: 1,
        /*alignSelf: 'flex-start',*/
        justifyContent: 'space-between',
        width:width,



    },
    tabButtons: {
        height: 40,
        alignItems:'center',
        paddingTop:7,
    },

    tabBorder:{
        borderBottomColor: '#483549',
        borderBottomWidth: 1,
    },

    container: {
        justifyContent: 'space-between',
        alignItems:'center',
        flex: 1,
        marginTop:10
    },

    BtnContainer: {
           // alignItems:'center',
          //  flex: 1,
        flexDirection:'row',
        marginTop:10,
       // justifyContent: 'center',


        },
    profileImage: {
        width: width/3,
        height:200,
        borderWidth: 1,
        //flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileForm: {

    },
  
    gradientImage:{
        top:3,
        height:40,
        width:width-20,
        marginBottom:10,
        justifyContent:'center',
        paddingHorizontal:10,
        backgroundColor:'#171717',
        borderWidth : 1,
        borderColor:'rgba(39,39,39,1)',
    },

    formInput: {
        
        marginLeft:10,
        marginRight:10,
        marginTop:15
    },

    gridView:{
        flex:1,
        width:width,
    },

    itemStyle:{
        position:'absolute',
        backgroundColor:'transparent',
        flex:0,
        alignSelf:'flex-end'

    },
    tabBackground: {
        alignItems:'center',
        width:'33%'
    },

    jkStyle: {
       // flex: 1,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf:'center',
        backgroundColor: "#301436",

    },

    editStyle:{
        position:'absolute',
        right:-10,
        bottom:-30,
        borderColor:'white',
        borderWidth:2,
        backgroundColor:'transparent'

    },
    textInputCustomStyle:{
        color:'white',
        height:40,
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular,
        fontSize:14
    }
    /// artist rofile

});