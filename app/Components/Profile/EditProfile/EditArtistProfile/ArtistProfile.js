/**
 * @ProvideModule HDAudioPlayer.containers.profile.ArtistProfile
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  StyleSheet,
  TextInput,
  FlatList,
  Alert
} from "react-native";

import styles from "../styles";
import { Images } from "../../../../constant/images";
import GridView from "react-native-super-grid";
import { CheckBox, Button } from "react-native-elements";
import { customOrange, customPurple } from "../../../Authentication/styles";
import Icon from "react-native-vector-icons/FontAwesome";
const { width, height } = Dimensions.get("window");
import CommonStylesheet from "../../../../utils/CommonStylesheet";
import { FullWidthButton } from "../../../Reusables/index";
import withConnect from "./withConnectArtistProfile";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import BTLImagePickerController from "../../../../utils/imagePicker";
import {
  BTLoader,
  fontFamilies,
  getExtensionFromMimeType,
  showInfoMessage
} from "../../../../constant/appConstant";
import { Content, Body, Separator } from "native-base";
import Video from "react-native-video";
import FitImage from "react-native-fit-image";
import Modal from "react-native-modalbox";
import { email } from "react-native-communications";
import Player from "app/Components/HDAudioPlayer/index";
import {
  DocumentPicker,
  DocumentPickerUtil
} from "react-native-document-picker";
import BTLButton from "../../../Reusables/containers/BTLButton";
import _ from "lodash";
const url =
  "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";

class ArtistProfile extends Component {
  constructor(props) {
    super(props);
    let dummy = {
      id: "last",
      user_id: 2,
      picture: "uploads/images/gM6JINKgNbYMznYhe9xcXV8AM8gkESog7picdFgT.jpeg",
      image_path:
        "http://192.168.0.158/btl/public/storage/uploads/images/xve0pznY0SrC5gNROzh6jklWESiamRo7Syw9YdSS.jpeg"
    };

    let dummyVideo = {
      id: "last",
      user_id: 2,
      video: "t-20180919100916.mp4",
      video_path:
        "http://192.168.0.158/btl/public/storage/uploads/videos/t-20180919100916.mp4"
    };
    this.state = {
      userDetail: {},

      isFirst: true,
      isOpen: false,
      isDisabled: false,
      swipeToClose: true,
      sliderValue: 0.3,
      coverImage: Images.placeholder,
      AlbumCoverBlob: {},
      name: "",
      bandName: "",
      artistProfile: null,
      paused: false,
      profileImage: Images.placeholder,
      artistImages: [dummy],
      artistVideos: [dummyVideo],
      geners: [],
      currentVideo:
        "http://192.168.0.158/btl/public/storage/uploads/videos/t-20180919100916.mp4",
      album: [],
      tester: [],
      addAlbum: false,
      isPlaying: false,
      songs: [],
      albumArt: undefined,
      test: false,
      albumNewName: "ALBUM_1"
    };

    /**
     * This is binding because some where we are using es5 syntax
     * @type {*|Function}
     */
    this.getProfileForEdit = this.props.getProfileForEdit.bind(this);
    this.getArtistProfile = this.props.getArtistProfile.bind(this);
    this.updateArtistProfile = this.props.updateArtistProfile.bind(this);
    this.uploadProfilePicture = this.props.uploadProfilePicture.bind(this);
    this.uploadCrouselImages = this.props.uploadCrouselImages.bind(this);
    this.uploadvideosForArtist = this.props.uploadvideosForArtist.bind(this);
    this.editAlbumCoverAndTitle = this.props.editAlbumCoverAndTitle.bind(this);
    this.saveTheNewAlbum = this.props.saveTheNewAlbum.bind(this);
    this.addNewTrack = this.props.addNewTrack.bind(this);
    this.removeCrouselImage = this.props.removeCrouselImage.bind(this);
    this.removeTrack = this.props.removeTrack.bind(this);
    this.deleteVideo = this.props.deleteVideo.bind(this);
  }

  componentWillMount() {
    this.setState({
      paused: true
    });
  }

  componentDidMount() {
    this.getProfileApiCall().then(result => {});
  }

  getProfileApiCall = async () => {
    await this.getProfileForEdit(this.props.userData);
    return new Promise((resolve, reject) => {
      resolve(this.props.userProfile);
    });
  };

  componentWillReceiveProps(nextProps) {
    let dummy = {
      id: "last",
      user_id: 2,
      picture: "uploads/images/gM6JINKgNbYMznYhe9xcXV8AM8gkESog7picdFgT.jpeg",
      image_path:
        "http://192.168.0.158/btl/public/storage/uploads/images/xve0pznY0SrC5gNROzh6jklWESiamRo7Syw9YdSS.jpeg"
    };

    let dummyVideo = {
      id: "last",
      user_id: 2,
      video: "t-20180919100916.mp4",
      video_path:
        "http://192.168.0.158/btl/public/storage/uploads/videos/t-20180919100916.mp4"
    };

    if (
      nextProps.userProfile &&
      this.props.userProfile !== nextProps.userProfile
    ) {
      this.setState({
        artistProfile: nextProps.userProfile
      });
      /** Genere list updation**/
      if (nextProps.userProfile.data.all_genres) {
        this.setState({
          geners: nextProps.userProfile.data.all_genres,
          isFirst: false
        });
      }

      //User Detail Updation
      if (nextProps.userProfile.data.user_details) {
        const imagePath =
          nextProps.userProfile.data.user_details.user_profile_image;
        this.setState({
          name: nextProps.userProfile.data.user_details.name,
          bandName:
            nextProps.userProfile.data.user_details.about_me === "null"
              ? ""
              : nextProps.userProfile.data.user_details.about_me,
          profileImage:
            imagePath === null
              ? Images.placeholder
              : { uri: imagePath.image_path }
        });

        let artisImageArray =
          nextProps.userProfile.data.user_details.user_carousel_images;

        artisImageArray.map(item => {
          if (item.id === "last") {
            artisImageArray.pop();
          }
        });

        artisImageArray.push(dummy);
        this.setState({
          artistImages: artisImageArray
        });
      }

      //Albums here :-
      if (nextProps.userProfile.data.user_details) {
        const {
          user_albums_with_tracks
        } = nextProps.userProfile.data.user_details;
        this.setState({
          album: user_albums_with_tracks
        });
      }

      //Videos getting there
      if (nextProps.userProfile.data.user_details) {
        let videoPath =
          nextProps.userProfile.data.user_details.user_video_with_path[0];
        this.setState({
          currentVideo: videoPath !== undefined ? videoPath.video_path : url
        });
        let videoArray =
          nextProps.userProfile.data.user_details.user_video_with_path;
        videoArray.map(item => {
          if (item.id === "last") {
            videoArray.pop();
          }
        });
        videoArray.push(dummyVideo);
        this.setState({
          artistVideos: videoArray
        });
      }
    }

    //Crousel Images Updatation
    if (
      nextProps.crouselImages &&
      nextProps.crouselImages.data.user_carousel_images
    ) {
      let artisImageArray = nextProps.crouselImages.data.user_carousel_images;
      if (artisImageArray.length > 0) {
        artisImageArray.map(item => {
          if (item.id === "last") {
            artisImageArray.pop();
          }
        });
      }

      artisImageArray.push(dummy);
      this.setState({
        artistImages: artisImageArray
      });
    }
    //Profile Image Updation
    if (nextProps.profileImage) {
      this.setState({
        profileImage: {
          uri: nextProps.profileImage.data.user_profile_image.image_path
        }
      });
    }

    if (nextProps.ProfileData) {
      if (nextProps.ProfileData.user_details) {
        const { name, about_me } = nextProps.ProfileData.user_details;
        this.setState({
          name: name,
          bandName: about_me
        });
      }
      if (nextProps.ProfileData.all_genres) {
        this.setState({
          geners: nextProps.ProfileData.all_genres
        });
      }
    }
    if (nextProps.uploadedVideos) {
      if (nextProps.uploadedVideos.user_video_with_path.length > 0) {
        this.setState({
          currentVideo:
            nextProps.uploadedVideos.user_video_with_path[0].video_path
        });
      }
      let videoArray = nextProps.uploadedVideos.user_video_with_path;
      videoArray.map(item => {
        if (item.id === "last") {
          videoArray.pop();
        }
      });
      videoArray.push(dummyVideo);
      this.setState({
        artistVideos: videoArray
      });
    }
    if (nextProps.albumData && this.props.albumData !== nextProps.albumData) {
      this.setState({
        album: nextProps.albumData.data.user_albums_with_tracks
      });
    }

    // this.setState({
    //     isFirst:false
    // })
  }

  addNewTracks = item => {
    // iPhone/Android
    DocumentPicker.show(
      {
        filetype: [DocumentPickerUtil.audio()]
      },
      (error, res) => {
        // Android
        let formData = new FormData();
        formData.append("title", res.fileName);
        formData.append("album_id", item.id);
        formData.append("user_id", item.user_id);
        formData.append("tracks", {
          uri: res.uri,
          name: res.fileName,
          filename: `${res.fileName}`,
          type: res.type
        });
        this.addNewTrack(formData, item.id);
      }
    );
  };

  /***
   * upload Profile Image
   * @returns {Promise<void>}
   */

  captureOrTakeandUploadImage = async () => {
    try {
      await BTLImagePickerController.pickImage()
        .then(response => {
          this.setState({
            profileImage: response
          });

          let timeStamp = Math.floor(Date.now() / 1000);

          let infoDict = getExtensionFromMimeType(response.url);
          let formData = new FormData();

          formData.append("picture", {
            uri: response.uri,
            name: `${timeStamp}.${infoDict.extension}`,
            filename: `${timeStamp}.${infoDict.extension}`,
            type: `${infoDict.mime}`
          });
          this.uploadProfilePicture(formData);
        })
        .then(error => {});
    } catch (e) {}
  };

  /**
   * Upload Videos
   * @returns {*}
   */
  uploadVideos = async () => {
    await BTLImagePickerController.pickVideos()
      .then(response => {
        let timeStamp = Math.floor(Date.now() / 1000);
        let infoDict = getExtensionFromMimeType(response.url.toLowerCase());
        let formData = new FormData();

        formData.append("video", {
          uri: response.uri,
          name: `${timeStamp}.${infoDict.extension}`,
          filename: `${timeStamp}.${infoDict.extension}`,
          type: `${infoDict.mime}`
        });
        this.uploadvideosForArtist(formData);
      })
      .then(error => {});
  };

  updateName = text => {
    this.setState({
      name: text
    });
  };

  updateBandName = text => {
    this.setState({
      bandName: text
    });
  };

  editNameBandGenres = () => {
    if (!this.validateBandGeners()) {
      return;
    }
    const { name, bandName, geners } = this.state;
    try {
      let formData = new FormData();
      formData.append("name", name);
      formData.append("about_me", bandName);
      geners.map(item => {
        if (item.is_checked_by_artist === 1) {
          formData.append("user_genres[]", item.id);
        }
      });
      this.updateArtistProfile(formData);
    } catch (e) {}
  };

  validateBandGeners = () => {
    const { name, bandName } = this.state;

    if (_.isEmpty(name)) {
      showInfoMessage("Please enter artist/band name.");
      return false;
    }
    if (_.isEmpty(bandName)) {
      showInfoMessage("Please enter artist/band bio name.");
      return false;
    }
    return true;
  };

  /**
   * upload CrouselImages
   * @returns {*}
   */
  uploadCrouselImagesToServer = async () => {
    try {
      await BTLImagePickerController.pickImage()
        .then(response => {
          let timeStamp = Math.floor(Date.now() / 1000);
          let infoDict = getExtensionFromMimeType(response.url);
          let formData = new FormData();

          formData.append("image", {
            uri: response.uri,
            name: `${timeStamp}.${infoDict.extension}`,
            filename: `${timeStamp}.${infoDict.extension}`,
            type: `${infoDict.mime}`
          });
          this.uploadCrouselImages(formData);
        })
        .then(error => {});
    } catch (e) {}
  };

  selectVideo = item => {
    this.setState({
      currentVideo: item.video_path
    });
  };

  conformRemoveTrack = item => {
    Alert.alert(
      "Are you sure to delete song ?",
      "",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Yes", onPress: () => this.deleteTrack(item) }
      ],
      { cancelable: false }
    );
  };

  deleteTrack = item => {
    this.removeTrack(item.id);
  };

  conformRemoveVideo = item => {
    Alert.alert(
      "Are you sure to delete video ?",
      "",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Yes", onPress: () => this.removeVideo(item) }
      ],
      { cancelable: false }
    );
  };

  removeVideo = item => {
    this.deleteVideo(item.id);
  };

  updateAlbumName = text => {
    this.setState({
      albumNewName: text
    });
  };

  handleVideo = () => {
    this.setState({
      paused: !this.state.paused
    });
  };

  addMoreAlbum = () => {
    this.setState({
      addAlbum: true
    });
  };

  saveNewAlbum = () => {
    if (!this.validateNewAlbum()) {
      return;
    }

    try {
      let formData = new FormData();
      formData.append("album_cover", this.state.AlbumCoverBlob);
      formData.append("title", this.state.albumNewName);
      formData.append("user_id", this.props.userData.id);
      this.saveTheNewAlbum(formData);
      this.closeModel();
    } catch (e) {}
  };

  validateNewAlbum = () => {
    const { AlbumCoverBlob, albumNewName } = this.state;

    if (_.isEmpty(AlbumCoverBlob)) {
      showInfoMessage("Please upload album cover.");
      return false;
    }

    if (_.isEmpty(albumNewName)) {
      showInfoMessage("Please enter album name.");
      return false;
    }
    return true;
  };

  selectAlbumCover = async () => {
    try {
      await BTLImagePickerController.pickImage()
        .then(response => {
          let timeStamp = Math.floor(Date.now() / 1000);
          let infoDict = getExtensionFromMimeType(response.url);
          let AlbumCover = {
            uri: response.uri,
            name: `${timeStamp}.${infoDict.extension}`,
            filename: `${timeStamp}.${infoDict.extension}`,
            type: `${infoDict.mime}`
          };
          this.setState({
            coverImage: { uri: response.uri },
            AlbumCoverBlob: AlbumCover
          });
        })
        .then(error => {});
    } catch (e) {}
  };

  conformRemoveImage = item => {
    Alert.alert(
      "Are you sure to remove crousel image ?",
      "",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Yes", onPress: () => this.removeImage(item) }
      ],
      { cancelable: false }
    );
  };

  removeImage = item => {
    this.removeCrouselImage(item.id);
  };

  renderCrousel = (item, index) => {
    const itemSize = (width - 50) / 4;
    if (item.id === "last") {
      return (
        <TouchableOpacity
          style={{ height: itemSize, width: itemSize }}
          onPress={this.uploadCrouselImagesToServer}
        >
          <Image source={Images.addNew} style={newStyle.userImage} />
        </TouchableOpacity>
      );
    }
    return (
      <ImageBackground
        source={{ uri: item.image_path }}
        style={{ height: itemSize, width: itemSize }}
        key={item.id}
      >
        <TouchableOpacity
          style={[styles.itemStyle]}
          onPress={() => this.conformRemoveImage(item)}
        >
          <Image style={newStyle.unfollowStyle} source={Images.unfollow1} />
        </TouchableOpacity>
      </ImageBackground>
    );
  };

  renderCheckBoxes = (item, index) => {
    return (
      <CheckBox
        title={item.genre_name}
        textStyle={newStyle.chekBoxTitleText}
        size={20}
        onPress={() => this.onSelectGenere(item, index)}
        checkedColor={"white"}
        uncheckedColor={"white"}
        checked={item.is_checked_by_artist === 1}
        containerStyle={newStyle.notificationCheckbox}
      />
    );
  };

  onSelectGenere = (item, index) => {
    let genreArray = this.state.geners;
    genreArray.map(genre => {
      if (genre === item) {
        genre.is_checked_by_artist = item.is_checked_by_artist === 0 ? 1 : 0;
      }
      this.setState({
        geners: genreArray
      });
    });
  };

  uploadCoverPicOfAlbum = async item => {
    try {
      await BTLImagePickerController.pickImage()
        .then(response => {
          this.setState({
            //profileImage: response
          });
          let timeStamp = Math.floor(Date.now() / 1000);
          let infoDict = getExtensionFromMimeType(response.url);
          let formData = new FormData();
          formData.append("album_cover", {
            uri: response.uri,
            name: `${timeStamp}.${infoDict.extension}`,
            filename: `${timeStamp}.${infoDict.extension}`,
            type: `${infoDict.mime}`
          });
          formData.append("album_title", item.album_title);
          let albumId = item.id;
          this.editAlbumCoverAndTitle(formData, albumId);
        })
        .then(error => {});
    } catch (e) {}
  };

  closeModel = () => {
    this.setState({
      coverImage: Images.placeholder,
      AlbumCoverBlob: {},
      addAlbum: false
    });
  };

  renderVideos = item => {
    let itemSize = (width - 50) / 4;
    if (item.id === "last") {
      return (
        <TouchableOpacity onPress={this.uploadVideos}>
          <FitImage
            source={Images.addNew}
            style={{ height: itemSize, width: itemSize }}
            key={item.id}
          />
        </TouchableOpacity>
      );
    }
    return (
      <TouchableOpacity
        onPress={() => {
          this.selectVideo(item);
        }}
      >
        <FitImage
          source={{ uri: item.video_thumbnail }}
          style={{ height: itemSize, width: itemSize }}
          key={item.id}
        />

        <TouchableOpacity
          style={newStyle.removeVideoIcon}
          onPress={() => this.conformRemoveVideo(item)}
        >
          <Image
            source={Images.unfollow1}
            style={{ height: itemSize * 0.4, width: itemSize * 0.4 }}
          />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  renderTracks = ({ albumArt, item, onSelect }) => {
    return (
      <TouchableOpacity
        style={newStyle.trackTop}
        onPress={() => onSelect(albumArt, item)}
      >
        <View style={{ flexDirection: "row" }}>
          <Icon
            name={"play-circle"}
            color={"white"}
            size={20}
            style={{ paddingRight: 10 }}
          />
          <View style={{ width: width - 90, flexDirection: "row" }}>
            <Text style={newStyle.trackTitleText}>{item.track_title}</Text>
            <Text style={newStyle.trackTimeText}>{item.track_time}</Text>
          </View>
          <TouchableOpacity
            style={{ marginRight: 0 }}
            onPress={() => {
              this.conformRemoveTrack(item);
            }}
          >
            <Icon name={"trash"} size={20} color={"red"} />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  };

  fullScreenClicked = () => {
    this.player.presentFullscreenPlayer();
  };

  onTrackPress = (albumArt, song) => {
    this.setState({
      isPlaying: true,
      songs: [song],
      albumArt: albumArt
    });
  };

  stopPlaying = () => {
    this.setState({
      isPlaying: false,
      songs: []
    });
  };

  render() {
    if (this.props.isLoading && this.state.isFirst) {
      return (
        <View style={newStyle.loaderView}>
          <BTLoader />
        </View>
      );
    }

    if (this.state.addAlbum) {
      return (
        <ScrollView style={newStyle.scrollViewStyle}>
          <View style={mstyles.modal}>
            {/* {this.props.isLoading && <BTLoader />} */}
            <View style={[newStyle.topContainer, { paddingVertical: 15 }]}>
              <Text style={newStyle.modelTitle}> ADD NEW ALBUM </Text>
              <View style={newStyle.albumImage}>
                <View style={[newStyle.formInput, { marginBottom: 8 }]}>
                  <Text style={CommonStylesheet.formTitle}>ALBUM NAME:</Text>
                  <View style={newStyle.gradientBgStyle}>
                    <TextInput
                      style={styles.textInputCustomStyle}
                      placeholderTextColor="white"
                      onChangeText={this.updateAlbumName}
                      underlineColorAndroid="transparent"
                      value={this.state.albumNewName}
                    />
                  </View>
                </View>
                <Text
                  style={[CommonStylesheet.formTitle, newStyle.albumCoverText]}
                >
                  ADD ALBUM COVER:
                </Text>
                <Image
                  source={this.state.coverImage}
                  style={newStyle.newCoverPic}
                />
                <TouchableOpacity
                  style={newStyle.newEditStyle}
                  onPress={this.selectAlbumCover}
                >
                  <Image
                    source={Images.profileEditButton}
                    style={newStyle.editButton}
                  />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                style={[newStyle.wrapperBtnComponent, { marginTop: 30 }]}
                onPress={this.saveNewAlbum}
              >
                <Text style={newStyle.modelButtonText}>{"SAVE"}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={this.closeModel}
                style={newStyle.wrapperBtnComponent}
              >
                <Text style={newStyle.modelButtonText}>{"CANCEL"}</Text>
              </TouchableOpacity>
              {this.props.isLoading && <BTLoader />}
            </View>
          </View>
        </ScrollView>
      );
    }

    return (
      <View style={newStyle.superView}>
        <KeyboardAwareScrollView style={newStyle.scrollViewStyle}>
          <View style={newStyle.formInput}>
            <Text style={CommonStylesheet.formTitle}>UPLOAD IMAGE:</Text>
            <View style={newStyle.parallaxHeader}>
              <Image
                source={this.state.profileImage}
                style={newStyle.imgStyle}
              />
              <TouchableOpacity
                style={newStyle.profileEditButtonView}
                onPress={this.captureOrTakeandUploadImage}
              >
                <Image
                  source={Images.profileEditButton}
                  style={newStyle.editButton}
                />
              </TouchableOpacity>
            </View>
          </View>

          <Text style={[CommonStylesheet.formTitle, newStyle.crouselText]}>
            CAROUSEL:
          </Text>
          <View style={[newStyle.formInput, newStyle.crouselGreadView]}>
            <GridView
              itemDimension={(width - 50) / 4}
              items={this.state.artistImages}
              renderItem={this.renderCrousel}
              spacing={9}
            />
          </View>
          <Separator style={{ height: 1 }} />
          <View style={newStyle.formInput}>
            <Text style={CommonStylesheet.formTitle}>GENRE</Text>
            <GridView
              itemDimension={(width - 50) / 4}
              items={this.state.geners}
              renderItem={this.renderCheckBoxes}
              spacing={10}
            />
          </View>
          <View style={newStyle.formInput}>
            <Text style={CommonStylesheet.formTitle}>ARTIST/BAND NAME:</Text>
            <View style={newStyle.gradientBgStyle}>
              <TextInput
                style={styles.textInputCustomStyle}
                placeholderTextColor="white"
                onChangeText={this.updateName}
                underlineColorAndroid="transparent"
                value={this.state.name}
              />
            </View>
          </View>
          <View style={newStyle.formInput}>
            <Text style={CommonStylesheet.formTitle}>ARTIST / BAND BIO</Text>
            <View style={newStyle.billingAddressStyle}>
              <TextInput
                style={[styles.textInputCustomStyle]}
                placeholderTextColor="white"
                multiline={true}
                onChangeText={this.updateBandName}
                numberOfLines={90}
                underlineColorAndroid="rgba(0,0,0,0)"
                value={this.state.bandName}
              />
            </View>
          </View>
          <View style={[newStyle.formInput, { alignItems: "center" }]}>
            <Button
              title={"Save"}
              onPress={this.editNameBandGenres}
              buttonStyle={[
                newStyle.wrapperBtnComponent,
                newStyle.profileSaveButton
              ]}
            />
            <BTLButton
              title={"Contact To Administrator"}
              btnStyle={newStyle.contectButtonView}
              titleColor={customOrange}
              textStyle={newStyle.contectButton}
              onPress={() =>
                email(
                  ["local.live2017@gmail.com"],
                  null,
                  null,
                  "Promote Me",
                  null
                )
              }
            />
          </View>
          <View style={newStyle.formInput}>
            <Text style={[CommonStylesheet.formTitle, { marginBottom: 5 }]}>
              VIDEO CLIPS:
            </Text>
            {this.state.artistVideos.length > 1 && (
              <View>
                <TouchableOpacity
                  style={newStyle.profileVideo}
                  onPress={this.handleVideo}
                >
                  <Video
                    ref={ref => {
                      this.player = ref;
                    }}
                    source={{ uri: `${this.state.currentVideo}` }}
                    style={newStyle.video}
                    onEnd={this.handleVideo}
                    repeat={true}
                    resizeMode={"stretch"}
                    onFullscreenPlayerDidDismiss={() =>
                      this.setState({ fullScreen: false })
                    }
                    paused={this.state.paused}
                  />
                  {this.state.paused && (
                    <Image
                      style={newStyle.playBtnImage}
                      source={Images.playBtn}
                    />
                  )}
                  <TouchableOpacity
                    style={newStyle.videoFullScreen}
                    onPress={this.fullScreenClicked}
                  >
                    <Image
                      style={newStyle.fullScreenIcon}
                      source={Images.fullScreen}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
              </View>
            )}
          </View>
          <View style={{ width: width }}>
            <GridView
              itemDimension={(width - 50) / 4}
              items={this.state.artistVideos}
              renderItem={this.renderVideos}
              spacing={9}
            />
          </View>

          <Separator style={newStyle.seperatorView} />
          <View style={newStyle.formInput}>
            <Text style={[CommonStylesheet.formTitle, { marginTop: 10 }]}>
              ALBUMS:
            </Text>
            {this.state.album.map(item => {
              let coverImage =
                item.album_cover === ""
                  ? Images.placeholder
                  : { uri: item.album_cover };
              let albumArt =
                item.album_cover === "" ? Images.placeholder : item.album_cover;
              return (
                <View style={[newStyle.topContainer, { paddingVertical: 10 }]}>
                  <View style={newStyle.albumImage}>
                    <Image source={coverImage} style={newStyle.imageStyle} />
                    <TouchableOpacity
                      style={[newStyle.newEditStyle, { marginVertical: 15 }]}
                      onPress={() => {
                        this.uploadCoverPicOfAlbum(item);
                      }}
                    >
                      <Image
                        source={Images.profileEditButton}
                        style={{ height: 40, width: 40 }}
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={newStyle.tracks}>
                    <FlatList
                      data={item.tracks}
                      renderItem={({ item }) =>
                        this.renderTracks({
                          albumArt: albumArt,
                          item: item,
                          onSelect: this.onTrackPress
                        })
                      }
                      space={4}
                    />
                    <View style={styles.susCview}>
                      <TouchableOpacity
                        style={newStyle.miscBtn}
                        onPress={() => this.addNewTracks(item)}
                      >
                        <Icon name={"plus"} size={22} color={"white"} />
                        <Text
                          style={[
                            CommonStylesheet.whiteColor,
                            { marginLeft: 5 }
                          ]}
                        >
                          Add Your Tracks
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <Separator style={{ height: 1, marginTop: 10 }} />
                </View>
              );
            })}
          </View>
          <View style={newStyle.formInput}>
            <FullWidthButton
              title={"ADD ANOTHER ALBUM"}
              OnTap={this.addMoreAlbum}
              style={{ width: width - 20 }}
            />
          </View>
        </KeyboardAwareScrollView>
        {this.props.isLoading && <BTLoader />}
        <Modal
          style={newStyle.modelView}
          position={"top"}
          ref={"modal"}
          isOpen={this.state.isPlaying}
          onClosed={() => this.setState({ isPlaying: false })}
        >
          {this.state.isPlaying && (
            <ScrollView>
              <Player
                isDownPressed={this.stopPlaying}
                tracks={this.state.songs}
                albumArt={this.state.albumArt}
              />
            </ScrollView>
          )}
        </Modal>
      </View>
    );
  }
}

function NotificationCheckboxes(props) {
  return (
    <CheckBox
      title={props.title}
      textStyle={newStyle.chekBoxTitleText}
      size={20}
      onPress={props.onPress}
      checkedColor={"white"}
      uncheckedColor={"white"}
      checked={props.checked}
      containerStyle={newStyle.notificationCheckbox}
    />
  );
}

export default withConnect(ArtistProfile);

export const newStyle = StyleSheet.create({
  superView: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "black"
  },

  scrollViewStyle: {
    backgroundColor: "black"
  },

  chekBoxTitleText: {
    color: customPurple,
    fontWeight: "normal"
  },

  profileEditButtonView: {
    position: "absolute",
    bottom: -15,
    right: 0
  },

  removeVideoIcon: {
    position: "absolute",
    top: 0,
    right: 0
  },

  trackTitleText: {
    width: (width - 80) / 2,
    color: "white",
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },

  trackTimeText: {
    width: (width - 80) / 2,
    marginLeft: 10,
    color: "white"
  },

  foreGroundContent: {
    top: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  trackTop: {
    flex: 1,
    width: width,
    paddingHorizontal: 0,
    paddingVertical: 8,
    flexDirection: "row",
    justifyContent: "space-between"
  },

  durationText: {
    flexDirection: "row",
    marginRight: 50
  },

  loaderView: {
    flex: 1,
    backgroundColor: "black",
    justifyContent: "center"
  },

  modelTitle: {
    alignSelf: "center",
    fontSize: 24,
    marginVertical: 5,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
    color: "white"
  },

  albumCoverText: {
    alignSelf: "flex-start",
    marginLeft: 10,
    marginBottom: 5
  },

  editButton: {
    height: 40,
    width: 40
  },

  modelButtonText: {
    color: "white",
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },

  profileImage: {
    bottom: -5,
    height: 40,
    width: 40
  },

  crouselText: {
    marginLeft: 10,
    marginTop: 15
  },

  crouselGreadView: {
    width: width,
    marginHorizontal: 0
  },

  modelView: {
    flex: 1,
    backgroundColor: "black"
  },

  profileSaveButton: {
    backgroundColor: "red",
    width: width - 20
  },

  contectButtonView: {
    backgroundColor: "rgba(36,8,0,1.0)",
    width: width - 20
  },

  contectButton: {
    color: customOrange,
    fontWeight: "800",
    fontSize: 18,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },

  videoFullScreen: {
    bottom: 14,
    position: "absolute",
    right: 10
  },

  fullScreenIcon: {
    height: 30,
    width: 30
  },

  seperatorView: {
    height: 1
  },

  parallaxHeader: {
    alignItems: "center",
    flex: 1,
    flexDirection: "column",
    paddingTop: 10,
    justifyContent: "center"
  },

  userImage: {
    height: (width - 50) / 4,
    width: (width - 50) / 4
  },

  imgStyle: {
    height: height * 0.4,
    marginHorizontal: 10,
    width: width - 20
  },

  notificationCheckbox: {
    backgroundColor: "#000000",
    borderWidth: 0,
    margin: 0,
    padding: 0,
    alignItems: "flex-start",
    width: "100%"
  },

  formInput: {
    marginTop: 10,
    marginHorizontal: 10,
    width: width - 20
  },

  billingAddressStyle: {
    height: 100,
    borderWidth: 1,
    borderColor: "rgba(39,39,39,1)",
    width: width - 20,
    paddingHorizontal: 10,
    backgroundColor: "rgba(23,23,23,1)",
    marginTop: 5
  },

  wrapperBtnComponent: {
    backgroundColor: "#301436",
    width: width - 20,
    marginHorizontal: 10,
    marginVertical: 5,
    height: 45,
    borderColor: "transparent",
    borderWidth: 0,
    alignItems: "center",
    justifyContent: "center"
  },

  profileVideo: {
    height: height / 3,
    width: width - 20,
    justifyContent: "center",
    alignItems: "center"
  },

  topContainer: {
    flex: 1
  },

  video: {
    flex: 1,
    position: "absolute",
    height: height / 3,
    width: width - 20
  },

  playBtnImage: {
    height: 100,
    width: 100
  },

  albumImage: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  tracks: {
    marginHorizontal: 10,
    flex: 1
  },

  imageStyle: {
    flex: 0.3,
    height: width / 3 - 10,
    width: width - 20,
    resizeMode: "cover"
  },

  newCoverPic: {
    flex: 1,
    resizeMode: "cover",
    width: width - 20,
    height: width / 2
  },

  addition: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 5
  },

  newEditStyle: {
    backgroundColor: "transparent",
    flex: 0,
    justifyContent: "flex-end",
    alignItems: "flex-end",
    right: 0,
    bottom: -20,
    zIndex: 9999,
    position: "absolute"
  },

  susCview: {
    marginTop: 30,
    alignItems: "flex-end",
    paddingRight: 10,
    flexDirection: "row"
  },

  miscBtn: {
    height: 40,
    borderWidth: 0,
    padding: 7,
    backgroundColor: "#3A1540",
    alignItems: "center",
    flexDirection: "row"
  },

  follwStyle: {
    left: 0,
    bottom: 0,
    height: ((width - 36) / 3) * 0.4,
    width: ((width - 36) / 3) * 0.4
  },

  unfollowStyle: {
    right: 0,
    top: 0,
    height: ((width - 50) / 4) * 0.4,
    width: ((width - 50) / 4) * 0.4
  },

  gradientBgStyle: {
    borderWidth: 1,
    borderColor: "rgba(39,39,39,1)",
    width: width - 20,
    paddingHorizontal: 10,
    backgroundColor: "rgba(23,23,23,1)",
    marginTop: 5
  }
});

const mstyles = StyleSheet.create({
  modal: {
    flex: 1,
    bottom: 0,
    paddingBottom: 10,
    backgroundColor: "black",
    paddingVertical: 20
  }
});
