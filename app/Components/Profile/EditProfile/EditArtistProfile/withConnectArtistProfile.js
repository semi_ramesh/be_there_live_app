/** Make Connection redux thinngs **/

import {} from 'react';

import {connect} from 'react-redux';
import {auth,serviceApi,shared} from 'app/redux/actions';

function  mapStateToProps(state) {

    const {user:userData} = state.auth;
    const {isLoading} = state.shared;
    const {
           myProfile,
           userProfile,
           ProfileData,
           profileImage,
           crouselImages,
           uploadedVideos,
           albumData,
           
      } = state.serviceApi;

    return{
        userData,
        isLoading,
        myProfile,
        userProfile,
        profileImage,
        crouselImages,
        ProfileData,
        uploadedVideos,
        albumData,
        
        

    }

}


function  mapDispatchToProps(dispatch) {

    return{
        getProfileForEdit :(formData)=>dispatch(serviceApi.getProfileForEdit(formData)),
        getArtistProfile:(formData) =>dispatch(serviceApi.getProfile(formData)),
        updateArtistProfile :(formData) =>dispatch(serviceApi.updateProfile(formData)),
        uploadProfilePicture:(formData)=>dispatch(serviceApi.uploadProfilePicture(formData)),
        uploadCrouselImages:(formData)=>dispatch(serviceApi.uploadCrouselImages(formData)),
        uploadvideosForArtist:(formData)=>dispatch(serviceApi.uploadvideosForArtist(formData)),
        editAlbumCoverAndTitle:(formData,id)=>dispatch(serviceApi.editAlbumCoverAndTitle(formData,id)),
        saveTheNewAlbum:(formData)=>dispatch(serviceApi.saveTheNewAlbum(formData)),
        addNewTrack:(formData ,id)=>dispatch(serviceApi.addNewTrack(formData,id)),
        removeCrouselImage:(id)=>dispatch(serviceApi.removeCrouselImage(id)),
        removeTrack:(id)=>dispatch(serviceApi.removeTrack(id)),
        deleteVideo:(id)=>dispatch(serviceApi.deleteVideo(id))

      }

}

export default function withConnect(wrapperComponent) {
    return connect(mapStateToProps,mapDispatchToProps)(wrapperComponent)
}