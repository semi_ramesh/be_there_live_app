import React ,{Component} from 'react'
import {View,Text,Image,ImageBackground,TouchableOpacity,Dimensions,StyleSheet,TextInput} from 'react-native'
import styles from '../styles'
import {Images} from "../../../../constant/images";
import GridView from 'react-native-super-grid';
import {customPurple} from "../../../Authentication/styles";
const {width,height} = Dimensions.get('window');
import withConnect from '../../withConnectComponent'
import CommonStylesheet from "../../../../utils/CommonStylesheet";

import {
    getExtensionFromMimeType,
    BTLoader,
    showInfoMessage,
    SERVICE_API,
    fontFamilies
} from "../../../../constant/appConstant";
import BTLImagePickerController from "../../../../utils/imagePicker";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import _ from 'lodash'


 class UserProfile extends Component{

     constructor(props) {

         super(props);
         this.state = {
             isFirst: true,
             profileImage: {},
             name: '',
             last_name: '',
             user_name: '',
             designation: '',
             instagram: '',
             twitter: '',
             connections: [],
             follwersStatus: 0,
         };
         this.getProfile = this.props.getProfile.bind(this);
         this.addorRemoveGenre = this.props.addorRemoveGenre.bind(this);
         this.uploadProfilePicture = this.props.uploadProfilePicture.bind(this);
         this.updateProfile = this.props.updateProfile.bind(this);
     }

     /**
      * Component life Cycle
      * @param nextProps
      */
     componentWillReceiveProps(nextProps) {
         if (!nextProps.myProfile) {
             return;
         }

         if (nextProps.myProfile && nextProps.myProfile.data.userDetail !== this.props.myProfile.data.userDetail) {
             if (nextProps.myProfile.data) {

                 const {
                     name,
                     email,
                     twitter,
                     instagram,
                     designation,
                     user_pictures,
                     user_profile_image
                 } = nextProps.myProfile.data.userDetail;

                 const {
                     users_genres
                 } = this.props.myProfile.data;

                 this.setState({
                     name: name,
                     user_name: email,
                     designation: designation,
                     instagram: instagram,
                     twitter: twitter,
                     connections: users_genres === undefined ? [] : users_genres,
                     profileImage: user_profile_image ? { uri: user_profile_image.image_path } : Images.placeholder,
                     isFirst: false,
                 })
             }

             if (nextProps.genereList) {
                 this.setState({
                     connections: nextProps.genereList.user_genres === undefined ? [] : nextProps.genereList.user_genres
                 })
             }

             if (this.props.profileImage !== nextProps.profileImage) {
                 this.setState({ 
                     profileImage: { uri: nextProps.profileImage.data.user_profile_image.image_path } 
                    })
             }
         }
     }

    shouldComponentUpdate(){
        return true
    }

    componentDidMount(){
        this.getProfile();
    }

     /**
      * Update existing Profile
      */
     updateIndividualProfile = () => {
         const {
             name,
             user_name,
             twitter,
             instagram,
             designation
         } = this.state;
         if (!this.validateForm()) {
             return
         }
         let formData = new FormData();
         formData.append('name', name);
         formData.append('username', user_name);
         formData.append('twitter', twitter);
         formData.append('instagram', instagram);
         formData.append('designation', designation);
         this.updateProfile(formData);
     };

     /**
      * Validate Profile Updatation form
      */
     validateForm = () => {
         const {
             name,
             user_name,
             twitter,
             instagram,
             designation
         } = this.state;
         if (_.isEmpty(name)) {
             showInfoMessage('Please enter your name.');
             return false
         }
         if (_.isEmpty(twitter)) {
             showInfoMessage('Please enter your twitter profile link.');
             return false
         }
         if (_.isEmpty(instagram)) {
             showInfoMessage('Please enter your instagram profile link.');
             return false
         }
         if (_.isEmpty(designation)) {
             showInfoMessage('Please enter your designation.');
             return false
         }
         return true
     }

     getIndividiualProfile = async () => {
         await this.getProfile(this.props.userData.id);
     };

     /**
      *Follow or Unfollow Status
      * **/
     followUnfollowUser = (item) => {

         let follwersStatus = item.is_checked_by_user === 0 ? 1 : 0
         let genreId = item.id;
         this.addorRemoveGenre(follwersStatus, genreId);
     };


     takePickAndUploadImage = async () => {
         try {
             await BTLImagePickerController.pickImage().then(response => {

                 this.setState({
                     profileImage: response
                 });

                 let timeStamp = Math.floor(Date.now() / 1000);
                 let infoDict = getExtensionFromMimeType(response.url)
                 let formData = new FormData();

                 formData.append('picture', {
                     uri: response.uri,
                     name: `${timeStamp}.${infoDict.extension}`,
                     filename: `${timeStamp}.${infoDict.extension}`,
                     type: `${infoDict.mime}`
                 });
                 this.uploadProfilePicture(formData);

             }).then(error => {
             });

         }
         catch (e) {
         }
     };

     editName = (text) => {
         this.setState({
             name: text
         })
     }

     editTwitter = (text) =>{
         this.setState({
             twitter:text
         })
     }

     editInstagram = (text) => {
         this.setState({
             instagram: text
         })
     }

     editDesignation = (text) => {
         this.setState({
             designation: text
         })
     }




     renderItem = (item) => {

         const userImage = item.image !== null ? { uri: item.image } : Images.user1;
         const followImage = item.is_checked_by_user === 1 ? Images.unfollow : Images.follow;
         const myFollowStyle = item.is_checked_by_user === 1 ? styles2.follwStyle : styles2.unfollowStyle;
         const itemStyle = item.is_checked_by_user === 1 ? styles2.itemStyle : styles2.itemStyleFollow;

         return (<View>
             <ImageBackground
                 defaultSource={Images.user1}
                 style={styles2.genreImage} source={userImage}>
                 <View style={styles2.genreImage}>
                     <TouchableOpacity style={itemStyle} onPress={() => this.followUnfollowUser(item)}>
                         <Image style={myFollowStyle} source={followImage} />
                     </TouchableOpacity>
                     <Text style={styles2.genreName}>{(item.genre_name).toUpperCase()}</Text>
                 </View>
             </ImageBackground></View>)
     }

     render() {


         if (this.props.isLoading && this.state.isFirst) {
             return (<View style={styles2.loaderView}>
                 <BTLoader /></View>)
         }
         const { userDetail, records } = this.state;


         return (
             <View style= {styles2.superView}>
                 <KeyboardAwareScrollView style={styles2.KeyboardAwareScrollViewStyle}>
                     <Text style={[CommonStylesheet.formTitle, styles2.uploadImageText]}>UPLOAD IMAGE:</Text>
                     <View style={styles2.parallaxHeader}>
                         <Image source={this.state.profileImage} style={styles2.imgStyle} />
                         <TouchableOpacity style={styles2.editImageButton}
                             onPress={this.takePickAndUploadImage}>
                             <Image source={Images.profileEditButton} style={styles2.editImage} />
                         </TouchableOpacity>
                     </View>

                     <View style={styles.wrapper}>
                         <View style={styles.innerWrapper}>
                             <View style={styles.container}>
                                 <View style={styles.formInput}>
                                     <Text style={CommonStylesheet.formTitle}>NAME:</Text>
                                     <ImageBackground style={styles.gradientImage}>
                                         <TextInput style={styles.textInputCustomStyle}
                                             placeholderTextColor="white"
                                             underlineColorAndroid='rgba(0,0,0,0)'
                                             onChangeText={this.editName}
                                             value={this.state.name} />
                                     </ImageBackground>
                                 </View>

                                 <View style={styles.formInput}>
                                     <Text style={CommonStylesheet.formTitle}>USER NAME:</Text>
                                     <ImageBackground style={styles.gradientImage}>
                                         <TextInput style={styles.textInputCustomStyle}
                                             editable={false} placeholderTextColor="white"
                                             underlineColorAndroid='rgba(0,0,0,0)'
                                             value={this.state.user_name}
                                         />
                                     </ImageBackground>
                                 </View>

                                 <View style={styles.formInput}>
                                     <Text style={CommonStylesheet.formTitle}>WHO ARE YOU?</Text>
                                     <ImageBackground style={styles.gradientImage}>
                                         <TextInput style={styles.textInputCustomStyle}
                                             placeholderTextColor="white"
                                             underlineColorAndroid='rgba(0,0,0,0)'
                                             value={this.state.designation}
                                             onChangeText={this.editDesignation}
                                         />
                                     </ImageBackground>
                                 </View>

                                 <View style={styles.formInput}>
                                     <Text style={CommonStylesheet.formTitle}>INSTAGRAM:</Text>
                                     <ImageBackground style={styles.gradientImage}>
                                         <TextInput style={styles.textInputCustomStyle}
                                             placeholderTextColor="white"
                                             underlineColorAndroid='rgba(0,0,0,0)'
                                             value={this.state.instagram}
                                             onChangeText={this.editInstagram}

                                         />
                                     </ImageBackground>
                                 </View>

                                 <View style={styles.formInput}>
                                     <Text style={CommonStylesheet.formTitle}>TWITTER:</Text>
                                     <ImageBackground style={styles.gradientImage}>
                                         <TextInput style={styles.textInputCustomStyle}
                                             placeholderTextColor="white"
                                             underlineColorAndroid='rgba(0,0,0,0)'
                                             value={this.state.twitter}
                                             onChangeText={this.editTwitter}
                                         />
                                     </ImageBackground>
                                 </View>
                                 <TouchableOpacity
                                     style={[styles.jkStyle, styles2.saveButton]}
                                     onPress={this.updateIndividualProfile}>
                                     <Text style={styles2.saveButtonText}>SAVE</Text>
                                 </TouchableOpacity>
                                 <View style={styles.formInput}>
                                     <Text style={styles2.myGerneText}>MY GENRES</Text>
                                     <View style={[styles.gridView, { width: width }]}>
                                         <GridView
                                             itemDimension={(width - 50) / 4}
                                             items={this.state.connections}
                                             renderItem={this.renderItem}
                                             spacing={9} />
                                     </View>
                                 </View>
                             </View>
                         </View>
                     </View>
                 </KeyboardAwareScrollView>
                 {this.props.isLoading && <BTLoader />}
             </View>
         )
     }
}
export default withConnect(UserProfile);

const styles2 = StyleSheet.create({

    loaderView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center'
    },

    superView: {
        flex: 1,
        justifyContent: 'center'
    },

    KeyboardAwareScrollViewStyle: {
        backgroundColor: 'black'
    },

    uploadImageText: {
        marginTop: 15,
        marginLeft: 10
    },

    editImageButton: {
        position: 'absolute',
        bottom: 5,
        right: 5
    },

    editImage: {
        bottom: -5,
        right: -5,
        height: 40,
        width: 40
    },

    saveButton: {
        width: width - 20,
        position: 'relative',
        top: 15,
        bottom: 5,
        marginHorizontal: 15
    },

    saveButtonText: {
        color: "white",
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
    },

    myGerneText: {
        fontSize: 16,
        marginHorizontal: 10,
        marginTop: 20,
        color: 'white',
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
    },

    container: {
        flex: 1,
        backgroundColor: 'black'
    },

    parallaxHeader: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'column',
        paddingVertical: 10,
        justifyContent: 'center',
    },

    follwStyle: {
        left: 0,
        bottom: 0,
        height: (width - 50) / 4 * 0.4,
        width: (width - 50) / 4 * 0.4
    },

    unfollowStyle: {
        right: 0,
        top: 0,
        height: (width - 50) / 4 * 0.4,
        width: (width - 50) / 4 * 0.4
    },

    itemStyle: {
        marginBottom: 0,
        flex: 1,
        justifyContent: 'flex-end'
    },

    itemStyleFollow: {
        marginBottom: 0,
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'flex-start'
    },

    genreImage: {
        flex: 1,
        justifyContent: 'center',
        //resizeMode: 'contain',
        height: (width - 50) / 4,
        width: (width - 50) / 4
    },

    
    genreName: {
        color: 'white',
        position: 'absolute',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
        fontSize: 12
    },

    imgStyle: {
        height: height * 0.4,
        marginHorizontal: 10,
        width: width - 20,
        resizeMode: 'cover',
    },

    gradientImage: {
        top: 5,
        height: 40,
        width: width - 30,
        marginBottom: 10,
        justifyContent: 'center',
        paddingHorizontal: 10,
        backgroundColor: 'rgba(23,23,23,1)',
        borderWidth: 1,
        borderColor: 'rgba(39,39,39,1)',
    },
});
