import React ,{Component} from 'react'
import UserProfile from "./UserProfile/UserProfile";
import ArtistProfile from "./EditArtistProfile/ArtistProfile";
import VenueProfile from "./EditVenueProfile/VenueProfile";
import {LoginUser} from "../../Subscription/SubscriptionsData";

export default class ProfileTab extends Component{

    constructor(props) {
        super(props);
        this.state = {}
    }
    render(){

        switch(LoginUser.userType) {
            case 2:
                return <UserProfile/>
            case 3:
                return <ArtistProfile/>
            case 4:
                return <VenueProfile/>
            default:
                return <UserProfile/>;
        }


    }


}