import React from "react";
import { connect } from "react-redux";
import { auth, serviceApi } from "app/redux/actions";

function mapStateToProps(state) {
  const { user: userData } = state.auth;
  const { myProfile, genereList, list, profileImage } = state.serviceApi;
  const { isLoading } = state.shared;
  return { userData, myProfile, genereList, list, isLoading, profileImage };
}

function mapDispatchToProps(dispatch) {
  return {
    getProfile: () => dispatch(serviceApi.getProfile()),
    followUnFollow: ({ formData, dataKey }) =>
      dispatch(serviceApi.followUnfollow({ formData, dataKey })),
    uploadProfilePicture: formData =>
      dispatch(serviceApi.uploadProfilePicture(formData)),
    updateProfile: formData => dispatch(serviceApi.updateProfile(formData)),
    addorRemoveGenre: (formData, genreId) =>
      dispatch(serviceApi.addorRemoveGenre(formData, genreId))
  };
}

export default function withConnect(WrrapedComponent) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(WrrapedComponent);
}
