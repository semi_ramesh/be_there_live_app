import React, { Component } from "react";
import { View, StatusBar } from "react-native";
import Header from "./Header";
import AlbumArt from "./AlbumArt";
import TrackDetails from "./TrackDetails";
import SeekBar from "./SeekBar";
import Controls from "./Controls";
import Video from "react-native-video";

export default class Player extends Component {
  static defaultProps = {
    albumArt: null
  };
  constructor(props) {
    super(props);

    this.state = {
      paused: false,
      totalLength: 1,
      currentPosition: 0,
      selectedTrack: 0,
      repeatOn: false,
      shuffleOn: false,
      loading: false,
      isChanging: false
    };
  }

  setDuration = data => {
    // console.log(totalLength);
    this.setState({ totalLength: Math.floor(data.duration) });
    this.setState({
      loading: false
    });
  };

  setTime = data => {
    //console.log(data);
    this.setState({ currentPosition: Math.floor(data.currentTime) });
  };

  seek = time => {
    time = Math.round(time);
    this.audioElement && this.audioElement.seek(time);
    this.setState({
      currentPosition: time,
      paused: false
    });
  };

  onBack = () => {
    if (this.state.currentPosition < 10 && this.state.selectedTrack > 0) {
      this.audioElement && this.audioElement.seek(0);
      this.setState({ isChanging: true });
      setTimeout(
        () =>
          this.setState({
            currentPosition: 0,
            paused: false,
            totalLength: 1,
            isChanging: false,
            selectedTrack: this.state.selectedTrack - 1
          }),
        0
      );
    } else {
      this.audioElement.seek(0);
      this.setState({
        currentPosition: 0
      });
    }
  };

  onEnd = () => {
    this.setState({
      paused: true
    });
  };

  onForward = () => {
    if (this.state.selectedTrack < this.props.tracks.length - 1) {
      this.audioElement && this.audioElement.seek(0);
      this.setState({ isChanging: true });
      setTimeout(
        () =>
          this.setState({
            currentPosition: 0,
            totalLength: 1,
            paused: false,
            isChanging: false,
            selectedTrack: this.state.selectedTrack + 1
          }),
        0
      );
    }
  };

  loadStart = () => {
    this.setState({
      loading: true
    });
    console.log("ssdd", this.state.loading);
  };

  onVideoLoaded = () => {
    this.setState({
      loading: false
    });
    console.log("ssdd", this.state.loading);
  };

  videoError = e => {
    //this.header.
    alert("Some thing wrong with audio file.");
    this.setState({
      loading: false
    });

    //console.log("What is Error there",e)
  };

  onReadyForDisplay = () => {};

  render() {
    const track = this.props.tracks[this.state.selectedTrack];

    const video = this.state.isChanging ? null : (
      <Video
        source={{ uri: track.track_url }} // Can be a URL or a local file.
        ref={ref => (this.audioElement = ref)}
        paused={this.state.paused} // Pauses playback entirely.
        resizeMode="cover" // Fill the whole screen at aspect ratio.
        repeat={false} // Repeat forever.
        onLoadStart={this.loadStart} // Callback when video starts to load
        onVideoLoad={this.onVideoLoaded}
        onLoad={this.setDuration} // Callback when video loads
        onProgress={this.setTime} // Callback every ~250ms with currentTime
        onEnd={this.onEnd} // Callback when playback finishes
        onError={this.videoError} // Callback when video cannot be loaded
        style={styles.audioElement}
      />
    );
    let name = track.user_details ? track.user_details.name : "Artist";
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />
        <Header
          ref={ref => (this.header = ref)}
          message=""
          onDownPress={this.props.isDownPressed}
        />
        <AlbumArt
          url={
            this.props.albumArt === null
              ? require("./img/ic_queue_music_white.png")
              : { uri: this.props.albumArt }
          }
        />
        <TrackDetails title={track.track_title} artist={name} />
        <SeekBar
          onSeek={this.seek}
          trackLength={this.state.totalLength}
          onSlidingStart={() => this.setState({ paused: true })}
          currentPosition={this.state.currentPosition}
        />
        <Controls
          showLoader={this.state.loading}
          onPressRepeat={() =>
            this.setState({ repeatOn: !this.state.repeatOn })
          }
          repeatOn={this.state.repeatOn}
          shuffleOn={this.state.shuffleOn}
          forwardDisabled={
            this.state.selectedTrack === this.props.tracks.length - 1
          }
          onPressShuffle={() =>
            this.setState({ shuffleOn: !this.state.shuffleOn })
          }
          onPressPlay={() => this.setState({ paused: false })}
          onPressPause={() => this.setState({ paused: true })}
          onBack={this.onBack}
          onForward={this.onForward}
          paused={this.state.paused}
        />
        {video}
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: "rgb(4,4,4)"
  },
  audioElement: {
    height: 0,
    width: 0
  }
};
