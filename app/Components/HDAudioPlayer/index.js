import React, { Component } from "react";
import Player from "./Player";

export default class Index extends Component {
  render() {
    return (
      <Player
        {...this.props}
        tracks={this.props.tracks}
        isDownPressed={this.props.isDownPressed}
      />
    );
  }
}
