import React from "react";

import { View, StyleSheet, Image, TouchableOpacity } from "react-native";
import { MaterialIndicator } from "react-native-indicators";

const showLoaderOrShowPlayPause = (
  showLoader,
  paused,
  onPressPlay,
  onPressPause
) => {
  if (showLoader) {
    return (
      <View style={styles.playButton}>
        <MaterialIndicator size={30} color={"white"} />
      </View>
    );
  }
  return !paused ? (
    <TouchableOpacity onPress={onPressPause}>
      <View style={styles.playButton}>
        <Image source={require("./img/ic_pause_white_48pt.png")} />
      </View>
    </TouchableOpacity>
  ) : (
    <TouchableOpacity onPress={onPressPlay}>
      <View style={styles.playButton}>
        <Image source={require("./img/ic_play_arrow_white_48pt.png")} />
      </View>
    </TouchableOpacity>
  );
};

const Controls = ({
  paused,
  shuffleOn,
  repeatOn,
  onPressPlay,
  onPressPause,
  onBack,
  onForward,
  onPressShuffle,
  onPressRepeat,
  forwardDisabled,
  showLoader
}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity activeOpacity={0.0} onPress={onPressShuffle}>
        {/*<Image style={[styles.secondaryControl, shuffleOn ? [] : styles.off]}*/}
        {/*source={require('./img/ic_shuffle_white.png')}/>*/}
      </TouchableOpacity>
      <View style={{ width: 40 }} />
      <TouchableOpacity onPress={onBack}>
        <Image source={require("./img/ic_skip_previous_white_36pt.png")} />
      </TouchableOpacity>
      <View style={{ width: 20 }} />
      {showLoaderOrShowPlayPause(showLoader, paused, onPressPlay, onPressPause)}
      <View style={{ width: 20 }} />
      <TouchableOpacity onPress={onForward} disabled={forwardDisabled}>
        <Image
          style={[forwardDisabled && { opacity: 0.3 }]}
          source={require("./img/ic_skip_next_white_36pt.png")}
        />
      </TouchableOpacity>
      <View style={{ width: 40 }} />
      <TouchableOpacity activeOpacity={0.0} onPress={onPressRepeat}>
        {/*<Image style={[styles.secondaryControl, repeatOn ? [] : styles.off]}*/}
        {/*source={require('./img/ic_repeat_white.png')}/>*/}
      </TouchableOpacity>
    </View>
  );
};

export default Controls;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 8
  },
  playButton: {
    height: 60,
    width: 60,
    borderWidth: 1,
    borderColor: "white",
    borderRadius: 60 / 2,
    alignItems: "center",
    justifyContent: "center"
  },
});
