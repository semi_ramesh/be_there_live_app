import React from "react";

import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions
} from "react-native";

const AlbumArt = ({ url, onPress }) => (
  <View style={styles.container}>
    <TouchableOpacity onPress={onPress}>
      <Image style={styles.image} source={url} />
    </TouchableOpacity>
  </View>
);

export default AlbumArt;

const { width } = Dimensions.get("window");
const imageSize = width - 100;

const styles = StyleSheet.create({
  container: {
    paddingLeft: 50,
    paddingRight: 50
  },
  image: {
    width: imageSize,
    height: imageSize
  }
});
