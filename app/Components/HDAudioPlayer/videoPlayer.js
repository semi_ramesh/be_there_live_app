/* eslint-disable max-len */

import React, { Component } from "react";
import { View, StatusBar, Dimensions, TouchableOpacity } from "react-native";
import Header from "./Header";
import TrackDetails from "./TrackDetails";
import SeekBar from "./SeekBar";
import Controls from "./Controls";
import Video from "react-native-video";

export default class VideoPlayer extends Component {
  static defaultProps = {};
  constructor(props) {
    super(props);

    this.state = {
      paused: false,
      totalLength: 1,
      currentPosition: 0,
      selectedTrack: 0,
      repeatOn: false,
      shuffleOn: false,
      isChanging: false,
      loading: false
    };
  }

  setDuration = data => {
    // console.log(totalLength);
    this.setState({ totalLength: Math.floor(data.duration) });
    this.setState({
      loading: false
    });
  };

  setTime = data => {
    //console.log(data);
    this.setState({ currentPosition: Math.floor(data.currentTime) });
  };

  seek = time => {
    time = Math.round(time);
    this.audioElement && this.audioElement.seek(time);
    this.setState({
      currentPosition: time,
      paused: false
    });
  };

  onBack = () => {
    if (this.state.currentPosition < 10 && this.state.selectedTrack > 0) {
      this.audioElement && this.audioElement.seek(0);
      this.setState({ isChanging: true });
      setTimeout(
        () =>
          this.setState({
            currentPosition: 0,
            paused: false,
            totalLength: 1,
            isChanging: false,
            selectedTrack: this.state.selectedTrack - 1
          }),
        0
      );
    } else {
      this.audioElement.seek(0);
      this.setState({
        currentPosition: 0
      });
    }
  };

  onForward = () => {
    if (this.state.selectedTrack < this.props.tracks.length - 1) {
      this.audioElement && this.audioElement.seek(0);
      this.setState({ isChanging: true });
      setTimeout(
        () =>
          this.setState({
            currentPosition: 0,
            totalLength: 1,
            paused: false,
            isChanging: false,
            selectedTrack: this.state.selectedTrack + 1
          }),
        0
      );
    }
  };

  fullScreenView = () => {
    this.audioElement.presentFullscreenPlayer();
  };

  loadStart = () => {
    this.setState({
      loading: true
    });
  };

  onVideoLoaded = () => {
    this.setState({
      loading: false
    });
  };

  videoError = e => {
    alert("Some thing wrong with video.");
    this.setState({
      loading: false
    });
  };

  onEnd = () => {
    this.setState({
      paused: true
    });
  };

  render() {
    const track = this.props.tracks[this.state.selectedTrack];
    const video = this.state.isChanging ? null : (
      <Video
        source={{ uri: track.video_url }} // Can be a URL or a local file.
        ref={ref => (this.audioElement = ref)}
        paused={this.state.paused} // Pauses playback entirely.
        resizeMode="cover" // Fill the whole screen at aspect ratio.
        repeat={true} // Repeat forever.
        onLoadStart={this.loadStart} // Callback when video starts to load
        onLoad={this.setDuration} // Callback when video loads
        onProgress={this.setTime} // Callback every ~250ms with currentTime
        onEnd={this.onEnd} // Callback when playback finishes
        onError={this.videoError} // Callback when video cannot be loaded
        style={styles.audioElement}
      />
    );
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />
        <Header
          message="Be There Live"
          onDownPress={this.props.isDownPressed}
        />
        <View style={styles.container2}>
          <TouchableOpacity
            onPress={() => console.log("Do Some special Aanimation")}
          >
            {video}
          </TouchableOpacity>
        </View>
        <TrackDetails
          title={track.video}
          artist={"unknown"}
          onMorePress={this.fullScreenView}
        />
        <SeekBar
          onSeek={this.seek}
          trackLength={this.state.totalLength}
          onSlidingStart={() => this.setState({ paused: true })}
          currentPosition={this.state.currentPosition}
        />
        <Controls
          showLoader={this.state.loading}
          onPressRepeat={() =>
            this.setState({ repeatOn: !this.state.repeatOn })
          }
          repeatOn={this.state.repeatOn}
          shuffleOn={this.state.shuffleOn}
          forwardDisabled={
            this.state.selectedTrack === this.props.tracks.length - 1
          }
          onPressShuffle={() =>
            this.setState({ shuffleOn: !this.state.shuffleOn })
          }
          onPressPlay={() => this.setState({ paused: false })}
          onPressPause={() => this.setState({ paused: true })}
          onBack={this.onBack}
          onForward={this.onForward}
          paused={this.state.paused}
        />
      </View>
    );
  }
}

const { width} = Dimensions.get("window");
const imageSize = width - 48;
const styles = {
  container: {
    flex: 1,
    backgroundColor: "rgb(4,4,4)"
  },
  audioElement: {
    width: imageSize,
    height: imageSize
  },

  container2: {
    paddingLeft: 24,
    paddingRight: 24
  }
};
