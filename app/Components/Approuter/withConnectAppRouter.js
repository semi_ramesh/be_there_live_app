import { connect } from "react-redux";
import { Router } from "react-native-router-flux";
import { auth} from "app/redux/actions";
/**
 *Here what your reducers provide state according dispatch actions
 */
function mapStateToProps(state) {
  const { user: userData } = state.auth;

  return { userData };
}

/** dispatch your actions here according your Component **/
function mapDispatchToProps(dispatch) {
  return {
    onAuthorize: userData => dispatch(auth.authorize(userData)),
    onSignOut: userData => dispatch(auth.deAuthorize(userData))
  };
}

/** This the router to setup Navigation with Redux **/
export const ReduxRouter = Router;

/** This the Wrrapper function to wrrap up Component and Reduce state into Props**/
export default function withConnect(WrappedComponent) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(WrappedComponent);
}
