/*
 * @provideModule BTL.HDAudioPlayer.Components.Approuter
 */

import React from "react";
import { View, AsyncStorage } from "react-native";
import withConnect, { ReduxRouter } from "./withConnectAppRouter";
import styles from "./styles";
import { Scene, Stack, Actions, ActionConst } from "react-native-router-flux";
import {
  BTLoader,
  fontFamilies,
  RouterScenes
} from "../../constant/appConstant";
import Login from "app/Components/Authentication/Login";
import Registeration from "app/Components/Authentication/Registeration";
import ConGratulations from "app/Components/Authentication/ConGratulations";
import ForgetPassword from "app/Components/Authentication/ForgetPassword";

//TABS COMPONENT
import Profile from "app/Components/Profile/Profile";

import Artist from "app/Components/Artist/Artist";
import SpotLight from "app/Components/SpotLight/SpotLight";
import AlertFeed from "app/Components/AlertFeed/AlertFeed";
import Venue from "app/Components/Venue/Venue";
import { customOrange } from "../Authentication/styles";
import VenueDetails from "../Venue/VenueDetail";
import OTPVerify from "../Authentication/OTPVerify";
import EditProfile from "../Profile/tabs";
import Notifications from "../Profile/Notification/Notifications";
import Accounts from "../Profile/Account/Accounts";
import Subscription from "../Subscription/Subscription";
import SubscriptionInfo from "../Subscription/SubscriptionInfo";

import GenreList from "../Artist/GenreList";
import ArtistList from "../Artist/ArtistList";
import ResetPassword from "app/Components/Authentication/ResetPassword";
import SearchScreen from "app/Components/SearchModule/SearchModule";
import NetState from "react-native-net-state";
import SpotLightPurchase from "app/Components/SpotLight/SpotLightPurchase";
import SearchDeatilScreen from "app/Components/SearchModule/searchDetail";
import SpotLightPaymentSuccessFul from "app/Components/SpotLight/SpotLightPaymentSuccessFul";
import SplashScreen from "../Authentication/SplashScreen/SplashScreen";

const {
  LOGIN,
  SPLASH,
  FORGETPASSWORD,
  RESET_PASSWORD,
  ROOTSCENE,
  REGISTERATION,
  CONGRATULATIONS,

  //tabs
  ALERTFEED,
  ARTIST,
  SPOTLIGHT,
  PROFILE,
  VENUE,
  VENUEDETAILS,
  OTPSCREEN,
  EDITPROFILE,
  NOTIFICATIONS,
  ACCOUNTS,
  SUBSCRIPTION,
  SUBSCRIPTIONINFO,
  GENRELIST,
  ARTISTLIST,
  SEARCH_SCREEN,
  SPOT_LIGHT_PURCHASE,
  SPOT_LIGHT_CONGRATS,
  SEARCH_DETAIL_SCREEN
} = RouterScenes;

class Approuter extends React.PureComponent {
  /** some predetermined props **/
  static propTypes = {};

  /** some props having default value **/
  static defaultProps = {};

  /** this is state of Components **/

  state = {};

  get routerProps() {
    return {
      passProps: true,
      onSignOut: this.props.onSignOut,
      onAuthorize: this.onAuthorize,
      userData: this.props.userData,
      cardStyle: { backgroundColor: "black" }
    };
  }

  get authNavigatorProps() {}

  get navigatorProps() {}

  get mainContentProps() {}

  get tabProps() {}

  /** constructor of Components **/
  constructor(props) {
    super(props);
    this.onAuthorize = this.props.onAuthorize.bind(this);
    this.onSignOut = this.props.onSignOut.bind(this);

    this.state = {
      isLoading: false
    };
  }

  componentDidMount() {
    console.log("RootScreen", this.props.userData);
    this.switchTo();
  }

  switchTo = () => {
    this.setState({ isLoading: true });
    try {
      AsyncStorage.getItem("userData", (err, myData) => {
        console.log("UsersStoredData", myData);
        if (myData !== null) {
          this.onAuthorize(JSON.parse(myData));
          this.setState({ isLoading: false });
          Actions.Profile();

          //Actions.SUBSCRIPTION();
        } else {
          Actions.LOGIN();
        }

        this.setState({ isLoading: false });
      });
    } catch (e) {
      this.setState({ isLoading: false });
    }
  };

  onEnter = data => {
    console.log("__onEnter", data);
  };

  onBack = () => {
    if (Actions.currentScene === PROFILE) {
      return;
    }
    // console.log('itemBack',item);
  };

  render() {
    var sceneConfig = {
      cardStyle: {
        backgroundColor: "black"
      }
    };

    return (
      <View style={{ flex: 1, backgroundColor: "black" }}>
        <View>
          <NetState />
        </View>
        <ReduxRouter
          backAndroidHandler={this.onBack}
          {...this.routerProps}
          navigationBarStyle={styles.navBar}
          titleStyle={{
            fontSize: 15,
            color: customOrange,
            fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
            fontWeight: "300"
          }}
        >
          <Scene key={ROOTSCENE} panHandlers={null}>
            <SplashScreen key={SPLASH} component={SplashScreen} hideNavBar />
            <Scene
              {...sceneConfig}
              key={LOGIN}
              component={Login}
              title="SIGN-IN"
              backButtonTintColor={"black"}
              hideNavBar
              type={ActionConst.RESET}
            />
            <Scene
              key={REGISTERATION}
              component={Registeration}
              back
              backButtonTintColor={"white"}
              title="REGISTRATION"
              hideNavBar
            />
            <Scene
              key={OTPSCREEN}
              component={OTPVerify}
              back
              backButtonTintColor={"white"}
              backTitle={"BACK"}
              hideNavBar={false}
            />
            <Scene
              key={CONGRATULATIONS}
              component={ConGratulations}
              back
              backButtonTintColor={"white"}
            />
            <Scene
              key={FORGETPASSWORD}
              component={ForgetPassword}
              back
              backButtonTintColor={"white"}
              title="FORGOT PASSWORD"
              hideNavBar={true}
            />
            <Scene
              key={SUBSCRIPTION}
              component={Subscription}
              back
              backButtonTintColor={"white"}
              title={"SUBSCRIPTION"}
              hideNavBar={true}
            />
            <Scene
              key={SUBSCRIPTIONINFO}
              component={SubscriptionInfo}
              back
              backButtonTintColor={"white"}
              title={"SUBSCRIBE"}
              hideNavBar={true}
            />
            <Scene
              key={RESET_PASSWORD}
              component={ResetPassword}
              back
              backButtonTintColor={"white"}
              title={"RESET PASSWORD"}
              hideNavBar={true}
            />
            <Scene key="MainContent" type={ActionConst.RESET}>
              <Scene
                tabs
                legacy={true}
                lazy
                key={"Tabs"}
                tabBarPosition={"bottom"}
                tabBarStyle={styles.tabBarStyle}
                showIcon={false}
                tabBarOnPress={navigation => {
                  console.log("Navigataion", navigation);
                  if (navigation.scene.focused) {
                    if (Actions.currentScene === "Profile") {
                      return;
                    }
                    Actions.pop();
                  } else {
                    navigation.jumpToIndex(navigation.scene.index);
                  }
                }}
                labelStyle={item => {
                  console.log(
                    "labelStyle = {this.props.focused?styles.active:styles.inactive}",
                    item
                  );
                }}
                navigationBarStyle={{ backgroundColor: "white" }}
                swipeEnabled={false}
              >
                <Stack key={"PROFILE"} panHandlers={null}>
                  <Scene
                    key={PROFILE}
                    component={Profile}
                    back
                    hideNavBar
                    panHandlers={null}
                  />
                  <Scene
                    key={EDITPROFILE}
                    component={EditProfile}
                    back
                    hideNavBar
                  />
                  <Scene
                    key={NOTIFICATIONS}
                    component={Notifications}
                    back
                    hideNavBar
                  />
                  <Scene key={ACCOUNTS} component={Accounts} back hideNavBar />
                </Stack>
                <Stack key={"spotlight"} panHandlers={null}>
                  <Scene
                    key={SPOTLIGHT}
                    component={SpotLight}
                    back
                    hideNavBar
                  />
                  <Scene
                    key={SPOT_LIGHT_PURCHASE}
                    component={SpotLightPurchase}
                    back
                    hideNavBar
                  />
                </Stack>
                <Stack key={"Artists"} panHandlers={null}>
                  <Scene
                    key={GENRELIST}
                    component={GenreList}
                    back
                    hideNavBar
                  />
                  <Scene
                    key={ARTISTLIST}
                    component={ArtistList}
                    back
                    hideNavBar
                  />
                  <Scene key={ARTIST} component={Artist} back hideNavBar />
                </Stack>
                <Stack key={"Venues"} panHandlers={null}>
                  <Scene key={VENUE} component={Venue} back hideNavBar />
                  <Scene
                    key={VENUEDETAILS}
                    component={VenueDetails}
                    back
                    hideNavBar
                  />
                </Stack>
                <Scene key={ALERTFEED} component={AlertFeed} back hideNavBar />
              </Scene>
            </Scene>
            <Scene
              key={SEARCH_SCREEN}
              component={SearchScreen}
              back
              hideNavBar
            />
            <Scene
              key={SEARCH_DETAIL_SCREEN}
              component={SearchDeatilScreen}
              back
              hideNavBar
            />
            <Scene
              key={SPOT_LIGHT_CONGRATS}
              component={SpotLightPaymentSuccessFul}
              back
              hideNavBar
            />
          </Scene>
        </ReduxRouter>
        {this.state.isLoading && <BTLoader />}
      </View>
    );
  }
}

export default withConnect(Approuter);
