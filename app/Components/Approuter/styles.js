import React from "react";
import { StyleSheet } from "react-native";
import { customPurple } from "../Authentication/styles";

export default StyleSheet.create({
  superContainer: {
    flex: 1,
    backgroundColor: "red"
  },
  navBar: {
    backgroundColor: "black"
  },
  tabBarStyle: {
    backgroundColor: "rgba(18,0,20,1.0)",
    justifyContent: "center"
  },

  active: {
    fontSize: 13,
    color: customPurple,
    fontWeight: "bold",
    textAlign: "center"
  },

  inActive: {
    fontSize: 12,
    color: customPurple,
    textAlign: "center"
  }
});
