import React, { Component } from "react";
import { View, FlatList, ScrollView } from "react-native";
import styles from "./Style";
import NavigationBar from "app/Components/Reusables/CustomNavBar";
import { AlertList } from "./AlertData";
import withConnect from "./withConnectAlert";
import { BTLoader } from "../../constant/appConstant";
import NoDataComponent from "app/Components/Reusables/noData";
let allAlerts = [];

class AlertFeed extends Component {
  constructor(prop) {
    super(prop);
    this.state = {
      Alert: [],
      page: 0,
      loading: true
    };
    this.getAlertFeed = this.props.getAlertFeed.bind(this);
  }

  componentDidMount() {
    this.getAlertFeed();
    //let allAlerts = [];
    // this.getData(0);
  }
  getData = num => {
    for (let i = num; i <= num + 10; i++) {
      let new_element = {
        title: `Some alert title ${i}`,
        message:
          "More complex, multi-select example demonstrating PureComponent usage for perf optimization and avoiding bugs."
      };
      allAlerts.push(new_element);
    }
    this.setState({
      Alert: allAlerts,
      loading: false
    });
  };

  componentWillReceiveProps(nextProps) {
    console.log("AlertFeed:", nextProps);
    if (nextProps.alertFeeds) {
      this.setState({
        Alert: nextProps.alertFeeds.data.alerts
      });
    }
  }

  onSearch = () => {
    console.log("hii");
  };

  loadMoreData = () => {
    this.setState({
      page: this.state.page + 1,
      loading: true
    });
    this.getData(this.state.page * 10);
  };

  renderProirInfo = () => {};

  alertData = (data, key) => {
    return <AlertList data={data} />;
  };

  render() {
    if (this.props.isLoading) {
      return (
        <View style={styles.topView}>
          <BTLoader />
        </View>
      );
    }
    if (this.state.Alert.length === 0) {
      return (
        <NoDataComponent
          text={" There are no alertfeed for you."}
          reloadData={this.getAlertFeed}
          isLoading={this.props.isLoading}
          title={"ALERTS"}
        />
      );
    }

    return (
      <View style={styles.topView}>
        <NavigationBar title={"ALERTS"} onSearch={this.onSearch} />
        <ScrollView>
          <View style={styles.container}>
            {this.state.Alert.length > 0 && (
              <FlatList
                keyExtractor={(item, index) => item + index}
                data={this.state.Alert}
                renderItem={({ item, index }) => this.alertData(item, index)}
              />
            )}
          </View>
        </ScrollView>
        {this.props.isLoading && <BTLoader />}
      </View>
    );
  }
}

export default withConnect(AlertFeed);
