
import { connect } from "react-redux";
import { serviceApi } from "app/redux/actions";

function mapStateToProps(state) {
  const { user: userData } = state.auth;
  const { alertFeeds } = state.serviceApi;
  const { isLoading } = state.shared;
  return { userData, alertFeeds, isLoading };
}

function mapDispatchToProps(dispatch) {
  return {
    getAlertFeed: formData => dispatch(serviceApi.getAlertFeed(formData))
  };
}

export default function withConnect(WrrapedComponent) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(WrrapedComponent);
}
