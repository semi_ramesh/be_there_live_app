
import { StyleSheet, Dimensions } from "react-native";
import { customOrange} from "../Authentication/styles";
import { fontFamilies } from "../../constant/appConstant";
const { width } = Dimensions.get("window");
export default StyleSheet.create({
  topView: {
    backgroundColor: "#000000",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  container: {
    flex: 10,
    backgroundColor: "black",
    paddingHorizontal: 5
  },
  whiteText: {
    color: "white",
    fontSize: 16
  },
  gradientImage: {
    top: 0,
    height: 40,
    width: 150,
    marginBottom: 10
    // marginLeft:20,
  },

  orangeTextStyle: {
    marginBottom: 10,
    left: 10,
    fontSize: 24,
    color: customOrange,
    fontWeight: "300"
  },

  alertTitle: {
    color: customOrange,
    fontSize: 14,
    fontFamily: fontFamilies.ITCAvantGardeStdBook
    // fontWeight:'600'
  },

  alertMessage: {
    color: "white",
    fontSize: 12,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },

  alertContainer: {
    marginTop: 10,

    borderWidth: 1,
    flex: 1,
    width: width - 20,
    borderRadius: 0,
    backgroundColor: "#171717",
    padding: 10
  }
});
