/***
 * @provideModule  BTL.Components.AlertData.AlertData
 */

import React from "react";
import styles from "./Style";
import { Actions } from "react-native-router-flux";
import { Text, TouchableOpacity } from "react-native";
export class AlertList extends React.PureComponent {
  componentDidMount() {
    //console.log("componentDidMount",this.props)
  }

  goToRelaventView = item => {

    switch (item.notification_for) {
      case "venue_event": {
        Actions.VENUEDETAILS({
          venueId: item.sender_id,
          popScene: "AlertFeed"
        });
        return;
      }
      case "artist_notification": {
        Actions.Artist({ artistId: item.sender_id, popScene: "AlertFeed" });
        return;
      }
      default: {
        alert(item.description);
        return;
      }
    }
  };

  render() {
    return (
      <TouchableOpacity
        style={styles.alertContainer}
        onPress={() => this.goToRelaventView(this.props.data)}
      >
        <Text style={styles.alertTitle}>{this.props.data.title}</Text>
        <Text style={styles.alertMessage}>{this.props.data.description}</Text>
      </TouchableOpacity>
    );
  }
}
