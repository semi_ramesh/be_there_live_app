import React, { Component } from "react";
import { TouchableOpacity, Text } from "react-native";
import { customOrange } from "../../Authentication/styles";
import { fontFamilies } from "../../../constant/appConstant";

export default class BTLButton extends Component {
  static defaultProps = {
    titleColor: "white"
  };
  render() {
    return (
      <TouchableOpacity
        style={[styles.button, this.props.btnStyle]}
        {...this.props}
      >
        <Text
          style={[
            this.props.textStyle,
            styles.textStyle,
            { color: this.props.titleColor }
          ]}
        >
          {this.props.title}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = {
  textStyle: {
    //color:'white',
    fontSize: 15,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
    textAlign: "center"
  },
  button: {
    marginVertical: 8,
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    flex: 1,

    marginHorizontal: 8,
    backgroundColor: customOrange
  }
};
