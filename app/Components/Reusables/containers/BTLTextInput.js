import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet
} from "react-native";
import { customPurple } from "../../Authentication/styles";
import { fontFamilies } from "../../../constant/appConstant";

export default class BTLTextInput extends Component {
  static defaultProps = {
    isDate: false
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  componentWillReceiveProps(nextprops) {}

  render() {
    return (
      <View style={styles.container}>
        {this.props.label && (
          <Text style={[styles.textStyle, this.props.labelStyle]}>
            {this.props.label}
          </Text>
        )}
        <View style={styles.textInputContainer}>
          {this.props.isDate && (
            <TouchableOpacity
              style={{ justifyContent: "center", height: 46 }}
              onPress={this.props.onTap}
            >
              <Text
                style={[
                  styles.textInput,
                  { justifyContent: "center", top: 14 }
                ]}
              >
                {this.props.date && this.props.date !== ""
                  ? this.props.date
                  : this.props.placeholder}
              </Text>
            </TouchableOpacity>
          )}
          {!this.props.isDate && (
            <TextInput
              underlineColorAndroid={"transparent"}
              placeholderTextColor={"rgba(255,255,255,0.5)"}
              style={[this.props.style, styles.textInput]}
              {...this.props}
            />
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 60,
    paddingVertical: 5,
    paddingHorizontal: 8,
    marginVertical: 10
  },

  textInputContainer: {
    height: 46,
    marginTop: 5,
    borderWidth: 1,
    paddingHorizontal: 4,
    borderColor: "rgba(76,76,76,1.0)",
    backgroundColor: "#171717"
  },
  textStyle: {
    color: customPurple,
    fontSize: 13,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },

  textInput: {
    color: "white",
    fontSize: 13,
    height: 46,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  }
});
