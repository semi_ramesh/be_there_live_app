/**
 * @ProvidesModule StripePaymentGateway
 **/

import React, { Component } from "react";
import { View, TouchableOpacity, Image, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import {
  BTLoader,
  fontFamilies,
  showInfoMessage,
  stripApiKey,
  BTL_REDIRECT_URL
} from "app/constant/appConstant";
import BTLTextInput from "./BTLTextInput";
import Modal from "react-native-modalbox";

export default class StripePaymentGatway extends Component {
  state = {};
  static defaultProps = {};
  static propTypes = {};

  constructor(props) {
    super(props);
  }
  componentDidMount() {}
  render() {
    return (
      <View style={styles.container}>

        <BTLTextInput/>
        <BTLTextInput/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 5,
    backgroundColor: "black",
    flex: 1
  }
});
