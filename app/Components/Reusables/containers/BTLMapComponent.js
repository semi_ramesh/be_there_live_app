/**
 * @ProvidesModule BTL.MapComponent
 *
 */

import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import MapView, { Marker, Callout } from "react-native-maps";

export default class BTLMapComponent extends Component {
  static defaultProps = {
    latitude: 26.9124,
    longitude: 75.7873,
    onUserPressed: () => {},
    onMapPressed: () => {}
  };

  static propTypes = {};

  constructor(props) {
    super(props);
    this.state = {
      initialRegion: {},

      region: {},

      userLatitudeLongitude: {}
    };
  }

  componentDidMount() {}

  componentWillReceiveProps(nextProps) {}

  onMapPress = () => {};

  gettingUserLocation = () => {};

  onUserImageClicked = () => {};

  onMarkerPress = () => {};

  render() {
    return (
      <View style={styles.mapContainer}>
        <MapView
          style={StyleSheet.absoluteFillObject}
          region={this.state.region}
          onPress={this.onMapPress}
          initialRegion={this.state.initialRegion}
          showsUserLocation={true}
        >
          <Marker coordinate={this.state.userLatitudeLongitude} zIndex={9999} />
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mapContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});
