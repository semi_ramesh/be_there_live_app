import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { deviceWidth } from "../../../constant/appConstant";

export default class FullWidthButton extends React.PureComponent {
  static defaultProps = {
    isSpotLighted: false
  };
  constructor(props) {
    super(props);
  }

  handlePress = () => {
    // if (this.props.isSpotLighted){
    //     alert('we are working on it');
    //     return;
    // }
    this.props.OnTap();
  };

  render() {
    let color = this.props.isSpotLighted ? "rgba(36,8,0,1.0)" : "#3A1540";
    let textColor = this.props.isSpotLighted ? "#E53100" : "#DA2DF9";
    let justifycontent = this.props.isSpotLighted ? "center" : "flex-start";
    const FontWeightStyle = this.props.isSpotLighted ? "800" : "normal";
    const paddingLeft = this.props.isSpotLighted ? (deviceWidth - 100) / 2 : 10;
    return (
      <View style={{ marginTop: 10 }}>
        <TouchableOpacity
          style={[
            {
              height: 45,
              borderWidth: 0,
              backgroundColor: color,
              flexDirection: "row",
              alignItems: "center",
              paddingLeft: paddingLeft,
              justifycontent: justifycontent,
              // marginLeft:15,
              marginRight: 10,
              flex: 1
            },
            this.props.style
          ]}
          onPress={this.handlePress}
        >
          {!this.props.isSpotLighted && (
            <Icon
              name={"plus"}
              color={"#E53100"}
              size={21}
              style={{ paddingRight: 10 }}
            />
          )}
          <Text style={{ color: textColor, fontWeight: FontWeightStyle }}>
            {this.props.title}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
