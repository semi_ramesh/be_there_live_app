import React from "react";
import { Images } from "../../../constant/images";
import { ImageBackground } from "react-native";
import { FormInput } from "react-native-elements";

export default class InputBox extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  handleInput = event => {
    this.props.OnInput(event);
  };

  render() {
    return (
      <ImageBackground source={Images.gradientBg} style={{ width: null }}>
        <FormInput
          onChange={e => this.handleInput(e)}
          placeholderTextColor="white"
          underlineColorAndroid="rgba(0,0,0,0)"
          value={this.props.value}
          name={this.props.name}
        />
      </ImageBackground>
    );
  }
}
