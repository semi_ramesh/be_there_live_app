
import FullWidthButton from "./containers/FullWidthButton";
import InputBox from "./containers/InputBox";

const Reuseables = {
  get FullWidthButton() {
    return FullWidthButton;
  },
  get InputBox() {
    return InputBox;
  }
};

module.exports = Reuseables;
