/**
 * @providesModule BTL.Components.Reusables.RefreshableList
 */

import React from "react";
import { ListView, RefreshControl } from "react-native";
import _ from "lodash";
//import  {DeviceUtils} from  './utils/DeviceUtils'
// Constants
import { DUMMY } from "app/constant/appConstant";

// Utils
// import Logger from 'HDAudioPlayer/utils/Logger';

/* eslint-disable no-underscore-dangle */

const DEFAULT_DATASOURCE = new ListView.DataSource({
  getRowData: (dataBlob, sectionID, rowID) =>
    dataBlob[sectionID][parseInt(rowID)],
  getSectionHeaderData: sectionID => sectionID,
  rowHasChanged: (r1, r2) => r1 !== r2,
  sectionHeaderHasChanged: (s1, s2) => s1 !== s2
});

/** Here we merging the previus data along with updated data**/
function mergeData(original, updated) {
  if (_.isEmpty(original)) {
    return updated;
  }

  const allKeys = _.uniq(_.flatMap([original, updated], _.keys));
  const allValues = allKeys.map(key =>
    _.compact([...original[key], ...updated[key]])
  );
  return _.zipObject(allKeys, allValues);
}

/** Here we transforming the dataBlob **/
function transformDataBlob(dataBlob) {
  if (_.isArray(dataBlob) && _.isArray(dataBlob[0])) {
    return dataBlob;
  }

  if (_.isPlainObject(dataBlob)) {
    const firstKey = _.keys(dataBlob)[0];
    const { [firstKey]: firstChild } = dataBlob;

    if (_.isPlainObject(firstChild) || _.isArray(firstChild)) {
      return dataBlob;
    }
  }

  return { [DUMMY.DEFAULT_SECTION_ID]: dataBlob };
}

/**
 * @README:
 * Function to check whether a transformed dataBlob is empty or not
 * First map all empty check of its values (`_.map` works for both Arrays & Plain objects)
 * Then reduce results (array of booleans) using `&&` operator.
 * This function can be written verbosely in pseudo-code as:
 *  return isEmpty(valuesOf(dataBlob)[0]) && isEmpty(valuesOf(dataBlob)[1]) && ...
 */

export function isEmptyDataBlob(dataBlob) {
  return _.reduce(
    _.map(dataBlob, _.isEmpty),
    (final, element) => final && element
  );
}

/** This class is what we need for make RefreshableList in all over project **/
export default class RefreshableList extends React.PureComponent {
  static propTypes = {
    // autoLoad: PropTypes.bool,
    // dataBlob: React.PropTypes.any.isRequired,
    // onLoadMore: React.PropTypes.func,
    // onRefresh: React.PropTypes.func,
    // renderEmptyData: React.PropTypes.func,
    // renderPlaceholder: React.PropTypes.func,
    // renderRow: React.PropTypes.func.isRequired,
  };

  static defaultProps = {
    autoLoad: true,
    onLoadMore: null,
    onRefresh: null,
    renderEmptyData: null,
    renderPlaceholder: null
  };

  constructor(props) {
    super(props);

    this.mounted = false;

    const {
      onLoadMore,
      onRefresh,
      renderEmptyData,
      renderPlaceholder,
      renderRow
    } = this.props;

    this.onLoadMore = onLoadMore == null ? () => {} : onLoadMore.bind(this);
    this.onRefresh = onRefresh == null ? () => {} : onRefresh.bind(this);
    this.renderEmptyData =
      renderEmptyData == null ? () => null : renderEmptyData.bind(this);
    this.renderPlaceholder =
      renderPlaceholder == null ? () => null : renderPlaceholder.bind(this);
    this.renderRow = renderRow == null ? () => {} : renderRow.bind(this);

    this.state = this.defaultState;
  }

  /** This is what first time our RefreshableList is loaded or mounted **/
  componentDidMount() {
    this.mounted = true;
    this.props.autoLoad && this.reload();
  }

  /** Once Components will get update means dataBlob will be change we need to make changes here **/
  componentDidUpdate(prevProps) {
    const { dataBlob } = this.props;
    const { dataBlob: prevDataBlob } = prevProps;

    if (dataBlob !== prevDataBlob) {
      this.populateData();
    }
  }

  /** componentWillUnmount  change mounted bool here**/
  componentWillUnmount() {
    this.mounted = false;
  }

  /** Data source getting method **/
  get emptyDataSource() {
    return DEFAULT_DATASOURCE.cloneWithRowsAndSections({
      [DUMMY.EMPTY_DATA]: { [DUMMY.EMPTY_DATA]: { empty: true } }
    });
  }
  /** Here we are getting place holder datasource for RefreshableList **/
  get placeholderDataSource() {
    return DEFAULT_DATASOURCE.cloneWithRowsAndSections({
      [DUMMY.PLACEHOLDER_DATA]: {
        [DUMMY.PLACEHOLDER_DATA]: { placeholder: true }
      }
    });
  }

  /** These will be get  defaultState of this Components RefreshableList **/
  get defaultState() {
    return {
      currentPage: 1,
      dataSource: DEFAULT_DATASOURCE,
      isLastPageReached: false,
      isLoadingMore: false,
      isRefreshing: false,
      isReloading: false
    };
  }
  /** DataSourceProps i guess they are taking about defaultProps **/
  /** This is what actully RefreshableList will display in Index **/
  get dataSourceProps() {
    const {
      dataSource: currentDataSource,
      isLoadingMore,
      isRefreshing,
      isReloading
    } = this.state;

    const isEmptyData = isEmptyDataBlob(currentDataSource._dataBlob);

    const placeholderDataSource = isReloading
      ? this.placeholderDataSource
      : currentDataSource;
    const dataSource =
      !isLoadingMore && !isRefreshing && isEmptyData
        ? this.emptyDataSource
        : placeholderDataSource;

    const placeholderRenderer = isReloading
      ? this.renderPlaceholder
      : this.renderRow;
    const renderRow =
      !isLoadingMore && !isRefreshing && isEmptyData
        ? this.renderEmptyData
        : placeholderRenderer;

    return { dataSource, renderRow };
  }

  // Public methods
  appendFirst = dataBlob => {
    const { dataSource } = this.state;
    const appendData = transformDataBlob(dataBlob);
    const newData = mergeData(appendData, dataSource._dataBlob);

    const newState = {
      dataSource: dataSource.cloneWithRowsAndSections(newData)
    };
    this.setState(newState, () => this.resetScrollOffset(true));
  };

  cancel = () => this.updateStates(this.defaultState);
  reload = () => {
    const { isRefreshing, isReloading } = this.state;
    if (isRefreshing || isReloading) {
      return;
    }

    const newState = { ...this.defaultState, isReloading: true };
    this.updateStates(newState, this.onRefresh);
  };

  loadMoreData = () => {
    const {
      currentPage,
      isLoadingMore,
      isLastPageReached,
      isRefreshing,
      isReloading
    } = this.state;
    if (isLoadingMore || isLastPageReached || isRefreshing || isReloading) {
      return;
    }

    const newState = { currentPage: currentPage + 1, isLoadingMore: true };
    this.updateStates(newState, () => this.onLoadMore(currentPage + 1));
  };

  refreshData = () => {
    const newState = { currentPage: 1, isRefreshing: true };
    this.updateStates(newState, this.onRefresh);
  };

  // Private methods
  resetScrollOffset = isRefreshing =>
    isRefreshing && this.listView.scrollTo({ animated: false, y: 0 });

  /* eslint-disable no-underscore-dangle */
  populateData = () => {
    const { dataSource, isLoadingMore, isRefreshing } = this.state;

    // xxx Only append data when loading more but not refreshing xxx
    //FIXME: Refreshing and Load More Issues
    const originalData =
      !isRefreshing && isLoadingMore ? dataSource._dataBlob : {};

    const updatedData = transformDataBlob(this.props.dataBlob);
    const newData = mergeData(originalData, updatedData);
    const isLastPageReached = isEmptyDataBlob(updatedData);

    const newState = {
      isLastPageReached,
      dataSource: dataSource.cloneWithRowsAndSections(newData),
      isLoadingMore: false,
      isRefreshing: false,
      isReloading: false
    };

    this.updateStates(newState, () => this.resetScrollOffset(isRefreshing));
  };
  /* eslint-enable no-underscore-dangle */

  // State helpers
  updateStates = (newState, callback) => {
    if (!this.mounted) {
      return;
    }
    this.setState(newState, callback);
  };

  renderRefreshControl = () => (
    <RefreshControl
      refreshing={this.state.isRefreshing}
      onRefresh={this.refreshData}
      progressViewOffset={220}
    />
  );

  render() {
    const { onLoadMore, onRefresh } = this.props;

    const loadMoreProps =
      onLoadMore !== null ? { onEndReached: this.loadMoreData } : null;
    const refreshProps =
      onRefresh !== null
        ? { refreshControl: this.renderRefreshControl() }
        : null;

    return (
      <ListView
        {...this.props}
        enableEmptySections
        ref={ref => (this.listView = ref)}
        removeClippedSubviews={true}
        keyboardShouldPersistTaps={true}
        {...this.dataSourceProps}
        {...loadMoreProps}
        {...refreshProps}
      />
    );
  }
}

/* eslint-enable no-underscore-dangle */
