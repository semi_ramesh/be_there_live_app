import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import Button from "./Button";
import NavigationBar from "./CustomNavBar";
import { fontFamilies } from "../../constant/appConstant";
export default class NoDataComponent extends Component {
  static defaultProps = {
    isLoading: false,
    text: "Something went wrong please try again.",
    reloadData: () => {},
    title: "BTL"
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <NavigationBar
          title={this.props.title}
          onSearch={() => {
            console.log("print on search");
          }}
          back={this.props.back}
        />
        <View style={styles.content}>
          <Text
            style={{
              color: "white",
              fontSize: 14,
              fontFamily: fontFamilies.ITCAvantGardeStdBookCn
            }}
          >
            {this.props.text}
          </Text>
          <Button
            text={"Reload"}
            onPress={() => this.props.reloadData()}
            loading={this.props.isLoading}
            textStyle={{ color: "white" }}
            style={styles.btn}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    top: 0,
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "black"
  },
  content: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "black"
  },
  btn: {
    backgroundColor: "#3A1540",
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 0

    //borderColor:'white',
    // borderWidth:1
  }
});
