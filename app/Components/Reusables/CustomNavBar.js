import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  StyleSheet,
  Dimensions
} from "react-native";
const { width } = Dimensions.get("window");
import { customOrange } from "../Authentication/styles";
import { Actions } from "react-native-router-flux";
import { Images } from "../../constant/images";

import { fontFamilies } from "app/constant/appConstant";

export default class NavigationBar extends Component {
  goToSearch = () => {
    Actions.Search();
  };

  backToPrevious = () => {
    if (Actions.currentScene === this.props.popScene) {
      console.log("Return back");
      return;
    }
    console.log("something wrong here", this.props.popScene);

    if (this.props.isLoading) {
      console.log("click before");
      return;
    }

    if (this.props.sceneKey) {
      //Actions.popTo(this.props.sceneKey)
      Actions[this.props.sceneKey]();
      return;
    }
    if (this.props.popScene === "AlertFeed") {
      Actions.pop();
      Actions.refresh({ popScene: "" });
      //Actions.popTo('AlertFeed');
      return;
    }
    if (this.props.popScene === "SpotLight") {
      Actions.SpotLight();
      return;
    }
    // if(this.props.popScene === 'Venue'){
    //     // Actions.pop();
    //     // Actions.refresh({popScene:''})
    //     Actions.popTo('_Venue');
    //     return;
    // }
    // if(this.props.popScene === 'Venue'){
    //   //  Actions.pop();
    //   //Actions.currentScene.props.navigationOptoions()
    // }
    if (this.props.popScene) {
      Actions.popTo(this.props.popScene);
      return;
    }

    Actions.pop();
  };

  render() {
    const { title, onSearch, back, search, align } = this.props;
    let textInput = !search ? "search" : "";
    return (
      <View style={styles.navBar}>
        <TouchableOpacity style={{ flex: 1 }}>
          {back !== undefined && back === true && (
            <TouchableOpacity
              style={{ height: 60, justifyContent: "center" }}
              onPress={this.backToPrevious}
            >
              <Text style={styles.backTextColor}>{"< BACK"}</Text>
            </TouchableOpacity>
          )}
        </TouchableOpacity>
        <View
          style={{ flex: 2, justifyContent: "center", alignItems: "center" }}
        >
          <Text style={styles.textColor}>{title}</Text>
        </View>
        <View style={{ flex: 1 }}>
          {!search && (
            <TouchableOpacity
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "flex-end"
              }}
              onPress={this.goToSearch}
            >
              <Image
                source={Images.searchIcon}
                style={{ height: 20, width: 20 }}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}

/** styles are **/
const styles = StyleSheet.create({
  navBar: {
    height: 60,
    width: width,
    flex: 0,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "black",
    alignItems: "center",
    paddingHorizontal: 15,
    paddingTop: 8
  },
  textColor: {
    /*color:'white',*/
    fontFamily: fontFamilies.ITCAvantGardeStdBook,
    color: customOrange,
    fontSize: 16
    //fontWeight:'600',
    //backgroundColor:'gray'
  },
  backTextColor: {
    /*color:'white',*/
    color: "white",
    fontSize: 10,
    fontWeight: "200",
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
    //backgroundColor:'gray'
  }
});
