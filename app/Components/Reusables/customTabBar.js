import React from "react";

import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity
} from "react-native";
import { Images } from "app/constant/images";
import { customPurple } from "../Authentication/styles";
import { Actions } from "react-native-router-flux";

export default class customTabBar extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentDict: {
        profile: true,
        spotLight: false,
        artist: false,
        venue: false,
        alertFeed: false
      }
    };
  }

  onPressProfile = () => {
    this.setState({
      currentDict: {
        profile: true,
        spotLight: false,
        artist: false,
        venue: false,
        alertFeed: false
      }
    });
    Actions.Profile();
  };

  onPressSpotLight = () => {
    Actions.SpotLight();
    this.setState({
      currentDict: {
        profile: false,
        spotLight: true,
        artist: false,
        venue: false,
        alertFeed: false
      }
    });
  };

  onPressArtist = () => {
    Actions.GENRELIST();
    this.setState({
      currentDict: {
        profile: false,
        spotLight: false,
        artist: true,
        venue: false,
        alertFeed: false
      }
    });
  };

  onPressVenue = () => {
    this.setState({
      currentDict: {
        profile: false,
        spotLight: false,
        artist: false,
        venue: true,
        alertFeed: false
      }
    });
    Actions.Venue();
  };

  onPressAlertFeed = () => {
    this.setState({
      currentDict: {
        profile: false,
        spotLight: false,
        artist: false,
        venue: false,
        alertFeed: true
      }
    });
    Actions.AlertFeed();
  };

  render() {
    const { currentDict } = this.state;
    const active1 = currentDict.profile ? styles.active : styles.inActive;
    const active2 = currentDict.spotLight ? styles.active : styles.inActive;
    const active3 = currentDict.artist ? styles.active : styles.inActive;
    const active4 = currentDict.venue ? styles.active : styles.inActive;
    const active5 = currentDict.alertFeed ? styles.active : styles.inActive;

    return (
      <View>
        <View
          source={Images.tabBarImage}
          style={[styles.barStyle, { backgroundColor: "rgba(18,0,20,1.0)" }]}
        >
          <TouchableOpacity style={styles.button} onPress={this.onPressProfile}>
            <Text style={active1}>PROFILE</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={this.onPressSpotLight}
          >
            <Text style={active2}>SPOTLIGHT</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={this.onPressArtist}>
            <Text style={active3}>ARTIST</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={this.onPressVenue}>
            <Text style={active4}>VENUE</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={this.onPressAlertFeed}
          >
            <Text style={active5}>ALERTFEED</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const { width } = Dimensions.get("window");
const styles = StyleSheet.create({
  barStyle: {
    flexDirection: "row",
    flex: 0,
    height: 60,
    width: width,
    justifyContent: "space-around",
    alignItems: "center"
  },
  active: {
    fontSize: 13,
    color: customPurple,
    fontWeight: "bold",
    textAlign: "center"
  },

  button: {
    marginHorizontal: 4,
    //width:width/5,
    height: 60,
    flex: 0,
    justifyContent: "center",
    alignItems: "center"
  },

  inActive: {
    fontSize: 12,
    color: customPurple,
    textAlign: "center"
  }
});
