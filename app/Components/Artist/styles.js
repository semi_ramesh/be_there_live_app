import { StyleSheet, Dimensions } from "react-native";
import { customOrange } from "../Authentication/styles";

const { height, width } = Dimensions.get("window");

export default StyleSheet.create({
  topView: {
    backgroundColor: "#000000",
    flex: 1,
    alignItems: "stretch",
    justifyContent: "center"
  },
  container: {
    flex: 10,
    backgroundColor: "black"
  },
  whiteText: {
    color: "white",
    fontSize: 16
  },
  gradientImage: {
    top: 0,
    height: 40,
    width: 150,
    marginBottom: 10,
    marginLeft: 20
  },

  orangeTextStyle: {
    marginBottom: 10,
    left: 10,
    fontSize: 24,
    color: customOrange,
    fontWeight: "300"
  },
  socialInfo: {
    flex: 0,
    flexDirection: "row",
    alignItems: "center"
  },
  socialIcon: {
    marginTop: 4,
    height: 12,
    width: 12,
    resizeMode: "contain",
    marginRight: 5
  },
  socialText: {
    fontSize: 16,
    color: "white"
  },
  editBackG: {
    marginTop: 10,
    height: 40,
    width: 150,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "black"
  },
  editText: {
    textAlign: "center",
    color: "white",
    fontSize: 16
  },
  editBtnStyle: {
    height: 40,
    width: 150
  },
  countPlate: {
    flex: 0,
    marginTop: 10,
    height: 70,
    justifyContent: "space-between",
    flexDirection: "row",
    backgroundColor: "black"
  },
  plates: {
    height: 70,
    width: width / 5 - 5,
    alignItems: "center"
  },
  itemStyle: {
    height: width / 2,
    width: width / 2,
    alignItems: "flex-start",
    justifyContent: "flex-end"
  },
  minus: {
    height: width / 5,
    width: width / 5,
    alignItems: "flex-start",
    justifyContent: "flex-end",
    backgroundColor: "transparent"
  },
  swipper: {
    flex: 1,
    backgroundColor: "gray",
    height: height / 2.3
  },
  crousel: {
    flex: 2,
    backgroundColor: "green"
  },
  listing: {
    flex: 2,
    backgroundColor: "black"
  },

  wrapper: {},
  slide1: {
    // flex: 1,
    // justifyContent: 'flex-start',
    // alignItems: 'flex-end',
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold"
  },

  followBtn: {
    top: 0,
    position: "absolute",
    right: 10,
    backgroundColor: "transparent",
    flex: 1,
    alignItems: "flex-start"
  },
  followImage: {
    right: -18,
    height: 70,
    width: 120,
    resizeMode: "contain"
  },
  previewContainer: {
    backgroundColor: "red",
    flex: 1,
    flexDirection: "row"
  },
  thumbnailContainer: {
    flex: 2,
    backgroundColor: "yellow"
  },
  thumbnail: {
    height: height / 3,
    flex: 0,
    alignItems: "center",
    justifyContent: "center"
  },

  playBtnImage: {
    height: 100,
    width: 100
  },
  playBtn: {
    position: "absolute",
    padding: 100,
    flex: 1,
    backgroundColor: "transparent"
  },
  singerBtn: {
    height: 100,
    width: 100,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 5,
    backgroundColor: "red"
  },
  singerImage: {
    height: 100,
    width: 100,
    resizeMode: "contain"
  },
  sideList: {
    flex: 1,
    backgroundColor: "green"
  },
  album: {
    marginTop: 8,
    flex: 1
  }
});
