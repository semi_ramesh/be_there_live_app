import { connect } from "react-redux";
import { serviceApi } from "app/redux/actions";

function mapStateToProps(state) {
  const { isLoading } = state.shared;
  const { user: userData } = state.auth;
  const {
    artistGenreList,
    artistList,
    artistProfile,
    artistListOfGenre,
    genreListArtist,
    latestArtistsList,
    allArtistsList,
    artistFollow
  } = state.serviceApi;

  return {
    userData,
    artistGenreList,
    artistList,
    artistProfile,
    artistFollow,
    artistListOfGenre,
    isLoading,
    genreListArtist,
    latestArtistsList,
    allArtistsList
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getProfile: () => dispatch(serviceApi.getProfile()),
    followUnFollow: ({ formData, dataKey }) =>
      dispatch(serviceApi.followUnfollow({ formData, dataKey })),
    getGenreList: () => dispatch(serviceApi.getGenreList()),
    getArtistListForParticularGenre: data =>
      dispatch(serviceApi.getArtistListForParticularGenre(data)),
    getArtistsProfile: Id => dispatch(serviceApi.getArtistsProfile(Id)),
    changeFollowsStatusOfArtist: (genreId, formData) =>
      dispatch(serviceApi.changeFollowsStatusOfArtist(genreId, formData))
  };
}

export default function withConnect(WrrapedComponent) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(WrrapedComponent);
}
