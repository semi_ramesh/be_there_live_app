import React from "react";
import {
  ScrollView,
  Platform,
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground
} from "react-native";

import NavigationBar from "../Reusables/CustomNavBar";
import { Images } from "../../constant/images";
import GridView from "react-native-super-grid";
import CommonStylesheet from "../../utils/CommonStylesheet";
import { Actions } from "react-native-router-flux";
const { width, height } = Dimensions.get("window");
import withConnect from "./withConnectArtist";
import { BTLoader, fontFamilies } from "../../constant/appConstant";
import NoDataComponent from "../Reusables/noData";
import ModalDropdown from "react-native-modal-dropdown";
import { ListItem } from "react-native-elements";
import FitImage from "react-native-fit-image";
import { isIphone5s } from "../Profile/styles";

class ArtistList extends React.PureComponent {
  static defaultProps = {
    artistList: [],
    artistListOfGenre: []
  };
  constructor(props) {
    super(props);
    this.state = {
      artistList: [],
      isFollowClicked: false,
      gerneName: "",
      hideLoader: false
    };
    this.getProfile = this.props.getProfile.bind(this);
    this.followUnFollow = this.props.followUnFollow.bind(this);
    this.getArtistListForParticularGenre = this.props.getArtistListForParticularGenre.bind(
      this
    );
    this.getArtistsProfile = this.props.getArtistsProfile.bind(this);
    this.changeFollowsStatusOfArtist = this.props.changeFollowsStatusOfArtist.bind(
      this
    );
  }

  componentDidMount() {
    this.setState({
      gerneName: this.props.dropDownTitle
    });

    this.getArtistList();
    if (Actions.currentScene === "ARTISTLIST") {
    }
  }

  getArtistList = () => {
    let id = this.props.id ? this.props.id : 1;
    if (id) {
      this.getArtistListForParticularGenre(id);
    }
  };

  componentWillReceiveProps(nextProps) {
    console.log("gerne name", nextProps);
    if (
      this.props.artistList !== nextProps.artistList &&
      nextProps.artistList
    ) {
      this.setState({
        artistList: nextProps.artistList,
        isFollowClicked: false
      });
    }
    if (
      this.props.allArtistsList !== nextProps.artistListOfGenre &&
      nextProps.allArtistsList
    ) {
      this.setState({
        artistList: nextProps.allArtistsList.artists_list
      });
    }
  }

  artistInfo = async id => {
    await Actions.Artist({ artistId: id, genreId: this.props.id });
  };

  followUnfollow = async item => {
    this.setState({
      isFollowClicked: true
    });

    let formData = new FormData();
    let followStatus = item.is_checked_by_user === 1 ? 0 : 1;
    formData.append("following_id", item.user_id);
    formData.append("follower_status", followStatus);
    formData.append("genre_id", item.genre_id);
    formData.append("response_type", "artist_list_of_genre");
    let dataKey = "allArtistsList";
    await this.followUnFollow({ formData, dataKey });
    await this.getProfile();
  };

  selectGernes = async () => {
    Actions.pop();
  };

  renderItem = item => {
    let userImage = item.user_detail.user_profile_image
      ? { uri: item.user_detail.user_profile_image.image_path }
      : Images.placeholder;
    let ImageForFollow =
      item.is_checked_by_user === 1 ? Images.unfollowUser : Images.follow;
    return (
      <TouchableOpacity
        key={item.id}
        style={{ width: width / 3.5 }}
        onPress={() => this.artistInfo(item.user_id)}
      >
        <FitImage
          indicatorColor={"white"}
          style={{
            height: width / 3.5,
            width: width / 3.5,
            position: "relative"
          }}
          source={userImage}
        />
        <TouchableOpacity
          style={GStyle.genreTitle}
          onPress={() => this.followUnfollow(item)}
        >
          <Image source={ImageForFollow} style={GStyle.followImage} />
        </TouchableOpacity>
        <Text
          numberOfLines={1}
          style={[
            CommonStylesheet.whiteColor,
            {
              fontSize: 10,
              fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
              marginVertical: 10,
              textAlign: "center"
            }
          ]}
        >
          {item.user_detail.name.toUpperCase()}
        </Text>
      </TouchableOpacity>
    );
  };

  selectTheGenerFromDropDown = item => {
    Actions.refresh({ dropDownTitle: item.genre_name });
    this.setState({
      gerneName: item.genre_name
    });
    this.getArtistListForParticularGenre(item.id);
  };

  renderDropDownItem = item => {
    return (
      <ListItem
        title={item.genre_name.toUpperCase()}
        containerStyle={{
          borderBottomColor: "rgba(112,128,144xx,1.0)",
          backgroundColor: "transparent",
          borderBottomWidth: 0.4
        }}
        onPress={() => this.selectTheGenerFromDropDown(item)}
        titleStyle={{
          color: "white",
          fontSize: 12,
          fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
        }}
      />
    );
  };

  render() {
    if (this.props.isLoading && !this.state.isFollowClicked) {
      return (
        <View style={GStyle.container}>
          <BTLoader />
        </View>
      );
    }

    if (this.state.artistList.length === 0) {
      return (
        <NoDataComponent
          title={"ARTISTS"}
          reloadData={this.getArtistList}
          back={true}
        />
      );
    }
    return (
      <View style={GStyle.container}>
        <NavigationBar title={"ARTISTS"} onSearch={this.onSearch} back={true} />
        <Text style={GStyle.genreText}>GENRE</Text>
        <ModalDropdown
          onSelect={this.selectTheGenerFromDropDown}
          textStyle={{
            color: "white",
            fontSize: 12,
            fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
          }}
          dropdownStyle={GStyle.dropDownModel}
          dropdownTextHighlightStyle={{
            color: "white",
            fontSize: 12,
            fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
          }}
          renderRow={this.renderDropDownItem}
          options={this.props.genres}
        >
          <ImageBackground style={GStyle.dropdownBg} source={Images.gradientBg}>
            <Text style={GStyle.genreButtonTitle}>
              {this.state.gerneName.toUpperCase()}
            </Text>
            <Image
              style={{ height: 12, width: 16, right: 5, position: "absolute" }}
              source={Images.dropDownArrow}
            />
          </ImageBackground>
        </ModalDropdown>
        <ScrollView>
          <View style={GStyle.genreContainer}>
            <GridView
              itemDimension={width / 3.5}
              items={this.state.artistList}
              renderItem={this.renderItem}
              spacing={10}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const GStyle = StyleSheet.create({
  container: {
    backgroundColor: "#000000",
    flex: 1
  },
  genreContainer: {},
  genreTitle: {
    //backgroundColor:'red',
    top: 0,
    flex: 0,
    alignItems: "flex-end",
    position: "absolute",
    right: isIphone5s ? 0 : 0
  },
  genreButtonTitle: {
    color: "white",
    flex: 0,
    textAlign: "center",
    alignItems: "flex-start",
    position: "absolute",
    left: 5
  },
  followImage: {
    // backgroundColor:'green',
    height: 50,
    width: 50,
    ...Platform.select({ android: { right: 0 }, ios: { right: 0 } }),
    ...Platform.select({ android: { top: -1 }, ios: { top: 0 } })
  },
  genreText: {
    color: "#da2df9",
    left: 10,
    fontSize: 10,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },

  dropdownBg: {
    marginVertical: 5,
    height: 36,
    width: width - 20,
    position: "relative",
    marginHorizontal: 10,
    paddingHorizontal: 5,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "rgba(23,23,23,1)",
    //borderWidth:0.4,
    //borderColor:'rgba(39,39,39,1)',
    justifyContent: "space-between"
  },
  dropDownModel: {
    marginTop: -10,
    backgroundColor: "rgba(23,23,23,1)",
    marginHorizontal: 10,
    alignSelf: "center",
    width: width - 20,
    borderColor: "darkgray",
    borderWidth: 0,
    height: height / 1.5
  }
});

export default withConnect(ArtistList);
