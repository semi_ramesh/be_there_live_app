/* eslint-disable max-len */
import React from "react";
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions
} from "react-native";
import { Images } from "../../constant/images";
import { customPurple } from "../Authentication/styles";
const { width } = Dimensions.get("window");

export const ARow = () => {
  let test = true;
  if (test) {
    return <View />;
  }
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image source={Images.dataRow} style={styles.dataRowStyle} />
      </View>
      <View style={styles.content}>
        <Text style={styles.textStyle}>{"TRENDING"}</Text>
        <Text style={styles.boldTextStyle}>{"5 SECONDS OF SUMMER"}</Text>
        <Text style={{ color: "white", marginTop: 5 }}>
          {"MAKE ME FEEL          0.45"}
        </Text>
        <TouchableOpacity style={styles.followBtn}>
          <Image source={Images.follow} style={styles.followImage} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

/** STYLES **/
const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    flex: 1,
    height: 150,
    padding: 8
  },
  imageContainer: {
    width: width / 2
  },
  dataRowStyle: {
    width: width / 2,
    height: 130,
    resizeMode: "contain"
  },

  content: {
    padding: 5,
    width: width / 2,
    height: 130,
    backgroundColor: "transparent"
  },
  textStyle: {
    color: customPurple
  },
  boldTextStyle: {
    marginTop: 21,
    color: "white",
    fontWeight: "500",
    fontSize: 15
  },
  followBtn: {
    top: -90,
    right: -100,
    height: 50,
    width: 100,
    backgroundColor: "transparent",
    flex: 0,
    alignItems: "flex-end"
  },
  followImage: {
    right: -18,
    height: 50,
    width: 50,
    resizeMode: "contain"
  }
});
