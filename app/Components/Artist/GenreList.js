/**
 *
 * @ProvideModule HDAudioPlayer.Components.Artist.GenereList
 *
 */

import React from "react";
import {
  ScrollView,
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity
} from "react-native";
import { customOrange } from "../Authentication/styles";
import Swiper from "react-native-swiper";
import NavigationBar from "../Reusables/CustomNavBar";
import { Images } from "../../constant/images";
import GridView from "react-native-super-grid";
import CommonStylesheet from "../../utils/CommonStylesheet";
import { Actions } from "react-native-router-flux";
const { width, height } = Dimensions.get("window");
import FitImage from "react-native-fit-image";
import withConnect from "./withConnectArtist";
import { BTLoader, fontFamilies } from "../../constant/appConstant";
import PropTypes from "prop-types";
import NoDataComponent from "../Reusables/noData";

class GenreList extends React.PureComponent {
  static defaultProps = {
    artistGenreList: {}
  };
  static propTypes = {
    artistGenreList: PropTypes.any
  };

  constructor(props) {
    super(props);
    this.state = {
      artistSliderData: [],
      genres: [],
      isFollowClicked: false,
      isFollow: false
    };
    this.getProfile = this.props.getProfile.bind(this);
    this.followUnFollow = this.props.followUnFollow.bind(this);
    this.getGenreList = this.props.getGenreList.bind(this);
    this.getArtistListForParticularGenre = this.props.getArtistListForParticularGenre.bind(
      this
    );
    this.changeFollowsStatusOfArtist = this.props.changeFollowsStatusOfArtist.bind(
      this
    );
  }

  componentDidMount() {
    this.getGenreList();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      this.props.artistGenreList !== nextProps.artistGenreList &&
      nextProps.artistGenreList
    ) {
      this.setState({
        artistSliderData: nextProps.artistGenreList.latest_artists_list,
        genres: nextProps.artistGenreList.genres_list
      });
    }

    if (
      this.props.latestArtistsList !== nextProps.latestArtistsList &&
      nextProps.latestArtistsList
    ) {
      this.setState({
        isFollowClicked: false,
        artistSliderData: nextProps.latestArtistsList.latest_artists_list
      });
    }
  }

  goToArtistList = async (genreId, genre_name) => {
    // await this.getArtistListForParticularGenre(genreId);
    if (Actions.currentScene === "ARTISTLIST") {
      return;
    }
    await Actions.ARTISTLIST({
      id: genreId,
      dropDownTitle: genre_name,
      genres: this.state.genres
    });
  };

  followUnfollow = async item => {
    this.setState({
      isFollowClicked: true,
      isFollow: !this.state.isFollow
    });
    let formData = new FormData();
    let followStatus = item.is_checked_by_user === 1 ? 0 : 1;
    formData.append("following_id", item.id);
    formData.append("follower_status", followStatus);
    formData.append("response_type", "genre_latest_artists_images");
    const dataKey = "latestArtistsList";
    await this.followUnFollow({ formData, dataKey });
    await this.getProfile();
  };

  renderItem = item => {
    if (item === undefined || item === null) {
      return null;
    }
    return (
      <TouchableOpacity
        key={item.id}
        onPress={() => this.goToArtistList(item.id, item.genre_name)}
        style={{ backgroundColor: "rgba(18,0,20,1.0)", alignItems: "center" }}
      >
        <FitImage
          style={{
            height: width / 3.5,
            width: width / 3.5,
            opacity: 0.2,
            position: "relative"
          }}
          source={{ uri: item.latest_genre_image }}
        />

        <View style={GStyle.genreTitle}>
          <Text
            style={[
              CommonStylesheet.whiteColor,
              { fontFamily: fontFamilies.AvantGardeITCbyBTBold }
            ]}
          >
            {item.genre_name.toUpperCase()}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    if (this.props.isLoading && !this.state.isFollowClicked) {
      return (
        <View style={GStyle.container}>
          <BTLoader />
        </View>
      );
    }
    if (
      this.state.artistSliderData.length === 0 &&
      this.state.genres.length === 0
    ) {
      return (
        <NoDataComponent title={"GENRES"} onReloadData={this.getGenreList} />
      );
    }

    return (
      <View style={GStyle.container}>
        <NavigationBar title={"GENRES"} onSearch={this.onSearch} />
        <ScrollView>
          <View style={GStyle.swipper}>
            <Swiper
              style={GStyle.wrapper}
              activeDotColor={customOrange}
              dotColor={"gray"}
              autoplay
              paginationStyle={{
                backgroundColor: "transparent",
                height: 20,
                bottom: 0,
                justifyContent: "flex-end",
                paddingRight: 20
              }}
              loop
            >
              {this.state.artistSliderData.map((item, index) => {
                if (!item.user_profile_image) {
                  return null;
                }
                const { width, height } = Image.resolveAssetSource(item);
                let ImageForFollow =
                  item.is_checked_by_user === 1
                    ? Images.unfollowUser
                    : Images.follow;

                return (
                  <View key={`MyKey_${index}`} style={GStyle.slide}>
                    <View style={GStyle.slideTextContainer}>
                      <Text style={GStyle.slideText} numberOfLines={1}>
                        {item.name}
                      </Text>
                    </View>
                    <FitImage
                      style={[GStyle.slide1]}
                      source={{ uri: item.user_profile_image.image_path }}
                      key={index}
                      resizeMode={"cover"}
                    />
                    <TouchableOpacity
                      style={GStyle.genreTitle1}
                      onPress={() => this.followUnfollow(item)}
                    >
                      <Image
                        source={ImageForFollow}
                        style={GStyle.followImage}
                      />
                    </TouchableOpacity>
                  </View>
                );
              })}
            </Swiper>
          </View>
          <View style={GStyle.genreContainer}>
            <GridView
              itemDimension={width / 3.5}
              items={this.state.genres}
              renderItem={this.renderItem}
              spacing={10}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const GStyle = StyleSheet.create({
  container: {
    backgroundColor: "#000000",
    flex: 1
  },
  swipper: {
    flex: 1,
    backgroundColor: "black",
    height: height / 3,
    width: width
  },
  slide1: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  slide: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "transparent",
    alignItems: "stretch"
  },
  slideText: {
    color: "white",
    padding: 5,
    fontSize: 12,
    fontWeight: "600",
    fontFamily: fontFamilies.ITCAvantGardeStdBook
  },
  slideTextContainer: {
    position: "absolute",
    bottom: 0,
    zIndex: 9999,
    backgroundColor: "#000",
    opacity: 0.7,
    width: width,
    height: 30,
    justifyContent: "center"
  },
  genreContainer: {},
  genreTitle: {
    position: "absolute",
    width: width / 3.5,
    alignItems: "center",
    justifyContent: "center",
    top: width / 3.5 / 2
  },
  genreTitle1: {
    backgroundColor: "transparent",
    flex: 0,
    alignItems: "flex-end",
    position: "absolute",
    right: 0,
    top: 0
  },
  followImage: {
    height: width * 0.2,
    width: width * 0.2
  }
});

export default withConnect(GenreList);
