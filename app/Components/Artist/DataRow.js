import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions
} from "react-native";

const { width } = Dimensions.get("window");
export default class DataRow extends Component {
  static propTypes = {};

  componentDidMount() {}
  //uri:data.video_thumbnail
  render() {
    const { data, onSelect } = this.props;
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.imageContainer}
          onPress={() => onSelect(data)}
        >
          <Image
            source={{ uri: data.video_thumbnail }}
            style={styles.dataRowStyle}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

/** STYLES **/
const styles = StyleSheet.create({
  container: {
    flex: 1,
    top: -10
  },
  imageContainer: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center"
    //width:width/3
  },
  dataRowStyle: {
    // top:-30,
    height: width / 5.5,

    width: width / 5.5,
    //width:undefined,
    resizeMode: "cover"
  }
});
