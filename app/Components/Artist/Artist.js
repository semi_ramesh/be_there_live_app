/**
 * @ProvideModule BTL.Components.Artists
 * module for ArtistAccount
 */

import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Platform,
  StyleSheet,
  ActivityIndicator
} from "react-native";
import styles from "./styles";
import NavigationBar from "app/Components/Reusables/CustomNavBar";
import { Images } from "../../constant/images";
import { customOrange } from "../Authentication/styles";
import Swiper from "react-native-swiper";
const { width: deviceWidth, height: deviceHeight } = Dimensions.get("window");
import DataRow from "./DataRow";
import GridView from "react-native-super-grid";
import Icon from "react-native-vector-icons/FontAwesome";
import Video from "react-native-video";
import FitImage from "react-native-fit-image";
import withConnect from "./withConnectArtist";
import { web } from "react-native-communications";
import { fontFamilies, BTLoader } from "app/constant/appConstant";
import moment from "moment";
import Player from "app/Components/HDAudioPlayer/index";
import Modal from "react-native-modalbox";
const url =
  "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";

class Artist extends Component {
  static defaultProps = {
    artistProfile: {},
    tour: [],
    popScene: "ARTISTLIST"
  };
  static propTypes = {};

  componentWillUnmount() {
    this.setState({
      isPlaying: false
    });
  }

  constructor(props) {
    super(props);
    this.state = {
      paused: true,
      fullScreen: false,
      currentVideo: url,
      isFollow: 1,
      url: "",
      artistProfile: {},
      isPlaying: false,
      songs: [],
      isAlert: true,
      alertMessage: "",
      profileImage: Images.placeholder,
      albumArt: Images.user1
    };
    this.followUnFollow = this.props.followUnFollow.bind(this);
    this.getArtistsProfile = this.props.getArtistsProfile.bind(this);
    this.changeFollowsStatusOfArtist = this.props.changeFollowsStatusOfArtist.bind(
      this
    );
  }

  componentDidMount() {
    this.getArtistProfile();
  }

  getArtistProfile = () => {
    if (this.props.artistId) {
      this.getArtistsProfile(this.props.artistId);
    }
  };

  componentWillReceiveProps(nextProps) {
    if (
      this.props.artistProfile !== nextProps.artistProfile &&
      nextProps.artistProfile
    ) {
      this.setState({
        artistProfile: nextProps.artistProfile,
        isFollow: nextProps.artistProfile.follow_details
          ? nextProps.artistProfile.follow_details.follow_status
          : 1,
        profileImage: nextProps.artistProfile.user_profile_image
          ? { uri: nextProps.artistProfile.user_profile_image.image_path }
          : Images.placeholder,
        currentVideo:
          nextProps.artistProfile.user_video_with_path.length > 0
            ? nextProps.artistProfile.user_video_with_path[0].video_path
            : url
      });
    }

    if (
      this.props.artistFollow !== nextProps.artistFollow &&
      nextProps.artistFollow
    ) {
      this.setState({
        isFollow: nextProps.artistFollow.artist_detail.follow_details
          ? nextProps.artistFollow.artist_detail.follow_details.follow_status
          : 1
      });
    }
  }

  onSelect = data => {
    this.setState({
      currentVideo: data.video_path
    });
  };
  handleVideo = () => {
    this.setState({
      paused: !this.state.paused
    });
  };

  onEndVideo = () => {
    this.player.seek(0);
    this.setState({
      paused: true
    });
  };

  renderRow = item => {
    return <DataRow data={item} onSelect={() => this.onSelect(item)} />;
  };

  fullScreenClicked = () => {
    this.player.presentFullscreenPlayer();
  };

  followUnfollowArtist = async item => {
    this.setState({
      isFollowClicked: true,
      isFollow: !this.state.isFollow
    });
    let formData = new FormData();
    let dataKey = "artistFollow";
    let followStatus = this.state.isFollow === 1 ? 0 : 1;
    formData.append("following_id", this.props.artistId);
    formData.append("follower_status", followStatus);
    formData.append("response_type", "artist_detail");
    await this.followUnFollow({ formData, dataKey });
  };

  /** Render slider **/
  renderSlider = () => {
    const { user_carousel_images } = this.state.artistProfile;
    if (
      user_carousel_images === undefined ||
      user_carousel_images.length === 0
    ) {
      let ImageForFollow =
        this.state.isFollow === 1 ? Images.unfollowUser : Images.follow;
      return (
        <View style={styles.swipper}>
          <View style={artistStyle.sliderContainer}>
            <FitImage
              indicator={() => {
                return <ActivityIndicator />;
              }}
              indicatorSize={1}
              indicatorColor={"gray"}
              originalWidth={deviceWidth}
              originalHeight={deviceHeight / 2.4}
              style={[
                styles.slide1,
                { width: deviceWidth, height: deviceWidth / 2 }
              ]}
              source={this.state.profileImage}
            />
            <TouchableOpacity
              style={[
                artistStyle.genreTitle1,
                { ...Platform.select({ ios: { right: -10 } }) }
              ]}
              onPress={() => this.followUnfollowArtist("")}
            >
              <Image
                source={ImageForFollow}
                style={[artistStyle.followImage]}
              />
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    return (
      <View style={styles.swipper}>
        <Swiper
          style={styles.wrapper}
          activeDotColor={customOrange}
          dotColor={"gray"}
          autoplay
          paginationStyle={artistStyle.paginationPageStyle}
          loop
        >
          {user_carousel_images.map((item, index) => {
            let ImageForFollow =
              this.state.isFollow === 1 ? Images.unfollowUser : Images.follow;
            return (
              <View key={item + index} style={{ flex: 1 }}>
                <FitImage
                  indicator={() => {
                    return <ActivityIndicator />;
                  }}
                  indicatorSize={1}
                  indicatorColor={"gray"}
                  originalWidth={deviceWidth}
                  originalHeight={deviceHeight / 2.4}
                  style={[
                    styles.slide1,
                    { width: deviceWidth, height: deviceWidth / 2 }
                  ]}
                  source={{ uri: item.image_path }}
                />
                <TouchableOpacity
                  style={artistStyle.genreTitle1}
                  onPress={() => this.followUnfollowArtist(item)}
                >
                  <Image
                    source={ImageForFollow}
                    style={artistStyle.followImage}
                  />
                </TouchableOpacity>
              </View>
            );
          })}
        </Swiper>
      </View>
    );
  };

  renderVideoPreview = () => {
    const { user_video_with_path } = this.state.artistProfile;
    if (user_video_with_path === undefined) {
      return null;
    }
    if (user_video_with_path && user_video_with_path.length === 0) {
      return null;
    }
    return (
      <View style={artistStyle.preview}>
        <TouchableOpacity
          style={artistStyle.previewOverlay}
          onPress={this.handleVideo}
        >
          <Video
            ref={ref => {
              this.player = ref;
            }}
            source={{ uri: `${this.state.currentVideo}` }}
            style={artistStyle.videoStyle}
            resizeMode={"stretch"}
            repeat={true}
            onFullscreenPlayerDidDismiss={() =>
              this.setState({ fullScreen: false })
            }
            onEnd={this.onEndVideo}
            paused={this.state.paused}
          />
          {this.state.paused && (
            <Image style={artistStyle.playBtnStyle} source={Images.playBtn} />
          )}
          <TouchableOpacity
            style={artistStyle.fullScreenBtn}
            onPress={this.fullScreenClicked}
          >
            <Image
              style={artistStyle.fullScreenImage}
              source={Images.fullScreen}
            />
          </TouchableOpacity>
        </TouchableOpacity>
        <View style={artistStyle.gridViewContainer}>
          <GridView
            style={artistStyle.gridViewOwen}
            items={user_video_with_path}
            renderItem={this.renderRow}
            spacing={4}
            horizontal={false}
          />
        </View>
      </View>
    );
  };

  /**
   * Render Tour Dates
   * @returns {*}
   */
  tourDates = () => {
    const { tour } = this.state.artistProfile;
    return (
      <View style={[styles.album, { marginTop: 0 }]}>
        {tour &&
          tour.map((item, index) => {
            let tourDate = moment(item.tour_date, "DD MMMM YYYY hh:mm:ss a");
            let day = tourDate.format("dddd").toUpperCase();
            tourDate = tourDate.format("DD MMMM YYYY");
            return (
              <View key={item + index} style={artistStyle.toursContainer}>
                <Text style={artistStyle.tourHeader}>
                  {day},{tourDate.toUpperCase()}
                </Text>
                <Text style={artistStyle.line_up}>{item.line_up}</Text>
                <ImageBackground
                  source={Images.gradientBg}
                  style={artistStyle.gradientBgStyle}
                >
                  <TouchableOpacity
                    style={artistStyle.button}
                    onPress={() => web(item.ticket_link)}
                  >
                    <Text style={artistStyle.ticketText}>
                      {"Ticket Portal".toUpperCase()}
                    </Text>
                  </TouchableOpacity>
                </ImageBackground>
              </View>
            );
          })}
      </View>
    );
  };

  renderAlert = () => {
    if (!this.state.artistProfile.artist_notification) {
      return null;
    }
    return (
      <View style={styles.album}>
        <View style={artistStyle.tourDatesContainer}>
          <Text style={artistStyle.tourDatesFont}>ALERT:</Text>
          <View style={artistStyle.alertBox}>
            <Text style={[artistStyle.tourDatesFont, artistStyle.extraFont]}>
              {this.state.artistProfile.artist_notification.description}
            </Text>
            <Text style={[artistStyle.tourDatesFont, artistStyle.extraFont]}>
              {this.state.artistProfile.artist_notification.complete_address}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  /**
   * Render Text Information
   * @returns {*}
   */
  renderTextInfo = () => {
    const { name, about_me } = this.state.artistProfile;
    let nameOf = name === undefined ? "" : name;
    let about = about_me === "null" ? "" : about_me;
    const { artistName, bioOfArtist } = this.state;
    return (
      <View style={artistStyle.textInfoContainer}>
        <Text style={artistStyle.tourDatesFont}>{nameOf.toUpperCase()}</Text>
        <Text
          style={[
            artistStyle.tourDatesFont,
            { fontSize: 14, fontWeight: "400" }
          ]}
          numberLines={1000}
        >
          {about}
        </Text>
      </View>
    );
  };

  isPlayerOpen = (song, tracks, albumArt) => {
    this.setState({
      isPlaying: !this.state.isPlaying,
      songs: [song],
      albumArt: albumArt
    });
  };

  isDownBtnPressed = () => {
    this.setState({
      isPlaying: false
    });
  };

  /** render  artist detail **/
  render() {
    if (!this.props.artistProfile) {
      return <View style={artistStyle.modal} />;
    }
    const { user_carousel_images } = this.state.artistProfile;
    if (this.props.isLoading && !this.state.isFollowClicked) {
      return (
        <View style={[styles.topView, { marginTop: 0 }]}>
          <BTLoader />
        </View>
      );
    }
    const slider = this.renderSlider();
    const info = this.renderTextInfo();
    const preView = this.renderVideoPreview();
    const tourDate = this.tourDates();
    const alert = this.renderAlert();
    const { user_albums_with_tracks } = this.state.artistProfile;

    return (
      <View style={[styles.topView, { marginTop: 0 }]}>
        <NavigationBar
          title={"ARTIST PROFILE"}
          back={true}
          popScene={this.props.popScene}
        />
        <ScrollView ref={ref => (this.scrollview = ref)}>
          <View style={[styles.container, { margin: 10 }]}>
            {slider}
            {this.state.artistProfile.artist_notification !== null && alert}
            {info}
            <View style={[artistStyle.textInfoContainer, { marginBottom: 10 }]}>
              <Text style={artistStyle.tourDatesFont}>VIDEO</Text>
            </View>
            {preView}
            <View style={artistStyle.textInfoContainer}>
              <Text style={artistStyle.tourDatesFont}>ALBUMS</Text>
            </View>
            {user_albums_with_tracks &&
              user_albums_with_tracks.map(item => {
                if (item.tracks.length === 0) {
                  return null;
                }
                return <Albums {...item} onTrackPress={this.isPlayerOpen} />;
              })}
            <View style={artistStyle.textInfoContainer}>
              <Text style={artistStyle.tourDatesFont}>TOUR DATES</Text>
            </View>
            {tourDate}
          </View>
        </ScrollView>
        <Modal
          style={artistStyle.modal}
          position={"top"}
          ref={"modal"}
          isOpen={this.state.isPlaying}
          onClosed={() => this.setState({ isPlaying: false })}
        >
          {this.state.isPlaying && (
            <ScrollView>
              <Player
                tracks={this.state.songs}
                albumArt={this.state.albumArt}
                isDownPressed={this.isDownBtnPressed}
              />
            </ScrollView>
          )}
        </Modal>
      </View>
    );
  }
}

function Albums(props) {
  return (
    <View style={[artistStyle.topContainer, { marginTop: 10 }]}>
      <View style={artistStyle.albumImage}>
        <Image
          source={{ uri: props.album_cover }}
          style={artistStyle.albumImageSecond}
        />
      </View>
      <View style={artistStyle.tracks}>
        <GridView
          itemDimension={deviceWidth / 2}
          items={props.tracks}
          renderItem={item =>
            renderTracks({
              item: item,
              onSelect: props.onTrackPress,
              tracks: props.tracks,
              albumArt: props.album_cover
            })
          }
        />
      </View>
    </View>
  );
}

function renderTracks({ item, onSelect, tracks, albumArt }) {
  return (
    <TouchableOpacity
      style={artistStyle.trackTop}
      onPress={() => onSelect(item, tracks, albumArt)}
    >
      <View style={{ flexDirection: "row" }}>
        <Icon
          name={"play-circle"}
          color={"white"}
          size={20}
          style={{ paddingRight: 10 }}
        />
        <Text style={artistStyle.iconStyle}>{item.track_title}</Text>
      </View>
      <View style={artistStyle.durationText}>
        <Text style={{ color: "white" }}>{item.track_time}</Text>
      </View>
    </TouchableOpacity>
  );
}

const artistStyle = StyleSheet.create({
  topContainer: {
    flex: 3,
    width: deviceWidth,
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "rgba(39,39,39,1)"
  },

  container: {
    flex: 1,
    justifyContent: "center"
  },

  albumImage: {
    height: deviceWidth / 3 - 10,
    width: deviceWidth / 3
  },

  tracks: {
    backgroundColor: "black",
    marginLeft: 4
  },

  toursContainer: {
    marginTop: 15,
    borderBottomWidth: 0.5,
    borderBottomColor: "rgba(48,40,50,1.0)",
    paddingBottom: 20
  },

  tourHeader: {
    marginVertical: 4,
    fontFamily: fontFamilies.ITCAvantGardeGothicDemi2,
    color: customOrange
  },

  line_up: {
    color: "white",
    marginVertical: 4,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },
  button: {
    marginVertical: 8,
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  paginationPageStyle: {
    backgroundColor: "black",
    height: 20,
    bottom: 0,
    justifyContent: "flex-end",
    paddingLeft: 20
  },
  textInfoContainer: {
    marginTop: 15,
    flex: 1
  },
  artistFontName: {
    fontSize: 26,
    color: "white"
  },

  artistDesc: {
    fontSize: 14,
    color: "white"
  },
  tourDatesContainer: {
    flex: 0,
    alignItems: "flex-start"
  },

  tourDatesFont: {
    color: "white",
    //fontWeight:'500',
    fontSize: 16,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },
  mediater: {
    flex: 0,
    alignItems: "flex-start",
    flexDirection: "row"
  },
  albumStyle: {
    backgroundColor: "transparent",
    flex: 0,
    alignItems: "flex-end",
    top: "80%",
    right: 0,
    zIndex: 9999,
    position: "absolute"
  },
  extension: {
    marginTop: 10,
    fontWeight: "600",
    fontSize: 24
  },

  trackTop: {
    width: (deviceWidth / 3) * 2,
    flexDirection: "row",
    justifyContent: "space-between"
  },

  durationText: {
    flexDirection: "row",
    marginRight: 50
  },

  albumImageSecond: {
    height: deviceWidth / 3 - 10,
    width: deviceWidth / 3,
    resizeMode: "cover"
  },
  genreTitle: {
    position: "absolute",
    width: deviceWidth / 3.5,
    alignItems: "center",
    justifyContent: "center",
    top: deviceWidth / 3.5 / 2
  },
  genreTitle1: {
    flex: 0,
    alignItems: "flex-end",
    position: "absolute",
    right: 0,
    top: 0
  },

  followImage: {
    height: deviceWidth * 0.2,
    width: deviceWidth * 0.2
  },
  modal: {
    backgroundColor: "black",
    flex: 1
  },
  iconStyle: {
    color: "white",
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
    width: 100
  },
  alertBox: {
    flex: 1,
    backgroundColor: "red",
    width: deviceWidth - 20
  },
  extraFont: {
    marginLeft: 5,
    marginVertical: 5,
    fontSize: 15,
    fontWeight: "400"
  },
  ticketText: {
    color: customOrange,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },
  gradientBgStyle: {
    flex: 0,
    height: 40,
    marginVertical: 4
  },
  fullScreenStyle: {
    bottom: 14,
    position: "absolute",
    right: 10
  },
  sliderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  preview: {
    flex: 1,
    flexDirection: "row",
    height: deviceHeight / 3,
    backgroundColor: "gray"
  },
  previewOverlay: {
    flex: 8,
    backgroundColor: "black",
    justifyContent: "center"
  },
  videoStyle: {
    flex: 1,
    height: 300,
    width: deviceWidth * 0.8
  },
  playBtnStyle: {
    alignSelf: "center",
    height: 70,
    width: 70,
    resizeMode: "contain",
    position: "absolute"
  },
  fullScreenBtn: {
    bottom: 14,
    position: "absolute",
    right: 10
  },
  fullScreenImage: {
    height: 30,
    width: 30
  },
  gridViewContainer: {
    flex: 2,
    backgroundColor: "black"
  },
  gridViewOwen: {
    flex: 1,
    paddingTop: 0
  }
});

export default withConnect(Artist);
