/**
**/

import React from 'react'
import {connect} from 'react-redux'
import {auth,serviceApi} from 'app/redux/actions'

function mapStateToProps(state){
    const {user:userData} = state.auth;
    const{
        venueLists,
        venueCrouselArray,
        venueDetail,
        venueEvents
    } = state.serviceApi;
    const {isLoading} = state.shared;
    return{
        userData,
        venueLists,
        isLoading,
        venueCrouselArray,
        venueDetail,
        venueEvents,
    }
}

function mapDispatchToProps(dispatch){
    return {
        getProfile:() => dispatch(serviceApi.getProfile()),
        followUnFollow:(formData,dataKey)=>dispatch(serviceApi.followUnfollow(formData,dataKey)),
        venuefollowUnfollow:(formData)=>dispatch(serviceApi.venuefollowUnfollow(formData)),
        getVenueList:()=>dispatch(serviceApi.getVenueList()),
        venueListNonCrousel:()=>dispatch(serviceApi.venueListNonCrousel()),
        venueProfileDetail:(venueId) =>dispatch(serviceApi.venueProfileDetail(venueId)),
        venueDetailList:(venueId)=>dispatch(serviceApi.venueDetailList(venueId))
    }
}

export default function withConnect(WrrapedComponent){

    return connect(mapStateToProps,mapDispatchToProps)(WrrapedComponent)
}