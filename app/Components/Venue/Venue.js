/**
 * @ProvidesModule app.containers.Venue.Venue
 */

import React ,{Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Image,
    ImageBackground,
    Dimensions,
    StyleSheet
} from 'react-native';
import styles from "./styles";
import NavigationBar from 'app/Components/Reusables/CustomNavBar'
import {customOrange} from "app/Components/Authentication/styles";
import {Images} from "app/constant/images";
import Swiper from 'react-native-swiper';
import GridView from 'react-native-super-grid';
const {height:deviceHeight,width:deviceWidth} = Dimensions.get('window');
import withConnect from './withConnectVenue'
import {Actions} from 'react-native-router-flux'
import {BTLoader, fontFamilies} from "app/constant/appConstant";
import FitImage from 'react-native-fit-image'
import NoDataComponent from 'app/Components/Reusables/noData'


 class Venue extends Component{


    static defaultProps = {
        venueCrouselArray:[],
        venueLists:[]
    };

    constructor(props){
        super(props);
        this.state = {
            venueCrouselArray:[],
            venueLists: [],
            isFollowClicked:false,
            isfollow:false
        };
        this.getProfile = this.props.getProfile.bind(this);
        this.venuefollowUnfollow = this.props.venuefollowUnfollow.bind(this);
        this.getVenueList = this.props.getVenueList.bind(this);
        this.venueListNonCrousel = this.props.venueListNonCrousel.bind(this);
        this.venueProfileDetail = this.props.venueProfileDetail.bind(this);
    }


    componentDidMount(){
        this.getDetailOfAllVenues().then();
    }

    getDetailOfAllVenues = async()=>{
        await this.getVenueList();
        await this.venueListNonCrousel();
    };


    componentWillReceiveProps(nextProps){


        if(this.props.venueLists !== nextProps.venueLists&&nextProps.venueLists){
            this.setState({
                isFollowClicked:false,
                venueLists:nextProps.venueLists
            })
        }

        if(this.props.venueCrouselArray!==nextProps.venueCrouselArray&&nextProps.venueCrouselArray){
            this.setState({
                isFollowClicked:false,
                venueCrouselArray:nextProps.venueCrouselArray
            })
        }

    }


    followUnfollow = async (item) => {
        this.setState({
            isFollowClicked:true
        });
        let followStatus = item.is_checked_by_user === 0?1:0;
        let formData = new FormData();
        formData.append('follower_status',followStatus);
        formData.append('response_type',"venue_list");
        formData.append('following_id',item.venue_id);
        await this.venuefollowUnfollow(formData);
        await this.getDetailOfAllVenues();
        await this.getProfile();
     };


    goToDetails=async (item)=>{
        await Actions.VENUEDETAILS({venueId:item.venue_id});
    };



    renderItem = (item) =>{

        const crouselImage = item.user_profile_image!==null?{uri:item.user_profile_image.image_path}:Images.placeholder
        const image = item.is_checked_by_user === 1 ? Images.unfollow1 : Images.follow ;
        return(
            <TouchableOpacity style={styles.venueListContainer} onPress={()=>this.goToDetails(item)}>
                <FitImage style={{height:deviceWidth/3.5, width:deviceWidth/3.5}} source = {crouselImage}>
                    <TouchableOpacity  style = {{position:'absolute',top:0,right:-1}} onPress = {()=>this.followUnfollow(item)}>
                        <Image style = {{height:50,width:50}} source = {image}/>
                    </TouchableOpacity>
                </FitImage>
                <Text numberOfLines = {1} style ={[styles.venueTitle,{marginVertical:10}]}>{item.venue_name}</Text>
            </TouchableOpacity>)
    };

    render(){

        if(this.props.isLoading&&!this.state.isFollowClicked){
            return (<View style = {styles.topView}><BTLoader/></View>)
        }
        if(this.state.venueCrouselArray.length===0||this.state.venueLists.length===0){
            return(<NoDataComponent reloadData ={this.getDetailOfAllVenues}
                                    isLoading = {this.props.isLoading}
                                    title ={'VENUE'}/>)
        }
        return(
            <View style = {styles.topView}>
                <NavigationBar title = {'VENUE'}/>
                <ScrollView>
                    <View style = {styles.container}>
                        {this.state.venueCrouselArray.length>0&&<View style = {GStyle.swipper}>
                            <Swiper style={GStyle.wrapper}
                                    activeDotColor = {customOrange}
                                    dotColor = {'gray'}
                                    autoplay
                                    paginationStyle = {GStyle.pagingnation}
                                    loop>
                                {this.state.venueCrouselArray.map( (item, index )=>{
                                    const image = item.is_checked_by_user === 1?Images.unfollow1 : Images.follow ;
                                    const crouselImage = item.user_profile_image === null?Images.placeholder:{uri:item.user_profile_image.image_path}
                                    return <View key = {`MyKey_${index}`} style={GStyle.slide}>
                                        <View style={GStyle.slideTextContainer}>
                                            <Text style={GStyle.slideText} numberOfLines={1}>{item.name}</Text>
                                        </View>
                                        <FitImage style={[GStyle.slide1]} source={crouselImage} key={index} resizeMode={'cover'}/>
                                        <TouchableOpacity style = {GStyle.followBtnStyle} onPress ={()=>this.followUnfollow(item)} >
                                            <Image source = {image} style = {GStyle.plusMinusImage} />
                                        </TouchableOpacity>
                                    </View>
                                })}
                            </Swiper>
                        </View>}
                        <View style={styles.venueContainer}>
                            <GridView
                                itemDimension={deviceWidth/3.5}
                                items={this.state.venueLists}
                                renderItem= {this.renderItem}
                                spacing={10}/>
                        </View>
                    </View>
                </ScrollView>
            </View>)
    }


}

export default withConnect(Venue)

const GStyle = StyleSheet.create({
    container:{
        backgroundColor:'#000000',
        flex:1
    },
    swipper:{
        flex:1,
        backgroundColor:'rgba(18,0,20,1.0)',
        height:deviceHeight/3,
        width:deviceWidth
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        alignItems: 'stretch',
    },
    slideText: {
        color: 'white',
        padding:5,
        fontSize:12,
        fontWeight:'600',
        fontFamily:fontFamilies.ITCAvantGardeStdBook
    },
    slideTextContainer: {
        position:'absolute',
        bottom:0,
        zIndex:9999,
        backgroundColor:"#000",
        opacity:0.7,
        width: deviceWidth,
        height: 30,
        justifyContent:'center'
    },
    genreContainer:{

    },
    pagingnation:{
        backgroundColor:'transparent',
        height:20,
        bottom:0,
        justifyContent:'flex-end',
        paddingRight:20
    },
    genreTitle:{
        position:'absolute',
        width:deviceWidth/3.5,
        alignItems:"center",
        justifyContent:'center',
        top:(deviceWidth/3.5)/2,
    },
    followBtnStyle:{
        right:0,
        top:0,
        height:80,
        width:80,
        position:'absolute'
    },
    plusMinusImage:{
        height:80,
        width:80,
        resizeMode:'contain'
    }

});