/**
 * @ProvidesModules app.Components.Venue.styles
 */

import React from 'react'
import {
    StyleSheet,
    Dimensions
} from 'react-native'

import {fontFamilies} from "app/constant/appConstant";
const {
    height,
    width
} = Dimensions.get('window');
export default StyleSheet.create({

    topView: {
        backgroundColor: '#000000',
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center'

    },
    listing: {
        flex: 2,
        backgroundColor: 'black',
    },
    container: {
        flex: 10,
        backgroundColor: 'black'
    },
    profileInfoContainer: {
        width: width,
        backgroundColor: 'black',
        flexDirection: 'row'
    },
    gridView: {
        flex: 0,
        backgroundColor: 'black',
        width: width,
    },

    imgStyle: {
        marginLeft: 10,
        resizeMode: 'contain',
        height: 175,
        width: 175,
    },
    swipper: {
        flex: 1,
        backgroundColor: 'rgba(18,0,20,1.0)',
        height: height / 3,
        width: width
    },
    crousel: {
        flex: 2,
        backgroundColor: 'green',
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: '#fff',
        fontSize: 16,
        fontWeight: '500',
        fontFamily:fontFamilies.ITCAvantGardeStdBook
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        alignItems: 'stretch',
    },
    slideText: {
        color: 'white',
        padding: 5,
        fontSize:12,
        fontFamily:fontFamilies.ITCAvantGardeStdBook

    },
    slideTextContainer: {
        position: 'absolute',
        bottom: 0,
        zIndex: 9999,
        backgroundColor: "#000",
        opacity: 0.5,
        width: width,
        height: 30
    },

    venueContainer: {
        marginTop: 10
    },
    itemStyle: {
        backgroundColor: 'transparent',
        flex: 0,
        alignItems: 'flex-end',
    },
    venueTitle: {
        color: '#FFF',
        textAlign: 'center',
        justifyContent: 'center',
        marginBottom: 9,
        fontSize:12,
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
    },
    venueListContainer: {
        marginTop: 10
    },

})