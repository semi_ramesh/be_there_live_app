/**
 * @ProvideModuels Components.Venue.DataRow
 *
 */


import React,{Component} from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground,
} from 'react-native';

import {Images} from "app/constant/images";
import {customPurple,customOrange} from "app/Components/Authentication/styles";
import {web} from 'react-native-communications'
import FastImage from 'react-native-fit-image'
import moment from 'moment'
import {fontFamilies} from "app/constant/appConstant";
const {width,height} = Dimensions.get('window');

export default  class DataRow extends Component {

    render(){
        return(<View style = {styles.container}>
        <View  style = {styles.imageContainer}>
            <FastImage   source = {{uri:`${this.props.data.image_path}`}} style = {styles.dataRowStyle}/>
        </View>
        <ImageBackground style = {styles.content} source = {Images.spotlightCellBg}>
            <Text style = {[styles.textStyle,{fontWeight:'300',fontSize:10,marginTop:10,fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular}]}>{(`${moment(this.props.data.event_date).format('dddd,  MMMM DD,YYYY')}`).toUpperCase()} </Text>
            <Text style = {[styles.textStyle,{fontSize:14, marginTop:18,fontFamily:fontFamilies.ITCAvantGardeGothicDemi2}]}>{this.props.data.artist_name}</Text>
            <Text style = {[styles.textStyle,{fontWeight:'200',fontSize:12,fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular}]}> {`${this.props.data.event_time}`}</Text>
            <TouchableOpacity style={[styles.ticketPortalBtn,{backgroundColor:'black'}]} onPress = {()=>web(this.props.data.ticket_link)}>
                <ImageBackground style = {{flex:1.0,justifyContent:'center',alignItems:'center'}} source = {Images.gradientBg}>
                <Text style = {styles.aText}>{'TICKET PORTAL'}</Text>
             </ImageBackground>
            </TouchableOpacity>
        </ImageBackground>
    </View>)
    }

}

/** STYLES **/
const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        flex:1,
        padding:8,
    },
    imageContainer:{
        flex:2,
        backgroundColor:'rgba(18,0,20,1.0)',
        justifyContent:'center',
        alignItems:'flex-start'

    },
    dataRowStyle:{
        flex:1,
        width:(2*width)/5.4,
        height:110,
        resizeMode:'center'
    },

    content:{
        flex:3,
        justifyContent:'center',
        backgroundColor:'rgba(36,1,39,1.0)'

    },
    textStyle:{
        marginLeft:10,
        marginVertical:3,
        color:'white'

    },
    boldTextStyle:{
        marginTop:21,
        color:'white',
        fontWeight:'bold',
        fontSize:15,
    },
    followBtn:{
        position:'absolute',
        backgroundColor:'transparent',
        flex:0,
        alignItems:'flex-end',
        width:60,
        alignSelf:"flex-end"
    },
    followImage:{
        top:0,
        height:50,
        width:50,
        resizeMode:'contain'
    },
    underLine:{
        marginVertical:2,
        height:1,
        flex:0.010,
        backgroundColor:'white',

    },

    songBtn:{
        flex:0.30,
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        marginVertical:5
    },

    ticketPortalBtn:{
        flex:1,
        justifyContent:'flex-end'
    },
    aText:{
        top:5,
        color: customOrange,
        fontSize:10,
        textAlign:'center',
        alignSelf:'center',
        fontFamily:fontFamilies.ITCAvantGardeStdBook
    }



});