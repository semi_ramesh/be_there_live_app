/**
 * @ProvidesModule app.Components.Venue.EventItem.
 */

import React,{Component} from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    StyleSheet,
    Dimensions ,
    ImageBackground,
    PixelRatio,
    Platform
} from 'react-native';
import {Images} from "app/constant/images";
import {
    customPurple,
    customOrange
} from "app/Components/Authentication/styles";

import {web} from 'react-native-communications'
import moment from 'moment'
import {fontFamilies} from "app/constant/appConstant";

export default class EventItem extends Component{

	
	  static defaultProps ={

	      onFollow :()=>{}

	  };

    constructor(props){
        super(props);
        this.state ={
            image:props.isActive===1?Images.follow:Images.unfollowUser,
        }
    }

    ticketPortalTapped=()=>{
        web(`${this.props.data.item.ticket_link}`);
    }

     onFollow = (data)=>{
	      this.props.onFollow(data)
     };


     render(){

         const {
             title,
             artist,
             timeDuration,
             is_checked
         } = this.props.data.item;
        const {
            amount,
            artist_name,
            id,
            image_path,
            is_checked_by_user,
            plan_id,
            promote_from,
            promote_till,
            event_date,
            ticket_link,
            event_time,
            user_id,
            venue_name
          } = this.props.data.item;

         const {isActive} = this.props;
         const backgroundColor = this.props.data.index ===0?'black':'rgba(48,12,79,1)';
         const image = this.props.isActive===0?Images.follow:Images.unfollowUser;
         return(
             <View style = {styles.container}>
                 <View style= {[styles.innerContainer,{backgroundColor:backgroundColor}]}>
                     <Text style={styles.timeText}>{(`${moment(event_date).format('dddd, MMMM DD ,YYYY')}`).toUpperCase()}</Text>
                     <Text style={[styles.titleText,{fontWeight:'900'}]}>{artist_name}</Text>
                     <Text style={styles.artistText}>{event_time}</Text>
            <TouchableOpacity style={styles.ticketPortalView}
        				  onPress={this.ticketPortalTapped}>
        	<ImageBackground source={Images.gradientBg} style={styles.gradientBgStyle}>
        		<Text style={styles.ticketPortalText}>{'Ticket Portal'.toUpperCase()}</Text>
        	</ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity  style = {styles.followBg}
                           onPress = {()=>this.onFollow(this.props.data.item)}>
                <Image source = {image} style = {styles.followImage}/>
        </TouchableOpacity>
        </View>
    </View>)
}
}

/** STYLES **/
const styles = StyleSheet.create({
    container:{
        flex:1,
        width:210  ,
      },
      innerContainer:{
          flex:1,
          width:200,
          backgroundColor:'rgba(48,12,79,1)' ,
          marginVertical:5,
          marginHorizontal:5
      },
    ticketPortalText:{
        fontSize:12,
        color:customOrange,
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
    },
    ticketPortalView:{
        width:200,
        height:30,
        marginTop:10,
        marginHorizontal:0,
        justifyContent:'center',
        alignItems:'center'
    },
    gradientBgStyle:{
        height:30,
        width:200,
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center'
    },
    timeText:{

        marginTop:5,
        width:160,
        color:'white',
        marginLeft:5,
        fontSize:11,
        fontWeight:'500',
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular,

    },

    titleText:{

        marginTop:10,
        marginLeft:10,
        color:'white',
        fontWeight:'200',
        fontSize:15,
        fontFamily:fontFamilies.ITCAvantGardeGothicDemi2,

    },


    artistText:{

        marginTop:2,
        color:'white',
        marginLeft:10,
        fontSize:12,
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular

    },

      followBg:{
        backgroundColor:'transparent',
        flex:0,
        alignItems:'flex-end',
        position:'absolute',
        right:0,
        top:0

    },

    followImage:{
        height:40,
        width:40,
    }

});


const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320;

export function normalize(size) {
  const newSize = size * scale;
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}