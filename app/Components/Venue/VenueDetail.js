/**
 * @ProvidesModule Venue Detail Component having Venue profile for according there user_id.
 */

import React ,{Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Image,
    ImageBackground,
    Dimensions,
    FlatList,
    StyleSheet
} from 'react-native'
import styles from "./styles";
import NavigationBar from 'app/Components/Reusables/CustomNavBar'
import {customOrange} from "../Authentication/styles";
import {Images} from "app/constant/images";
import Swiper from 'react-native-swiper';
import DataRow from "./DataRow";
import FitImage from 'react-native-fit-image'
const {height:deviceHeight,width:deviceWidth} = Dimensions.get('window');
const {height,width} = Dimensions.get('window');
import withConnect from './withConnectVenue'
import {BTLoader, fontFamilies} from "app/constant/appConstant";
import NoDataComponent from "app/Components/Reusables/noData";
import EventItem from './EventItem'

class VenueDetails extends Component{

    static defaultProps = {
        venueDetail:{},
        venueEvents:[],
        popScene:'Venue'
    };

    constructor(props){
        super(props);
        this.state = {
            crouselImages:[],
            events: [],
            upComingEvents:[],
            name:'',
            followDetail:{},
            profileImage :Images.placeholder

        };
        this.getProfile = this.props.getProfile.bind(this);
        this.followUnFollow = this.props.followUnFollow.bind(this);
        this.venueProfileDetail = this.props.venueProfileDetail.bind(this);
        this.venueDetailList = this.props.venueDetailList.bind(this)
    }



    componentDidMount(){
       this.getVenueDetail()
    }

    getVenueDetail =  ()=>{
        if(this.props.venueId){
            this.venueProfileDetail(this.props.venueId);
            this.venueDetailList(this.props.venueId)
        }

    };

    crouselVenueFollow = async (item)=>{
        this.setState({
            isFollowClicked:true,
            isfollow:!this.state.isfollow
        });
        let formData = new FormData();
        let followStatus = 1;
        if(!this.state.followDetail){
            followStatus = 1
        }
        if(this.state.followDetail){
            followStatus = this.state.followDetail.follow_status?0:1;
        }
        formData.append('following_id',this.props.venueId);
        formData.append('follower_status',followStatus);
        formData.append('response_type','genre_list_artist');
        let dataKey =  'genreListArtist';
        await this.followUnFollow({formData,dataKey})
        await this.getVenueDetail();
        await this.getProfile();
    };

    componentWillReceiveProps(nextProps){
        if(this.props.venueDetail !== nextProps.venueDetail&&nextProps.venueDetail){

            this.setState({
                profieImage:nextProps.venueDetail.user_profile_image ?{uri:nextProps.venueDetail.user_profile_image.image_path}:Images.placeholder,
                crouselImages:nextProps.venueDetail.user_carousel_images,
                name:nextProps.venueDetail.name,
                followDetail:nextProps.venueDetail.follow_details
            })
        }
        
        if(this.props.venueEvents !== nextProps.venueEvents&&nextProps.venueEvents){
            this.setState({
                events:nextProps.venueEvents.to_night_event,
                upComingEvents:nextProps.venueEvents.upcoming_events
            })
        }

    };

    renderRow=(item)=>{
        return <DataRow data={item.item}/>
    };

    renderEvent = (data) => {
         return<EventItem data = {data}
                          onFollow = {this.crouselVenueFollow}
                          isActive = {this.state.followDetail?this.state.followDetail.follow_status:0}/>

    };

     render(){

        if(this.props.isLoading&&!this.state.name){
            return (<View style = {styles.topView}>
                         <BTLoader/>
                     </View>)
        }
        if (this.state.crouselImages.length === 0){
            this.state.crouselImages.push({image_path:this.state.profileImage,name:this.state.name})
        }
        // if(this.state.events.length===0&&this.state.upComingEvents.length === 0){
        //     return(<NoDataComponent text={'No events available for this venue.'} reloadData ={this.getVenueDetail}
        //                             isLoading = {this.props.isLoading}
        //                             title ={this.state.name} back  = {true}/>)
        // }
        return(
            <View style = {styles.topView}>
                <NavigationBar  title ={this.state.name} popScene = {this.props.popScene} back={true}/>
                <ScrollView>
                    <View style = {styles.container}>
                        <View style = {styles.swipper}>
                            <Swiper style={styles.wrapper}
                                    activeDotColor = {customOrange}
                                    dotColor = {'gray'}
                                    autoplay
                                    paginationStyle = {GStyle.pagingnation}
                                    loop>
                                {this.state.crouselImages.length>0&&this.state.crouselImages.map( (item, index )=>{
                                    let  image = Images.follow;
                                    if(this.state.followDetail){
                                     image = this.state.followDetail.follow_status === 0?Images.follow :Images.unfollow1;
                                    }
                                    const crouselImage = item.image_path === null || item.image_path === this.state.profileImage ?this.state.profileImage:{uri:item.image_path}
                                    return <View key = {`MyKey_${index}`} style={GStyle.slide}>
                                            <View style={GStyle.slideTextContainer}>
                                                <Text style={GStyle.slideText} numberOfLines={1}>{this.state.name}</Text>
                                            </View>
                                            <FitImage style={[GStyle.slide1]} source={crouselImage} key={index} resizeMode={'cover'}/>
                                            <TouchableOpacity style = {GStyle.followBtnStyle} onPress ={ ()=>this.crouselVenueFollow(item)} >
                                                <Image source = {image} style = {GStyle.plusMinusImage} />
                                            </TouchableOpacity>
                                    </View>
                                })}
                            </Swiper>
                        </View>
                        {this.state.upComingEvents.length>0&&<View style = {[styles.listing,{marginVertical:10}]}>
                            <View style = {GStyle.textContainer}>
                                <Text style = {styles.text}> UPCOMING SHOWS</Text>
                            </View>
                            <FlatList
                                data={this.state.upComingEvents}
                                horizontal={true}
                                space={10}
                                renderItem={this.renderEvent}/>
                        </View>}
                        {this.state.events.length>0&&<View style = {[styles.listing ,{marginVertical:10}]}>
                            <View style = {GStyle.textContainer}>
                                <Text style = {styles.text}>SHOWS</Text>
                            </View>
                            <FlatList
                                keyExtractor = {(item,index)=>item+index}
                                data={this.state.events}
                                renderItem={(item) => this.renderRow(item)}
                            />
                        </View>}
                    </View>
                </ScrollView>
            </View>
        )
    }


}

export  default withConnect(VenueDetails)

const GStyle = StyleSheet.create({
    container:{
        backgroundColor:'#000000',
        flex:1
    },
    swipper:{
        flex:1,
        backgroundColor:'rgba(18,0,20,1.0)',
        height:deviceHeight/3,
        width:deviceWidth
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        alignItems: 'stretch',
    },
    slideText: {
        color: 'white',
        left:5,
        top:4,
        fontSize:12,
        fontFamily:fontFamilies.ITCAvantGardeStdBook
    },
    slideTextContainer: {
        position:'absolute',
        bottom:0,
        zIndex:9999,
        backgroundColor:"#000",
        opacity:0.5,
        width: deviceWidth,
        height: 30,
        justifyContent:'center'
    },
    genreContainer:{

    },
    genreTitle:{
        position:'absolute',
        width:deviceWidth/3.5,
        alignItems:"center",
        justifyContent:'center',
        top:(deviceWidth/3.5)/2,
    },
    pagingnation:{
        backgroundColor:'transparent',
        height:20,
        bottom:0,
        justifyContent:'flex-end',
        paddingRight:20
    },
    followBtnStyle:{
        right:0,
        top:0,
        height:80,
        width:80,
        position:'absolute',
    },
    plusMinusImage:{
        height:80,
        width:80,
        resizeMode:'contain'
    },
    textContainer:{
        flex:0,
        alignItems:'flex-start',
        marginLeft:5
    }

});