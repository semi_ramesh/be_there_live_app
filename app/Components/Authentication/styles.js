/**
 * @provideModule BTL.Component.Authentication.styles
 */

import { Dimensions, StyleSheet } from "react-native";
import { fontFamilies } from "app/constant/appConstant";
const { height, width } = Dimensions.get("window");

export const customOrange = "#e53100";
export const customPurple = "#da2df9";
export const iconColor = "#FFFFFF";

export default StyleSheet.create({
  loginSuperView: {
    flexGrow: 1
  },

  splashContainer: {
    //flex:1,
    top: 0,
    height: height - 0,
    width: width,
    bottom: 0,
    paddingVertical: 20
    //backgroundColor:'red'
  },
  contentContainerStyleAuth: {
    //flex:1,
    //justifyContent:'center',
    flexGrow: 1,
    alignItems: "center"

    // ...Platform.select({android:{height:height+30}})
  },
  orangeTextStyle: {
    top: 20,
    left: 10,
    fontSize: 24,
    color: customOrange,
    fontWeight: "300"
  },
  gradientImage: {
    top: 0,
    height: 45,
    width: width - 16,
    marginBottom: 10,
    justifyContent: "center",
    paddingHorizontal: 10,
    backgroundColor: "rgba(23,23,23,1)",
    borderWidth: 1,
    borderColor: "rgba(39,39,39,1)"
  },
  feildContainer: {
    flex: 30,
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "flex-end",
    marginBottom: 30
  },

  forgetPasswordFeilds: {
    flex: 30,
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "center"
  },

  buttonFlexWidth: {
    flex: 0,
    flexDirection: "row",
    justifyContent: "space-between",
    width: width - 10
  },
  halfBtn: {
    flex: 1,
    height: 40,
    width: 80,
    backgroundColor: customOrange,
    marginHorizontal: 5,
    justifyContent: "center",
    alignItems: "center"
  },

  halfBtnNormal: {
    flex: 1,
    height: 40,
    width: 80,
    marginHorizontal: 5,
    justifyContent: "center",
    alignItems: "center"
  },

  jkStyle: {
    flex: 1,
    height: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  checkBoxContainer: {
    marginTop: 5,
    flex: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    width: width,
    paddingLeft: 0
  },
  checkBoxStyle: {
    backgroundColor: "transparent",
    borderWidth: 0
  },
  subscribStyle: {
    flex: 0,
    marginHorizontal: 10,
    marginTop: 10,
    backgroundColor: customOrange,
    width: width - 20,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  subscribStyle1: {
    flex: 0,
    marginHorizontal: 20,
    marginTop: 10,
    backgroundColor: "transparent",
    width: width - 20,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  textInputCustomStyle: {
    color: "white",
    height: 50,
    fontFamily: "TeX Gyre Adventor"
    //fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
  },
  checkLabel: {
    color: "white",
    top: 2,
    marginLeft: 0,
    fontWeight: "300",
    fontSize: 12,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },
  notificationCheckbox: {
    backgroundColor: "transparent",
    borderColor: "transparent",
    margin: 0,
    padding: 10,
    alignItems: "flex-start",
    width: 200
  },
  registerationText: {
    color: customOrange,
    marginTop: 10,
    marginLeft: 10,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
    fontSize: 20
  },
  feildText: {
    color: "white",
    marginBottom: 20,
    fontSize: 20,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },
  signInTextStyle: {
    color: "white",
    fontSize: 15,
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },
  btnRegisterText: {
    color: "white",
    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
  },
  containerView: { backgroundColor: "black" }
});
