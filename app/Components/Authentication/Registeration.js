/** Registration Process **/

import React from "react";
import {
  View,
  Text,
  TextInput,
  ImageBackground,
  StyleSheet,
  Platform,
  TouchableOpacity
} from "react-native";

import styles, { customOrange } from "./styles";
import { Images } from "app/constant/images";
import { Actions } from "react-native-router-flux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import ModalPickerImage from "app/Components/Reusables/ModalPickerImage";
import PhoneInput from "react-native-phone-input";

import _ from "lodash";
import ServiceApi from "app/utils/ServiceApi/index";
import {
  BTLoader,
  fontFamilies,
  showInfoMessage
} from "app/constant/appConstant";
import CheckBox from "app/thirdPartyComponents/react-native-checkbox/checkbox.js";

export default class Registeration extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
      //Profile states
      email: "",
      password: "",
      mobileNumber: "",
      fullName: "",
      confirmPassword: "",
      isLoading: false
    };
    this.onPressFlag = this.onPressFlag.bind(this);
    this.selectCountry = this.selectCountry.bind(this);
    this.state = {
      pickerData: null
    };
  }

  componentDidMount() {
    this.setState({
      pickerData: this.phone.getPickerData()
    });
  }

  onPressFlag() {}

  selectCountry(country) {
    this.phone.selectCountry(country.iso2);
    console.log("Country code", country);
  }

  goToSignIn = () => {
    Actions.pop();
  };

  goToCongratulations = () => {
    Actions.OTPSCREEN();
  };

  /** Remember Me Actions **/
  onCheck = () => {
    this.setState({
      checked: !this.state.checked
    });
  };

  ValidateEmail = mail => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return true;
    }
    return false;
  };
  /** Register Api call **/
  onRegister = async () => {
    const { email, password, mobileNumber, fullName } = this.state;
    if (!this.validateForm()) {
      return;
    }

    this.setState({
      isLoading: true
    });

    try {
      let formData = new FormData();
      formData.append("name", fullName);
      formData.append("email", email);
      formData.append("phone_no", mobileNumber);
      formData.append("password", password);
      formData.append("isEmail", "0");
      formData.append("isMobile", "0");

      const response = await ServiceApi.registerUser(formData);
      if (response.status === true) {
        if (this.state.checked) {
        }
        Actions.SUBSCRIPTION({
          NewUserData: response.data,
          checked: this.state.checked
        });
      }
      this.setState({
        isLoading: false
      });
      showInfoMessage(response.message);
    } catch (e) {
      this.setState({
        isLoading: false
      });
      showInfoMessage("Some thing went wrong. Please try again.");
    }
  };

  /** Validate Form here **/
  validateForm = () => {
    const {
      email,
      password,
      fullName,
      mobileNumber,
      confirmPassword
    } = this.state;

    if (_.isEmpty(fullName)) {
      showInfoMessage("Please Enter your full name .");
      return false;
    }
    if (_.isEmpty(email)) {
      showInfoMessage("Please enter email.");
      return false;
    }
    if (!this.ValidateEmail(email)) {
      showInfoMessage("Please enter your valid mail address.");
      return false;
    }
    if (_.isEmpty(mobileNumber) || mobileNumber.length < 12) {
      showInfoMessage("Please enter correct  mobile Number");
      return false;
    }

    if (mobileNumber.length < 10 && mobileNumber.length > 15) {
      showInfoMessage("Please enter valid mobile Number");
      return false;
    }

    if (_.isEmpty(password)) {
      showInfoMessage("Please enter your password.");
      return false;
    }

    if (password.length < 6) {
      showInfoMessage("Password must be atleast 6 character");
      return false;
    }
    if (password !== confirmPassword) {
      showInfoMessage("Password doesn't match.");
      return false;
    }
    return true;
  };

  render() {
    const ContainerView =
      Platform.OS === "ios" ? KeyboardAwareScrollView : KeyboardAwareScrollView;
    return (
      <ContainerView
        style={styles.containerView}
        keyboardShouldPersistTaps={true}
      >
        <ImageBackground style={styles.splashContainer} source={Images.bgImage}>
          <Text style={styles.registerationText}>{"REGISTRATION"}</Text>
          <ContainerView
            style={styles.loginSuperView}
            keyboardShouldPersistTaps={true}
            contentContainerStyle={styles.contentContainerStyleAuth}
          >
            <View>
              <View style={styles.feildContainer}>
                <Text style={styles.feildText}>{"REGISTRATION"}</Text>
                <View style={styles.gradientImage}>
                  <TextInput
                    placeholder={"FULL NAME"}
                    style={styles.textInputCustomStyle}
                    placeholderTextColor="white"
                    underlineColorAndroid="rgba(0,0,0,0)"
                    onChangeText={text => this.setState({ fullName: text })}
                  />
                </View>
                <View style={styles.gradientImage}>
                  <TextInput
                    placeholder={"EMAIL"}
                    style={styles.textInputCustomStyle}
                    placeholderTextColor="white"
                    keyboardType={"email-address"}
                    underlineColorAndroid="rgba(0,0,0,0)"
                    onChangeText={text => this.setState({ email: text })}
                  />
                </View>
                <View style={styles.gradientImage}>
                  <PhoneInput
                    ref={ref => {
                      this.phone = ref;
                    }}
                    onChangePhoneNumber={text =>
                      this.setState({ mobileNumber: text })
                    }
                    textStyle={styles.textInputCustomStyle}
                    onPressFlag={this.onPressFlag}
                  />
                </View>
                <View style={styles.gradientImage}>
                  <TextInput
                    placeholder={"PASSWORD"}
                    placeholderTextColor="white"
                    secureTextEntry
                    style={styles.textInputCustomStyle}
                    underlineColorAndroid="rgba(0,0,0,0)"
                    onChangeText={text => this.setState({ password: text })}
                  />
                </View>
                <View style={styles.gradientImage}>
                  <TextInput
                    placeholder={"CONFIRM PASSWORD"}
                    secureTextEntry
                    style={styles.textInputCustomStyle}
                    placeholderTextColor="white"
                    underlineColorAndroid="rgba(0,0,0,0)"
                    onChangeText={text =>
                      this.setState({ confirmPassword: text })
                    }
                  />
                </View>
                <View style={styles.buttonFlexWidth}>
                  <ImageBackground
                    style={styles.halfBtnNormal}
                    source={Images.gradientBg}
                  >
                    <TouchableOpacity
                      style={styles.jkStyle}
                      onPress={this.goToSignIn}
                    >
                      <Text style={styles.signInTextStyle}>SIGN IN</Text>
                    </TouchableOpacity>
                  </ImageBackground>
                  <TouchableOpacity
                    style={[styles.halfBtn]}
                    onPress={this.onRegister}
                  >
                    <Text style={styles.btnRegisterText}>REGISTER</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.checkBoxContainer}>
                  <PlanCheckboxes
                    label={"REMEMBER ME"}
                    checked={this.state.checked}
                    onChange={this.onCheck}
                  />
                </View>
              </View>
              {this.state.isLoading && <BTLoader />}
            </View>
          </ContainerView>
          <ModalPickerImage
            ref={ref => {
              this.myCountryPicker = ref;
            }}
            data={this.state.pickerData}
            onChange={country => {
              this.selectCountry(country);
            }}
            cancelText="Cancel"
          />
        </ImageBackground>
      </ContainerView>
    );
  }
}

function PlanCheckboxes(props) {
  return (
    <CheckBox
      label={props.label}
      labelStyle={styles.checkLabel}
      size={15}
      checkedImage={Images.check}
      uncheckedImage={Images.unCheck}
      checked={props.checked}
      onChange={props.onChange}
      containerStyle={styles.notificationCheckbox}
    />
  );
}
