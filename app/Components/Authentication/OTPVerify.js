import React from "react";
import {
  View,
  Text,
  TextInput,
  ImageBackground,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import styles from "./styles";
import { Images } from "app/constant/images";
import { Actions } from "react-native-router-flux";

export default class OTPVerify extends React.PureComponent {
  demo = () => {
    //Actions.pop()
    Actions.CONGRATULATIONS();
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.loginSuperView}>
        <ImageBackground style={styles.splashContainer} source={Images.bgImage}>
          <View style={{ flex: 1, alignItems: "flex-start" }}>
            <Text style={styles.orangeTextStyle}>VERIFICATION</Text>
          </View>
          <View style={styles.feildContainer}>
            <Text
              style={{
                color: "white",
                marginHorizontal: 20,
                fontSize: 20,
                textAlign: "center"
              }}
              numberOfLines={10}
            >
              {" "}
              Enter OTP
            </Text>
            <ImageBackground
              source={Images.gradientBg}
              style={[styles.gradientImage, { marginTop: 55 }]}
            >
              <TextInput
                style={styles.textInputCustomStyle}
                placeholder={"Enter OTP"}
                placeholderTextColor="white"
                underlineColorAndroid="rgba(0,0,0,0)"
              />
            </ImageBackground>
            <TouchableOpacity style={styles.subscribStyle} onPress={this.demo}>
              <Text style={{ color: "white", fontWeight: "300", fontSize: 18 }}>
                Verify
              </Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}
