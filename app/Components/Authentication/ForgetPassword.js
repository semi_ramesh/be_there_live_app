/**
 * BTL.Component.Authentication.ForgetPassword
 */

import React from "react";
import {
  View,
  Text,
  ImageBackground,
  TextInput,
  TouchableOpacity
} from "react-native";
import styles from "./styles";
import { Images } from "app/constant/images";
import { Actions } from "react-native-router-flux";
import ServiceApi from "app/utils/ServiceApi/index";
import { showInfoMessage, BTLoader } from "app/constant/appConstant";
import NavBar from "app/Components/Reusables/CustomNavBar";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
export default class ForgetPassword extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      source: "",
      isLoading: false
    };
  }

  componentDidMount() {}

  componentWillReceiveProps() {}

  goToRegisteration = () => {
    Actions.REGISTERATION();
  };

  send = async () => {
    const { source, isLoading } = this.state;
    if (source === "") {
      showInfoMessage("Please enter your registered email.");
      return;
    }
    let formData = new FormData();
    formData.append("email", source);
    this.setState({
      isLoading: true
    });
    try {
      const response = await ServiceApi.forgetPassword(formData);

      console.log("Forget response", response);

      if (response.status === true) {
        Actions.RESET_PASSWORD({ otpNumber: response.data });
      }
      this.setState({
        isLoading: false
      });
      showInfoMessage(response.message);
    } catch (e) {
      showInfoMessage("Some thing went wrong , Please try again.");
      this.setState({
        isLoading: false
      });
    }
  };

  render() {
    return (
      <KeyboardAwareScrollView
        style={styles.loginSuperView}
        keyboardShouldPersistTaps={true}
      >
        <ImageBackground style={styles.splashContainer} source={Images.bgImage}>
          <NavBar title={"FORGOT PASSWORD"} back search />
          <View style={styles.forgetPasswordFeilds}>
            <Text
              style={{
                color: "white",
                marginVertical: 10,
                fontFamily: "TeX Gyre Adventor",
                marginHorizontal: 20,
                fontSize: 14,
                textAlign: "center"
              }}
              numberOfLines={10}
            >
              {" "}
              Enter your email , we will send you a mail to recover your
              password.
            </Text>
            <View
              source={Images.gradientBg}
              style={[
                styles.gradientImage,
                { borderWidth: 0, borderColor: "white" }
              ]}
            >
              <TextInput
                placeholder={"Email"}
                style={{ color: "white" }}
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                onChangeText={text => this.setState({ source: text })}
              />
            </View>
            <TouchableOpacity
              style={[styles.subscribStyle]}
              onPress={this.send}
            >
              <Text style={{ color: "white", fontWeight: "300", fontSize: 18 }}>
                SEND
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.subscribStyle1} onPress={this.demo}>
              <Text
                style={{
                  color: "white",
                  fontFamily: "TeX Gyre Adventor",
                  fontWeight: "300",
                  fontSize: 14
                }}
                onPress={() => Actions.REGISTERATION()}
              >
                Create New account
              </Text>
            </TouchableOpacity>
          </View>
          {this.state.isLoading && <BTLoader />}
        </ImageBackground>
      </KeyboardAwareScrollView>
    );
  }
}
