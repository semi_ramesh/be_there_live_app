/**
 * BTL.Component.Authentication.ResetPassword
 **/

import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  ImageBackground,
  TextInput
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import styles from "./styles";
import { Images } from "../../constant/images";
import {
  BTLoader,
  fontFamilies,
  showInfoMessage
} from "app/constant/appConstant";
import { Actions } from "react-native-router-flux";
import ServiceApi from "app/utils/ServiceApi/index";
import _ from "lodash";
import NavBar from "app/Components/Reusables/CustomNavBar";

export default class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otpNumber: "",
      newPassword: "",
      confirmPassword: "",
      isLoading: false
    };
  }

  componentDidMount() {
    this.setState({
      otpNumber: this.props.otpNumber
    });
    //console.log("opt Number",this.props.otpNumber);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      otpNumber: nextProps.otpNumber
    });
  }

  goToForgotPassword = () => {
    Actions.FORGOTPASSWORD();
  };

  goToRegister = () => {
    Actions.REGISTER();
  };

  resetPassword = async () => {
    if (!this.validateForm()) {
      return;
    }

    let formData = new FormData();
    formData.append("otp", this.state.otpNumber);
    formData.append("password", this.state.newPassword);
    formData.append("confirm_password", this.state.confirmPassword);
    this.setState({
      isLoading: true
    });
    try {
      const res = await ServiceApi.resetPassword(formData);
      if (res.status === true) {
        showInfoMessage(res.message);
        Actions.LOGIN();
      } else {
        showInfoMessage(res.message);
      }
      this.setState({
        isLoading: false
      });
    } catch (e) {
      this.setState({
        isLoading: false
      });
    }
  };

  validateForm = () => {
    const { newPassword, confirmPassword, otpNumber } = this.state;

    if (_.isEmpty(newPassword)) {
      showInfoMessage("Please enter new password.");
      return false;
    }
    if (newPassword !== confirmPassword) {
      showInfoMessage("Password doesn't match.");
      return false;
    }
    if (this.state.otpNumber === "") {
      showInfoMessage(
        "OTP you have entered , is not correct,please enter correct."
      );
      return false;
    }
    return true;
  };

  render() {
    return (
      <KeyboardAwareScrollView
        style={styles.loginSuperView}
        keyboardShouldPersistTaps={true}
        behavior="padding"
      >
        <ImageBackground style={styles.splashContainer} source={Images.bgImage}>
          <NavBar title={"RESET PASSWORD"} back search />
          <View style={styles.forgetPasswordFeilds}>
            <View source={Images.gradientBg} style={styles.gradientImage}>
              <TextInput
                style={styles.textInputCustomStyle}
                placeholder={"OTP"}
                placeholderTextColor="white"
                underlineColorAndroid="rgba(0,0,0,0)"
                value={`${this.state.otpNumber}`}
                onChangeText={text =>
                  this.setState({
                    otpNumber: text
                  })
                }
              />
            </View>
            <View style={styles.gradientImage} source={Images.gradientBg}>
              <TextInput
                style={styles.textInputCustomStyle}
                placeholder={" Enter new password."}
                placeholderTextColor="white"
                underlineColorAndroid="rgba(0,0,0,0)"
                onChangeText={text =>
                  this.setState({
                    newPassword: text
                  })
                }
                textContentType={"password"}
                secureTextEntry={true}
              />
            </View>
            <View style={styles.gradientImage} source={Images.gradientBg}>
              <TextInput
                style={styles.textInputCustomStyle}
                placeholder={" Confirm new password."}
                placeholderTextColor="white"
                underlineColorAndroid="rgba(0,0,0,0)"
                onChangeText={text =>
                  this.setState({
                    confirmPassword: text
                  })
                }
                textContentType={"password"}
                secureTextEntry={true}
              />
            </View>
            <View style={styles.buttonFlexWidth}>
              <TouchableOpacity
                style={[styles.halfBtn]}
                onPress={this.resetPassword}
              >
                <Text
                  style={{
                    color: "white",
                    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
                  }}
                >
                  {" "}
                  RESET
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          {this.state.isLoading && <BTLoader />}
        </ImageBackground>
      </KeyboardAwareScrollView>
    );
  }
}
