import React from "react";
import {
  View,
  Text,
  ImageBackground,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import styles from "./styles";
import { Images } from "app/constant/images";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome";
export default class ConGratulations extends React.PureComponent {
  demo = () => {
    //Actions.Tabs()
    Actions.SUBSCRIPTION();
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.loginSuperView}>
        <ImageBackground style={styles.splashContainer} source={Images.bgImage}>
          <View style={{ flex: 1, alignItems: "flex-start" }}>
            <Text style={styles.orangeTextStyle}>CONFIRMATION</Text>
          </View>
          <View style={styles.feildContainer}>
            <Icon name="cog" size={40} color={"white"} />
            <Text
              style={{ color: "white", marginHorizontal: 20, fontSize: 20 }}
            >
              CONGRATULATIONS
            </Text>
            <TouchableOpacity style={styles.subscribStyle} onPress={this.demo}>
              <Text style={{ color: "white", fontWeight: "300", fontSize: 18 }}>
                GO TO SETTINGS
              </Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}
