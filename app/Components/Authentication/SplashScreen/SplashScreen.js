/**
 * @Provide Screen Splash for loading
 *
 **/

import React from "react";
import {
  View,
  Image,
  Dimensions,
  StyleSheet,
  AsyncStorage
} from "react-native";
const { height, width } = Dimensions.get("window");
import { Images } from "app/constant/images";
import { Actions } from "react-native-router-flux";
export default class SplashScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    };
    this.onAuthorize = this.props.onAuthorize.bind(this);
  }

  componentDidMount() {}

  switchTo = () => {
    this.setState({ isLoading: true });
    try {
      AsyncStorage.getItem("userData", (err, myData) => {
        console.log("UsersStoredData", myData);
        if (myData !== null) {
          this.onAuthorize(JSON.parse(myData));
          this.setState({ isLoading: false });
          Actions.Profile();
          return;
        } else {
          Actions.LOGIN();
        }
        this.setState({ isLoading: false });
      });
    } catch (e) {
      this.setState({ isLoading: false });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.splash} source={Images.logoImage} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: height,
    width: width,
    backgroundColor: "black"
  },
  splash: {
    resizeMode: "contain",
    height: height,
    width: width
  }
});
