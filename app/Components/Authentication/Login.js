/**
 * @provideModule  LogIn Component
 */

import React from "react";
import {
  View,
  Image,
  Text,
  TextInput,
  ImageBackground,
  KeyboardAvoidingView,
  StyleSheet,
  ScrollView,
  Keyboard,
  Alert,
  Platform,
  TouchableOpacity,
  AsyncStorage,
  AppState,
  Dimensions
} from "react-native";

const { height, width } = Dimensions.get("window");

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import ServiceApi from "app/utils/ServiceApi/index";
import styles, { customOrange, customPurple } from "./styles";
import { Images } from "app/constant/images";
import {
  BTLoader,
  fontFamilies,
  showInfoMessage
} from "app/constant/appConstant";
import { Actions } from "react-native-router-flux";
import { FormInput, Button } from "react-native-elements";
import CheckBox from "app/thirdPartyComponents/react-native-checkbox/checkbox.js";

import _ from "lodash";
import { Alerts } from "app/utils/Alert";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
      isLoading: false,
      mobileNumber: "",
      password: ""
    };

    this.onAuthorize = this.props.onAuthorize.bind(this);
  }

  goToRegister = () => Actions.REGISTERATION();

  goToForgotPassword = () => Actions.FORGOTPASSWORD();

  onLogIn = async () => {
    // Actions.SUBSCRIPTION();
    // return;
    const { mobileNumber, password } = this.state;
    if (!this.validateForm()) {
      return;
    }
    //Put our api calls using  async await  try catching ....
    this.setState({
      isLoading: true
    });
    try {
      const response = await ServiceApi.logIn(mobileNumber, password);
      let data = response;
      if (data.status) {
        if (this.state.checked) {
          AsyncStorage.setItem(
            "userData",
            JSON.stringify(response.data),
            () => {
              this.onAuthorize(response.data);
            }
          );
        } else {
          this.onAuthorize(response.data);
        }

        //Actions.SUBSCRIPTION({NewUserData:response.data});
        Actions.Profile();

        this.setState({
          isLoading: false
        });
        showInfoMessage(data.message);
      } else {
        this.setState({
          isLoading: false
        });
        if (response.data.show_resend_link) {
          Alert.alert(
            "Alert",
            data.message,
            [
              {
                text: "Ok",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "Resend", onPress: this.resendEmailLink }
            ],
            { cancelable: false }
          );
        }
        //showInfoMessage(data.message);
      }
    } catch (e) {
      this.setState({
        isLoading: false
      });
      console.log("real_error", e);
      showInfoMessage("Invalid email or password.");
    }
  };

  resendEmailLink = async () => {
    this.setState({
      isLoading: true
    });
    try {
      let formData = new FormData();
      formData.append("email", this.state.mobileNumber);
      const response = await ServiceApi.resendLinkToVerifyEmail(formData);
      if (response.status) {
        showInfoMessage("Email sent successfully");
      }
      this.setState({
        isLoading: false
      });
    } catch (e) {
      this.setState({
        isLoading: false
      });
    }
  };
  componentDidMount() {
    // Actions.SUBSCRIPTIONINFO();
  }

  /** Validation of log in form **/
  validateForm = () => {
    const { mobileNumber, password } = this.state;
    if (_.isEmpty(mobileNumber)) {
      showInfoMessage("Please enter your email.");
      return false;
    }
    if (_.isEmpty(password)) {
      showInfoMessage("Please enter your password.");
      return false;
    }
    return true;
  };

  onCheck = () =>
    this.setState({
      checked: !this.state.checked
    });

  render() {
    const { mobileNumber, password } = this.state;
    const ContainerView =
      Platform.OS === "ios" ? KeyboardAwareScrollView : KeyboardAwareScrollView;
    // alert(this.props.loading);
    // noinspection JSRemoveUnnecessaryParentheses
    // noinspection JSRemoveUnnecessaryParentheses
    return (
      <ContainerView keyboardShouldPersistTaps={true}>
        <ImageBackground style={styles.splashContainer} source={Images.bgImage}>
          <Text
            style={{
              color: customOrange,
              marginLeft: 10,
              marginTop: 10,
              fontSize: 20
            }}
          >
            {"SIGN-IN"}{" "}
          </Text>
          <ContainerView
            enableOnAndroid={true}
            style={styles.loginSuperView}
            keyboardShouldPersistTaps={true}
            contentContainerStyle={styles.contentContainerStyleAuth}
          >
            <View>
              <View
                style={[styles.feildContainer, { marginBottom: height * 0.18 }]}
              >
                <Text
                  style={{
                    color: "white",
                    fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular,
                    marginBottom: 20,
                    fontSize: 20
                  }}
                >
                  {"SIGN IN"}{" "}
                </Text>
                <View style={styles.gradientImage}>
                  <TextInput
                    style={styles.textInputCustomStyle}
                    placeholder={"EMAIL"}
                    placeholderTextColor="white"
                    underlineColorAndroid="rgba(0,0,0,0)"
                    value={mobileNumber}
                    keyboardType={"email-address"}
                    onChangeText={text =>
                      this.setState({
                        mobileNumber: text
                      })
                    }
                  />
                </View>
                <View style={styles.gradientImage}>
                  <TextInput
                    style={styles.textInputCustomStyle}
                    placeholder={"PASSWORD"}
                    placeholderTextColor="white"
                    underlineColorAndroid="rgba(0,0,0,0)"
                    value={password}
                    onChangeText={text =>
                      this.setState({
                        password: text
                      })
                    }
                    textContentType={"password"}
                    secureTextEntry={true}
                  />
                </View>
                <View style={styles.buttonFlexWidth}>
                  <TouchableOpacity
                    style={[styles.halfBtn]}
                    onPress={this.onLogIn}
                  >
                    <Text
                      style={{
                        color: "white",
                        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
                      }}
                    >
                      SIGN IN{" "}
                    </Text>
                  </TouchableOpacity>
                  <ImageBackground
                    style={styles.halfBtnNormal}
                    source={Images.gradientBg}
                  >
                    <TouchableOpacity
                      style={styles.jkStyle}
                      onPress={this.goToRegister}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontFamily:
                            fontFamilies.ItcAvantGardeGothicBookRegular
                        }}
                      >
                        REGISTER
                      </Text>
                    </TouchableOpacity>
                  </ImageBackground>
                </View>
                <View style={styles.checkBoxContainer}>
                  <PlanCheckboxes
                    title={"REMEMBER ME"}
                    checked={this.state.checked}
                    onChange={this.onCheck}
                  />
                  <TouchableOpacity
                    style={{
                      marginLeft: 20,
                      marginTop: 5
                    }}
                    onPress={this.goToForgotPassword}
                  >
                    <Text
                      style={{
                        color: "white",
                        fontWeight: "300",
                        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
                      }}
                    >
                      Forgot password?{" "}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              {this.state.isLoading && <BTLoader />}
            </View>
          </ContainerView>
        </ImageBackground>
      </ContainerView>
    );
  }
}

function PlanCheckboxes(props) {
  const style = StyleSheet.create({
    notificationCheckbox: {
      backgroundColor: "transparent",
      borderColor: "transparent",
      margin: 0,
      padding: 10,
      alignItems: "flex-start",
      width: 200
    }
  });

  return (
    <CheckBox
      label={props.title}
      labelStyle={{
        color: "white",
        top: 2,
        marginLeft: 0,
        fontSize: 12,
        fontWeight: "300",
        fontFamily: fontFamilies.ItcAvantGardeGothicBookRegular
      }}
      size={20}
      checkedImage={Images.check}
      uncheckedImage={Images.unCheck}
      checked={props.checked}
      onChange={props.onChange}
      containerStyle={style.notificationCheckbox}
    />
  );
}

export default Login;
