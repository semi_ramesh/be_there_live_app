/**
* Image Constant
**/

import React from 'react'
export const Images = {
    bgImage: require('app/assets/signinbg.png'),
    loginIconImage: require('app/assets/signInTop.png'),
    gradientBg: require('app/assets/gradientBg.png'),
    tabBarImage: require('app/assets/tabbar.png'),
    defaultUser: require('app/assets/user.png'),
    //social media
    twitter: require('app/assets/twitter.png'),
    insta: require('app/assets/insta.png'),

    //default artist
    user1: require('app/assets/1.png'),
    user2: require('app/assets/2.png'),
    unfollow: require('app/assets/unfollow.png'),
    unfollow1: require('app/assets/unfollow1.png'),
    follow: require('app/assets/follow.png'),
    unfollowUser:require('app/assets/unfollowUser.png'),
    //DataRow
    dataRow: require('app/assets/dataRow.jpg'),
    //Preview
    playBtn: require('app/assets/play.png'),
    artists: require('app/assets/artists.png'),

    location_maps: require('app/assets/maps.png'),

    logoImage:require('app/assets/logo.png'),
    OrangeImage:require('app/assets/OrangeBg.png'),
    emptyImage:require('app/assets/musicEmpty.png'),

    spotlightCellBg :require('app/assets/spotlight_cell_bg.png'),

    heart_redImage: require('app/assets/heart.png'),
    heart_white :require('app/assets/hearts-white.png'),
    music :require('app/assets/music.png'),
    placeholder:require('app/assets/user.png'),
    fullScreen :require('app/assets/fullScreen.png'),
    addNew :require('app/assets/addNew.png'),
    penImage:require('app/assets/pen.png'),
    dropDownArrow:require('app/assets/dropDownArrow.png'),
    searchIcon:require('app/assets/search_icon.png'),
    tabGredientBg:require('app/assets/tab_gredient_bg.png'),
    pushAlertBg:require('app/assets/push_alert_bg.png'),
    profileEditButton:require('app/assets/profileEditButton.png'),
    searchBg:require('app/assets/search_bg.png'),
    back:require('app/assets/back.png'),
    iTunesIcon:require('app/assets/iTunes_icon.png'),
    searchSectionBg:require('app/assets/sectionBg.png'),
    searchBgOverlay:require('app/assets/bgOverlay.png'),
    spotLightPurchase:require('app/assets/spotlightPurchase.png'),
    check:require('app/assets/check.png'),
    unCheck:require('app/assets/unCheck.png'),
    editProfileBg:require('app/assets/editProfileBg.png')

}