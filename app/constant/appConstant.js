/**
 *@ProvidesModule AppConstant
 *
 **/

import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator
} from "react-native-indicators";
import React from "react";
import GridView from "react-native-super-grid";
import { Dimensions, View, Platform } from "react-native";
import { showMessage, hideMessage } from "react-native-flash-message";

export const { width: deviceWidth, height: deviceHeight } = Dimensions.get(
  "window"
);
import Placeholder from "rn-placeholder";

const customOrange = "#e53100";
const customPurple = "#da2df9";
export const stripApiKey = "pk_test_UHNl4cC6tHkAjbIciiMSGLM2";
export const BTL_REDIRECT_URL = "BTL://redirect";
export const RouterScenes = {
  ROOTSCENE: "ROOT_SCENE",
  AUTHENTICATION: "AUTHENTICATION",
  SPLASH: "SPLASH",
  HOME: "HOME",
  LOGIN: "LOGIN",
  REGISTERATION: "REGISTERATION",
  FORGETPASSWORD: "FORGOTPASSWORD",
  SUBSCRIPTIONPAGE: "SUBSCRIPTIONPAGE",
  USER_ROLE: "USER_ROLE",
  CONGRATULATIONS: "CONGRATULATIONS",
  RESET_PASSWORD: "RESET_PASSWORD",

  //Tabs
  PROFILE: "Profile",
  SPOTLIGHT: "SpotLight",
  ARTIST: "Artist",
  VENUE: "Venue",
  ALERTFEED: "AlertFeed",

  VENUEDETAILS: "VENUEDETAILS",
  OTPSCREEN: "OTPSCREEN",
  EDITPROFILE: "EDITPROFILE",
  NOTIFICATIONS: "NOTIFICATIONS",
  ACCOUNTS: "ACCOUNTS",
  SUBSCRIPTION: "SUBSCRIPTION",
  SUBSCRIPTIONINFO: "SUBSCRIPTIONINFO",
  GENRELIST: "GENRELIST",
  ARTISTLIST: "ARTISTLIST",
  CUSTOMCARDSCREEN: "CustomCardScreen",

  //Search Screen
  SEARCH_SCREEN: "Search",
  SEARCH_DETAIL_SCREEN: "SearchDetail",
  SPOT_LIGHT_PURCHASE: "SpotLightPurchase",

  //IndiVidulas Public Profile
  INDIVIDUAL_PUBLIC_PROFILE: "individualPublicProfile",
  SPOT_LIGHT_CONGRATS: "SpotLightCongrats"
};

export const DUMMY = {
  DEFAULT_SECTION_ID: "___DEFAULT-SECTION-ID___",
  EMPTY_DATA: "___EMPTY-DATA___",
  PLACEHOLDER_DATA: "___PLACEHOLDER-DATA___"
};

export const SERVICE_API = {
  //Live
  BASE_URI: "http://btl.demoserver4semidot.com/api/",

  //JAYDEEP BACK END
  //BASE_URI : "http://192.168.0.158/btl/public/api/",

  //CHANDAN BACKEND
  //BASE_URI: "http://192.168.0.250:8000/api/",

  //BASE_URI2 :"http://192.168.0.158/btl/public/",
  // BASE_URI2:'http://btl.demoserver4semidot.com/',
  X_API_KEY:
    "f6e74506bc66ddaeeece6ef765d39c7a276a2dc0e02ba84c673054b28f8aefb86e9102ec00bc0ce5",

  API_VERSION: "v1",
  PAGINATION: 20,
  REQUEST_METHODS: {
    GET: "get",
    POST: "post",
    PUT: "put",
    DELETE: "del"
  },
  RESPONSE_STATUSES: {
    SUCCESS: 200,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    NOT_FOUND: 404,
    UNPROCESSABLE_ENTITY: 422,
    INTERNAL_SERVER_ERROR: 500,
    BAD_GATEWAY: 502,
    SERVICE_UNAVAILABLE: 503
  }
};

// Date & time formats
export const FORMATS = {
  JSON_DATE: "YYYY-MM-DDTHH:mm:ss.SSSZ",
  JSON_DATE_UTC: "YYYY-MM-DDTHH:mm:ss.SSS[Z]",
  TIME_SLOT: "HH:mm",
  DATE_ID: "YYYY-MM-DD",
  START_TIME: {
    HOUR_FORMAT: "HH:mm",
    LONG_DATE_FORMAT: "dddd, [ngày] DD [tháng] MM",
    SHORT_DATE_FORMAT: "dddd (DD/MM)"
  },
  EXPIRATION_DATE: "DD/MM/YYYY"
};

export const BTLoader = () => {
  return (
    <View
      style={{
        flex: 0,
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        height: deviceHeight,
        width: deviceWidth,
        backgroundColor: "rgba(0,0,0,0.4)"
      }}
    >
      <BarIndicator color="white" count={5} />
    </View>
  );
};

export const SearchBTLoader = () => {
  return (
    <View
      style={{
        flex: 0,
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        height: 50,
        width: deviceWidth,
        backgroundColor: "rgba(0,0,0,0.5)"
      }}
    >
      >
      <BarIndicator color="white" count={5} />
    </View>
  );
};

export function showInfoMessage(message) {
  if (message === "") {
    return;
  }
  showMessage({
    message: message,
    description: "",
    type: "default",
    backgroundColor: customPurple, // background color
    color: "white"
  });
}

export const GOOGLE_API_KEY = "AIzaSyCCKugKDxDH84bwx5zGpx1-fIw_UkfrIzE";

/** Decide extension and mime **/
export const getExtensionFromMimeType = path => {
  let infoDict = {
    extension: "",
    mime: ""
  };
  let lastPart = path.split("/").pop();
  let extension = lastPart.split(".").pop();

  switch (extension) {
    case "jpg": {
      infoDict = {
        extension: ".jpg",
        mime: "image/jpeg"
      };
      return infoDict;
    }
    case "jpeg": {
      infoDict = {
        extension: ".jpeg",
        mime: "image/jpeg"
      };
      return infoDict;
    }
    case "png": {
      infoDict = {
        extension: ".png",
        mime: "image/png"
      };

      return infoDict;
    }
    case "gif": {
      infoDict = {
        extension: ".gif",
        mime: "image/gif"
      };
      return infoDict;
    }
    case "bmp": {
      infoDict = {
        extension: ".bmp",
        mime: "image/bmp"
      };
      return infoDict;
    }
    case "txt": {
      infoDict = {
        extension: ".txt",
        mime: "application/plain"
      };
      return infoDict;
    }
    case "rtf": {
      infoDict = {
        extension: ".rtf",
        mime: "application/plain"
      };
      return infoDict;
    }
    case "pdf": {
      infoDict = {
        extension: ".pdf",
        mime: "application/pdf"
      };
      return infoDict;
    }
    case "doc": {
      infoDict = {
        extension: ".doc",
        mime: "application/msword"
      };
      return infoDict;
    }
    case "docx": {
      infoDict = {
        extension: ".docx",
        mime: "application/msword"
      };
      return infoDict;
    }
    case "xls": {
      infoDict = {
        extension: ".xls",
        mime: "application/x-msexcel"
      };
      return infoDict;
    }
    case "xlsx": {
      infoDict = {
        extension: ".xlsx",
        mime: "application/x-msexcel"
      };
      return infoDict;
    }
    case "mp4": {
      infoDict = {
        extension: ".mp4",
        mime: "video/mpeg"
      };
      return infoDict;
    }
    case "mkv": {
      infoDict = {
        extension: ".mkv",
        mime: "video/x-matroska"
      };
      return infoDict;
    }
    case "avi": {
      infoDict = {
        extension: ".avi",
        mime: "video/avi"
      };
      return infoDict;
    }
    case "3gp": {
      infoDict = {
        extension: ".3gp",
        mime: "video/3gpp"
      };
      return infoDict;
    }
    case "mov": {
      infoDict = {
        extension: ".mov",
        mime: "video/quicktime"
      };
      return infoDict;
    }

    default: {
      //alert('Please select valid file');
      return infoDict;
    }
  }
};

renderPlaceHolder = () => {
  return (
    <Placeholder.Box
      size={200}
      color="rgba(218,45,249,1)"
      hasRadius
      animate="fade"
    />
  );
};

renderPlaceHolderArtist = () => {
  return (
    <Placeholder.Box
      size={deviceWidth / 2}
      color="rgba(218,45,249,1)"
      hasRadius
      animate="fade"
    />
  );
};

export const fontFamilies = {
  //ITCAvantGardeStdBook
  ITCAvantGardeStdBook: "ITCAvantGardeStd-Bk",
  ITCAvantGardeStdBookCn: "ITCAvantGardeStd-BkCn",
  ITCAvantGardeStdBkCnObl: "ITCAvantGardeStd-BkCnObl",
  ITCAvantGardeStdBkObl: "ITCAvantGardeStd-BkObl",

  //ITCAvantGardeStdBold
  ITCAvantGardeStdBold: "ITCAvantGardeStd-Bold",
  ITCAvantGardeStdBoldCn: "ITCAvantGardeStd-BoldCn",
  ITCAvantGardeStdBoldCnObl: "ITCAvantGardeStd-BoldCnObl",
  ITCAvantGardeStdBoldObl: "ITCAvantGardeStd-BoldObl",

  //ITCAvantGardeStdDemi fontFamily
  ITCAvantGardeStdDemi: "ITCAvantGardeStd-Demi",
  ITCAvantGardeStdDemiCn: "ITCAvantGardeStd-DemiCn",
  ITCAvantGardeStdDemiCnObl: "ITCAvantGardeStd-DemiCnObl",
  ITCAvantGardeStdDemiObl: "ITCAvantGardeStd-DemiObl",

  //ITCAvantGardeStdMd fontFamily
  ITCAvantGardeStdMd: "ITCAvantGardeStd-Md",
  ITCAvantGardeStdMdCn: "ITCAvantGardeStd-MdCn",
  ITCAvantGardeStdMdCnObl: "ITCAvantGardeStd-MdCnObl",
  ITCAvantGardeStdMdObl: "ITCAvantGardeStd-MdObl",

  //ITCAvantGardeStdXLt fontFamily
  ITCAvantGardeStdXLt: "ITCAvantGardeStd-XLt",
  ITCAvantGardeStdXLtCn: "ITCAvantGardeStd-XLtCn",
  ITCAvantGardeStdXLtCnObl: "ITCAvantGardeStd-XLtCnObl",
  ITCAvantGardeStdXLtObl: "ITCAvantGardeStd-XLtObl",

  //AvantGarde-Bold
  AvantGardeBold: "AvantGarde-Bold",
  AvantGardeBoldObl: "AvantGarde-BoldObl",
  AvantGardeBook: "AvantGarde-Book",
  AvantGardeBookOblique: "AvantGarde-BookOblique",

  //AvantGardeCondBold
  AvantGardeCondBold: "AvantGarde-CondBold",
  AvantGardeCondBook: "AvantGarde-CondBook",
  AvantGardeCondDemi: "AvantGarde-CondDemi",
  AvantGardeCondMedium: "AvantGarde-CondMedium",

  //AvantGarde-Demi
  AvantGardeDemi: "AvantGarde-Demi",
  AvantGardeDemiOblique: "AvantGarde-DemiOblique",
  AvantGardeExtraLight: "AvantGarde-ExtraLight",
  AvantGardeExtraLightObl: "AvantGarde-ExtraLightObl",

  //
  AvantGardeMedium: "AvantGarde-Medium",
  AvantGardeMediumObl: "AvantGarde-MediumObl",
  AvantGardeITCbyBTBold: "AvantGardeITCbyBT-Bold",
  AvantGardeITCbyBTBoldCondensed: "AvantGardeITCbyBT-BoldCondensed",
  AvantGardeITCbyBTBoldOblique: "AvantGardeITCbyBT-BoldOblique",

  //
  AvantGardeITCbyBTBook: "AvantGardeITCbyBT-Book",
  AvantGardeITCbyBTBookCondensed: "AvantGardeITCbyBT-BookCondensed",
  AvantGardeITCbyBTBookOblique: "AvantGardeITCbyBT-BookOblique",
  AvantGardeITCbyBTDemi: "AvantGardeITCbyBT-Demi",

  //
  AvantGardeITCbyBTDemiCondensed: "AvantGardeITCbyBT-DemiCondensed",
  AvantGardeITCbyBTDemiOblique: "AvantGardeITCbyBT-DemiOblique",
  AvantGardeITCbyBTExtraLight: "AvantGardeITCbyBT-ExtraLight",
  AvantGardeITCbyBTExtraLightObl: "AvantGardeITCbyBT-ExtraLightObl",
  AvantGardeITCbyBTMedium: "AvantGardeITCbyBT-Medium",
  //
  AvantGardeITCbyBTMediumCond: "AvantGardeITCbyBT-MediumCond",
  AvantGardeITCbyBTMediumOblique: "AvantGardeITCbyBT-MediumOblique",
  ITCAvantGardeGothicMedium: "TeX Gyre Adventor Bold",
  ITCAvantGardeGothicBdObl2: "ITCAvantGardeGothic-BdObl2",
  ITCAvantGardeGothicBdObl3: "ITCAvantGardeGothic-BdObl3",
  ITCAvantGardeGothicBdObl4: "ITCAvantGardeGothic-BdObl4",
  ITCAvantGardeGothicBold2: "ITCAvantGardeGothic-Bold2",
  //
  ITCAvantGardeGothicCondDemi2: "ITCAvantGardeGothic-CondDemi2",
  ITCAvantGardeGothicDemi2: "ITCAvantGardeGothic-Demi2",
  ITCAvantGardeGothicDemi3: "ITCAvantGardeGothic-Demi3",
  ITCAvantGardeGothicExLtObl2: "ITCAvantGardeGothic-ExLtObl2",
  ITCAvantGardeGothicMedObliq2: "ITCAvantGardeGothic-MedObliq2",
  ITCAvantGardeGothicMedObliq3: "ITCAvantGardeGothic-MedObliq3",
  //
  ITCAvantGardeGothicOblDemi2: "ITCAvantGardeGothic-OblDemi2",
  ITCAvantGardeGothicOblDemi3: "ITCAvantGardeGothic-OblDemi3",
  ITCAvantGardeGothicOblDemi4: "ITCAvantGardeGothic-OblDemi4",
  ITCAvantGardeGothicOblDemi5: "ITCAvantGardeGothic-OblDemi5",
  ITCAvantGardeGothicOblDemi6: "ITCAvantGardeGothic-OblDemi6",
  ITCAvantGardeGothicOblDemi7: "ITCAvantGardeGothic-OblDemi7",
  ITCAvantGardeGothicOblDemi8: "ITCAvantGardeGothic-OblDemi8",
  ITCAvantGardeGothicOblique2: "ITCAvantGardeGothic-Oblique2",

  ItcAvantGardeGothicBookRegular:
    Platform.OS === "android" ? "gothicbook" : "TeX Gyre Adventor"
};

export const PlaceHolderComponent = () => {
  let mapArray = [1];
  let placeHolderArray = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    10,
    10,
    101,
    101,
    10,
    34,
    10,
    10,
    10,
    101,
    101,
    10,
    34,
    10,
    10,
    10,
    101,
    101,
    10,
    34
  ];
  return (
    <View
      style={{
        marginTop: 50,
        backgroundColor: "rgba(70,6,80,1)",
        justifyContent: "flex-start",
        paddingHorizontal: 10,
        height: deviceHeight
      }}
    >
      {mapArray.map((item, index) => {
        return (
          <View key={`${index}`} style={{ marginTop: 5 }}>
            <Placeholder.ImageContent
              size={100}
              animate="fade"
              lineNumber={4}
              lineSpacing={7}
              lastLineWidth="40%"
              color={"rgba(218,45,249,1)"}
            />
          </View>
        );
      })}
      <View
        style={{
          marginVertical: 10,
          flexDirection: "row",
          justifyContent: "space-around"
        }}
      >
        {placeHolderArray.map((item, index) => {
          if (index > 4) {
            return null;
          }
          return (
            <Placeholder.Box
              size={100}
              key={`index_${index}`}
              color="rgba(218,45,249,1)"
              hasRadius
              animate="fade"
            />
          );
        })}
      </View>
      <View>
        <GridView
          itemDimension={deviceWidth / 5}
          items={placeHolderArray}
          renderItem={renderPlaceHolder}
          spacing={10}
        />
      </View>
    </View>
  );
};

export const ArtistPlaceHolderComponent = () => {
  let mapArray = [1];
  let placeHolderArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 101, 101, 10];
  return (
    <View
      style={{
        backgroundColor: "rgba(70,6,80,1)",
        justifyContent: "center",
        paddingHorizontal: 0,
        alignItems: "center"
      }}
    >
      {mapArray.map((item, index) => {
        return (
          <View key={`${index}`} style={{ marginTop: 5 }}>
            <Placeholder.ImageContent
              size={deviceWidth - 10}
              animate="fade"
              lineNumber={0}
              lineSpacing={7}
              lastLineWidth="40%"
              color={"rgba(218,45,249,1)"}
            />
          </View>
        );
      })}

      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <GridView
          //itemDimension={deviceWidth/4}
          items={placeHolderArray}
          renderItem={renderPlaceHolderArtist}
          spacing={6}
        />
      </View>
    </View>
  );
};

let placeHolderArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 101, 101, 10];
export const GenreListPlaceHolder = () => {
  let placeHolderArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 101, 101, 10];
  return (
    <View
      style={{
        backgroundColor: "rgba(70,6,80,1)",
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <Placeholder.ImageContent
        size={deviceWidth}
        animate="fade"
        lineNumber={0}
        position={"left"}
        lineSpacing={7}
        lastLineWidth="40%"
        color={"rgba(218,45,249,1)"}
      />

      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <GridView
          itemDimension={deviceWidth / 3.5}
          items={placeHolderArray}
          renderItem={() => {
            return (
              <Placeholder.Box
                height={deviceWidth / 3.5}
                color="rgba(218,45,249,1)"
                animate="fade"
                width={deviceWidth / 3.5}
                borderRadius={0}
                backgroundColor={"rgba(218,45,249,1)"}
                spacing={10}
              />
            );
          }}
          spacing={10}
        />
      </View>
    </View>
  );
};

export const ArtistListPlaceHolder = () => {
  return (
    <View
      style={{
        backgroundColor: "rgba(70,6,80,1)",
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <GridView
        itemDimension={deviceWidth / 3.5}
        items={placeHolderArray}
        renderItem={() => {
          return (
            <Placeholder.Box
              height={deviceWidth / 3.5}
              color="rgba(218,45,249,1)"
              animate="fade"
              width={deviceWidth / 3.5}
              borderRadius={0}
              backgroundColor={"rgba(218,45,249,1)"}
              spacing={10}
            />
          );
        }}
        spacing={10}
      />
    </View>
  );
};

export const VenuesDetailPlaceHolder = () => {
  return (
    <View
      style={{
        backgroundColor: "rgba(70,6,80,1)",
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <Placeholder.ImageContent
        size={deviceWidth}
        animate="fade"
        lineNumber={0}
        position={"left"}
        lineSpacing={7}
        lastLineWidth="40%"
        color={"rgba(218,45,249,1)"}
      />

      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <GridView
          itemDimension={deviceWidth - 20}
          items={placeHolderArray}
          renderItem={() => {
            return (
              <Placeholder.ImageContent
                size={100}
                animate="fade"
                lineNumber={5}
                position={"left"}
                lineSpacing={7}
                lastLineWidth="40%"
                color={"rgba(218,45,249,1)"}
              />
            );
          }}
          spacing={6}
        />
      </View>
    </View>
  );
};

export const ArtistPlaceHolderComponentData = () => {
  return (
    <View>
      <Placeholder.ImageContent
        size={deviceWidth}
        animate="fade"
        lineNumber={0}
        position={"left"}
        lineSpacing={7}
        lastLineWidth="40%"
        color={"rgba(218,45,249,1)"}
      />
      <View style={{ flexDirection: "row" }}>
        <Placeholder.ImageContent
          size={deviceWidth}
          animate="fade"
          lineNumber={0}
          position={"left"}
          lineSpacing={7}
          lastLineWidth="40%"
          color={"rgba(218,45,249,1)"}
        />
      </View>
    </View>
  );
};
