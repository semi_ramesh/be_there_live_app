import Singleton from 'singleton';
import { SERVICE_API } from 'app/constant/appConstant';
import FrisbeeConnector from './frisbeeConnector';
const { REQUEST_METHODS: { POST } } = SERVICE_API;
import {Platform,AsyncStorage} from 'react-native'
 class ServiceApi extends Singleton {

     /**  Constructor **/
     constructor() {
         super();
         this.connector = new FrisbeeConnector();
     }

     /** LogIn Api Call **/
     async logIn(userName, password) {

         let deviceToken = '';
         try{
             const value = await AsyncStorage.getItem('device_token');
             if (value !== null) {
                 // We have data!!
                 deviceToken = value;
                 console.log(value);
             }
         } catch (error) {
             // Error retrieving data
         }
         try {
             const path = 'auth/login';
             var formData = new FormData();
             formData.append("email", `${userName}`);
             formData.append("password", password);
             formData.append('deviceId',deviceToken);
             formData.append('deviceType',Platform.OS);
             const response = await this.connector.performRequest(POST, path, {formData});

             const userData = response;
             //console.log('Daatttta',JSON.parse(userData.toString()));
             if (userData == null)
                 throw new Error(`Data validation failed for User ${JSON.stringify(response.body)}`);

             return userData;
         } catch (error) {
             throw error;
         }
      }
     
          /** Register user API call **/
          registerUser = async(formData) =>{
              try {
                 const path = 'auth/signup';
                 const response = await this.connector.performRequest(POST, path, {formData});
                 console.log('Response',response);
                 const userData = response;
                 if (userData == null)
                 throw new Error(`Data validation failed for User ${JSON.stringify(response.body)}`);

                 return userData;
         }
         catch (error) {
             throw error;
         }
       };

         /**Forget Password Api **/
         forgetPassword = async(formData) =>{
         try{
             const path = 'auth/forgetpassword';
             const response = await this.connector.performRequest(POST, path, {formData});
             console.log('Response',response);
             const responseData = response ;
             if (responseData == null)
                 throw new Error(`Data validation failed for User ${JSON.stringify(response )}`);

             return responseData;
         }
         catch(e){
             throw error;
         }
        }

     /***
      * ResetPassword Api call
      */
     resetPassword = async(formData) =>{
         try{
             const path = 'auth/resetpassword';
             const response = await this.connector.performRequest(POST, path, {formData});
             const responseData = response ;
             if (responseData == null)
                 throw new Error(`Data validation failed for User ${JSON.stringify(response )}`);

             return responseData;
         }
         catch(e){
             throw error;
         }
     };

     subscribeUser = async(formData) => {
         try{
             const path = 'auth/subscribe';
             const response = await this.connector.performRequest(POST, path, {formData});
             const responseData = response ;
             if (responseData == null)
                 throw new Error(`Data validation failed for User ${JSON.stringify(response )}`);

             return responseData;
         }
         catch(e){
             throw error;
         }
     }


     makePaymentBasedOnFormData = async (formData) =>{
         try{
             const path = 'subscribe';
             const response = await this.connector.performRequest(POST, path, {formData});
             const responseData = response ;
             if (responseData == null)
                 throw new Error(`Data validation failed for User ${JSON.stringify(response )}`);

             return responseData;
         }
         catch(e){
             throw error;
         }
     };


     resendLinkToVerifyEmail = async(formData) =>{
         try{
             const path = 'resend-link';
             const response = await this.connector.performRequest(POST, path, {formData});
             const responseData = response ;
             if (responseData == null)
                 throw new Error(`Data validation failed for User ${JSON.stringify(response )}`);

             return responseData;
         }
         catch(e){
             throw error;
         }
     }

 }
   export default ServiceApi.get();