
import Frisbee from 'frisbee';
import qs from 'qs';

import { SERVICE_API } from 'app/constant/appConstant';
import axios from 'axios';
const api =(method,formdata,path,transformer)=>{

    return axios({
        method: method,
        headers: {
            'Accept': 'application/json',
            'Content-Type': "application/json; charset=utf-8",

        },
        url: SERVICE_API.BASE_URI+path,
        transformResponse:[transformer],
        data:formdata

    });
};


const frisbee = new Frisbee(
    {   baseURI:SERVICE_API.BASE_URI,
        headers: {
          'Content-Type': 'multipart/form-data'
        }
    });


const newFrisBee = new Frisbee({
    baseURI:'https://randomuser.me/api/',
    headers: {
    'Content-Type': 'multipart/form-data'}
})

let fetchData = (method,data,path)=> {

    console.log('formdata',data);
    return fetch(`${SERVICE_API.BASE_URI}${path}`, {

        method: method, // *GET, POST, PUT, DELETE, etc.
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        headers: {
            'Accept': 'application/json',
        },
        body: data
    })
}

const arrayFormat = 'indices';


export default class FrisbeeConnector {

    newRequest = async(method,formData,paths)=>{
      try {
           const response = await newFrisBee[method](`?results=5`);
           return response
      }
      catch (e) {
          console.log("error",e);
      }

    }

     performRequest= async(method,paths,opts ={})=>{
        try{
            const { formData } = opts;
            console.log("bho",formData);

           // let response  = await api(method,formData,paths,(data=>{
           //     //let responseData  = JSON.stringify(data);
           //     //let x = JSON.parse(responseData);
           //     //console.log('data______',x);
           //     return data
           //  }));
           const response = await frisbee[method](`${paths}`, { body: formData });
           // // const response = await fetchData(method,formData,paths);
           //  console.log("bhfo",response);
           //  let oldStr = response.data;
           // // oldStr = oldStr.slice(0, oldStr.length-1);
           //  console.log('remove',  oldStr);
           //  oldStr = JSON.parse(oldStr);
           //  console.log(oldStr);
          // console.log("newresponse",response.data);
            //let newResponse = JSON.stringify(response.data.trim());
            //let parseJson = this.JSONize(newResponse).trim();
           // let jsonObject = response.data.trim();
            //return JSON.parse(jsonObject);
            console.log("good__",response);
            return response.body
        }
        catch (e) {
            throw e;

        }
    }

    JSONize = (str)=> {
        return str
        // wrap keys without quote with valid double quote
            .replace(/([\$\w]+)\s*:/g, (_, $1)=>{return '"'+$1+'":'})
            // replacing single quote wrapped ones to double quote
            .replace(/'([^']+)'/g, (_, $1)=>{return '"'+$1+'"'})
    }




    /** extract errors **/
    extractErrorsFromResponse =(response) =>{
        const { SUCCESS, UNAUTHORIZED } = SERVICE_API.RESPONSE_STATUSES;

        const { status, body } = response;

        if (status === SUCCESS && body != null)
            return;

        if (status != null && status === UNAUTHORIZED)
            throw new Error();

        if (body != null && body.error_description != null)
            throw new Error(body.error_description);
        //console.log("This is body:",body)
        throw new Error(`${body.message}`);
    }



}
