import {Alert} from "react-native";

export const Alerts = {

    success: (message) => {
        Alert.alert('Success', message);
    },

    error: (message) => {
        Alert.alert('Ooops', message);
    },

    warning: (message) => {
        Alert.alert('Warning', message);
    },

    info: (message) => {
        Alert.alert('Info', message);
    }

}
