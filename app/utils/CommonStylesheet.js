import {StyleSheet, Dimensions} from 'react-native';
import {customPurple} from "../Components/Authentication/styles";
import {fontFamilies} from "../constant/appConstant";
const {height, width} =  Dimensions.get('window');

export default StyleSheet.create({
    whiteColor: {
        color:'white',
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
    },
    mt5: {
        marginTop:5
    },
    mt10: {
        marginTop:10
    },
    titles: {
        color: 'white',
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular
    },
    formTitle: {
        color: customPurple,
        fontFamily:fontFamilies.ItcAvantGardeGothicBookRegular,
        fontSize:10
    }

});

export const styleConstant = {
    buttonBackground:'#311437',
    buttonTitleColor: '#B427CE'
}