
import Singleton from 'singleton';
import { SERVICE_API } from 'app/constant/appConstant';
const { REQUEST_METHODS: { POST } } = SERVICE_API;
import {Platform,AsyncStorage} from 'react-native'
import ImagePicker from 'react-native-image-picker';
class BTLImagePickerController extends Singleton{
    constructor(props){
        super(props);
        this.imagePicker = ImagePicker;


    }

    pickImage = () =>{

        return new Promise((resolve , reject)=>{
            let options = {
                title: 'Select Avatar',

                storageOptions: {
                    skipBackup: true,
                    path: 'images'
                }
            };
            this.imagePicker.showImagePicker(options, (response) => {
                console.log('Response = ', response);

                if (response.didCancel) {
                    reject("User cancelled image picker");
                    console.log('User cancelled image picker');
                }
                else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                    reject(response.error)
                }
                else if (response.customButton) {
                    console.log('User tapped custom button: ', response.customButton);

                }
                else {
                    let source = Platform.OS === 'ios'?{ uri: response.uri,url:response.uri ,data:response.data }:{url:response.path, uri: response.uri,data:response.data};

                    resolve (source);

                }
            });
        });
    }

    pickVideos = () =>{

        return new Promise((resolve , reject)=>{
            let options = {
                title: 'Select Video',
                mediaType:'video',
                takePhotoButtonTitle:'Record Video',
                storageOptions: {
                    skipBackup: true,
                    path: 'videos'
                }
            };
            this.imagePicker.showImagePicker(options, (response) => {
                console.log('Response = ', response);

                if (response.didCancel) {
                    reject("User cancelled image picker");
                    console.log('User cancelled image picker');
                }
                else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                    reject(response.error)
                }
                else if (response.customButton) {
                    console.log('User tapped custom button: ', response.customButton);

                }
                else {
                    let source = Platform.OS === 'ios'?{ uri: response.uri,url:response.uri ,data:response.data }:{url:response.path, uri: response.uri,data:response.data};
                    resolve (source);

                }
            });
        });
    }






}

export default BTLImagePickerController.get()