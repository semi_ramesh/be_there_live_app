
import { AUTH } from '../types';

const DEFAULT_STATES = {
    user: null,
};

export default function authReducer(state = DEFAULT_STATES, action) {

    const { type } = action;

    if (type === AUTH.AUTHORIZE) {
        const { payload } = action;
        return { ...state,  user: payload ,token:payload.oauth_access_token.id};
    }

    if(type === AUTH.DEAUTHORIZE){

        return{...state,user:null,token:null}
    }

    return state;
}