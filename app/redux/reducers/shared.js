import _ from 'lodash';

// Utils


// Locals
import { AXIOS_REQUEST_SUFFIXES } from '../constant';
import {  SERVICE_API} from '../types';

const REQUEST_START_TYPES = Object.values(SERVICE_API);
const REQUEST_FINISH_TYPES = _.flatMap(
  Object.values(AXIOS_REQUEST_SUFFIXES),
  suffix => (REQUEST_START_TYPES.map(type => type + suffix))
);
const TRY_KEYS = _.flatMap(Object.values(AXIOS_REQUEST_SUFFIXES) ,
        suffix => (REQUEST_START_TYPES.map( type => type+suffix)))
const DEFAULT_STATES = {
  /**
   * For redux-persist auto rehydration.
   * This state will be set to true in `createStorePersistor` when the rehydration done
   * Use this everywhere to start any initialized actions AFTER this state is `true`
   */

   isLoading: false,
};

export default function sharedReducer(state = DEFAULT_STATES, action) {
  const { type } = action;

  if (_.includes(REQUEST_START_TYPES, type)) {

    return {...state , isLoading:true};
  }

  if (_.includes(REQUEST_FINISH_TYPES, type)) {

    return {...state,isLoading:false};
  }

  return state;
}