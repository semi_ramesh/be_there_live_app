import { combineReducers } from 'redux';
import auth from './auth';
import shared from './shared';
import serviceApi from './serviceApi'

export default combineReducers({ auth,shared,serviceApi });