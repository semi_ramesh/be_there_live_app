
/**
 * Service api reducers
 * **/


import _ from 'lodash';

import { AXIOS_REQUEST_SUFFIXES } from '../constant';
import { SERVICE_API } from '../types';
const { ERROR, SUCCESS } = AXIOS_REQUEST_SUFFIXES;
const ERROR_ACTIONS = Object.values(SERVICE_API).map(action => action + ERROR);
const SUCCESS_ACTIONS = Object.values(SERVICE_API).map(action => action + SUCCESS);

const DEFAULT_STATES = {
    errors: null,
    monthDetail:[],
    weekDetail:null,
    eachWeekDetail:null,
    yourHealthHistory:[],
    listData:[]

};

export default function serviceApiReducer(state = DEFAULT_STATES, action) {

  const { type } = action;

  if (_.includes(ERROR_ACTIONS, type)) {
    const { errors } = state;

    return {
      ...state,
      errors: { ...errors, ...action.payload,},
    };
  }

  if (_.includes(SUCCESS_ACTIONS, type)) {
    const { data } = action.payload;
    return { ...state, ...data };
  }

  return state;
}
