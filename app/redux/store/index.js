import reducers from '../reducers';
import { applyMiddleware, createStore, compose } from 'redux';
import axiosMiddleware from './axiosMiddileware';
import reduxThunk from 'redux-thunk';
//import reduxReset from 'redux-reset';

const store = createStore(
  reducers,
    compose(
    applyMiddleware(reduxThunk, axiosMiddleware),
  )
);
export  default store;