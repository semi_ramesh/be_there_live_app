
/** Axios MiddileWare **/

import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import { createAction } from 'redux-actions';
import {Actions} from 'react-native-router-flux';
import {AsyncStorage} from 'react-native'
// Constants
import { SERVICE_API } from 'app/constant/appConstant';

// Locals
import { auth } from '../actions';
import { AXIOS_REQUEST_SUFFIXES } from '../constant';
const { ERROR, SUCCESS } = AXIOS_REQUEST_SUFFIXES;

/** Creating axios client **/
const axiosClient = axios.create({
  baseURL: SERVICE_API.BASE_URI,
  responseType: 'json',
});

export default axiosMiddleware(axiosClient, {
  errorSuffix: ERROR,
  successSuffix: SUCCESS,
  onError: ({ action, dispatch, error }) => {
    const { UNAUTHORIZED } = SERVICE_API.RESPONSE_STATUSES;
    try {
      const { status } = error.response;
      //TODO: 401 error will be handled by alert
      // Deauthorize user on 401 status response
      if (status === UNAUTHORIZED) {
          AsyncStorage.getItem('userData',(err,result)=>{
              if(result){
                  AsyncStorage.removeItem('userData',()=>{
                      dispatch(auth.deAuthorize());
                      Actions.LOGIN();
                      alert('You are logged in from another device.');
                  });
              }
          });


         return;
      }

      console.log('error',error);

      const { payload: { dataKey }, type, types } = action;
      const actionType = type === null ? types[types.length - 1] : type + ERROR;
      const errorAction = createAction(actionType);
      dispatch(errorAction({ [dataKey || actionType]: error }));
    } catch (_) {
          console.log('ertt',_);
          const { payload: { dataKey }, type, types } = action;
          const actionType = type === null ? types[types.length - 1] : type + ERROR;
          const errorAction = createAction(actionType);
       dispatch(errorAction({ [dataKey || actionType]: error }));
      //console.log('some thing went wrong in AXIOS_MODDILEWARE')
    }
  },
});


