


/** Service api engagged with redux to state Management Tool **/
import { createAction } from 'redux-actions';
import axios from 'axios';
import _ from 'lodash';
import { DEVICE_TYPE, AXIOS_REQUEST_SUFFIXES } from '../constant';
import { SERVICE_API } from '../types';

import {showInfoMessage} from  'app/constant/appConstant'
import {Actions} from 'react-native-router-flux'
import {authorize} from './auth'
/** Building Header of Api **/
function buildAuthHeader(state, { jsonParams } = {}) {

  const { auth: { token: authToken } } = state;
  console.log('yyyyy',authToken);
 // const contentType = { 'Content-Type': 'application/form-data' } ;
  const contentType  = {'Content-type':'multipart/form-data'};
  const authorization =  { 'X-Requested-With': authToken };
  return { ...authorization, ...contentType };
}


/** Creating Request for apis **/
function buildRequestAction(callback, opts = {}) {


  return async (dispatch, getState) => {


    const state = getState();
    const { auth: { user: userData } } = state;
    if (userData === null) return;

    const options = callback(userData);
    if (options === null) return;

    const {
      actionType, dataKey, headers: passedHeaders, jsonParams, transformer, ...rest
    } = options;

    const headers = { ...passedHeaders, ...buildAuthHeader(state, { jsonParams }) };
    const transformResponse = data => {
      try {

          console.log("mydataStatus",data);

          return transformer(data);

      } catch (_)
      {
          return data;
      }
    };

    const action = createAction(actionType);
    const request = { headers, transformResponse, ...rest };
    dispatch(action({ dataKey, request }));
  };
}

/**  detail now **/
 export function getProfile() {
                        console.log("this is called from profile");
                        const  dataKey =  'myProfile';
                        return buildRequestAction((userData)=> {
                            const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue"
                            return{
                               dataKey:dataKey,
                               actionType:SERVICE_API.GET_IND_PROFILE,
                               jsonParams: false,
                               method: 'get',
                               transformer : data => ({[dataKey]: data}),
                               url: `profile/${userData.id}`,
                           }
                        });

        }




/**
 * Unfollow or follow for Individuals users
 * @param formData
 */
 export  function followUnfollow({formData,dataKey}) {
     console.log('followUNFOLLOW',formData,'dataKey',dataKey);

     return buildRequestAction((userData)=> {
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.FOLLOW_UNFOLLOW,
            data:formData,
            jsonParams: false,
            method: 'post',
            transformer : data => ({[dataKey]: data.additional}),
            url: `change-follow-status/${userData.id}`,
        }
    });

 }

export  function venuefollowUnfollow(formData) {
    console.log('SELECTED lIST',formData);
    const  dataKey =  'venueLists';
    return buildRequestAction((userData)=> {
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.FOLLOW_UNFOLLOW,
            data:formData,
            jsonParams: false,
            method: 'post',
            transformer : data => {
                //showInfoMessage(data.message);
                return({[dataKey]: data.additional.venues_data})
            },
            url: `change-follow-status/${userData.id}`,
        }
    });

}





 /**
  * Upload Profile Picture
  *
  * **/
 export  function uploadProfilePicture (formData){
     console.log('profileImage',formData);
     const  dataKey =  'profileImage';
     return buildRequestAction((userData)=> {
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue"
         return{
             dataKey:dataKey,
             actionType:SERVICE_API.UPLOAD_PROFILE_IMAGE,
             data:formData,
             jsonParams: false,
             method: 'post',
             transformer : data => {
                 showInfoMessage(data.message);
                 return({[dataKey]: data})
             },
             url: `${userType}/profileimage/${userData.id}`,
        }
     });
  };


 /** Upload Audio or Video
  * @param formData
  * @returns {*}
  */
  export function uploadvideosForArtist(formData){

      console.log("videoupload",formData);
      const dataKey = 'uploadedVideos';
     return buildRequestAction((userData)=>{
      console.log("user id is:",)
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue"
         return{
             dataKey:dataKey,
             actionType:SERVICE_API.UPLOAD_VIDEOS,
             data:formData,
             jsonParams: false,
             method: 'post',
             transformer : data => {
                 showInfoMessage(data.message);
                 return({[dataKey]: data})
             },
             url: `artist/addprofilevideo/${userData.id}`,
         }
     });

  }

 export function uploadCrouselImages(formData) {
     const dataKey = 'crouselImages';
   console.log("crousel Images",formData);
     return buildRequestAction((userData)=>{
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue"
         return{
             dataKey:dataKey,
             actionType:SERVICE_API.UPLOAD_CROUSEL_IMAGE,
             data:formData,
             jsonParams: false,
             method: 'post',
             transformer : data => {
                 showInfoMessage(data.message);
                 return({[dataKey]: data})
             },
             url: `${userType}/add-carousel-images/${userData.id}`,
         }
     });
 }


/**
 * Get Profile Api
 */
export function getProfileForEdit() {
 console.log("how many times");
 const dataKey = 'userProfile';
    return buildRequestAction((userData)=> {
        const userType = userData.user_type_id === 5?'individual':userData.user_type_id  === 3 ? "artist":"venue"
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.GET_PROFILE_MYGENERE,
            jsonParams: false,
            method: 'get',
            transformer : data => ({[dataKey]: data}),
            url: `${userType}/getprofile/${userData.id}`,
        }
    });

 }

/**
 * Get Profile Api for MyGenere
 */

 export function getMyProfileGenere() {
    //console.log('myProfile',userData);
    const  dataKey =  'myGenreList';
    return buildRequestAction((userData)=> {
        const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue"
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.GET_PROFILE_MYGENERE,
            jsonParams: false,
            method: 'get',
            transformer : data => ({[dataKey]: data}),
            url: `${userType}/getprofile/${userData.id}`,
        }
    });

 }

 export function  addorRemoveGenre(followStatus,genreId){
     console.log('form___',followStatus,genreId);
     const  dataKey =  'genereList';
     return buildRequestAction((userData)=> {
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue"
         return{
             dataKey:dataKey,
             actionType:SERVICE_API.GET_PROFILE_MYGENERE,
             jsonParams: false,
             method: 'get',
             transformer : data => ({[dataKey]: data}),
             url: `individual/update-user-genre/${genreId}/${userData.id}/${followStatus}`,
         }
     });
 }


/** Get Profile and Post Profile **/
 export  function updateProfile(formData) {
     console.log('ProfileData',formData);
     const  dataKey =  'ProfileData';
     return buildRequestAction((userData)=> {
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue"
        console.log('path',`${userType}/editprofile/${userData.id}`)

         return{
            dataKey:dataKey,
            actionType:SERVICE_API.POST_PROFILE,
            data:formData,
            jsonParams: false,
            method: 'post',
            transformer : data => {
                console.log("_____",data);
                showInfoMessage(data.message);
                getProfile(userData);
                return({[dataKey]: data})
            },
            url: `${userType}/editprofile/${userData.id}`,
        }
    });

 }

/**
 * Get Notification setting for every type of users
 *
 */
 export function getNotificationSetting(formData) {
     console.log("notification_data",formData);
     const dataKey = 'notification';
     return buildRequestAction((userData)=>{
        const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.GET_NOTIFICATION,
            jsonParams: false,
            method: 'get',
            transformer : data => {
                //showInfoMessage(data.message);
                return({[dataKey]: data})
            },
            url: `${userType}/notification/${userData.id}`,
        }
    })

 }

/**
 * Update Notification
 * @param formData
 * @returns {*}
 */
 export function updateNotification(formData) {

    console.log("notification",formData);
    const dataKey = 'notification';
    return buildRequestAction((userData)=>{
        const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.EDIT_NOTIFICATION,
            jsonParams: false,
            data:formData,
            method: 'post',
            transformer : data => {
                showInfoMessage(data.message);
                getNotificationSetting(userData);
                return({[dataKey]: data.data})
            },
            url: `${userType}/editnotification/${userData.id}`,
        }
    })
 }

 /**
  * Account Detail update
  */
 export function getAccountDetail(DATA) {
     const dataKey = 'accountDetail';
     return buildRequestAction((userData)=>{
        const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.ACCOUNT,
            method: 'get',
            transformer : data => {
                console.log('Check it out',data);
                //showInfoMessage(data.message);
                return({[dataKey]: data})
            },
            url: `${userType}/account/${userData.id}`,
           }
        })
 }

/**
 * Update Account Detail
 **/
 export function updateAccountDetail(formData){
    console.log('account2formData',formData);
    const dataKey = 'accountDetail2';
    return buildRequestAction((userData)=>{
        const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.EDIT_NOTIFICATION,
            jsonParams: false,
            method: 'post',
            data :formData,
            transformer : data => {
                showInfoMessage(data.message);
                getAccountDetail(userData);
                return({[dataKey]: data})
            },
            url: `${userType}/update-account/${userData.id}`,
        }

        })
 }

 /**
  * Follow Unfollow on Profile Setting
  **/

 export function followUnFollow(formData) {
   const dataKey = 'followed';
   return buildRequestAction((userData)=>{
      return{
          dataKey:dataKey,
          actionType:SERVICE_API.FOLLOW_UNFOLLOW_ON_SETTING,
          jsonParams:false,
          method:'post',
          data:formData,
          transformer:data=>{
              showInfoMessage(data.message);
              return({[dataKey]:data})
          },
          url:'individual/'
      }
   });

   }

   //Artist things

 export function editAlbumCoverAndTitle(formData,id) {
     const dataKey = "albumData";
     console.log("+++++++",formData,`artist/edit-album/${id}`);
     return buildRequestAction((userData)=>{
         return{
             dataKey: dataKey,
             actionType:SERVICE_API.ALBUM_EDIT,
             jsonParams:false,
             method:'post',
             data:formData,
             transformer:data=>{
                 showInfoMessage(data.message);
                 return({[dataKey]:data})
             },
            url:`artist/edit-album/${id}`
         }

     })

 }

 /**
  *
  * ADD NEW ALBUM
  * **/

 export function saveTheNewAlbum(formData) {
     const dataKey = "albumData";
     console.log("+++++++",formData);
     return buildRequestAction((userData)=>{
         return{
             dataKey: dataKey,
             actionType:SERVICE_API.ALBUM_ADD_FULL,
             jsonParams:false,
             method:'post',
             data:formData,
             transformer:data=>{
                 showInfoMessage(data.message);
                 return({[dataKey]:data})
             },
             url:`artist/addprofilealbums/${userData.id}`
         }

     })
 }



  export function addNewTrack(formData,alubumId){

        const dataKey = "albumData";
        console.log("+++++++89",formData);
       return buildRequestAction((userData)=>{
          return{
              dataKey: dataKey,
              actionType:SERVICE_API.ADD_NEW_TRACKS,
              jsonParams:false,
              method:'post',
              data:formData,
              transformer:data=>{
                  console.log('musics',data);
                  showInfoMessage(data.message);
                  return({[dataKey]: data})
              },
              url:`artist/add-tracks-to-album/${alubumId}/${userData.id}`
          }

      })
  }

/**
 * Get Account
 *
 */

 export function getAccountOfArtist() {
    const dataKey = "artistAccountDetail";
    return buildRequestAction((userData)=>{
        return{
            dataKey: dataKey,
            actionType:SERVICE_API.ACCOUNT_DETAIL_ARTIST,
            jsonParams:false,
            method:'get',
            transformer:data=>{
                //showInfoMessage(data.message);
                return({[dataKey]:data})
            },
            url:`artist/account/${userData.id}`
          }
        })
  }
  
  export function updateAccountOfArtist(formData) {
      const dataKey = "artistAccountDetail";
      console.log("+++++++89",formData);
      return buildRequestAction((userData)=>{
          return{
              dataKey: dataKey,
              actionType:SERVICE_API.UPDATE_ACCOUT_ARTIST,
              jsonParams:false,
              method:'post',
              data:formData,
              transformer:data=>{
                  showInfoMessage(data.message);
                  return({[dataKey]:data})
              },
              url:`artist/update-account/${userData.id}`
          }

      })
  }

  //Artist Account Push ADD_NEW_TOUR
export function pushNotificationToFollowers(formData) {
    const dataKey = "push";
    console.log("+++++++89",formData);
    return buildRequestAction((userData)=>{
        return{
            dataKey: dataKey,
            actionType:SERVICE_API.ARTIST_PUSH,
            jsonParams:false,
            method:'post',
            data:formData,
            transformer:data=>{
                showInfoMessage('Alert Sent');
                return({[dataKey]:data})
            },
            url:`artist/add-general-notification/${userData.id}`
        }

    })
}

//Add_New_Tour_for_Artist


export function addNewTours(formData) {
    const dataKey = "toursData";
    console.log("+++++++89",formData);
    return buildRequestAction((userData)=>{
        return{
            dataKey: dataKey,
            actionType:SERVICE_API.ADD_NEW_TOUR,
            jsonParams:false,
            method:'post',
            data:formData,
            transformer:data=>{
                showInfoMessage(data.message);
                return({[dataKey]:data})
            },
            url:`artist/add-tour/${userData.id}`
        }

    })
}

 //Add new Event for venue
 export function addNewEvent(formData) {
    console.log("notification_data",formData);
    const dataKey = 'venueEventList';
    return buildRequestAction((userData)=>{
        const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.ADD_VENUE_EVENT,
            jsonParams: false,
            method: 'post',
            data:formData,
            transformer : data => {
                showInfoMessage(data.message);
                return({[dataKey]: data})
            },
            url: `${userType}/new-event-alert/${userData.id}`,
        }
    })

  }

  //Delete existing Event
  export function deleteEvent(eventId) {
    console.log("notification_data",eventId);
    const dataKey = 'venueEventList';
    return buildRequestAction((userData)=>{
        const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.DELETE_VENUE_EVENT,
            jsonParams: false,
            method: 'get',
            transformer : data => {
                showInfoMessage(data.message);
                return({[dataKey]: data})
            },
            url: `${userType}/event/delete/${eventId}`,
        }
    })
  }
  export function  sendPushAlert(eventId) {
    console.log("notification_data",eventId);
    const dataKey = 'messageData';
    return buildRequestAction((userData)=>{
        const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.SEND_PUSH_ALERT,
            jsonParams: false,
            method: 'get',
            transformer : data => {
                showInfoMessage(data.message);
                return({[dataKey]: data})
            },
            url: `${userType}/event/pushalert/${eventId}`,
        }
    })

}

/** remove Venue CrouselImage **/
export  function removeCrouselImage(id){
    console.log("notification_data",id);
    const dataKey = 'crouselImages';
    return buildRequestAction((userData)=>{
        const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.REMOVE_CROUSEL_IMAGE,
            jsonParams: false,
            method: 'get',
            transformer : data => {
                showInfoMessage(data.message);
                return({[dataKey]: data})
            },
            url: `${userType}/delete-user-picture-by-id/${id}`,
        }
    })
}


export function getAlertFeed (formData){
    console.log("notification_data",formData);
    const dataKey = 'alertFeeds';
    return buildRequestAction((userData)=>{
        const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.SEND_PUSH_ALERT,
            jsonParams: false,
            method: 'get',
            transformer : data => {
                //showInfoMessage(data.message);
                return({[dataKey]: data})
            },
            url: `alert-feed/${userData.id}`,
        }
    })


}


 //Artist Tab Apis
 export function getGenreList() {

     console.log("notification_data");
     const dataKey = 'artistGenreList';
     return buildRequestAction((userData)=>{
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
         return{
             dataKey:dataKey,
             actionType:SERVICE_API.GET_GENER_LIST_FOR_ARTIST,
             jsonParams: false,
             method: 'get',
             transformer : data => {
                 //showInfoMessage(data.message);
                 return({[dataKey]: data.data})
             },
             url: `genre/list`,
         }
     })
 }

 
  export function getArtistListForParticularGenre(Id) {
     console.log("notification_data");
     const dataKey = 'artistList';
     return buildRequestAction((userData)=>{
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
         return{
             dataKey:dataKey,
             actionType:SERVICE_API.GET_ARTIST_BY_GENERE,
             jsonParams: false,
             method: 'get',
             transformer : data => {
                 //showInfoMessage(data.message);
                 return({[dataKey]: data.data.artists_list})
             },
             url: `genre/artists/${Id}/${userData.id}`,
         }
     })
 }

  //Artist Profile for Genre Sequence
  export function getArtistsProfile(artistId){
      console.log("notification_data");
      const dataKey = 'artistProfile';
      return buildRequestAction((userData)=>{
          const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
          return{
              dataKey:dataKey,
              actionType:SERVICE_API.GET_ARTIST_BY_GENERE,
              jsonParams: false,
              method: 'get',
              transformer : data => {
               console.log("jaydeep",data);
                 // showInfoMessage(data.message);
                  return({[dataKey]: data.data.user_details})
              },
              url: `artist/getprofile/${artistId}`,
          }
      })
  }


  //SpotLight Content
  export function getSpotLightData(){
    const dataKey = 'spotLight';
      return buildRequestAction((userData)=>{
          const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
          return{
              dataKey:dataKey,
              actionType:SERVICE_API.GET_SPOT_LIGHTED_CONTENT,
              jsonParams: false,
              method: 'get',
              transformer : data => {
                 // showInfoMessage(data.message);
                  return({[dataKey]: data})
              },
              url: `spotlight/${userData.id}`,
          }
      })



 }


 //Change the Follow status
 export function changeFollowsStatusOfArtist(genreId,formData) {
   ///genre/artists/change-follow-status/{genre_id}/{follower_id}
     const dataKey = 'artistListOfGenre';
     return buildRequestAction((userData)=>{
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
         return{
             dataKey:dataKey,
             actionType:SERVICE_API.CHANGE_STATUS_OF_ARTIST,
             jsonParams: false,
             method: 'post',
             data:formData,
             transformer : data => {

                 console.log("artist_genere",data);
                 getProfile(userData.id)
                 //showInfoMessage(data.message);
                 return({[dataKey]: data.artists_list})
             },
             url: `genre/artists/change-follow-status/${genreId}/${userData.id}`,
         }
     })

 }


 //Venue All Apis
 export  function getVenueList(){
     const dataKey = 'venueCrouselArray';
     return buildRequestAction((userData)=>{
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
         return{
             dataKey:dataKey,
             actionType:SERVICE_API.GET_VENUE_CROUSEL,
             jsonParams: false,
             method: 'get',
             transformer : data => {
                 //showInfoMessage(data.message);
                 return({[dataKey]: data.data})
             },
             url: `venue/${userData.id}`,
         }
     })
 }

 export function venueProfileDetail(venueId) {
     const dataKey = 'venueDetail';
     return buildRequestAction((userData)=>{
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
         return{
             dataKey:dataKey,
             actionType:SERVICE_API.GET_VENUE_PROFILE_DETAIL,
             jsonParams: false,
             method: 'get',
             transformer : data => {
                 //showInfoMessage(data.message);
                 return({[dataKey]: data.data.venue_profile})
             },
             url: `venue/profile/${venueId}`,
         }
     })

 }
 
 export  function venueDetailList(venueId) {
    console.log("RESULTS",venueId);
    // venue/profile/
     const dataKey = 'venueEvents';
     return buildRequestAction((userData)=>{
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
         return{
             dataKey:dataKey,
             actionType:SERVICE_API.GET_VENUE_EVENT_LIST,
             jsonParams: false,
             method: 'get',
             transformer : data => {
                 console.log("___venueListing",data);
                 //showInfoMessage(data.message);
                 return({[dataKey]: data.data})
             },
             url: `venue/events/${venueId}`,
         }
     })
 }


 


 export  function venueListNonCrousel() {
     const dataKey = 'venueLists';
     return buildRequestAction((userData)=>{
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
         return{
             dataKey:dataKey,
             actionType:SERVICE_API. GET_VENUE_LIST,
             jsonParams: false,
             method: 'get',
             transformer : data => {
                 showInfoMessage(data.message);
                 return({[dataKey]: data.data})
             },
             url: `venue/list/${userData.id}`,
         }
     })
 }


 export  function seacrhDataforTerm(searchTerm) {

     const dataKey = 'searchResults';
     return buildRequestAction((userData)=>{
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
         return{
             dataKey:dataKey,
             actionType:SERVICE_API. SEARCH_RESULTS,
             jsonParams: false,
             method: 'get',
             transformer : data => {
                 //showInfoMessage(data.message);
                 return({[dataKey]: data})
             },
             url: `search/${searchTerm}`
         }
     })

 }

 export function searchNextFormData(formData,searchTerm) {
    console.log('search-next',formData,"dd",searchTerm);

     const dataKey = 'nextSearchResults';
     return buildRequestAction((userData)=>{
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
         return{
             dataKey:dataKey,
             actionType:SERVICE_API.SEARCH_RESULTS_SHOW_MORE,
             jsonParams: false,
             method: 'post',
             data:formData,
             transformer : data => {
                 console.log('Search_next_results',data);
                 //showInfoMessage(data.message);
                 return({[dataKey]: data})
             },
             url: `search-next/${searchTerm}`
         }
     })


 }

 export function changeTheLikeStatus(formData) {

     console.log('er-next',formData,"dd");

     const dataKey = 'likedSongs';
     return buildRequestAction((userData)=>{
         const userType = userData.user_type_id === 2?'individual':userData.user_type_id  === 3 ? "artist":"venue";
         return{
             dataKey:dataKey,
             actionType:SERVICE_API.LIKE_SPOT_LIGHT,
             jsonParams: false,
             method: 'post',
             data:formData,
             transformer : data => {
                 console.log('like_results',data);
                 //showInfoMessage(data.message);
                 return({[dataKey]: data.additional.trending_songs})
             },
             url: `change-like-status/${userData.id}`
         }
     })

 }
 export function getPlanForFeatured() {
    const dataKey = 'featuredPlan';
    return buildRequestAction((userData)=>{
       return{
           dataKey:dataKey,
           actionType:SERVICE_API.GET_SPOTLIGHEN_PLAN_DETAIL,
           jsonParams:false,
           method:'get',
           transformer:data=>{
               console.log('plan_data',data);
               return{[dataKey]:data}},
           url :'promotions/plans'
         }
    });
}

export function payAndGetfeaturedAmongUsers(formData){

    console.log('form_fetaures',formData);
    const dataKey = 'features';

    return buildRequestAction((userData)=>{
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.PAY_AND_GET_FETURED,
            jsonParams:false,
            data:formData,
            method:'post',
            transformer:data=>{
                if(data.status === true){
                    //authorize(data.data.user_details);
                    Actions.SpotLightCongrats({spotLightData:data.data.user_details})
                }
                showInfoMessage(data.message);

                console.log('plan_data',data);
                return{[dataKey]:data}},
            url :'promotions/pay'
        }
    });
}




/**
 * TODO:MOHIT
 **/


/** remove Artist Track **/
export  function removeTrack(id){
    const dataKey = 'albumData';
    return buildRequestAction((userData)=>{
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.REMOVE_TRACK,
            jsonParams: false,
            method: 'get',
            transformer : data => {
                showInfoMessage(data.message);
                return({[dataKey]: data})
            },
            url: `artist/delete-single-track-by-id/${id}`,
        }
    })
}


/** remove Artist Track **/
export  function deleteVideo(id){
    console.log("Albums Data",id);
    const dataKey = 'uploadedVideos';
    return buildRequestAction((userData)=>{
        return{
            dataKey:dataKey,
            actionType:SERVICE_API.REMOVE_VIDEO,
            jsonParams: false,
            method: 'get',
            transformer : data => {
                showInfoMessage(data.message);
                return({[dataKey]: data})
            },
            url: `artist/delete-user-video-by-id/${id}`,
        }
    })
}