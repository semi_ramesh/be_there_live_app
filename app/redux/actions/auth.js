/**
 * redux-saga
 */

import {createAction} from 'redux-actions'
import  {AUTH} from "../types";

export  const authorize = createAction(AUTH.AUTHORIZE);
export  const deAuthorize = createAction(AUTH.DEAUTHORIZE);

