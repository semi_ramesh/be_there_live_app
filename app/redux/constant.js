
import {Platform} from 'react-native';
export const AXIOS_REQUEST_SUFFIXES = { ERROR: ':ERROR', SUCCESS: ':SUCCESS' };

export const DEVICE_TYPE = Platform.select({ android: 2, ios: 1 });
