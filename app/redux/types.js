/**
 * @ProvidesModule BeThereLive.Redux.Types
 */


export const AUTH = {
    AUTHORIZE:            'AUTH:AUTHORIZE',
    DEAUTHORIZE:          'AUTH:DEAUTHORIZE',

};

export  const SERVICE_API = {

     SET_LMP :'SERVICE_API:SET_LMP',
     GET_IND_PROFILE :'SERVICE_API_GET_PROFILE',
     FOLLOW_UNFOLLOW:'SERVICE_API_FOLLOW_UNFOLLOW',
     POST_PROFILE:'SERVICE_API_POST_PROFILE',
     GET_NOTIFICATION:'SERVICE_API_GET_NOTIFICATION',
     EDIT_NOTIFICATION:'SERVICE_API_EDIT_NOTIFICATION',
     ACCOUNT:'SERVICE_API_ACCOUNT',
     EDIT_ACCOUNT:'SERVICE_API_EDIT_ACCOUNT',
     GET_PROFILE_MYGENERE:'SERVICE_API_GET_PROFILE_MYGENERE',
     FOLLOW_UNFOLLOW_ON_SETTING:'SERVICE_API:FOLLOW_UNFOLLOW_ONSETTING',

    // Artist Tab for All Profile
     UPLOAD_PROFILE_IMAGE:'SERVICE_API:UPLOAD_PROFILE_PIC',
     UPLOAD_VIDEOS:'SERVICE_API:UPLOAD_VIDEOS',
     UPLOAD_CROUSEL_IMAGE:'SERVICE_API:UPLOAD_CROUSEL_IMAGE',

    //Artist album edit
     ALBUM_EDIT:'SERVICE_API:ALBUM_EDIT',
     ALBUM_ADD_FULL:'SERVICE_API:ALBUM_ADD_FULL',
     ADD_NEW_TRACKS :'SERVICE_API:ADD_NEW_TRACKS',

    // AccountOf Artists,
     ACCOUNT_DETAIL_ARTIST:'SERVICE_API:ACCOUNT_DETAIL_ARTIST',
     UPDATE_ACCOUT_ARTIST:'SERVICE_API:UPDATE_ACCOUNT_ARTIST',
     ADD_GENRE:'SERVICE_API:ADD_GENRE',

    //Push Alerts for Artists
    ARTIST_PUSH:'SERVICE_API:ARTIST_PUSH',
    ADD_NEW_TOUR:'SERVICE_API:ADD_TOUR',

    //add  ORNEW Venue event
    ADD_VENUE_EVENT:'SERVICE_API:ADD_VENUE_EVENT',
    DELETE_VENUE_EVENT:'SERVICE_API:DELETE_VENUE_EVENT',
    SEND_PUSH_ALERT:'SERVICE_API:SEND_PUSH_ALERT',

    //REMOVE CROUSEL imAGE
    REMOVE_CROUSEL_IMAGE:'SERVICE_API:REMOVE_CROUSEL_IMAGE',

    //Artist Tab
    GET_GENER_LIST_FOR_ARTIST:'SERVICE_API:GET_GENRE_LIST',
    GET_ARTIST_BY_GENERE :'SERVICE_API: GET_ARTIST_BY_GENERE',
    GET_ARTIST_PROFILE_DETAIL:'SERVICE_API: GET_ARTIST_PROFILE_DETAIL',
    CHANGE_STATUS_OF_ARTIST:'SERVICE_API:CHANGE_STATUS_OF_ARTIST',

    //SpotLight
    GET_SPOT_LIGHTED_CONTENT :'SERVICE_API:GET_SPOTLIGHT_CONTENT',

    // Venue Tab All API Call
    GET_VENUE_LIST:'SERVICE_API:GET_VENUE_LIST',
    GET_VENUE_CROUSEL:'SERVICE_API:GET_VENUE_CROUSEL',

    //Venue Detail
    GET_VENUE_EVENT_LIST:'SERVICE_API:GET_VENUE_EVENT_LIST',
    GET_VENUE_PROFILE_DETAIL:'SERVICE_API:GET_VENUE_PROFILE_DETAIL',

    SEARCH_RESULTS :'SERVICE_API:SEARCH_RESULTS',
    SEARCH_RESULTS_SHOW_MORE :'SERVICE_API:SEARCH_RESULTS_SHOW_MORE',
    LIKE_SPOT_LIGHT:'SERVICE_API:LIKE_SPOT_LIGHT',

    //SPOTLIGHT GET FEATURED
    GET_SPOTLIGHEN_PLAN_DETAIL:'SERVICE_API:GET_SPOTLIGHEN_PLAN_DETAIL',
    PAY_AND_GET_FETURED:'SERVICE_API:PAY_AND_GET_FETURED',

    //Delete track and Video from Artist.

    //REMOVE TRACK
    REMOVE_TRACK:'SERVICE_API:REMOVE_TRACK',

    //REMOVE ARTIST VIDEO
    REMOVE_VIDEO:'SERVICE_API:REMOVE_VIDEO',


};