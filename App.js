/**
 * Sample React Native Index
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, View, SafeAreaView } from "react-native";
import store from "./app/redux/store/index";
import Approuter from "./app/Components/Approuter/appRouter";
import { Provider } from "react-redux";
import FlashMessage from "react-native-flash-message";
console.disableYellowBox = true;
type Props = {};
import SplashScreen from "react-native-splash-screen";
import FCMController from "app/FCMController/FCMController";
export default class App extends Component<Props> {
  componentDidMount() {
    if (Platform.OS === "android") SplashScreen.hide();
  }
  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
        <Provider store={store}>
          <View style={{ flex: 1 }}>
            <Approuter />
            <FCMController />
          </View>
        </Provider>
        <FlashMessage position={"top"} titleStyle={{ alignItems: "center" }} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: "black"
  }
});
